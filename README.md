This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Table of Contents

- [Building the Backend](#building-the-backend)
- [Flowable BPMN Engine](#flowable-bpmn-engine)
- [Install Flowable Webapps For Process Authoring](#install-flowable-webapps-for-process-authoring)
- [Process Authoring](#process-authoring)
- [Generate the SDK Libraries](#generate-the-sdk-libraries)

## Building the Backend and developing
  - All the configuration properties are in src/main/resources/application.properties
  - Create a src/main/resources/application-dev.properties and pass -Dspring.profiles.active=dev as an app parameter in your ID config
  - If running from command line pass  -Dspring-boot.run.profiles=dev
  - The [Flowable Docs](https://flowable.org/docs/userguide) are your friend
  - Your backend will be running here: [Swagger Docs](http://localhost:8181/swagger-ui.html)
  - Your frontend will be running here: [Webpack Dev Server](http://localhost:3000)
## Install Flowable Webapps For Process Authoring
  - The modeler will be here http://localhost:8080/flowable-modeler/#/processes
  - The REST docs will be here: http://localhost:8080/flowable-rest/docs/?url=specfile/process/flowable.json#/
  - Download [Tomcat](http://apache.mirrors.tds.net/tomcat/tomcat-7/v7.0.90/bin/apache-tomcat-7.0.90.zip) unzip it and run it (bin/startup.s)
  - Download [Flowable 6.2.1](https://github.com/flowable/flowable-engine/releases/download/flowable-6.2.1/flowable-6.2.1.zip)
  - Unzip flowable and copy the wars directory to the tomcat/webapps directory
  - Swagger has a bug that keeps us on an earlier version of Spring so are libraries are just a little behind until its fixed.
  - Run MySQL and edit the following files to configure your database connection
    - `<tomcat>/webapps/flowable-admin/WEB-INF/classes/META-INF/flowable-ui-app/flowable-ui-app.properties`
    - `<tomcat>/webapps/flowable-idm/WEB-INF/classes/META-INF/flowable-ui-app/flowable-ui-app.properties`
    - `<tomcat>/webapps/flowable-modeler/WEB-INF/classes/META-INF/flowable-ui-app/flowable-ui-app.properties`
    - `<tomcat>/webapps/flowable-task/WEB-INF/classes/META-INF/flowable-ui-app/flowable-ui-app.properties`
    - `<tomcat>/webapps/flowable-rest/WEB-INF/classes/db.properties`
  - Copy a mysql jdbc jar into <tomcat>/lib 
## Process Authoring
  - You already have a process installed.  You can edit it or create a brand new one
  - After installing the Flowable Webapps go to http://localhost:8080/flowable-modeler
  - Use the drag and drop editor to modify the workflow.  Attributes or each element are modified in the lower pane
  - Click save on the top left when done.
  - Then you need to go to the Apps menu on the top right.  Workflows are published as part of an app.  Create an app and add the workflow.
  - Login `admin` p: `test`
  - Mouseover the "Download Process" block and click the magnifying glass icon
  - The [Flowable Docs](https://flowable.org/docs/userguide) are your friend
## Generate the SDK Libraries [You have a lot of languages to choose from](https://swagger.io/tools/swagger-codegen/)
 - `git clone https://github.com/swagger-api/swagger-codegen`
 - `cd swagger-codegen`
 - `mvn clean package`
 - `java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate -i http://petstore.swagger.io/v2/swagger.json -l java -o /var/tmp/php_api_client`
 - `java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate -i http://localhost:8181/v2/api-docs  -l java -o /var/tmp/php_api_client`
    

## Excercising The Process Engine
0.1) List all the process definitions
`curl -X GET "http://localhost:8181/repository/process-definitions?name=Download%20Process" -H "accept: application/json"`

0) Look at the process definition diagram
`http://localhost:8181/repository/process-definitions/<id_of_process_definition_from_previous_step>/image

1) Create a new workflow instance
`curl -X POST "http://localhost:8181/runtime/process-instances" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"processDefinitionKey\": \"download_approval_no_review\", \"variables\": [ { \"name\": \"creatorEmail\", \"value\": \"joshdeford@onprem.com\" }, { \"name\": \"assetMediaId\", \"value\": \"1\" } ]}"`

2) Look at the process instance diagram (in a browser)
`http://localhost:8181/runtime/process-instances/<id_returned_from_step_1>/diagram`

3) List all the tasks
`http://localhost:8181/runtime/tasks?processInstanceId=<id_returned_from_step_1>`

4) Complete the task returned from step 3
`curl -X POST "http://localhost:8181/runtime/tasks/<task_id_from_3>" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"action\": \"complete\"}"`

5) Look at the process instance diagram again
`http://localhost:8181/runtime/process-instances/<id_returned_from_step_1>/diagram`

6) Check your email

7) List all the tasks again
`http://localhost:8181/runtime/tasks?processInstanceId=<id_returned_from_step_1>`

8) Complete the task returned from step 7
`curl -X POST "http://localhost:8181/runtime/tasks/<task_id_from_7>" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"action\": \"complete\"}"`

9) Look at the process instance diagram again
`http://localhost:8181/runtime/process-instances/<id_returned_from_step_1>/diagram`

10) Check your email

11) List all the tasks again
`http://localhost:8181/runtime/tasks?processInstanceId=<id_returned_from_step_1>`

12) Complete one of the two tasks returned from step 11
`curl -X POST "http://localhost:8181/runtime/tasks/<task_id_from_7>" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"action\": \"complete\"}"`

13) Look at the process instance diagram again
`http://localhost:8181/runtime/process-instances/<id_returned_from_step_1>/diagram`

14) Complete the other one of the two tasks returned from step 11
`curl -X POST "http://localhost:8181/runtime/tasks/<task_id_from_7>" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"action\": \"complete\"}"`

15) Try to look at the process instance diagram again.  But its gone.
`http://localhost:8181/runtime/process-instances/<id_returned_from_step_1>/diagram`

package com.onprem.oks;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onprem.oks.configmanagement.ComponentConfiguration;
import com.onprem.oks.configmanagement.ComponentConfigurationRepository;
import com.onprem.oks.configmanagement.ComponentProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ComponentConfigurationTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private ComponentConfigurationRepository configurationRepository;

  @Test
  public void whenFindByName_thenReturnEmployee() throws JsonProcessingException {

    List configProperties = new ArrayList();
    configProperties.add(new ComponentProperty("key1", "value1"));
    configProperties.add(new ComponentProperty("key2", "value2"));

    ComponentConfiguration configuration = new ComponentConfiguration(1, "testConfig");
    configurationRepository.save(configuration);

    ComponentConfiguration retrievedConfiguration = configurationRepository.findByVersion(1);

    List<ComponentProperty> retrievedProperties = retrievedConfiguration.getProperties();

  }

}
import {
  Avatar,
  Divider,
  Drawer, FormControl, FormHelperText, Input, InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText, MenuItem, Select,
  WithStyles,
  withStyles
} from "@material-ui/core";
import InboxIcon from "@material-ui/icons/MoveToInbox";

import * as React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { styles } from "../common/theme";
import { closeNavDrawer } from "../store/app/actions";
import { AppStore } from "../store/app/reducers";
import { requestEntities } from "../store/entities/actions";
import { State } from "../store/State";
import { EntityStore } from "../store/entities/reducers";
import { Entity, EntitySchema } from "../store/entities/types";
import * as socket from "../common/util/websocketListener";
import { Client } from "stompjs";
import { Role } from "../generated-redux-api/types";
import { setActiveRole } from "../store/flowable/actions";
import { extractPageEntities } from "../common/util/appHelpers";
import { FlowableStore } from "../store/flowable/reducers";

interface NavDrawerProps extends RouteComponentProps<void>, WithStyles<typeof styles> {
  entityStore: EntityStore;
  appStore: AppStore;
  closeNavDrawer: typeof closeNavDrawer;
  setActiveRole: typeof setActiveRole;
  requestEntities: typeof requestEntities;
  roleSchema: EntitySchema;
  availableRoles: Role[];
  tasks: FlowableStore["tasks"];
  activeRole: Role;
}

interface NavDrawerState {
  isOpen: boolean;
  hasRoles: boolean;
  currentRole?: string;
}

const rolePage = "navDrawerRoles";

class NavDrawer extends React.Component<NavDrawerProps & InjectedIntlProps, NavDrawerState> {

  private socketClient: Client;

  constructor(props: NavDrawerProps & InjectedIntlProps) {
    super(props);
    this.state = {
      "isOpen": false,
      "hasRoles": false
    };
  }

  public componentWillUnmount() {
    if (this.socketClient && this.socketClient.disconnect) {
      this.socketClient.disconnect(() => { /* required but not needed */ });
    }
  }


  /**
   * On mount, request role entities
   */
  public componentDidUpdate(prevProps: NavDrawerProps, prevState: NavDrawerState) {
    const { roleSchema } = this.props;
    if (roleSchema && !this.state.hasRoles) {
      this.updateRoles("new")();
      // These are the same prefixes used in BaseEntityListener.java
      const topicTypes = ["new", "updated", "deleted", "linked", "unlinked"];
      const registrations = topicTypes.map((type: string) => {
        return {
          "destination": `/topic/roles/${type}`,
          "type": `roles_${type}`,
          "callback": this.updateRoles(type)
        };
      });
      if (this.socketClient && this.socketClient.disconnect) {
        this.socketClient.disconnect(() => { /* */
        });
      }
      this.socketClient = socket.register(registrations);
      this.setState({ "hasRoles": true });
    }
  }

  public render() {
    const { entityStore, appStore, classes, availableRoles, activeRole, tasks  } = this.props;
    const { currentRole  } = this.state;
    const entityProfiles = Object.keys(entityStore.schemaByUri).map((entitySchemaUri: string) => {
      const entitySchema: EntitySchema = entityStore.schemaByUri[entitySchemaUri];
      return <ListItem key={`item_${entitySchemaUri}`} button={true} onClick={this.onEntityClick(entitySchema.rel)}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary={ entitySchema.title } />
          </ListItem>;
    });

    const tasksTitle = activeRole ? `Tasks for Role: ${activeRole.name}` : "All Tasks";

    const sideList = (
        <div className={classes.entityList}>
          <List>{entityProfiles}</List>
          <Divider />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="role-helper">Role</InputLabel>
            <Select
                value={currentRole}
                onChange={this.handleRoleChange}
                input={<Input name="role" id="role-helper" />}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {
                Object.keys(availableRoles).map((uri: string) => {
                  const role: Role = availableRoles[uri];
                  return <MenuItem key={uri} value={(role as Entity).uri}>{role.name}</MenuItem>;
               })
              }
            </Select>
            <FormHelperText>Select a user role for the tasks</FormHelperText>
          </FormControl>
          <List>{
            <ListItem key="tasks" button={true} onClick={this.onTasksClick()}>
              <Avatar>
                {Object.keys(tasks).length}
              </Avatar>
              <ListItemText primary={ tasksTitle } />
            </ListItem>
          }</List>
        </div>
    );

    return <Drawer open={appStore.navDrawerOpen} onClose={this.closeNavDrawer}>
      <div
          tabIndex={0}
          role="button"
          onKeyDown={this.closeNavDrawer}
      >
        <div>
          { sideList }
        </div>
      </div>
    </Drawer>;
  }

  private handleRoleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { setActiveRole: setActiveRolesThunk, availableRoles, history } = this.props;
    setActiveRolesThunk(availableRoles[event.target.value]);
    this.closeNavDrawer();
    history.push("/tasks");
    this.setState({ "currentRole": event.target.value });
  }

  private onTasksClick = () => () => {
    const {  history } = this.props;
    this.closeNavDrawer();
    history.push("/tasks");
  }

  private onEntityClick = (entityRel: string) => () => {
    const {  history } = this.props;
    this.closeNavDrawer();
    history.push(`/entities/${ entityRel }`);
  }

  private updateRoles = (type: string) => () => {
    const { requestEntities: requestEntitiesThunk, roleSchema } = this.props;
    requestEntitiesThunk(roleSchema, { "page": 0, "sort": "entityName", "collectionName": rolePage, "size": 10000 });
  }

  private closeNavDrawer = () => {
    const { closeNavDrawer: closeNavDrawerThunk } = this.props;
    closeNavDrawerThunk();
  }

}

function mapStateToProps(state: State) {
  const { entityStore, appStore, flowableStore } = state;
  const roles = extractPageEntities(appStore, entityStore, rolePage, "roles");

  return {
    "entityStore": entityStore,
    "appStore": appStore,
    "roleSchema": entityStore.schemaByRel.roles,
    "availableRoles":roles,
    "tasks":flowableStore.tasks,
    "activeRole":flowableStore.activeRole
  };
}

export default connect(
    mapStateToProps,
    // This is the magic that allows the dispatch 'thunk' actions to be created and puts them in props
    { closeNavDrawer, requestEntities, setActiveRole }
)(withStyles(styles)<{}>(withRouter(injectIntl(NavDrawer))));

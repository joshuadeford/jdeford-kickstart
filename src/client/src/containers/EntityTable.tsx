import {
  Button, Typography,
  Divider, Paper,
  WithStyles,
  withStyles
} from "@material-ui/core";
import * as React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { DeleteDialog } from "../components/DeleteDialog";
import EntityRow from "../components/EntityRow";

// import {styles} from '../common/theme';
import { cancelEditEntityLocal,
  createEntity,
  deleteEntity,
  editEntityLocal,
  requestEntities,
  updateEntity,
  relateEntity,
  unrelateEntity } from "../store/entities/actions";
import { faPlus } from "@fortawesome/fontawesome-free-solid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { styles } from "../common/theme";
import { Entity, EntitySchema, SchemaProperty } from "../store/entities/types";
import { EntityStore } from "../store/entities/reducers";
import { State } from "../store/State";
import * as socket from "../common/util/websocketListener";
import { Client } from "stompjs";
import { PageRequest } from "../store/app/reducers";
import { extractPageEntities } from "../common/util/appHelpers";
import { errorAction } from "../store/app/actions";

interface EntityTableRouteProps {
  entityRel: string;
}

interface EntityTableProps extends RouteComponentProps<EntityTableRouteProps>, WithStyles<typeof styles> {
  entityStore: EntityStore;
  requestEntities: typeof requestEntities;
  editEntityLocal: typeof editEntityLocal;
  createEntity: typeof createEntity;
  deleteEntity: typeof deleteEntity;
  updateEntity: typeof updateEntity;
  relateEntity: typeof relateEntity;
  unrelateEntity: typeof unrelateEntity;
  errorAction: typeof errorAction;
  cancelEditEntityLocal: typeof cancelEditEntityLocal;
  entitySchema: EntitySchema;
  entityPageByUri: { [uri: string]: Entity };
}

interface EntityTableState {
  pageSize: number;
  deleteDialogIsOpen: boolean;
  messageKey?: string;
  details?: string;
  entityToDelete?: Entity;
  nextPageRequest: PageRequest;
  hasRendered: boolean;
}

const pageCollectionName = "entityTableElements";

class EntityTable extends React.Component<EntityTableProps & InjectedIntlProps, EntityTableState> {

  private socketClient: Client;

  constructor(props: EntityTableProps & InjectedIntlProps) {
    super(props);
    this.state = {
      "deleteDialogIsOpen": false,
      "messageKey": "",
      "details": "",
      "pageSize": 10,
      "hasRendered": false,
      "nextPageRequest": {
        "collectionName": pageCollectionName,
        "page": 0,
        "size": 10,
        "sort": "entityName:asc"
      }
    };
  }

  public componentDidMount() {
    this.fetchEntities();
  }

  public componentWillUnmount() {
    if (this.socketClient) {
      this.socketClient.disconnect(() => { /* **/ } );
    }
    const { cancelEditEntityLocal: cancelEditEntityLocalThunk } = this.props;
    cancelEditEntityLocalThunk();
  }

  /**
   * On mount, request all entities
   */
  public componentDidUpdate(prevProps: EntityTableProps, prevState: EntityTableState) {
    const { entitySchema } = this.props;
    // Get the action thunk off of props.  It was put there because of the magic
    // object passed as the second argument to redux's connect method
    const { entitySchema: previousSchema } = prevProps;
    if (entitySchema) {
      if(!this.state.hasRendered || (!previousSchema || entitySchema.uri !== previousSchema.uri)) {
        this.fetchEntities();
        this.setState({ "hasRendered": true });
      }
    }
  }

  public manageSocketClients() {
    const { entitySchema } = this.props;
    if (this.socketClient) {
      this.socketClient.disconnect(() => { /* required but not needed */ });
    }

    // These are the same prefixes used in BaseEntityListener.java
    const topicTypes = ["new", "updated", "deleted", "linked", "unlinked"];

    const registrations = topicTypes.map((type: string) => {
      return {
        "destination": `/topic/${entitySchema.rel}/${type}`,
        "type": `${entitySchema.rel}_${type}`,
        "callback": this.handleSocketMessage(type)
      };
    });

    this.socketClient = socket.register(registrations);
   }


  /**
   * After an entity has been edited update it on the server
   */
  public handleUpdateEntity = (entity: Entity) => {
    // Get the action thunk off of props.  It was put there because of the magic
    // object passed as the second argument to redux's connect method
    const { createEntity: createEntityThunk, updateEntity: updateEntityThunk } = this.props;
    if (entity.uri) {
      updateEntityThunk(entity);
    } else {
      createEntityThunk(entity, pageCollectionName);
    }
  }

  /**
   * After the delete dialog has been confirmed delete the entity on the server
   */
  public handleDeleteEntity = () => {
    if (this.state.entityToDelete) {
      // Get the action thunk off of props.  It was put there because of the magic
      // object passed as the second argument to redux's connect method
      const { deleteEntity: deleteEntityThunk } = this.props;
      this.setState({
        "deleteDialogIsOpen": false
      });
      deleteEntityThunk(this.state.entityToDelete);
    }
  }



  /**
   * When the delete entity button is clicked open a dialog to confirm deletion
   */
  public openDeleteDialog = (entity: Entity) => {
    this.setState({
      "deleteDialogIsOpen": true,
      "entityToDelete": entity
    });
  }


  /**
   * Add a new bag of data representing an entity that will be edited.
   */
  public addRow = () => {
    const { editEntityLocal: editEntityLocalThunk, cancelEditEntityLocal: cancelEditEntityLocalThunk, entitySchema } = this.props;
    cancelEditEntityLocalThunk();
    editEntityLocalThunk(
        {
          "uri": "",
          "schema": entitySchema,
          "entityName": "",
          "id": ""
        });
  }

  public shouldComponentUpdate(nextProps: EntityTableProps, nextState: EntityTableState) {
    const { entitySchema }  = this.props;
    return !!entitySchema;
  }

  public render () {
    const { intl, entityPageByUri, entityStore, entitySchema, classes } = this.props;
    if (!entitySchema) {
      return <div/>;
    }

    if (!entityPageByUri) {
      return <div/>;
    } else {
      const entityTypeName = entitySchema.title;

      const entityRows = [];
      // Iterate through all entity items creating a row for each.
      // Each entity item that has an editId will have an entity editor displayed
      // rather than a read-only row.
      for (const uri of Object.keys(entityPageByUri)) {
        const entity = entityPageByUri[uri];
        entityRows.push( this.createEntityRow(entity) );
      }
      if (entityStore.editingItem && !entityStore.editingItem.uri) {
        const newRow = this.createEntityRow(entityStore.editingItem, true);
        entityRows.unshift(newRow);
      }
      const nameForDeletion = this.state.entityToDelete && this.state.entityToDelete.schema.title || "";

      return <Paper className={classes.paper}>
        <div>
          <div>
            <Typography paragraph={true} className={classes.pageTitle}>{ intl.formatMessage({ "id": "entity_type" })} { entityTypeName }</Typography>
          </div>

          <div>
            <Button className={classes.newEntityButton} onClick={this.addRow} variant="outlined" >
              <FontAwesomeIcon icon={faPlus}/>
            </Button>
            <Divider/>
            {entityRows}
            <DeleteDialog isOpen={this.state.deleteDialogIsOpen}
                          handleDelete={this.handleDeleteEntity}
                          handleClose={this.closeDialog}
                          intl={intl}
                          nameForDeletion={nameForDeletion}/>
          </div>
        </div>
      </Paper>;
    }
  }

  private fetchEntities() {
    try {
      const { requestEntities: requestEntitiesThunk, entitySchema } = this.props;
      if (entitySchema) {
        requestEntitiesThunk(entitySchema, this.state.nextPageRequest);
        this.manageSocketClients();
      }
    } catch (e) {
      errorAction("error_requesting_entities", e.message);
    }
  }


  private createEntityRow(entity: Entity, startOpen: boolean = false) {
    const { entityStore, entitySchema } = this.props;
    return <EntityRow
        startOpen={startOpen}
        itemMapsByRel={entityStore.itemMapsByRel}
        entitySchema={entitySchema}
        handleShowUnrelated={this.handleShowUnrelated}
        handleUnrelateEntity={this.handleUnrelateEntity(entity)}
        handleRelateEntity={this.handleRelateEntity(entity)}
        handleDeleteEntity={this.openDeleteDialog}
        handleUpdateEntity={this.handleUpdateEntity}
        key={entity.uri}
        id={entity.uri}
        entity={entity}/>;
  }

  private closeDialog = () => {
    this.setState({ "deleteDialogIsOpen": false });
  }

  private handleRelateEntity = (owningEntity: Entity) => (entity: Entity, property: SchemaProperty) => {
    const { relateEntity: relateEntityThunk } = this.props;
    relateEntityThunk(owningEntity, entity, property);
  }

  private handleUnrelateEntity = (owningEntity: Entity) => (entity: Entity, property: SchemaProperty) => {
    const { unrelateEntity: unrelateEntityThunk } = this.props;
    unrelateEntityThunk(owningEntity, entity, property);
  }

  private handleShowUnrelated = (rel: string) => {
    // The clients served by this method aren't going to access stuff through a page
    // so a bogus page collection name is sent in.
    const { requestEntities: requestEntitiesThunk, entityStore } = this.props;
      requestEntitiesThunk(entityStore.schemaByRel[rel],
          {
            "page": 0,
            "sort": "entityName:ASC",
            "collectionName": "nocollection",
            "size": 10000 },
          false
      );
  }

  private handleSocketMessage = (type: string) => () => {
    // When a socket message is received go get all the entities again regardless
    // of whether it was a creation, deletion, update etc...
    // You may want to optimize this
    const { requestEntities: requestEntitiesThunk, entitySchema } = this.props;
    requestEntitiesThunk(entitySchema, this.state.nextPageRequest);
  }
}


// TODO implement componentShouldUpdate to see if the page is available and the elements in the page have been loaded:w
function mapStateToProps(state: State, ownProps: EntityTableProps) {
  const { entityStore, appStore } = state;
  const { match } = ownProps;
  const rel = match.params.entityRel;
  const entityPageItems = extractPageEntities(appStore, entityStore, pageCollectionName, rel);

  return {
    "entityPageByUri": entityPageItems,
    "entityStore": entityStore,
    "entitySchema": entityStore.schemaByRel[rel]
  };
}

export default connect(
    mapStateToProps,
    // This is the magic that allows the dispatch 'thunk' actions to be created and puts them in props
    { editEntityLocal,
      createEntity,
      requestEntities,
      updateEntity,
      deleteEntity,
      cancelEditEntityLocal,
      relateEntity,
      unrelateEntity }
)(withStyles(styles)<{}>(withRouter(injectIntl(EntityTable))));

import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "../common/theme";
import { State } from "../store/State";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { connect } from "react-redux";
import { Role, TaskResponse } from "../generated-redux-api/types";
import { listTasks  } from "../generated-redux-api/action/Tasks";

import SubmitTaskPanel from "./SubmitTaskPanel";
import * as socket from "../common/util/websocketListener";
import { Client } from "stompjs";

interface TaskListProps {
  tasks: TaskResponse[];
  activeRole: Role;
  listTasks: typeof listTasks;
}

interface TaskListProps extends RouteComponentProps<TaskListProps>, WithStyles<typeof styles> {
}

class TaskList extends React.Component<TaskListProps & InjectedIntlProps> {

  private socketClient: Client;

  constructor(props: TaskListProps & InjectedIntlProps) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {
    this.listTasks();

    this.socketClient = socket.register([{
      "type": "taskCreated",
      "destination": "/topic/taskCreated",
      "callback": this.listTasks
    },{
      "type": "taskCompleted",
      "destination": "/topic/taskCompleted",
      "callback": this.listTasks
    }
    ]);
  }

  public componentWillUnmount() {
    if (this.socketClient && this.socketClient.disconnect) {
      this.socketClient.disconnect(() => { /* */ });
    }
  }

  public componentDidUpdate(prevProps: TaskListProps) {
    const { activeRole } = this.props;
    // Compare sorted concatenation of role names
    const previousRoleKey = prevProps.activeRole ? prevProps.activeRole.name : undefined;
    const roleKey = activeRole ? activeRole.name : undefined;
    if (roleKey !== previousRoleKey) {
      this.listTasks();
    }
  }


  public render() {
    const { tasks } = this.props;
    const taskElements = Object.keys(tasks).map((taskUri: string) => {
      const task: TaskResponse = tasks[taskUri];
      return <SubmitTaskPanel onTaskComplete={listTasks} task={task} key={taskUri}/>;
    });
    return <React.Fragment>
      { taskElements }
    </React.Fragment>;
  }

  private listTasks = () => {
    const { listTasks: listTasksThunk, activeRole } = this.props;
      if (activeRole) {
        listTasksThunk({ "candidateGroup": activeRole.name });
      } else {
        listTasksThunk({ "active": true });
      }
  }
}

function mapStateToProps(state: State, ownProps: TaskListProps) {
  const { flowableStore } = state;

  return {
    "tasks": flowableStore.tasks || {},
    "formData": flowableStore.forms || {},
    "activeRole": flowableStore.activeRole
  };
}


export default connect(
    mapStateToProps,
    // This is the magic that allows the dispatch 'thunk' actions to be created and puts them in props
    { listTasks }
)(withStyles(styles)<{}>(withRouter(injectIntl(TaskList))));

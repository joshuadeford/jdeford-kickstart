import Button from "@material-ui/core/Button";
import * as React from "react";
import {
  FormDataResponse,
  RestFormProperty,
  TaskResponse
} from "../generated-redux-api/types";
import {
  Card,
  CardActions,
  CardContent, Checkbox,
  Divider, FormControlLabel,
  TextField,
  Typography,
  withStyles
} from "@material-ui/core";
import { connect } from "react-redux";
import { styles } from "../common/theme";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { State } from "../store/State";
import { WithStyles } from "@material-ui/core/styles";
import { getFormDataUsingGET, submitFormUsingPOST  } from "../generated-redux-api/action/Forms";

interface SubmitTaskPanelProps extends WithStyles<typeof styles> {
  formData: FormDataResponse;
  task: TaskResponse;
  submitFormUsingPOST: typeof submitFormUsingPOST;
  getFormDataUsingGET: typeof getFormDataUsingGET;
  onTaskComplete(): void;
}

interface SubmitTaskPanelState {
  [id: string]: {
    id: string;
    value: string;
  };
}

/**
 * A modal Dialog used to prompt the user to confirm deletion.
 * It provides a confirmation button and a cancel button.
 */
class SubmitTaskPanel extends React.Component<SubmitTaskPanelProps & InjectedIntlProps, SubmitTaskPanelState> {

  constructor(props: SubmitTaskPanelProps & InjectedIntlProps) {
    super(props);
    this.state = {
    };
  }

  public componentDidMount() {
    const { task, getFormDataUsingGET: getFormData } = this.props;
    getFormData({ "taskId": task.id });
  }


  public render () {
    const {
      formData,
      task,
      intl,
      classes
    } = this.props;

    const taskName = task.name;
    const formDataCells = (formData.formProperties || []).map((formProperty: RestFormProperty) => {
        const propertyId = formProperty.id || "";
        let fieldEditor: any;
        if (formProperty.type === "long" || formProperty.type === "string") {
          fieldEditor = <TextField
              key={propertyId}
              label={formProperty.name}
              className={classes.textField}
              value={this.state[propertyId] ? this.state[propertyId].value : ""}
              onChange={this.updateFormProperty(formProperty)}
              margin="normal"
          />;
        } else if (formProperty.type === "boolean") {
          fieldEditor = <FormControlLabel
              control={
                <Checkbox
                    key={propertyId}
                    checked={this.state[propertyId] ? this.state[propertyId].value : false }
                    onChange={this.updateFormProperty(formProperty)}
                    value="checked"
                />
              }
              label={formProperty.name}
          />;
        } else {
          throw new Error("The task type " + formProperty.type + " has no editor for it.  One should be defined.");
        }
        return <div key={`edit_${formProperty.id}`}>
          { fieldEditor }
          <Divider/>
        </div>;
    });
    return (
      <Card >
        <CardContent>
          <Typography gutterBottom={true} variant="headline" component="h2">
            { taskName }
          </Typography>
          <Typography id="update-task-description">
            { formDataCells }
          </Typography>
        </CardContent>
        <CardActions>
          <Button onClick={this.handleComplete(task)} color="primary" autoFocus={true}>
            {intl.formatMessage({ "id": "complete_task" })}
          </Button>
        </CardActions>
      </Card>

    );
  }
  private updateFormProperty = (property: RestFormProperty) => (changeEvent: React.ChangeEvent<HTMLInputElement>) => {
    let value: any = property.type === "boolean" ? changeEvent.target.checked : changeEvent.target.value;
    if (property.type === "long") {
      value = value.trim() * 1;
    }
    const newProperty = { "id": property.id, "value": value } as { id: string; value: string; };

    const propertyId = property.id as string;
    this.setState({
      ...this.state,
      [propertyId]: newProperty
    });
  }

  private handleComplete = (task: TaskResponse) => () => {
    const { submitFormUsingPOST: submitForm, onTaskComplete } = this.props;

    submitForm({
      "properties": Object.keys(this.state).map((key: string) => this.state[key]),
      "taskId": task.id,
      "action": "complete"
    });
    onTaskComplete();
  }

}

function mapStateToProps(state: State, ownProps: any) {
  const { flowableStore } = state;
  const { task } = ownProps;
  const formData = flowableStore.forms[task.url as string];

  return {
    "formData": formData || {}
  };
}

export default connect(mapStateToProps, {
  getFormDataUsingGET,
  submitFormUsingPOST
})(withStyles(styles)<SubmitTaskPanelProps>(injectIntl(SubmitTaskPanel)));

import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import { WithStyles, withStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import * as React from "react";
import { connect } from "react-redux";
import { styles } from "../common/theme";
import NavDrawer from "./NavDrawer";
import { openNavDrawer, errorAction, errorAcknowledged } from "../store/app/actions";
import { State } from "../store/State";
import { ErrorDialog } from "../components/ErrorDialog";
import { AppStore } from "../store/app/reducers";
import "react-toastify/dist/ReactToastify.css";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { fetchEntityProfiles } from "../store/entities/actions";

interface LayoutProps {
 openNavDrawer: typeof openNavDrawer;
 errorAcknowledged: typeof errorAcknowledged;
 errorAction: typeof errorAction;
 appStore: AppStore;
 fetchEntityProfiles: typeof fetchEntityProfiles;
}

interface LayoutState {
  drawerOpen: boolean;
  didRender: boolean;
}

class Layout extends React.Component<WithStyles<typeof styles> & LayoutProps & InjectedIntlProps, LayoutState> {

  public state = {
    "didRender": false,
    "drawerOpen": false
  };

  public componentDidMount() {
    if (!this.state.didRender) {
      const { fetchEntityProfiles: fetchEntityProfilesThunk } = this.props;
      fetchEntityProfilesThunk(() => { // TODO delete the callback
      });
      this.setState({ "didRender": true });
    }
  }

  public shouldComponentUpdate() {
    return this.state.didRender;
  }

  public render() {
    const { classes, appStore, intl } = this.props;
    return <React.Fragment>
      <AppBar position="static">
        <Toolbar>
          <IconButton onClick={this.toggleDrawer(true)} className={classes.menuButton}
                      color="inherit" aria-label="Menu">
            <MenuIcon/>
          </IconButton>
        </Toolbar>
      </AppBar>

      <NavDrawer key="navDrawer"/>
      <ErrorDialog isOpen={!!appStore.messageKey}
                   handleClose={this.closeError}
                   intl={intl}
                   messageKey={appStore.messageKey}
                   details={appStore.details || ""}/>

    </React.Fragment>;
  }

  private toggleDrawer = (open: boolean) => () => {
    const { openNavDrawer: openNavDrawerThunk } = this.props;

    openNavDrawerThunk();
  }

  private closeError = () => {
    const { errorAcknowledged: ack } = this.props;
    ack();
  }
}


function mapStateToProps(state: State) {
  const { appStore } = state;

  return {
    "appStore": appStore
  };
}


export default connect(mapStateToProps, { fetchEntityProfiles, openNavDrawer, errorAction, errorAcknowledged })(withStyles(styles)(injectIntl(Layout)));


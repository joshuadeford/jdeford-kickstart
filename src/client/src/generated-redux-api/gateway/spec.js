
// Auto-generated, edits will be overwritten
const spec = {
  'host': 'localhost:3000/process-api',
  'schemes': [
    'http'
  ],
  'basePath': '',
  'contentTypes': [],
  'accepts': [
    'application/json'
  ]
}
export default spec

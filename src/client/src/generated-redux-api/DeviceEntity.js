/** @module DeviceEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllDevice
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfDevice>} OK
 */
export function findAllDeviceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllDeviceUsingGETOperation, parameters)
}

/**
 * saveDevice
 * 
 * @param {module:types.Device} body body
 * @return {Promise<module:types.ResourceOfDevice>} OK
 */
export function saveDeviceUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveDeviceUsingPOSTOperation, parameters)
}

/**
 * findOneDevice
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfDevice>} OK
 */
export function findOneDeviceUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneDeviceUsingGETOperation, parameters)
}

/**
 * saveDevice
 * 
 * @param {string} id id
 * @param {module:types.Device} body body
 * @return {Promise<module:types.ResourceOfDevice>} OK
 */
export function saveDeviceUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveDeviceUsingPUTOperation, parameters)
}

/**
 * deleteDevice
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteDeviceUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteDeviceUsingDELETEOperation, parameters)
}

/**
 * saveDevice
 * 
 * @param {string} id id
 * @param {module:types.Device} body body
 * @return {Promise<module:types.ResourceOfDevice>} OK
 */
export function saveDeviceUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveDeviceUsingPATCHOperation, parameters)
}

/**
 * deviceUser
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function deviceUserUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deviceUserUsingGETOperation, parameters)
}

/**
 * deviceUser
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function deviceUserUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(deviceUserUsingPOSTOperation, parameters)
}

/**
 * deviceUser
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function deviceUserUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(deviceUserUsingPUTOperation, parameters)
}

/**
 * deviceUser
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deviceUserUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deviceUserUsingDELETEOperation, parameters)
}

/**
 * deviceUser
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function deviceUserUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(deviceUserUsingPATCHOperation, parameters)
}

const findAllDeviceUsingGETOperation = {
  path: '/api/devices',
  method: 'get'
}

const saveDeviceUsingPOSTOperation = {
  path: '/api/devices',
  contentTypes: ['application/json'],
  method: 'post'
}

const findOneDeviceUsingGETOperation = {
  path: '/api/devices/{id}',
  method: 'get'
}

const saveDeviceUsingPUTOperation = {
  path: '/api/devices/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteDeviceUsingDELETEOperation = {
  path: '/api/devices/{id}',
  method: 'delete'
}

const saveDeviceUsingPATCHOperation = {
  path: '/api/devices/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const deviceUserUsingGETOperation = {
  path: '/api/devices/{id}/user',
  method: 'get'
}

const deviceUserUsingPOSTOperation = {
  path: '/api/devices/{id}/user',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const deviceUserUsingPUTOperation = {
  path: '/api/devices/{id}/user',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const deviceUserUsingDELETEOperation = {
  path: '/api/devices/{id}/user',
  method: 'delete'
}

const deviceUserUsingPATCHOperation = {
  path: '/api/devices/{id}/user',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

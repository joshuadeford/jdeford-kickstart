/** @module types */
// Auto-generated, edits will be overwritten

/**
 * @typedef Artifact
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Asset
 * @memberof module:types
 * 
 * @property {string} assetDescription 
 * @property {string} assetId 
 * @property {string} assetModel 
 * @property {string} assetModelYears 
 * @property {string} assetName 
 * @property {string} assetType 
 * @property {date} availabilityStartDate 
 * @property {string} copyrightArea 
 * @property {string} copyrightOwner 
 * @property {string} copyrightUsageMedia 
 * @property {string} description 
 * @property {string} displayId 
 * @property {string} expiryDate 
 * @property {string} fileUrl 
 * @property {string} id 
 * @property {string} mediaId 
 * @property {module:types.User} owner 
 * @property {string} practicableCount 
 * @property {string} variant 
 */

/**
 * @typedef AssetOrder
 * @memberof module:types
 * 
 * @property {boolean} approved 
 * @property {module:types.Asset} asset 
 * @property {module:types.User} creator 
 * @property {string} id 
 * @property {string} status 
 */

/**
 * @typedef AttachmentResponse
 * @memberof module:types
 * 
 * @property {string} contentUrl 
 * @property {string} description 
 * @property {string} externalUrl contentUrl:In case the attachment is a link to an external resource, the externalUrl contains the URL to the external content. If the attachment content is present in the Flowable engine, the contentUrl will contain an URL where the binary content can be streamed from.
 * @property {string} id 
 * @property {string} name 
 * @property {string} processInstanceUrl 
 * @property {string} taskUrl 
 * @property {date} time 
 * @property {string} type Can be any arbitrary value. When a valid formatted media-type (e.g. application/xml, text/plain) is included, the binary content HTTP response content-type will be set the the given value.
 * @property {string} url 
 * @property {string} userId 
 */

/**
 * @typedef BaseElement
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef BpmnModel
 * @memberof module:types
 * 
 * @property {object} dataStores 
 * @property {object} definitionsAttributes 
 * @property {object} errors 
 * @property {object} flowLocationMap 
 * @property {module:types.Artifact[]} globalArtifacts 
 * @property {module:types.Import[]} imports 
 * @property {module:types.Interface[]} interfaces 
 * @property {object} itemDefinitions 
 * @property {object} labelLocationMap 
 * @property {object} locationMap 
 * @property {module:types.Process} mainProcess 
 * @property {object} messageFlows 
 * @property {module:types.Message[]} messages 
 * @property {object} namespaces 
 * @property {module:types.Pool[]} pools 
 * @property {module:types.Process[]} processes 
 * @property {module:types.Resource[]} resources 
 * @property {module:types.Signal[]} signals 
 * @property {string} sourceSystemId 
 * @property {string[]} startEventFormTypes 
 * @property {string} targetNamespace 
 * @property {string[]} userTaskFormTypes 
 */

/**
 * @typedef CommentRequest
 * @memberof module:types
 * 
 * @property {string} author 
 * @property {string} id 
 * @property {string} message 
 * @property {boolean} saveProcessInstanceId 
 * @property {string} type 
 * @property {string} url 
 */

/**
 * @typedef CommentResponse
 * @memberof module:types
 * 
 * @property {string} author 
 * @property {string} id 
 * @property {string} message 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} taskId 
 * @property {string} taskUrl 
 * @property {date} time 
 */

/**
 * @typedef ComponentConfiguration
 * @memberof module:types
 * 
 * @property {string} componentName 
 * @property {string} id 
 * @property {module:types.ComponentProperty[]} properties 
 * @property {number} version 
 */

/**
 * @typedef ComponentProperty
 * @memberof module:types
 * 
 * @property {module:types.ComponentConfiguration} configuration 
 * @property {string} id 
 * @property {string} name 
 * @property {string} value 
 */

/**
 * @typedef DataResponseOfDeploymentResponse
 * @memberof module:types
 * 
 * @property {module:types.DeploymentResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfEventSubscriptionResponse
 * @memberof module:types
 * 
 * @property {module:types.EventSubscriptionResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfExecutionResponse
 * @memberof module:types
 * 
 * @property {module:types.ExecutionResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfGroupResponse
 * @memberof module:types
 * 
 * @property {module:types.GroupResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfHistoricActivityInstanceResponse
 * @memberof module:types
 * 
 * @property {module:types.HistoricActivityInstanceResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfHistoricDetailResponse
 * @memberof module:types
 * 
 * @property {module:types.HistoricDetailResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfHistoricProcessInstanceResponse
 * @memberof module:types
 * 
 * @property {module:types.HistoricProcessInstanceResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfHistoricTaskInstanceResponse
 * @memberof module:types
 * 
 * @property {module:types.HistoricTaskInstanceResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfHistoricVariableInstanceResponse
 * @memberof module:types
 * 
 * @property {module:types.HistoricVariableInstanceResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfJobResponse
 * @memberof module:types
 * 
 * @property {module:types.JobResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfListOfMapOfstringAndobject
 * @memberof module:types
 * 
 * @property {module:types.MapOfstringAndobject[][]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfModelResponse
 * @memberof module:types
 * 
 * @property {module:types.ModelResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfProcessDefinitionResponse
 * @memberof module:types
 * 
 * @property {module:types.ProcessDefinitionResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfProcessInstanceResponse
 * @memberof module:types
 * 
 * @property {module:types.ProcessInstanceResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfTaskResponse
 * @memberof module:types
 * 
 * @property {module:types.TaskResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataResponseOfUserResponse
 * @memberof module:types
 * 
 * @property {module:types.UserResponse[]} data 
 * @property {string} order 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {number} total 
 */

/**
 * @typedef DataSpec
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {boolean} collection 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} itemSubjectRef 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef DataStore
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} dataState 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} itemSubjectRef 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef DecisionTableResponse
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {string} deploymentId 
 * @property {string} description 
 * @property {string} id 
 * @property {string} key 
 * @property {string} name 
 * @property {string} parentDeploymentId 
 * @property {string} resourceName 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {number} version 
 */

/**
 * @typedef DeploymentResourceResponse
 * @memberof module:types
 * 
 * @property {string} contentUrl 
 * @property {string} id 
 * @property {string} mediaType Contains the media-type the resource has. This is resolved using a (pluggable) MediaTypeResolver and contains, by default, a limited number of mime-type mappings.
 * @property {string} type Type of resource
 * @property {string} url For a single resource contains the actual URL to use for retrieving the binary resource
 */

/**
 * @typedef DeploymentResponse
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {date} deploymentTime 
 * @property {string} id 
 * @property {string} name 
 * @property {string} tenantId 
 * @property {string} url 
 */

/**
 * @typedef Device
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} modelName 
 * @property {string} name 
 * @property {module:types.User} user 
 */

/**
 * @typedef ooEventListener
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} entityType 
 * @property {string} events 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} implementation 
 * @property {string} implementationType 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef EventResponse
 * @memberof module:types
 * 
 * @property {string} action 
 * @property {string} id 
 * @property {string[]} message 
 * @property {string} processInstanceUrl 
 * @property {string} taskUrl 
 * @property {date} time 
 * @property {string} url 
 * @property {string} userId 
 */

/**
 * @typedef EventSubscriptionResponse
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} configuration 
 * @property {date} created 
 * @property {string} eventName 
 * @property {string} eventType 
 * @property {string} executionId 
 * @property {string} executionUrl 
 * @property {string} id 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} tenantId 
 * @property {string} url 
 */

/**
 * @typedef ExecutionActionRequest
 * @memberof module:types
 * 
 * @property {string} action Action to perform: Either signal, trigger, signalEventReceived or messageEventReceived
 * @property {string} messageName Message of the signal
 * @property {string} signalName Name of the signal
 * @property {module:types.RestVariable[]} transientVariables 
 * @property {module:types.RestVariable[]} variables 
 */

/**
 * @typedef ExecutionChangeActivityStateRequest
 * @memberof module:types
 * 
 * @property {string} cancelActivityId activityId to be canceled
 * @property {string} startActivityId activityId to be started
 */

/**
 * @typedef ExecutionQueryRequest
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} id 
 * @property {string} messageEventSubscriptionName 
 * @property {string} order 
 * @property {string} parentId 
 * @property {string} processBusinessKey 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {string} processInstanceId 
 * @property {module:types.QueryVariable[]} processInstanceVariables 
 * @property {string} signalEventSubscriptionName 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {module:types.QueryVariable[]} variables 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef ExecutionResponse
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} id 
 * @property {string} parentId 
 * @property {string} parentUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} superExecutionId 
 * @property {string} superExecutionUrl 
 * @property {boolean} suspended 
 * @property {string} tenantId 
 * @property {string} url 
 */

/**
 * @typedef ExtensionAttribute
 * @memberof module:types
 * 
 * @property {string} name 
 * @property {string} namespace 
 * @property {string} namespacePrefix 
 * @property {string} value 
 */

/**
 * @typedef ExtensionElement
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} childElements 
 * @property {string} elementText 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} name 
 * @property {string} namespace 
 * @property {string} namespacePrefix 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef FieldExtension
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} expression 
 * @property {object} extensionElements 
 * @property {string} fieldName 
 * @property {string} id 
 * @property {string} stringValue 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef FlowElement
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} documentation 
 * @property {module:types.FlowableListener[]} executionListeners 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef FlowableListener
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} customPropertiesResolverImplementation 
 * @property {string} customPropertiesResolverImplementationType 
 * @property {string} event 
 * @property {object} extensionElements 
 * @property {module:types.FieldExtension[]} fieldExtensions 
 * @property {string} id 
 * @property {string} implementation 
 * @property {string} implementationType 
 * @property {string} onTransaction 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef FormDataResponse
 * @memberof module:types
 * 
 * @property {string} deploymentId 
 * @property {string} formKey 
 * @property {module:types.RestFormProperty[]} formProperties 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} taskId 
 * @property {string} taskUrl 
 */

/**
 * @typedef FormDefinitionResponse
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {string} deploymentId 
 * @property {string} description 
 * @property {string} id 
 * @property {string} key 
 * @property {string} name 
 * @property {string} resourceName 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {number} version 
 */

/**
 * @typedef GraphicInfo
 * @memberof module:types
 * 
 * @property {module:types.BaseElement} element 
 * @property {boolean} expanded 
 * @property {number} height 
 * @property {number} width 
 * @property {number} x 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 * @property {number} y 
 */

/**
 * @typedef GroupRequest
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} name 
 * @property {string} type 
 * @property {string} url 
 */

/**
 * @typedef GroupResponse
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} name 
 * @property {string} type 
 * @property {string} url 
 */

/**
 * @typedef HistoricActivityInstanceQueryRequest
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} activityInstanceId 
 * @property {string} activityName 
 * @property {string} activityType 
 * @property {string} executionId 
 * @property {boolean} finished 
 * @property {string} order 
 * @property {string} processDefinitionId 
 * @property {string} processInstanceId 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} taskAssignee 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef HistoricActivityInstanceResponse
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} activityName 
 * @property {string} activityType 
 * @property {string} assignee 
 * @property {string} calledProcessInstanceId 
 * @property {number} durationInMillis 
 * @property {date} endTime 
 * @property {string} executionId 
 * @property {string} id 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {date} startTime 
 * @property {string} taskId 
 * @property {string} tenantId 
 */

/**
 * @typedef HistoricDetailQueryRequest
 * @memberof module:types
 * 
 * @property {string} activityInstanceId 
 * @property {string} executionId 
 * @property {string} id 
 * @property {string} order 
 * @property {string} processInstanceId 
 * @property {boolean} selectOnlyFormProperties 
 * @property {boolean} selectOnlyVariableUpdates 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} taskId 
 */

/**
 * @typedef HistoricDetailResponse
 * @memberof module:types
 * 
 * @property {string} activityInstanceId 
 * @property {string} detailType 
 * @property {string} executionId 
 * @property {string} id 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} propertyId 
 * @property {string} propertyValue 
 * @property {number} revision 
 * @property {string} taskId 
 * @property {string} taskUrl 
 * @property {date} time 
 * @property {module:types.RestVariable} variable 
 */

/**
 * @typedef HistoricIdentityLinkResponse
 * @memberof module:types
 * 
 * @property {string} groupId 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} taskId 
 * @property {string} taskUrl 
 * @property {string} type 
 * @property {string} userId 
 */

/**
 * @typedef HistoricProcessInstanceQueryRequest
 * @memberof module:types
 * 
 * @property {boolean} excludeSubprocesses 
 * @property {boolean} finished 
 * @property {date} finishedAfter 
 * @property {date} finishedBefore 
 * @property {boolean} includeProcessVariables 
 * @property {string} involvedUser 
 * @property {string} order 
 * @property {string} processBusinessKey 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {string} processInstanceId 
 * @property {string[]} processInstanceIds 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {date} startedAfter 
 * @property {date} startedBefore 
 * @property {string} startedBy 
 * @property {string} superProcessInstanceId 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {module:types.QueryVariable[]} variables 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef HistoricProcessInstanceResponse
 * @memberof module:types
 * 
 * @property {string} businessKey 
 * @property {string} deleteReason 
 * @property {number} durationInMillis 
 * @property {string} endActivityId 
 * @property {date} endTime 
 * @property {string} id 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} startActivityId 
 * @property {date} startTime 
 * @property {string} startUserId 
 * @property {string} superProcessInstanceId 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {module:types.RestVariable[]} variables 
 */

/**
 * @typedef HistoricTaskInstanceQueryRequest
 * @memberof module:types
 * 
 * @property {date} dueDate 
 * @property {date} dueDateAfter 
 * @property {date} dueDateBefore 
 * @property {string} executionId 
 * @property {boolean} finished 
 * @property {boolean} includeProcessVariables 
 * @property {boolean} includeTaskLocalVariables 
 * @property {string} order 
 * @property {string} parentTaskId 
 * @property {string} processBusinessKey 
 * @property {string} processBusinessKeyLike 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {string} processDefinitionKeyLike 
 * @property {string} processDefinitionName 
 * @property {string} processDefinitionNameLike 
 * @property {boolean} processFinished 
 * @property {string} processInstanceId 
 * @property {module:types.QueryVariable[]} processVariables 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} taskAssignee 
 * @property {string} taskAssigneeLike 
 * @property {string} taskCandidateGroup 
 * @property {string} taskCategory 
 * @property {date} taskCompletedAfter 
 * @property {date} taskCompletedBefore 
 * @property {date} taskCompletedOn 
 * @property {date} taskCreatedAfter 
 * @property {date} taskCreatedBefore 
 * @property {date} taskCreatedOn 
 * @property {string} taskDefinitionKey 
 * @property {string} taskDefinitionKeyLike 
 * @property {string} taskDeleteReason 
 * @property {string} taskDeleteReasonLike 
 * @property {string} taskDescription 
 * @property {string} taskDescriptionLike 
 * @property {string} taskId 
 * @property {string} taskInvolvedUser 
 * @property {number} taskMaxPriority 
 * @property {number} taskMinPriority 
 * @property {string} taskName 
 * @property {string} taskNameLike 
 * @property {string} taskOwner 
 * @property {string} taskOwnerLike 
 * @property {number} taskPriority 
 * @property {module:types.QueryVariable[]} taskVariables 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {boolean} withoutDueDate 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef HistoricTaskInstanceResponse
 * @memberof module:types
 * 
 * @property {string} assignee 
 * @property {string} category 
 * @property {date} claimTime 
 * @property {string} deleteReason 
 * @property {string} description 
 * @property {date} dueDate 
 * @property {number} durationInMillis 
 * @property {date} endTime 
 * @property {string} executionId 
 * @property {string} formKey 
 * @property {string} id 
 * @property {string} name 
 * @property {string} owner 
 * @property {string} parentTaskId 
 * @property {number} priority 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {date} startTime 
 * @property {string} taskDefinitionKey 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {module:types.RestVariable[]} variables 
 * @property {number} workTimeInMillis 
 */

/**
 * @typedef HistoricVariableInstanceQueryRequest
 * @memberof module:types
 * 
 * @property {boolean} excludeTaskVariables 
 * @property {string} executionId 
 * @property {string} processInstanceId 
 * @property {string} taskId 
 * @property {string} variableName 
 * @property {string} variableNameLike 
 * @property {module:types.QueryVariable[]} variables 
 */

/**
 * @typedef HistoricVariableInstanceResponse
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {string} taskId 
 * @property {module:types.RestVariable} variable 
 */

/**
 * @typedef IOSpecification
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string[]} dataInputRefs 
 * @property {module:types.DataSpec[]} dataInputs 
 * @property {string[]} dataOutputRefs 
 * @property {module:types.DataSpec[]} dataOutputs 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Import
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} importType 
 * @property {string} location 
 * @property {string} namespace 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Interface
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} implementationRef 
 * @property {string} name 
 * @property {module:types.Operation[]} operations 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef ItemDefinition
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} itemKind 
 * @property {string} structureRef 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef JobResponse
 * @memberof module:types
 * 
 * @property {date} createTime 
 * @property {date} dueDate 
 * @property {string} exceptionMessage 
 * @property {string} executionId 
 * @property {string} executionUrl 
 * @property {string} id 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {number} retries 
 * @property {string} tenantId 
 * @property {string} url 
 */

/**
 * @typedef Lane
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string[]} flowReferences 
 * @property {string} id 
 * @property {string} name 
 * @property {module:types.Process} parentProcess 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Link
 * @memberof module:types
 * 
 * @property {string} href 
 * @property {string} rel 
 * @property {boolean} templated 
 */

/**
 * @typedef MembershipRequest
 * @memberof module:types
 * 
 * @property {string} userId 
 */

/**
 * @typedef MembershipResponse
 * @memberof module:types
 * 
 * @property {string} groupId 
 * @property {string} url 
 * @property {string} userId 
 */

/**
 * @typedef Message
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} itemRef 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef MessageFlow
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} messageRef 
 * @property {string} name 
 * @property {string} sourceRef 
 * @property {string} targetRef 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef ModelAndView
 * @memberof module:types
 * 
 * @property {boolean} empty 
 * @property {object} model 
 * @property {object} modelMap 
 * @property {boolean} reference 
 * @property {string} status 
 * @property {module:types.View} view 
 * @property {string} viewName 
 */

/**
 * @typedef ModelRequest
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {string} deploymentId 
 * @property {string} key 
 * @property {string} metaInfo 
 * @property {string} name 
 * @property {string} tenantId 
 * @property {number} version 
 */

/**
 * @typedef ModelResponse
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {date} createTime 
 * @property {string} deploymentId 
 * @property {string} deploymentUrl 
 * @property {string} id 
 * @property {string} key 
 * @property {date} lastUpdateTime 
 * @property {string} metaInfo 
 * @property {string} name 
 * @property {string} sourceExtraUrl 
 * @property {string} sourceUrl 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {number} version 
 */

/**
 * @typedef ModelVariant
 * @memberof module:types
 * 
 * @property {string} brand 
 * @property {string} id 
 * @property {string} model 
 * @property {string} variant 
 */

/**
 * @typedef Operation
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string[]} errorMessageRef 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} implementationRef 
 * @property {string} inMessageRef 
 * @property {string} name 
 * @property {string} outMessageRef 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Pool
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {boolean} executable 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} name 
 * @property {string} processRef 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef Process
 * @memberof module:types
 * 
 * @property {module:types.Artifact[]} artifacts 
 * @property {object} attributes 
 * @property {string[]} candidateStarterGroups 
 * @property {string[]} candidateStarterUsers 
 * @property {module:types.ValuedDataObject[]} dataObjects 
 * @property {string} documentation 
 * @property {module:types.EventListener[]} eventListeners 
 * @property {boolean} executable 
 * @property {module:types.FlowableListener[]} executionListeners 
 * @property {object} extensionElements 
 * @property {object} flowElementMap 
 * @property {module:types.FlowElement[]} flowElements 
 * @property {string} id 
 * @property {module:types.FlowElement} initialFlowElement 
 * @property {module:types.IOSpecification} ioSpecification 
 * @property {module:types.Lane[]} lanes 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef ProcessDefinitionActionRequest
 * @memberof module:types
 * 
 * @property {string} action Action to perform: Either activate or suspend
 * @property {string} category 
 * @property {date} date Date (ISO-8601) when the suspension/activation should be executed. If omitted, the suspend/activation is effective immediately.
 * @property {boolean} includeProcessInstances Whether or not to suspend/activate running process-instances for this process-definition. If omitted, the process-instances are left in the state they are
 */

/**
 * @typedef ProcessDefinitionResponse
 * @memberof module:types
 * 
 * @property {string} category 
 * @property {string} deploymentId 
 * @property {string} deploymentUrl 
 * @property {string} description 
 * @property {string} diagramResource Contains a graphical representation of the process, null when no diagram is available.
 * @property {boolean} graphicalNotationDefined Indicates the process definition contains graphical information (BPMN DI).
 * @property {string} id 
 * @property {string} key 
 * @property {string} name 
 * @property {string} resource Contains the actual deployed BPMN 2.0 xml.
 * @property {boolean} startFormDefined 
 * @property {boolean} suspended 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {number} version 
 */

/**
 * @typedef ProcessEngineInfoResponse
 * @memberof module:types
 * 
 * @property {string} exception 
 * @property {string} name 
 * @property {string} resourceUrl 
 * @property {string} version 
 */

/**
 * @typedef ProcessInstanceActionRequest
 * @memberof module:types
 * 
 * @property {string} action Action to perform: Either activate or suspend
 */

/**
 * @typedef ProcessInstanceCreateRequest
 * @memberof module:types
 * 
 * @property {string} businessKey 
 * @property {string} message 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {boolean} returnVariables 
 * @property {string} tenantId 
 * @property {module:types.RestVariable[]} transientVariables 
 * @property {module:types.RestVariable[]} variables 
 */

/**
 * @typedef ProcessInstanceQueryRequest
 * @memberof module:types
 * 
 * @property {boolean} excludeSubprocesses 
 * @property {boolean} includeProcessVariables 
 * @property {string} involvedUser 
 * @property {string} order 
 * @property {string} processBusinessKey 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {string} processInstanceId 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} subProcessInstanceId 
 * @property {string} superProcessInstanceId 
 * @property {boolean} suspended 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {module:types.QueryVariable[]} variables 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef ProcessInstanceResponse
 * @memberof module:types
 * 
 * @property {string} activityId 
 * @property {string} businessKey 
 * @property {boolean} completed 
 * @property {boolean} ended 
 * @property {string} id 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {boolean} suspended 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {module:types.RestVariable[]} variables 
 */

/**
 * @typedef QueryVariable
 * @memberof module:types
 * 
 * @property {string} name 
 * @property {string} operation 
 * @property {string} type 
 * @property {object} value 
 */

/**
 * @typedef Resource
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} name 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef ResourceOfAsset
 * @memberof module:types
 * 
 * @property {string} assetDescription 
 * @property {string} assetId 
 * @property {string} assetModel 
 * @property {string} assetModelYears 
 * @property {string} assetName 
 * @property {string} assetType 
 * @property {date} availabilityStartDate 
 * @property {string} copyrightArea 
 * @property {string} copyrightOwner 
 * @property {string} copyrightUsageMedia 
 * @property {string} description 
 * @property {string} displayId 
 * @property {string} expiryDate 
 * @property {string} fileUrl 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} mediaId 
 * @property {module:types.User} owner 
 * @property {string} practicableCount 
 * @property {string} variant 
 */

/**
 * @typedef ResourceOfAssetOrder
 * @memberof module:types
 * 
 * @property {boolean} approved 
 * @property {module:types.Asset} asset 
 * @property {module:types.User} creator 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} status 
 */

/**
 * @typedef ResourceOfComponentConfiguration
 * @memberof module:types
 * 
 * @property {string} componentName 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {module:types.ComponentProperty[]} properties 
 * @property {number} version 
 */

/**
 * @typedef ResourceOfComponentProperty
 * @memberof module:types
 * 
 * @property {module:types.ComponentConfiguration} configuration 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} name 
 * @property {string} value 
 */

/**
 * @typedef ResourceOfDevice
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} modelName 
 * @property {string} name 
 * @property {module:types.User} user 
 */

/**
 * @typedef ResourceOfModelVariant
 * @memberof module:types
 * 
 * @property {string} brand 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} model 
 * @property {string} variant 
 */

/**
 * @typedef ResourceOfRole
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} name 
 * @property {module:types.User[]} users 
 */

/**
 * @typedef ResourceOfUser
 * @memberof module:types
 * 
 * @property {module:types.Device[]} devices 
 * @property {string} dob 
 * @property {string} email 
 * @property {string} id 
 * @property {module:types.Link[]} links 
 * @property {string} password 
 * @property {string} role 
 * @property {module:types.Role[]} roles 
 * @property {number} salary 
 * @property {string} userName 
 */

/**
 * @typedef ResourceSupport
 * @memberof module:types
 * 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfAsset
 * @memberof module:types
 * 
 * @property {module:types.Asset[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfAssetOrder
 * @memberof module:types
 * 
 * @property {module:types.AssetOrder[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfComponentConfiguration
 * @memberof module:types
 * 
 * @property {module:types.ComponentConfiguration[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfComponentProperty
 * @memberof module:types
 * 
 * @property {module:types.ComponentProperty[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfDevice
 * @memberof module:types
 * 
 * @property {module:types.Device[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfListOfAsset
 * @memberof module:types
 * 
 * @property {module:types.Asset[][]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfListOfModelVariant
 * @memberof module:types
 * 
 * @property {module:types.ModelVariant[][]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfListOfUser
 * @memberof module:types
 * 
 * @property {module:types.User[][]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfModelVariant
 * @memberof module:types
 * 
 * @property {module:types.ModelVariant[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfRole
 * @memberof module:types
 * 
 * @property {module:types.Role[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef ResourcesOfUser
 * @memberof module:types
 * 
 * @property {module:types.User[]} content 
 * @property {module:types.Link[]} links 
 */

/**
 * @typedef RestActionRequest
 * @memberof module:types
 * 
 * @property {string} action 
 */

/**
 * @typedef RestEnumFormProperty
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} name 
 */

/**
 * @typedef RestFormProperty
 * @memberof module:types
 * 
 * @property {string} datePattern 
 * @property {module:types.RestEnumFormProperty[]} enumValues 
 * @property {string} id 
 * @property {string} name 
 * @property {boolean} readable 
 * @property {boolean} required 
 * @property {string} type 
 * @property {string} value 
 * @property {boolean} writable 
 */

/**
 * @typedef RestIdentityLink
 * @memberof module:types
 * 
 * @property {string} group 
 * @property {string} type 
 * @property {string} url 
 * @property {string} user 
 */

/**
 * @typedef RestVariable
 * @memberof module:types
 * 
 * @property {string} name Name of the variable
 * @property {string} scope 
 * @property {string} type Type of the variable.
 * @property {object} value Value of the variable.
 * @property {string} valueUrl When reading a variable of type binary or serializable, this attribute will point to the URL where the raw binary data can be fetched from.
 */

/**
 * @typedef Role
 * @memberof module:types
 * 
 * @property {string} id 
 * @property {string} name 
 * @property {module:types.User[]} users 
 */

/**
 * @typedef Signal
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {string} name 
 * @property {string} scope 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef SignalEventReceivedRequest
 * @memberof module:types
 * 
 * @property {boolean} async If true, handling of the signal will happen asynchronously. Return code will be 202 - Accepted to indicate the request is accepted but not yet executed. If false,
 *                       handling the signal will be done immediately and result (200 - OK) will only return after this completed successfully. Defaults to false if omitted.
 * @property {string} signalName Name of the signal
 * @property {string} tenantId ID of the tenant that the signal event should be processed in
 * @property {module:types.RestVariable[]} variables Array of variables (in the general variables format) to use as payload to pass along with the signal. Cannot be used in case async is set to true, this will result in an error.
 */

/**
 * @typedef SubmitFormRequest
 * @memberof module:types
 * 
 * @property {string} action 
 * @property {string} businessKey 
 * @property {string} processDefinitionId 
 * @property {module:types.RestFormProperty[]} properties 
 * @property {string} taskId 
 */

/**
 * @typedef TableMetaData
 * @memberof module:types
 * 
 * @property {string[]} columnNames 
 * @property {string[]} columnTypes 
 * @property {string} tableName 
 */

/**
 * @typedef TableResponse
 * @memberof module:types
 * 
 * @property {number} count 
 * @property {string} name 
 * @property {string} url 
 */

/**
 * @typedef TaskActionRequest
 * @memberof module:types
 * 
 * @property {string} action Action to perform: Either complete, claim, delegate or resolve
 * @property {string} assignee If action is claim or delegate, you can use this parameter to set the assignee associated
 * @property {module:types.RestVariable[]} transientVariables If action is complete, you can use this parameter to set transient variables
 * @property {module:types.RestVariable[]} variables If action is complete, you can use this parameter to set variables
 */

/**
 * @typedef TaskQueryRequest
 * @memberof module:types
 * 
 * @property {boolean} active 
 * @property {string} assignee 
 * @property {string} assigneeLike 
 * @property {string} candidateGroup 
 * @property {string[]} candidateGroupIn 
 * @property {string} candidateOrAssigned 
 * @property {string} candidateUser 
 * @property {string} category 
 * @property {date} createdAfter 
 * @property {date} createdBefore 
 * @property {date} createdOn 
 * @property {string} delegationState 
 * @property {string} description 
 * @property {string} descriptionLike 
 * @property {date} dueAfter 
 * @property {date} dueBefore 
 * @property {date} dueDate 
 * @property {boolean} excludeSubTasks 
 * @property {string} executionId 
 * @property {boolean} includeProcessVariables 
 * @property {boolean} includeTaskLocalVariables 
 * @property {string} involvedUser 
 * @property {number} maximumPriority 
 * @property {number} minimumPriority 
 * @property {string} name 
 * @property {string} nameLike 
 * @property {string} order 
 * @property {string} owner 
 * @property {string} ownerLike 
 * @property {number} priority 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionKey 
 * @property {string} processDefinitionKeyLike 
 * @property {string} processDefinitionName 
 * @property {string} processDefinitionNameLike 
 * @property {string} processInstanceBusinessKey 
 * @property {string} processInstanceBusinessKeyLike 
 * @property {string} processInstanceId 
 * @property {module:types.QueryVariable[]} processInstanceVariables 
 * @property {number} size 
 * @property {string} sort 
 * @property {number} start 
 * @property {string} taskDefinitionKey 
 * @property {string} taskDefinitionKeyLike 
 * @property {module:types.QueryVariable[]} taskVariables 
 * @property {string} tenantId 
 * @property {string} tenantIdLike 
 * @property {boolean} unassigned 
 * @property {boolean} withoutDueDate 
 * @property {boolean} withoutTenantId 
 */

/**
 * @typedef TaskRequest
 * @memberof module:types
 * 
 * @property {string} assignee 
 * @property {boolean} assigneeSet 
 * @property {string} category 
 * @property {boolean} categorySet 
 * @property {string} delegationState 
 * @property {boolean} delegationStateSet 
 * @property {string} description 
 * @property {boolean} descriptionSet 
 * @property {date} dueDate 
 * @property {boolean} duedateSet 
 * @property {string} formKey 
 * @property {boolean} formKeySet 
 * @property {string} name 
 * @property {boolean} nameSet 
 * @property {string} owner 
 * @property {boolean} ownerSet 
 * @property {string} parentTaskId 
 * @property {boolean} parentTaskIdSet 
 * @property {number} priority 
 * @property {boolean} prioritySet 
 * @property {string} tenantId 
 * @property {boolean} tenantIdSet 
 */

/**
 * @typedef TaskResponse
 * @memberof module:types
 * 
 * @property {string} assignee 
 * @property {string} category 
 * @property {date} createTime 
 * @property {string} delegationState Delegation-state of the task, can be null, "pending" or "resolved".
 * @property {string} description 
 * @property {date} dueDate 
 * @property {string} executionId 
 * @property {string} executionUrl 
 * @property {string} formKey 
 * @property {string} id 
 * @property {string} name 
 * @property {string} owner 
 * @property {string} parentTaskId 
 * @property {string} parentTaskUrl 
 * @property {number} priority 
 * @property {string} processDefinitionId 
 * @property {string} processDefinitionUrl 
 * @property {string} processInstanceId 
 * @property {string} processInstanceUrl 
 * @property {boolean} suspended 
 * @property {string} taskDefinitionKey 
 * @property {string} tenantId 
 * @property {string} url 
 * @property {module:types.RestVariable[]} variables 
 */

/**
 * @typedef User
 * @memberof module:types
 * 
 * @property {module:types.Device[]} devices 
 * @property {string} dob 
 * @property {string} email 
 * @property {string} id 
 * @property {string} password 
 * @property {string} role 
 * @property {module:types.Role[]} roles 
 * @property {number} salary 
 * @property {string} userName 
 */

/**
 * @typedef UserInfoRequest
 * @memberof module:types
 * 
 * @property {string} key 
 * @property {string} value 
 */

/**
 * @typedef UserInfoResponse
 * @memberof module:types
 * 
 * @property {string} key 
 * @property {string} url 
 * @property {string} value 
 */

/**
 * @typedef UserRequest
 * @memberof module:types
 * 
 * @property {string} email 
 * @property {string} firstName 
 * @property {string} id 
 * @property {string} lastName 
 * @property {string} password 
 * @property {string} pictureUrl 
 * @property {string} url 
 */

/**
 * @typedef UserResponse
 * @memberof module:types
 * 
 * @property {string} email 
 * @property {string} firstName 
 * @property {string} id 
 * @property {string} lastName 
 * @property {string} password 
 * @property {string} pictureUrl 
 * @property {string} url 
 */

/**
 * @typedef ValuedDataObject
 * @memberof module:types
 * 
 * @property {object} attributes 
 * @property {string} documentation 
 * @property {module:types.FlowableListener[]} executionListeners 
 * @property {object} extensionElements 
 * @property {string} id 
 * @property {module:types.ItemDefinition} itemSubjectRef 
 * @property {string} name 
 * @property {string} type 
 * @property {object} value 
 * @property {number} xmlColumnNumber 
 * @property {number} xmlRowNumber 
 */

/**
 * @typedef View
 * @memberof module:types
 * 
 * @property {string} contentType 
 */

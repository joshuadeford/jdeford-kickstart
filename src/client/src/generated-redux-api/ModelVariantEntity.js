/** @module ModelVariantEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllModelVariant
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfModelVariant>} OK
 */
export function findAllModelVariantUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllModelVariantUsingGETOperation, parameters)
}

/**
 * saveModelVariant
 * 
 * @param {module:types.ModelVariant} body body
 * @return {Promise<module:types.ResourceOfModelVariant>} OK
 */
export function saveModelVariantUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveModelVariantUsingPOSTOperation, parameters)
}

/**
 * findByBrandAndModelLikeModelVariant
 * 
 * @param {object} options Optional options
 * @param {string} [options.brand] brand
 * @param {string} [options.modelHint] modelHint
 * @return {Promise<module:types.ResourcesOfListOfModelVariant>} OK
 */
export function findByBrandAndModelLikeModelVariantUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      brand: options.brand,
      modelHint: options.modelHint
    }
  }
  return gateway.request(findByBrandAndModelLikeModelVariantUsingGETOperation, parameters)
}

/**
 * findByModelLikeModelVariant
 * 
 * @param {object} options Optional options
 * @param {string} [options.hint] hint
 * @return {Promise<module:types.ResourcesOfListOfModelVariant>} OK
 */
export function findByModelLikeModelVariantUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      hint: options.hint
    }
  }
  return gateway.request(findByModelLikeModelVariantUsingGETOperation, parameters)
}

/**
 * findOneModelVariant
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfModelVariant>} OK
 */
export function findOneModelVariantUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneModelVariantUsingGETOperation, parameters)
}

/**
 * saveModelVariant
 * 
 * @param {string} id id
 * @param {module:types.ModelVariant} body body
 * @return {Promise<module:types.ResourceOfModelVariant>} OK
 */
export function saveModelVariantUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveModelVariantUsingPUTOperation, parameters)
}

/**
 * deleteModelVariant
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteModelVariantUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteModelVariantUsingDELETEOperation, parameters)
}

/**
 * saveModelVariant
 * 
 * @param {string} id id
 * @param {module:types.ModelVariant} body body
 * @return {Promise<module:types.ResourceOfModelVariant>} OK
 */
export function saveModelVariantUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveModelVariantUsingPATCHOperation, parameters)
}

const findAllModelVariantUsingGETOperation = {
  path: '/api/modelVariants',
  method: 'get'
}

const saveModelVariantUsingPOSTOperation = {
  path: '/api/modelVariants',
  contentTypes: ['application/json'],
  method: 'post'
}

const findByBrandAndModelLikeModelVariantUsingGETOperation = {
  path: '/api/modelVariants/search/findByBrandAndModelLike',
  method: 'get'
}

const findByModelLikeModelVariantUsingGETOperation = {
  path: '/api/modelVariants/search/findByModelLike',
  method: 'get'
}

const findOneModelVariantUsingGETOperation = {
  path: '/api/modelVariants/{id}',
  method: 'get'
}

const saveModelVariantUsingPUTOperation = {
  path: '/api/modelVariants/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteModelVariantUsingDELETEOperation = {
  path: '/api/modelVariants/{id}',
  method: 'delete'
}

const saveModelVariantUsingPATCHOperation = {
  path: '/api/modelVariants/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

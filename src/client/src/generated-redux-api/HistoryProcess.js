/** @module HistoryProcess */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List of historic process instances
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] An id of the historic process instance.
 * @param {string} [options.processDefinitionKey] The process definition key of the historic process instance.
 * @param {string} [options.processDefinitionId] The process definition id of the historic process instance.
 * @param {string} [options.businessKey] The business key of the historic process instance.
 * @param {string} [options.involvedUser] An involved user of the historic process instance.
 * @param {boolean} [options.finished] Indication if the historic process instance is finished.
 * @param {string} [options.superProcessInstanceId] An optional parent process id of the historic process instance.
 * @param {boolean} [options.excludeSubprocesses] Return only historic process instances which aren’t sub processes.
 * @param {string} [options.finishedAfter] Return only historic process instances that were finished after this date.
 * @param {string} [options.finishedBefore] Return only historic process instances that were finished before this date.
 * @param {string} [options.startedAfter] Return only historic process instances that were started after this date.
 * @param {string} [options.startedBefore] Return only historic process instances that were started before this date.
 * @param {string} [options.startedBy] Return only historic process instances that were started by this user.
 * @param {boolean} [options.includeProcessVariables] An indication if the historic process instance variables should be returned as well.
 * @param {string} [options.tenantId] Only return instances with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return instances with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns instances without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @return {Promise<module:types.DataResponseOfHistoricProcessInstanceResponse>} Indicates that historic process instances could be queried.
 */
export function listHistoricProcessInstances(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      processInstanceId: options.processInstanceId,
      processDefinitionKey: options.processDefinitionKey,
      processDefinitionId: options.processDefinitionId,
      businessKey: options.businessKey,
      involvedUser: options.involvedUser,
      finished: options.finished,
      superProcessInstanceId: options.superProcessInstanceId,
      excludeSubprocesses: options.excludeSubprocesses,
      finishedAfter: options.finishedAfter,
      finishedBefore: options.finishedBefore,
      startedAfter: options.startedAfter,
      startedBefore: options.startedBefore,
      startedBy: options.startedBy,
      includeProcessVariables: options.includeProcessVariables,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId
    }
  }
  return gateway.request(listHistoricProcessInstancesOperation, parameters)
}

/**
 * Get a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.HistoricProcessInstanceResponse>} Indicates that the historic process instances could be found.
 */
export function getHistoricProcessInstance(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(getHistoricProcessInstanceOperation, parameters)
}

/**
 * Delete a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<object>} OK
 */
export function deleteHistoricProcessInstance(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(deleteHistoricProcessInstanceOperation, parameters)
}

/**
 * List comments on a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.CommentResponse[]>} Indicates the process instance was found and the comments are returned.
 */
export function listHistoricProcessInstanceComments(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(listHistoricProcessInstanceCommentsOperation, parameters)
}

/**
 * Parameter saveProcessInstanceId is optional, if true save process instance id of task with comment.
 * 
 * @param {module:types.CommentResponse} comment comment
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.CommentResponse>} OK
 */
export function createCommentUsingPOST(comment, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      comment
    }
  }
  return gateway.request(createCommentUsingPOSTOperation, parameters)
}

/**
 * Get a comment on a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.commentId] commentId
 * @return {Promise<module:types.CommentResponse>} Indicates the historic process instance and comment were found and the comment is returned.
 */
export function getCommentUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      commentId: options.commentId
    }
  }
  return gateway.request(getCommentUsingGETOperation, parameters)
}

/**
 * Delete a comment on a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.commentId] commentId
 * @return {Promise<object>} OK
 */
export function deleteCommentUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      commentId: options.commentId
    }
  }
  return gateway.request(deleteCommentUsingDELETEOperation, parameters)
}

/**
 * List identity links of a historic process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.HistoricIdentityLinkResponse[]>} Indicates request was successful and the identity links are returned
 */
export function listHistoricProcessInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(listHistoricProcessInstanceIdentityLinksOperation, parameters)
}

/**
 * The response body contains the binary value of the variable. When the variable is of type binary, the content-type of the response is set to application/octet-stream, regardless of the content of the variable or the request accept-type header. In case of serializable, application/x-java-serialized-object is used as content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.variableName] variableName
 * @return {Promise<string>} Indicates the process instance was found and the requested variable data is returned.
 */
export function getHistoricProcessInstanceVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      variableName: options.variableName
    }
  }
  return gateway.request(getHistoricProcessInstanceVariableDataOperation, parameters)
}

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of historic process instances, but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long. On top of that, the query allows for filtering based on process variables. The variables property is a JSON-array containing objects with the format as described here.
 * 
 * @param {module:types.HistoricProcessInstanceQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfHistoricProcessInstanceResponse>} Indicates request was successful and the process instances are returned
 */
export function queryHistoricProcessInstance(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryHistoricProcessInstanceOperation, parameters)
}

const listHistoricProcessInstancesOperation = {
  path: '/history/historic-process-instances',
  method: 'get'
}

const getHistoricProcessInstanceOperation = {
  path: '/history/historic-process-instances/{processInstanceId}',
  method: 'get'
}

const deleteHistoricProcessInstanceOperation = {
  path: '/history/historic-process-instances/{processInstanceId}',
  method: 'delete'
}

const listHistoricProcessInstanceCommentsOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/comments',
  method: 'get'
}

const createCommentUsingPOSTOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/comments',
  contentTypes: ['application/json'],
  method: 'post'
}

const getCommentUsingGETOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/comments/{commentId}',
  method: 'get'
}

const deleteCommentUsingDELETEOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/comments/{commentId}',
  method: 'delete'
}

const listHistoricProcessInstanceIdentityLinksOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/identitylinks',
  method: 'get'
}

const getHistoricProcessInstanceVariableDataOperation = {
  path: '/history/historic-process-instances/{processInstanceId}/variables/{variableName}/data',
  method: 'get'
}

const queryHistoricProcessInstanceOperation = {
  path: '/query/historic-process-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

/** @module Engine */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * Get engine info
 */
export function getEngineInfoUsingGET() {
  return gateway.request(getEngineInfoUsingGETOperation)
}

/**
 * List engine properties
 */
export function getPropertiesUsingGET() {
  return gateway.request(getPropertiesUsingGETOperation)
}

const getEngineInfoUsingGETOperation = {
  path: '/management/engine',
  method: 'get'
}

const getPropertiesUsingGETOperation = {
  path: '/management/properties',
  method: 'get'
}

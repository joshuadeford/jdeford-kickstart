/** @module Jobs */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List deadletter jobs
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return job with the given id
 * @param {string} [options.processInstanceId] Only return jobs part of a process with the given id
 * @param {string} [options.executionId] Only return jobs part of an execution with the given id
 * @param {string} [options.processDefinitionId] Only return jobs with the given process definition id
 * @param {boolean} [options.timersOnly] If true, only return jobs which are timers. If false, this parameter is ignored. Cannot be used together with 'messagesOnly'.
 * @param {boolean} [options.messagesOnly] If true, only return jobs which are messages. If false, this parameter is ignored. Cannot be used together with 'timersOnly'
 * @param {boolean} [options.withException] If true, only return jobs for which an exception occurred while executing it. If false, this parameter is ignored.
 * @param {string} [options.dueBefore] Only return jobs which are due to be executed before the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.dueAfter] Only return jobs which are due to be executed after the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.exceptionMessage] Only return jobs with the given exception message
 * @param {string} [options.tenantId] Only return jobs with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return jobs with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns jobs without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {boolean} [options.locked] If true, only return jobs which are locked.  If false, this parameter is ignored.
 * @param {boolean} [options.unlocked] If true, only return jobs which are unlocked. If false, this parameter is ignored.
 * @param {string} [options.sort] Enum: id, dueDate, executionId, processInstanceId, retries, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfJobResponse>} Indicates the requested jobs were returned.
 */
export function listDeadLetterJobs(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processInstanceId: options.processInstanceId,
      executionId: options.executionId,
      processDefinitionId: options.processDefinitionId,
      timersOnly: options.timersOnly,
      messagesOnly: options.messagesOnly,
      withException: options.withException,
      dueBefore: options.dueBefore,
      dueAfter: options.dueAfter,
      exceptionMessage: options.exceptionMessage,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      locked: options.locked,
      unlocked: options.unlocked,
      sort: options.sort
    }
  }
  return gateway.request(listDeadLetterJobsOperation, parameters)
}

/**
 * Get a single deadletter job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<module:types.JobResponse>} Indicates the suspended job exists and is returned.
 */
export function getDeadletterJobUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getDeadletterJobUsingGETOperation, parameters)
}

/**
 * Move a single deadletter job
 * 
 * @param {module:types.RestActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function executeDeadLetterJobActionUsingPOST(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(executeDeadLetterJobActionUsingPOSTOperation, parameters)
}

/**
 * Delete a deadletter job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function deleteDeadLetterJobUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(deleteDeadLetterJobUsingDELETEOperation, parameters)
}

/**
 * Get the exception stacktrace for a deadletter job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<string>} Indicates the requested job was not found and the stacktrace has been returned. The response contains the raw stacktrace and always has a Content-type of text/plain.
 */
export function getDeadLetterJobStacktraceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getDeadLetterJobStacktraceUsingGETOperation, parameters)
}

/**
 * List jobs
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return job with the given id
 * @param {string} [options.processInstanceId] Only return jobs part of a process with the given id
 * @param {string} [options.executionId] Only return jobs part of an execution with the given id
 * @param {string} [options.processDefinitionId] Only return jobs with the given process definition id
 * @param {boolean} [options.timersOnly] If true, only return jobs which are timers. If false, this parameter is ignored. Cannot be used together with 'messagesOnly'.
 * @param {boolean} [options.messagesOnly] If true, only return jobs which are messages. If false, this parameter is ignored. Cannot be used together with 'timersOnly'
 * @param {boolean} [options.withException] If true, only return jobs for which an exception occurred while executing it. If false, this parameter is ignored.
 * @param {string} [options.dueBefore] Only return jobs which are due to be executed before the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.dueAfter] Only return jobs which are due to be executed after the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.exceptionMessage] Only return jobs with the given exception message
 * @param {string} [options.tenantId] Only return jobs with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return jobs with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns jobs without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {boolean} [options.locked] If true, only return jobs which are locked.  If false, this parameter is ignored.
 * @param {boolean} [options.unlocked] If true, only return jobs which are unlocked. If false, this parameter is ignored.
 * @param {string} [options.sort] Enum: id, dueDate, executionId, processInstanceId, retries, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfJobResponse>} Indicates the requested jobs were returned.
 */
export function listJobs(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processInstanceId: options.processInstanceId,
      executionId: options.executionId,
      processDefinitionId: options.processDefinitionId,
      timersOnly: options.timersOnly,
      messagesOnly: options.messagesOnly,
      withException: options.withException,
      dueBefore: options.dueBefore,
      dueAfter: options.dueAfter,
      exceptionMessage: options.exceptionMessage,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      locked: options.locked,
      unlocked: options.unlocked,
      sort: options.sort
    }
  }
  return gateway.request(listJobsOperation, parameters)
}

/**
 * Get a single job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<module:types.JobResponse>} Indicates the job exists and is returned.
 */
export function getJobUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getJobUsingGETOperation, parameters)
}

/**
 * Execute a single job
 * 
 * @param {module:types.RestActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function executeJobActionUsingPOST(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(executeJobActionUsingPOSTOperation, parameters)
}

/**
 * Delete a job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function deleteJobUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(deleteJobUsingDELETEOperation, parameters)
}

/**
 * Get the exception stacktrace for a job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<string>} Indicates the requested job was not found and the stacktrace has been returned. The response contains the raw stacktrace and always has a Content-type of text/plain.
 */
export function getJobStacktraceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getJobStacktraceUsingGETOperation, parameters)
}

/**
 * List suspended jobs
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return job with the given id
 * @param {string} [options.processInstanceId] Only return jobs part of a process with the given id
 * @param {string} [options.executionId] Only return jobs part of an execution with the given id
 * @param {string} [options.processDefinitionId] Only return jobs with the given process definition id
 * @param {boolean} [options.timersOnly] If true, only return jobs which are timers. If false, this parameter is ignored. Cannot be used together with 'messagesOnly'.
 * @param {boolean} [options.messagesOnly] If true, only return jobs which are messages. If false, this parameter is ignored. Cannot be used together with 'timersOnly'
 * @param {boolean} [options.withException] If true, only return jobs for which an exception occurred while executing it. If false, this parameter is ignored.
 * @param {string} [options.dueBefore] Only return jobs which are due to be executed before the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.dueAfter] Only return jobs which are due to be executed after the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.exceptionMessage] Only return jobs with the given exception message
 * @param {string} [options.tenantId] Only return jobs with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return jobs with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns jobs without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {boolean} [options.locked] If true, only return jobs which are locked.  If false, this parameter is ignored.
 * @param {boolean} [options.unlocked] If true, only return jobs which are unlocked. If false, this parameter is ignored.
 * @param {string} [options.sort] Enum: id, dueDate, executionId, processInstanceId, retries, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfJobResponse>} Indicates the requested jobs were returned.
 */
export function listSuspendedJobs(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processInstanceId: options.processInstanceId,
      executionId: options.executionId,
      processDefinitionId: options.processDefinitionId,
      timersOnly: options.timersOnly,
      messagesOnly: options.messagesOnly,
      withException: options.withException,
      dueBefore: options.dueBefore,
      dueAfter: options.dueAfter,
      exceptionMessage: options.exceptionMessage,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      locked: options.locked,
      unlocked: options.unlocked,
      sort: options.sort
    }
  }
  return gateway.request(listSuspendedJobsOperation, parameters)
}

/**
 * Get a single suspended job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<module:types.JobResponse>} Indicates the suspended job exists and is returned.
 */
export function getSuspendedJobUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getSuspendedJobUsingGETOperation, parameters)
}

/**
 * Delete a suspended job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function deleteSuspendeJobUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(deleteSuspendeJobUsingDELETEOperation, parameters)
}

/**
 * Get the exception stacktrace for a suspended job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<string>} Indicates the requested job was not found and the stacktrace has been returned. The response contains the raw stacktrace and always has a Content-type of text/plain.
 */
export function getSuspendedJobStacktraceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getSuspendedJobStacktraceUsingGETOperation, parameters)
}

/**
 * List timer jobs
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return job with the given id
 * @param {string} [options.processInstanceId] Only return jobs part of a process with the given id
 * @param {string} [options.executionId] Only return jobs part of an execution with the given id
 * @param {string} [options.processDefinitionId] Only return jobs with the given process definition id
 * @param {boolean} [options.timersOnly] If true, only return jobs which are timers. If false, this parameter is ignored. Cannot be used together with 'messagesOnly'.
 * @param {boolean} [options.messagesOnly] If true, only return jobs which are messages. If false, this parameter is ignored. Cannot be used together with 'timersOnly'
 * @param {boolean} [options.withException] If true, only return jobs for which an exception occurred while executing it. If false, this parameter is ignored.
 * @param {string} [options.dueBefore] Only return jobs which are due to be executed before the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.dueAfter] Only return jobs which are due to be executed after the given date. Jobs without duedate are never returned using this parameter.
 * @param {string} [options.exceptionMessage] Only return jobs with the given exception message
 * @param {string} [options.tenantId] Only return jobs with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return jobs with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns jobs without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {boolean} [options.locked] If true, only return jobs which are locked.  If false, this parameter is ignored.
 * @param {boolean} [options.unlocked] If true, only return jobs which are unlocked. If false, this parameter is ignored.
 * @param {string} [options.sort] Enum: id, dueDate, executionId, processInstanceId, retries, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfJobResponse>} Indicates the requested jobs were returned.
 */
export function listTimerJobs(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processInstanceId: options.processInstanceId,
      executionId: options.executionId,
      processDefinitionId: options.processDefinitionId,
      timersOnly: options.timersOnly,
      messagesOnly: options.messagesOnly,
      withException: options.withException,
      dueBefore: options.dueBefore,
      dueAfter: options.dueAfter,
      exceptionMessage: options.exceptionMessage,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      locked: options.locked,
      unlocked: options.unlocked,
      sort: options.sort
    }
  }
  return gateway.request(listTimerJobsOperation, parameters)
}

/**
 * Get a single timer job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<module:types.JobResponse>} Indicates the timer job exists and is returned.
 */
export function getTimerJobUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getTimerJobUsingGETOperation, parameters)
}

/**
 * Move a single timer job
 * 
 * @param {module:types.RestActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function executeTimerJobActionUsingPOST(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(executeTimerJobActionUsingPOSTOperation, parameters)
}

/**
 * Delete a timer job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<object>} OK
 */
export function deleteTimerJobUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(deleteTimerJobUsingDELETEOperation, parameters)
}

/**
 * Get the exception stacktrace for a timer job
 * 
 * @param {object} options Optional options
 * @param {string} [options.jobId] jobId
 * @return {Promise<string>} Indicates the requested job was not found and the stacktrace has been returned. The response contains the raw stacktrace and always has a Content-type of text/plain.
 */
export function getTimerJobStacktraceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      jobId: options.jobId
    }
  }
  return gateway.request(getTimerJobStacktraceUsingGETOperation, parameters)
}

const listDeadLetterJobsOperation = {
  path: '/management/deadletter-jobs',
  method: 'get'
}

const getDeadletterJobUsingGETOperation = {
  path: '/management/deadletter-jobs/{jobId}',
  method: 'get'
}

const executeDeadLetterJobActionUsingPOSTOperation = {
  path: '/management/deadletter-jobs/{jobId}',
  contentTypes: ['application/json'],
  method: 'post'
}

const deleteDeadLetterJobUsingDELETEOperation = {
  path: '/management/deadletter-jobs/{jobId}',
  method: 'delete'
}

const getDeadLetterJobStacktraceUsingGETOperation = {
  path: '/management/deadletter-jobs/{jobId}/exception-stacktrace',
  method: 'get'
}

const listJobsOperation = {
  path: '/management/jobs',
  method: 'get'
}

const getJobUsingGETOperation = {
  path: '/management/jobs/{jobId}',
  method: 'get'
}

const executeJobActionUsingPOSTOperation = {
  path: '/management/jobs/{jobId}',
  contentTypes: ['application/json'],
  method: 'post'
}

const deleteJobUsingDELETEOperation = {
  path: '/management/jobs/{jobId}',
  method: 'delete'
}

const getJobStacktraceUsingGETOperation = {
  path: '/management/jobs/{jobId}/exception-stacktrace',
  method: 'get'
}

const listSuspendedJobsOperation = {
  path: '/management/suspended-jobs',
  method: 'get'
}

const getSuspendedJobUsingGETOperation = {
  path: '/management/suspended-jobs/{jobId}',
  method: 'get'
}

const deleteSuspendeJobUsingDELETEOperation = {
  path: '/management/suspended-jobs/{jobId}',
  method: 'delete'
}

const getSuspendedJobStacktraceUsingGETOperation = {
  path: '/management/suspended-jobs/{jobId}/exception-stacktrace',
  method: 'get'
}

const listTimerJobsOperation = {
  path: '/management/timer-jobs',
  method: 'get'
}

const getTimerJobUsingGETOperation = {
  path: '/management/timer-jobs/{jobId}',
  method: 'get'
}

const executeTimerJobActionUsingPOSTOperation = {
  path: '/management/timer-jobs/{jobId}',
  contentTypes: ['application/json'],
  method: 'post'
}

const deleteTimerJobUsingDELETEOperation = {
  path: '/management/timer-jobs/{jobId}',
  method: 'delete'
}

const getTimerJobStacktraceUsingGETOperation = {
  path: '/management/timer-jobs/{jobId}/exception-stacktrace',
  method: 'get'
}

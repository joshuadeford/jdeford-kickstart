/** @module Executions */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * The request body can contain all possible filters that can be used in the List executions URL query. On top of these, it’s possible to provide an array of variables and processInstanceVariables to include in the query, with their format described here.
 * 
 * The general paging and sorting query-parameters can be used for this URL.
 * 
 * @param {module:types.ExecutionQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfExecutionResponse>} Indicates request was successful and the executions are returned.
 */
export function queryExecutions(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryExecutionsOperation, parameters)
}

/**
 * List of executions
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return models with the given version.
 * @param {string} [options.activityId] Only return executions with the given activity id.
 * @param {string} [options.processDefinitionKey] Only return process instances with the given process definition key.
 * @param {string} [options.processDefinitionId] Only return process instances with the given process definition id.
 * @param {string} [options.processInstanceId] Only return executions which are part of the process instance with the given id.
 * @param {string} [options.messageEventSubscriptionName] Only return executions which are subscribed to a message with the given name.
 * @param {string} [options.signalEventSubscriptionName] Only return executions which are subscribed to a signal with the given name.
 * @param {string} [options.parentId] Only return executions which are a direct child of the given execution.
 * @param {string} [options.tenantId] Only return process instances with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return process instances with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns process instances without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {string} [options.sort] Enum: processInstanceId, processDefinitionId, processDefinitionKey, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfExecutionResponse>} Indicates request was successful and the executions are returned
 */
export function listExecutions(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      activityId: options.activityId,
      processDefinitionKey: options.processDefinitionKey,
      processDefinitionId: options.processDefinitionId,
      processInstanceId: options.processInstanceId,
      messageEventSubscriptionName: options.messageEventSubscriptionName,
      signalEventSubscriptionName: options.signalEventSubscriptionName,
      parentId: options.parentId,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      sort: options.sort
    }
  }
  return gateway.request(listExecutionsOperation, parameters)
}

/**
 * Signal event received
 * 
 * @param {module:types.ExecutionActionRequest} actionRequest actionRequest
 * @return {Promise<object>} OK
 */
export function executeExecutionActionUsingPUT(actionRequest) {
  const parameters = {
    body: {
      actionRequest
    }
  }
  return gateway.request(executeExecutionActionUsingPUTOperation, parameters)
}

/**
 * Get an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @return {Promise<module:types.ExecutionResponse>} Indicates the execution was found and returned.
 */
export function getExecutionUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    }
  }
  return gateway.request(getExecutionUsingGETOperation, parameters)
}

/**
 * Execute an action on an execution
 * 
 * @param {module:types.ExecutionActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @return {Promise<module:types.ExecutionResponse>} Indicates the execution was found and the action is performed.
 */
export function performExecutionActionUsingPUT(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(performExecutionActionUsingPUTOperation, parameters)
}

/**
 * Returns all activities which are active in the execution and in all child-executions (and their children, recursively), if any.
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @return {Promise<string[]>} Indicates the execution was found and activities are returned.
 */
export function listExecutionActiveActivities(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    }
  }
  return gateway.request(listExecutionActiveActivitiesOperation, parameters)
}

/**
 * Change the state of an execution
 * 
 * @param {module:types.ExecutionChangeActivityStateRequest} activityStateRequest activityStateRequest
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @return {Promise<object>} Indicates the execution was found and the action is performed.
 */
export function changeActivityStateUsingPOST(activityStateRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    },
    body: {
      activityStateRequest
    }
  }
  return gateway.request(changeActivityStateUsingPOSTOperation, parameters)
}

/**
 * List variables for an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.scope] scope
 * @return {Promise<module:types.RestVariable[]>} Indicates the execution was found and variables are returned.
 */
export function listExecutionVariables(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(listExecutionVariablesOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (array of RestVariable) or by passing a multipart/form-data Object.
 * Any number of variables can be passed into the request body array.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.body] Update a task variable
 * @param {string} [options.name] Required name of the variable
 * @param {string} [options.type] Type of variable that is updated. If omitted, reverts to raw JSON-value type (string, boolean, integer or double)
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable..
 * @return {Promise<object>} OK
 */
export function createExecutionVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    },
    body: {
      body: options.body
    },
    formData: {
      name: options.name,
      type: options.type,
      scope: options.scope
    }
  }
  return gateway.request(createExecutionVariableOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (array of RestVariable) or by passing a multipart/form-data Object.
 * Any number of variables can be passed into the request body array.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.body] Update a task variable
 * @param {string} [options.name] Required name of the variable
 * @param {string} [options.type] Type of variable that is updated. If omitted, reverts to raw JSON-value type (string, boolean, integer or double)
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable..
 * @return {Promise<object>} OK
 */
export function createOrUpdateExecutionVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    },
    body: {
      body: options.body
    },
    formData: {
      name: options.name,
      type: options.type,
      scope: options.scope
    }
  }
  return gateway.request(createOrUpdateExecutionVariableOperation, parameters)
}

/**
 * Delete all variables for an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @return {Promise<object>} OK
 */
export function deleteLocalVariablesUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId
    }
  }
  return gateway.request(deleteLocalVariablesUsingDELETEOperation, parameters)
}

/**
 * Get a variable for an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<module:types.RestVariable>} Indicates both the execution and variable were found and variable is returned.
 */
export function getExecutionVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getExecutionVariableOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable) or by passing a multipart/form-data Object.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.body] Update a variable on an execution
 * @param {ref} [options.file] 
 * @param {string} [options.name] 
 * @param {string} [options.type] 
 * @param {string} [options.scope] 
 * @return {Promise<module:types.RestVariable>} Indicates both the process instance and variable were found and variable is updated.
 */
export function updateExecutionVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId,
      variableName: options.variableName
    },
    body: {
      body: options.body
    },
    formData: {
      file: options.file,
      name: options.name,
      type: options.type,
      scope: options.scope
    }
  }
  return gateway.request(updateExecutionVariableOperation, parameters)
}

/**
 * Delete a variable for an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<object>} OK
 */
export function deletedExecutionVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(deletedExecutionVariableOperation, parameters)
}

/**
 * Get the binary data for an execution
 * 
 * @param {object} options Optional options
 * @param {string} [options.executionId] executionId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<string>} Indicates the execution was found and the requested variables are returned.
 */
export function getExecutionVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      executionId: options.executionId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getExecutionVariableDataOperation, parameters)
}

const queryExecutionsOperation = {
  path: '/query/executions',
  contentTypes: ['application/json'],
  method: 'post'
}

const listExecutionsOperation = {
  path: '/runtime/executions',
  method: 'get'
}

const executeExecutionActionUsingPUTOperation = {
  path: '/runtime/executions',
  contentTypes: ['application/json'],
  method: 'put'
}

const getExecutionUsingGETOperation = {
  path: '/runtime/executions/{executionId}',
  method: 'get'
}

const performExecutionActionUsingPUTOperation = {
  path: '/runtime/executions/{executionId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const listExecutionActiveActivitiesOperation = {
  path: '/runtime/executions/{executionId}/activities',
  method: 'get'
}

const changeActivityStateUsingPOSTOperation = {
  path: '/runtime/executions/{executionId}/change-state',
  contentTypes: ['application/json'],
  method: 'post'
}

const listExecutionVariablesOperation = {
  path: '/runtime/executions/{executionId}/variables',
  method: 'get'
}

const createExecutionVariableOperation = {
  path: '/runtime/executions/{executionId}/variables',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'post'
}

const createOrUpdateExecutionVariableOperation = {
  path: '/runtime/executions/{executionId}/variables',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'put'
}

const deleteLocalVariablesUsingDELETEOperation = {
  path: '/runtime/executions/{executionId}/variables',
  method: 'delete'
}

const getExecutionVariableOperation = {
  path: '/runtime/executions/{executionId}/variables/{variableName}',
  method: 'get'
}

const updateExecutionVariableOperation = {
  path: '/runtime/executions/{executionId}/variables/{variableName}',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'put'
}

const deletedExecutionVariableOperation = {
  path: '/runtime/executions/{executionId}/variables/{variableName}',
  method: 'delete'
}

const getExecutionVariableDataOperation = {
  path: '/runtime/executions/{executionId}/variables/{variableName}/data',
  method: 'get'
}

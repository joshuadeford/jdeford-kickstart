/** @module History */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List historic activity instances
 * 
 * @param {object} options Optional options
 * @param {string} [options.activityId] An id of the activity instance.
 * @param {string} [options.activityInstanceId] An id of the historic activity instance.
 * @param {string} [options.activityName] The name of the historic activity instance.
 * @param {string} [options.activityType] The element type of the historic activity instance.
 * @param {string} [options.executionId] The execution id of the historic activity instance.
 * @param {boolean} [options.finished] Indication if the historic activity instance is finished.
 * @param {string} [options.taskAssignee] The assignee of the historic activity instance.
 * @param {string} [options.processInstanceId] The process instance id of the historic activity instance.
 * @param {string} [options.processDefinitionId] The process definition id of the historic activity instance.
 * @param {string} [options.tenantId] Only return instances with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return instances with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns instances without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @return {Promise<module:types.DataResponseOfHistoricActivityInstanceResponse>} Indicates that historic activity instances could be queried.
 */
export function listHistoricActivityInstances(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      activityId: options.activityId,
      activityInstanceId: options.activityInstanceId,
      activityName: options.activityName,
      activityType: options.activityType,
      executionId: options.executionId,
      finished: options.finished,
      taskAssignee: options.taskAssignee,
      processInstanceId: options.processInstanceId,
      processDefinitionId: options.processDefinitionId,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId
    }
  }
  return gateway.request(listHistoricActivityInstancesOperation, parameters)
}

/**
 * Get historic detail
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] The id of the historic detail.
 * @param {string} [options.processInstanceId] The process instance id of the historic detail.
 * @param {string} [options.executionId] The execution id of the historic detail.
 * @param {string} [options.activityInstanceId] The activity instance id of the historic detail.
 * @param {string} [options.taskId] The task id of the historic detail.
 * @param {boolean} [options.selectOnlyFormProperties] Indication to only return form properties in the result.
 * @param {boolean} [options.selectOnlyVariableUpdates] Indication to only return variable updates in the result.
 * @return {Promise<module:types.DataResponseOfHistoricDetailResponse>} Indicates that historic detail could be queried.
 */
export function listHistoricDetails(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processInstanceId: options.processInstanceId,
      executionId: options.executionId,
      activityInstanceId: options.activityInstanceId,
      taskId: options.taskId,
      selectOnlyFormProperties: options.selectOnlyFormProperties,
      selectOnlyVariableUpdates: options.selectOnlyVariableUpdates
    }
  }
  return gateway.request(listHistoricDetailsOperation, parameters)
}

/**
 * The response body contains the binary value of the variable. When the variable is of type binary, the content-type of the response is set to application/octet-stream, regardless of the content of the variable or the request accept-type header. In case of serializable, application/x-java-serialized-object is used as content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.detailId] detailId
 * @return {Promise<string>} Indicates the historic detail instance was found and the requested variable data is returned.
 */
export function getHistoricDetailVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      detailId: options.detailId
    }
  }
  return gateway.request(getHistoricDetailVariableDataOperation, parameters)
}

/**
 * The response body contains the binary value of the variable. When the variable is of type binary, the content-type of the response is set to application/octet-stream, regardless of the content of the variable or the request accept-type header. In case of serializable, application/x-java-serialized-object is used as content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<string>} Indicates the task instance was found and the requested variable data is returned.
 */
export function getHistoricTaskInstanceVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getHistoricTaskInstanceVariableDataOperation, parameters)
}

/**
 * List of historic variable instances
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] The process instance id of the historic variable instance.
 * @param {string} [options.taskId] The task id of the historic variable instance.
 * @param {boolean} [options.excludeTaskVariables] Indication to exclude the task variables from the result.
 * @param {string} [options.variableName] The variable name of the historic variable instance.
 * @param {string} [options.variableNameLike] The variable name using the like operator for the historic variable instance.
 * @return {Promise<module:types.DataResponseOfHistoricVariableInstanceResponse>} Indicates that historic variable instances could be queried.
 */
export function listHistoricVariableInstances(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      processInstanceId: options.processInstanceId,
      taskId: options.taskId,
      excludeTaskVariables: options.excludeTaskVariables,
      variableName: options.variableName,
      variableNameLike: options.variableNameLike
    }
  }
  return gateway.request(listHistoricVariableInstancesOperation, parameters)
}

/**
 * The response body contains the binary value of the variable. When the variable is of type binary, the content-type of the response is set to application/octet-stream, regardless of the content of the variable or the request accept-type header. In case of serializable, application/x-java-serialized-object is used as content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.varInstanceId] varInstanceId
 * @return {Promise<string>} Indicates the variable instance was found and the requested variable data is returned.
 */
export function getHistoricInstanceVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      varInstanceId: options.varInstanceId
    }
  }
  return gateway.request(getHistoricInstanceVariableDataOperation, parameters)
}

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of historic task instances, but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long.
 * 
 * @param {module:types.HistoricActivityInstanceQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfHistoricActivityInstanceResponse>} Indicates request was successful and the activities are returned
 */
export function queryActivityInstancesUsingPOST(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryActivityInstancesUsingPOSTOperation, parameters)
}

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of historic process instances, but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long.
 * 
 * @param {module:types.HistoricDetailQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfHistoricDetailResponse>} Indicates request was successful and the historic details are returned
 */
export function queryHistoricDetailUsingPOST(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryHistoricDetailUsingPOSTOperation, parameters)
}

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of historic process instances, but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long. On top of that, the query allows for filtering based on process variables. The variables property is a JSON-array containing objects with the format as described here.
 * 
 * @param {module:types.HistoricVariableInstanceQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfHistoricVariableInstanceResponse>} Indicates request was successful and the tasks are returned
 */
export function queryVariableInstancesUsingPOST(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryVariableInstancesUsingPOSTOperation, parameters)
}

const listHistoricActivityInstancesOperation = {
  path: '/history/historic-activity-instances',
  method: 'get'
}

const listHistoricDetailsOperation = {
  path: '/history/historic-detail',
  method: 'get'
}

const getHistoricDetailVariableDataOperation = {
  path: '/history/historic-detail/{detailId}/data',
  method: 'get'
}

const getHistoricTaskInstanceVariableDataOperation = {
  path: '/history/historic-task-instances/{taskId}/variables/{variableName}/data',
  method: 'get'
}

const listHistoricVariableInstancesOperation = {
  path: '/history/historic-variable-instances',
  method: 'get'
}

const getHistoricInstanceVariableDataOperation = {
  path: '/history/historic-variable-instances/{varInstanceId}/data',
  method: 'get'
}

const queryActivityInstancesUsingPOSTOperation = {
  path: '/query/historic-activity-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

const queryHistoricDetailUsingPOSTOperation = {
  path: '/query/historic-detail',
  contentTypes: ['application/json'],
  method: 'post'
}

const queryVariableInstancesUsingPOSTOperation = {
  path: '/query/historic-variable-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

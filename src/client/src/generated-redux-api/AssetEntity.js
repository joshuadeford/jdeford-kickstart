/** @module AssetEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllAsset
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfAsset>} OK
 */
export function findAllAssetUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllAssetUsingGETOperation, parameters)
}

/**
 * saveAsset
 * 
 * @param {module:types.Asset} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function saveAssetUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveAssetUsingPOSTOperation, parameters)
}

/**
 * findByMediaIdAsset
 * 
 * @param {object} options Optional options
 * @param {string} [options.param0] param0
 * @return {Promise<module:types.ResourcesOfListOfAsset>} OK
 */
export function findByMediaIdAssetUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      param0: options.param0
    }
  }
  return gateway.request(findByMediaIdAssetUsingGETOperation, parameters)
}

/**
 * findOneAsset
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function findOneAssetUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneAssetUsingGETOperation, parameters)
}

/**
 * saveAsset
 * 
 * @param {string} id id
 * @param {module:types.Asset} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function saveAssetUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveAssetUsingPUTOperation, parameters)
}

/**
 * deleteAsset
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteAssetUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteAssetUsingDELETEOperation, parameters)
}

/**
 * saveAsset
 * 
 * @param {string} id id
 * @param {module:types.Asset} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function saveAssetUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveAssetUsingPATCHOperation, parameters)
}

/**
 * assetOwner
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOwnerUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOwnerUsingGETOperation, parameters)
}

/**
 * assetOwner
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOwnerUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOwnerUsingPOSTOperation, parameters)
}

/**
 * assetOwner
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOwnerUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOwnerUsingPUTOperation, parameters)
}

/**
 * assetOwner
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function assetOwnerUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOwnerUsingDELETEOperation, parameters)
}

/**
 * assetOwner
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOwnerUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOwnerUsingPATCHOperation, parameters)
}

const findAllAssetUsingGETOperation = {
  path: '/api/assets',
  method: 'get'
}

const saveAssetUsingPOSTOperation = {
  path: '/api/assets',
  contentTypes: ['application/json'],
  method: 'post'
}

const findByMediaIdAssetUsingGETOperation = {
  path: '/api/assets/search/findByMediaId',
  method: 'get'
}

const findOneAssetUsingGETOperation = {
  path: '/api/assets/{id}',
  method: 'get'
}

const saveAssetUsingPUTOperation = {
  path: '/api/assets/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteAssetUsingDELETEOperation = {
  path: '/api/assets/{id}',
  method: 'delete'
}

const saveAssetUsingPATCHOperation = {
  path: '/api/assets/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const assetOwnerUsingGETOperation = {
  path: '/api/assets/{id}/owner',
  method: 'get'
}

const assetOwnerUsingPOSTOperation = {
  path: '/api/assets/{id}/owner',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const assetOwnerUsingPUTOperation = {
  path: '/api/assets/{id}/owner',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const assetOwnerUsingDELETEOperation = {
  path: '/api/assets/{id}/owner',
  method: 'delete'
}

const assetOwnerUsingPATCHOperation = {
  path: '/api/assets/{id}/owner',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

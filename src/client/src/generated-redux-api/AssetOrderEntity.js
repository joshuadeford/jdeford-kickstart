/** @module AssetOrderEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllAssetOrder
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfAssetOrder>} OK
 */
export function findAllAssetOrderUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllAssetOrderUsingGETOperation, parameters)
}

/**
 * saveAssetOrder
 * 
 * @param {module:types.AssetOrder} body body
 * @return {Promise<module:types.ResourceOfAssetOrder>} OK
 */
export function saveAssetOrderUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveAssetOrderUsingPOSTOperation, parameters)
}

/**
 * findOneAssetOrder
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfAssetOrder>} OK
 */
export function findOneAssetOrderUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneAssetOrderUsingGETOperation, parameters)
}

/**
 * saveAssetOrder
 * 
 * @param {string} id id
 * @param {module:types.AssetOrder} body body
 * @return {Promise<module:types.ResourceOfAssetOrder>} OK
 */
export function saveAssetOrderUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveAssetOrderUsingPUTOperation, parameters)
}

/**
 * deleteAssetOrder
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteAssetOrderUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteAssetOrderUsingDELETEOperation, parameters)
}

/**
 * saveAssetOrder
 * 
 * @param {string} id id
 * @param {module:types.AssetOrder} body body
 * @return {Promise<module:types.ResourceOfAssetOrder>} OK
 */
export function saveAssetOrderUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveAssetOrderUsingPATCHOperation, parameters)
}

/**
 * assetOrderAsset
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function assetOrderAssetUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOrderAssetUsingGETOperation, parameters)
}

/**
 * assetOrderAsset
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function assetOrderAssetUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderAssetUsingPOSTOperation, parameters)
}

/**
 * assetOrderAsset
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function assetOrderAssetUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderAssetUsingPUTOperation, parameters)
}

/**
 * assetOrderAsset
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function assetOrderAssetUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOrderAssetUsingDELETEOperation, parameters)
}

/**
 * assetOrderAsset
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfAsset>} OK
 */
export function assetOrderAssetUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderAssetUsingPATCHOperation, parameters)
}

/**
 * assetOrderCreator
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOrderCreatorUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOrderCreatorUsingGETOperation, parameters)
}

/**
 * assetOrderCreator
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOrderCreatorUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderCreatorUsingPOSTOperation, parameters)
}

/**
 * assetOrderCreator
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOrderCreatorUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderCreatorUsingPUTOperation, parameters)
}

/**
 * assetOrderCreator
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function assetOrderCreatorUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(assetOrderCreatorUsingDELETEOperation, parameters)
}

/**
 * assetOrderCreator
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function assetOrderCreatorUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(assetOrderCreatorUsingPATCHOperation, parameters)
}

const findAllAssetOrderUsingGETOperation = {
  path: '/api/assetOrders',
  method: 'get'
}

const saveAssetOrderUsingPOSTOperation = {
  path: '/api/assetOrders',
  contentTypes: ['application/json'],
  method: 'post'
}

const findOneAssetOrderUsingGETOperation = {
  path: '/api/assetOrders/{id}',
  method: 'get'
}

const saveAssetOrderUsingPUTOperation = {
  path: '/api/assetOrders/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteAssetOrderUsingDELETEOperation = {
  path: '/api/assetOrders/{id}',
  method: 'delete'
}

const saveAssetOrderUsingPATCHOperation = {
  path: '/api/assetOrders/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const assetOrderAssetUsingGETOperation = {
  path: '/api/assetOrders/{id}/asset',
  method: 'get'
}

const assetOrderAssetUsingPOSTOperation = {
  path: '/api/assetOrders/{id}/asset',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const assetOrderAssetUsingPUTOperation = {
  path: '/api/assetOrders/{id}/asset',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const assetOrderAssetUsingDELETEOperation = {
  path: '/api/assetOrders/{id}/asset',
  method: 'delete'
}

const assetOrderAssetUsingPATCHOperation = {
  path: '/api/assetOrders/{id}/asset',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

const assetOrderCreatorUsingGETOperation = {
  path: '/api/assetOrders/{id}/creator',
  method: 'get'
}

const assetOrderCreatorUsingPOSTOperation = {
  path: '/api/assetOrders/{id}/creator',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const assetOrderCreatorUsingPUTOperation = {
  path: '/api/assetOrders/{id}/creator',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const assetOrderCreatorUsingDELETEOperation = {
  path: '/api/assetOrders/{id}/creator',
  method: 'delete'
}

const assetOrderCreatorUsingPATCHOperation = {
  path: '/api/assetOrders/{id}/creator',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

/** @module UserEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllUser
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfUser>} OK
 */
export function findAllUserUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllUserUsingGETOperation, parameters)
}

/**
 * saveUser
 * 
 * @param {module:types.User} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function saveUserUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveUserUsingPOSTOperation, parameters)
}

/**
 * findByEmailUser
 * 
 * @param {object} options Optional options
 * @param {string} [options.param0] param0
 * @return {Promise<module:types.ResourcesOfListOfUser>} OK
 */
export function findByEmailUserUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      param0: options.param0
    }
  }
  return gateway.request(findByEmailUserUsingGETOperation, parameters)
}

/**
 * findByRolesUser
 * 
 * @param {object} options Optional options
 * @param {string} [options.role] role
 * @param {number} [options.page] 
 * @param {number} [options.size] 
 * @param {string} [options.sort] 
 * @return {Promise<module:types.ResourcesOfListOfUser>} OK
 */
export function findByRolesUserUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      role: options.role,
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findByRolesUserUsingGETOperation, parameters)
}

/**
 * findByUserNameUser
 * 
 * @param {object} options Optional options
 * @param {string} [options.name] name
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function findByUserNameUserUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      name: options.name
    }
  }
  return gateway.request(findByUserNameUserUsingGETOperation, parameters)
}

/**
 * findOneUser
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function findOneUserUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneUserUsingGETOperation, parameters)
}

/**
 * saveUser
 * 
 * @param {string} id id
 * @param {module:types.User} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function saveUserUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveUserUsingPUTOperation, parameters)
}

/**
 * deleteUser
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteUserUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteUserUsingDELETEOperation, parameters)
}

/**
 * saveUser
 * 
 * @param {string} id id
 * @param {module:types.User} body body
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function saveUserUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveUserUsingPATCHOperation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourcesOfDevice>} OK
 */
export function userDevicesUsingGET_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(userDevicesUsingGET_1Operation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfDevice>} OK
 */
export function userDevicesUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userDevicesUsingPOSTOperation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfDevice>} OK
 */
export function userDevicesUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userDevicesUsingPUTOperation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function userDevicesUsingDELETE_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(userDevicesUsingDELETE_1Operation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfDevice>} OK
 */
export function userDevicesUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userDevicesUsingPATCHOperation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @param {string} deviceId deviceId
 * @return {Promise<module:types.ResourceOfDevice>} OK
 */
export function userDevicesUsingGET(id, deviceId) {
  const parameters = {
    path: {
      id,
      deviceId
    }
  }
  return gateway.request(userDevicesUsingGETOperation, parameters)
}

/**
 * userDevices
 * 
 * @param {string} id id
 * @param {string} deviceId deviceId
 * @return {Promise<object>} OK
 */
export function userDevicesUsingDELETE(id, deviceId) {
  const parameters = {
    path: {
      id,
      deviceId
    }
  }
  return gateway.request(userDevicesUsingDELETEOperation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourcesOfRole>} OK
 */
export function userRolesUsingGET_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(userRolesUsingGET_1Operation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfRole>} OK
 */
export function userRolesUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userRolesUsingPOSTOperation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfRole>} OK
 */
export function userRolesUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userRolesUsingPUTOperation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function userRolesUsingDELETE_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(userRolesUsingDELETE_1Operation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfRole>} OK
 */
export function userRolesUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(userRolesUsingPATCHOperation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @param {string} roleId roleId
 * @return {Promise<module:types.ResourceOfRole>} OK
 */
export function userRolesUsingGET(id, roleId) {
  const parameters = {
    path: {
      id,
      roleId
    }
  }
  return gateway.request(userRolesUsingGETOperation, parameters)
}

/**
 * userRoles
 * 
 * @param {string} id id
 * @param {string} roleId roleId
 * @return {Promise<object>} OK
 */
export function userRolesUsingDELETE(id, roleId) {
  const parameters = {
    path: {
      id,
      roleId
    }
  }
  return gateway.request(userRolesUsingDELETEOperation, parameters)
}

const findAllUserUsingGETOperation = {
  path: '/api/users',
  method: 'get'
}

const saveUserUsingPOSTOperation = {
  path: '/api/users',
  contentTypes: ['application/json'],
  method: 'post'
}

const findByEmailUserUsingGETOperation = {
  path: '/api/users/search/findByEmail',
  method: 'get'
}

const findByRolesUserUsingGETOperation = {
  path: '/api/users/search/findByRoles',
  method: 'get'
}

const findByUserNameUserUsingGETOperation = {
  path: '/api/users/search/findByUserName',
  method: 'get'
}

const findOneUserUsingGETOperation = {
  path: '/api/users/{id}',
  method: 'get'
}

const saveUserUsingPUTOperation = {
  path: '/api/users/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteUserUsingDELETEOperation = {
  path: '/api/users/{id}',
  method: 'delete'
}

const saveUserUsingPATCHOperation = {
  path: '/api/users/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const userDevicesUsingGET_1Operation = {
  path: '/api/users/{id}/devices',
  method: 'get'
}

const userDevicesUsingPOSTOperation = {
  path: '/api/users/{id}/devices',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const userDevicesUsingPUTOperation = {
  path: '/api/users/{id}/devices',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const userDevicesUsingDELETE_1Operation = {
  path: '/api/users/{id}/devices',
  method: 'delete'
}

const userDevicesUsingPATCHOperation = {
  path: '/api/users/{id}/devices',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

const userDevicesUsingGETOperation = {
  path: '/api/users/{id}/devices/{deviceId}',
  method: 'get'
}

const userDevicesUsingDELETEOperation = {
  path: '/api/users/{id}/devices/{deviceId}',
  method: 'delete'
}

const userRolesUsingGET_1Operation = {
  path: '/api/users/{id}/roles',
  method: 'get'
}

const userRolesUsingPOSTOperation = {
  path: '/api/users/{id}/roles',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const userRolesUsingPUTOperation = {
  path: '/api/users/{id}/roles',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const userRolesUsingDELETE_1Operation = {
  path: '/api/users/{id}/roles',
  method: 'delete'
}

const userRolesUsingPATCHOperation = {
  path: '/api/users/{id}/roles',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

const userRolesUsingGETOperation = {
  path: '/api/users/{id}/roles/{roleId}',
  method: 'get'
}

const userRolesUsingDELETEOperation = {
  path: '/api/users/{id}/roles/{roleId}',
  method: 'delete'
}

/** @module Runtime */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * Signal event received
 * 
 * @param {module:types.SignalEventReceivedRequest} signalRequest signalRequest
 * @return {Promise<object>} OK
 */
export function signalEventReceivedUsingPOST(signalRequest) {
  const parameters = {
    body: {
      signalRequest
    }
  }
  return gateway.request(signalEventReceivedUsingPOSTOperation, parameters)
}

const signalEventReceivedUsingPOSTOperation = {
  path: '/runtime/signals',
  contentTypes: ['application/json'],
  method: 'post'
}

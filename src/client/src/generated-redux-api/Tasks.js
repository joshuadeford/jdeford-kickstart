/** @module Tasks */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List of tasks
 * 
 * @param {object} options Optional options
 * @param {string} [options.name] Only return models with the given version.
 * @param {string} [options.nameLike] Only return tasks with a name like the given name.
 * @param {string} [options.description] Only return tasks with the given description.
 * @param {string} [options.priority] Only return tasks with the given priority.
 * @param {string} [options.minimumPriority] Only return tasks with a priority greater than the given value.
 * @param {string} [options.maximumPriority] Only return tasks with a priority lower than the given value.
 * @param {string} [options.assignee] Only return tasks assigned to the given user.
 * @param {string} [options.assigneeLike] Only return tasks assigned with an assignee like the given value.
 * @param {string} [options.owner] Only return tasks owned by the given user.
 * @param {string} [options.ownerLike] Only return tasks assigned with an owner like the given value.
 * @param {string} [options.unassigned] Only return tasks that are not assigned to anyone. If false is passed, the value is ignored.
 * @param {string} [options.delegationState] Only return tasks that have the given delegation state. Possible values are pending and resolved.
 * @param {string} [options.candidateUser] Only return tasks that can be claimed by the given user. This includes both tasks where the user is an explicit candidate for and task that are claimable by a group that the user is a member of.
 * @param {string} [options.candidateGroup] Only return tasks that can be claimed by a user in the given group.
 * @param {string} [options.candidateGroups] Only return tasks that can be claimed by a user in the given groups. Values split by comma.
 * @param {string} [options.involvedUser] Only return tasks in which the given user is involved.
 * @param {string} [options.taskDefinitionKey] Only return tasks with the given task definition id.
 * @param {string} [options.taskDefinitionKeyLike] Only return tasks with a given task definition id like the given value.
 * @param {string} [options.processInstanceId] Only return tasks which are part of the process instance with the given id.
 * @param {string} [options.processInstanceBusinessKey] Only return tasks which are part of the process instance with the given business key.
 * @param {string} [options.processInstanceBusinessKeyLike] Only return tasks which are part of the process instance which has a business key like the given value.
 * @param {string} [options.processDefinitionId] Only return tasks which are part of a process instance which has a process definition with the given id.
 * @param {string} [options.processDefinitionKey] Only return tasks which are part of a process instance which has a process definition with the given key.
 * @param {string} [options.processDefinitionKeyLike] Only return tasks which are part of a process instance which has a process definition with a key like the given value.
 * @param {string} [options.processDefinitionName] Only return tasks which are part of a process instance which has a process definition with the given name.
 * @param {string} [options.processDefinitionNameLike] Only return tasks which are part of a process instance which has a process definition with a name like the given value.
 * @param {string} [options.executionId] Only return tasks which are part of the execution with the given id.
 * @param {string} [options.createdOn] Only return tasks which are created on the given date.
 * @param {string} [options.createdBefore] Only return tasks which are created before the given date.
 * @param {string} [options.createdAfter] Only return tasks which are created after the given date.
 * @param {string} [options.dueOn] Only return tasks which are due on the given date.
 * @param {string} [options.dueBefore] Only return tasks which are due before the given date.
 * @param {string} [options.dueAfter] Only return tasks which are due after the given date.
 * @param {boolean} [options.withoutDueDate] Only return tasks which don’t have a due date. The property is ignored if the value is false.
 * @param {boolean} [options.excludeSubTasks] Only return tasks that are not a subtask of another task.
 * @param {boolean} [options.active] If true, only return tasks that are not suspended (either part of a process that is not suspended or not part of a process at all). If false, only tasks that are part of suspended process instances are returned.
 * @param {boolean} [options.includeTaskLocalVariables] Indication to include task local variables in the result.
 * @param {boolean} [options.includeProcessVariables] Indication to include process variables in the result.
 * @param {string} [options.tenantId] Only return tasks with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return tasks with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns tasks without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {string} [options.candidateOrAssigned] Select tasks that has been claimed or assigned to user or waiting to claim by user (candidate user or groups).
 * @param {string} [options.category] Select tasks with the given category. Note that this is the task category, not the category of the process definition (namespace within the BPMN Xml).
 * @return {Promise<module:types.DataResponseOfTaskResponse>} Indicates request was successful and the tasks are returned
 */
export function listTasks(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      name: options.name,
      nameLike: options.nameLike,
      description: options.description,
      priority: options.priority,
      minimumPriority: options.minimumPriority,
      maximumPriority: options.maximumPriority,
      assignee: options.assignee,
      assigneeLike: options.assigneeLike,
      owner: options.owner,
      ownerLike: options.ownerLike,
      unassigned: options.unassigned,
      delegationState: options.delegationState,
      candidateUser: options.candidateUser,
      candidateGroup: options.candidateGroup,
      candidateGroups: options.candidateGroups,
      involvedUser: options.involvedUser,
      taskDefinitionKey: options.taskDefinitionKey,
      taskDefinitionKeyLike: options.taskDefinitionKeyLike,
      processInstanceId: options.processInstanceId,
      processInstanceBusinessKey: options.processInstanceBusinessKey,
      processInstanceBusinessKeyLike: options.processInstanceBusinessKeyLike,
      processDefinitionId: options.processDefinitionId,
      processDefinitionKey: options.processDefinitionKey,
      processDefinitionKeyLike: options.processDefinitionKeyLike,
      processDefinitionName: options.processDefinitionName,
      processDefinitionNameLike: options.processDefinitionNameLike,
      executionId: options.executionId,
      createdOn: options.createdOn,
      createdBefore: options.createdBefore,
      createdAfter: options.createdAfter,
      dueOn: options.dueOn,
      dueBefore: options.dueBefore,
      dueAfter: options.dueAfter,
      withoutDueDate: options.withoutDueDate,
      excludeSubTasks: options.excludeSubTasks,
      active: options.active,
      includeTaskLocalVariables: options.includeTaskLocalVariables,
      includeProcessVariables: options.includeProcessVariables,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      candidateOrAssigned: options.candidateOrAssigned,
      category: options.category
    }
  }
  return gateway.request(listTasksOperation, parameters)
}

/**
 * Create Task
 * 
 * @param {module:types.TaskRequest} taskRequest taskRequest
 * @return {Promise<module:types.TaskResponse>} OK
 */
export function createTaskUsingPOST(taskRequest) {
  const parameters = {
    body: {
      taskRequest
    }
  }
  return gateway.request(createTaskUsingPOSTOperation, parameters)
}

/**
 * Get a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.TaskResponse>} Indicates the task was found and returned.
 */
export function getTaskUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(getTaskUsingGETOperation, parameters)
}

/**
 * Tasks actions
 * 
 * @param {module:types.TaskActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<object>} Indicates the action was executed.
 */
export function executeTaskActionUsingPOST(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(executeTaskActionUsingPOSTOperation, parameters)
}

/**
 * All request values are optional. For example, you can only include the assignee attribute in the request body JSON-object, only updating the assignee of the task, leaving all other fields unaffected. When an attribute is explicitly included and is set to null, the task-value will be updated to null. Example: {"dueDate" : null} will clear the duedate of the task).
 * 
 * @param {module:types.TaskRequest} taskRequest taskRequest
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.TaskResponse>} Indicates the task was updated.
 */
export function updateTaskUsingPUT(taskRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      taskRequest
    }
  }
  return gateway.request(updateTaskUsingPUTOperation, parameters)
}

/**
 * Delete a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.cascadeHistory] Whether or not to delete the HistoricTask instance when deleting the task (if applicable). If not provided, this value defaults to false.
 * @param {string} [options.deleteReason] Reason why the task is deleted. This value is ignored when cascadeHistory is true.
 * @return {Promise<object>} OK
 */
export function deleteTaskUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    query: {
      cascadeHistory: options.cascadeHistory,
      deleteReason: options.deleteReason
    }
  }
  return gateway.request(deleteTaskUsingDELETEOperation, parameters)
}

/**
 * List events for a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.EventResponse[]>} Indicates the task was found and the events are returned.
 */
export function listTaskEvents(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listTaskEventsOperation, parameters)
}

/**
 * Get an event on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.eventId] eventId
 * @return {Promise<module:types.EventResponse>} Indicates the task and event were found and the event is returned.
 */
export function getEventUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      eventId: options.eventId
    }
  }
  return gateway.request(getEventUsingGETOperation, parameters)
}

/**
 * Delete an event on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.eventId] eventId
 * @return {Promise<object>} OK
 */
export function deleteEventUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      eventId: options.eventId
    }
  }
  return gateway.request(deleteEventUsingDELETEOperation, parameters)
}

/**
 * List of sub tasks for a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.TaskResponse[]>} Indicates request was successful and the  sub tasks are returned
 */
export function listTaskSubtasks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listTaskSubtasksOperation, parameters)
}

/**
 * Delete all local variables on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<object>} OK
 */
export function deleteAllLocalTaskVariablesUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(deleteAllLocalTaskVariablesUsingDELETEOperation, parameters)
}

const listTasksOperation = {
  path: '/runtime/tasks',
  method: 'get'
}

const createTaskUsingPOSTOperation = {
  path: '/runtime/tasks',
  contentTypes: ['application/json'],
  method: 'post'
}

const getTaskUsingGETOperation = {
  path: '/runtime/tasks/{taskId}',
  method: 'get'
}

const executeTaskActionUsingPOSTOperation = {
  path: '/runtime/tasks/{taskId}',
  contentTypes: ['application/json'],
  method: 'post'
}

const updateTaskUsingPUTOperation = {
  path: '/runtime/tasks/{taskId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteTaskUsingDELETEOperation = {
  path: '/runtime/tasks/{taskId}',
  method: 'delete'
}

const listTaskEventsOperation = {
  path: '/runtime/tasks/{taskId}/events',
  method: 'get'
}

const getEventUsingGETOperation = {
  path: '/runtime/tasks/{taskId}/events/{eventId}',
  method: 'get'
}

const deleteEventUsingDELETEOperation = {
  path: '/runtime/tasks/{taskId}/events/{eventId}',
  method: 'delete'
}

const listTaskSubtasksOperation = {
  path: '/runtime/tasks/{taskId}/subtasks',
  method: 'get'
}

const deleteAllLocalTaskVariablesUsingDELETEOperation = {
  path: '/runtime/tasks/{taskId}/variables',
  method: 'delete'
}

/** @module ProcessInstances */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * The request body can contain all possible filters that can be used in the List process instances URL query. On top of these, it’s possible to provide an array of variables to include in the query, with their format described here.
 * 
 * The general paging and sorting query-parameters can be used for this URL.
 * 
 * @param {module:types.ProcessInstanceQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfProcessInstanceResponse>} Indicates request was successful and the process-instances are returned
 */
export function queryProcessInstancesUsingPOST(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryProcessInstancesUsingPOSTOperation, parameters)
}

/**
 * List process instances
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return models with the given version.
 * @param {string} [options.processDefinitionKey] Only return process instances with the given process definition key.
 * @param {string} [options.processDefinitionId] Only return process instances with the given process definition id.
 * @param {string} [options.businessKey] Only return process instances with the given businessKey.
 * @param {string} [options.involvedUser] Only return process instances in which the given user is involved.
 * @param {boolean} [options.suspended] If true, only return process instance which are suspended. If false, only return process instances which are not suspended (active).
 * @param {string} [options.superProcessInstanceId] Only return process instances which have the given super process-instance id (for processes that have a call-activities).
 * @param {string} [options.subProcessInstanceId] Only return process instances which have the given sub process-instance id (for processes started as a call-activity).
 * @param {boolean} [options.excludeSubprocesses] Return only process instances which aren’t sub processes.
 * @param {boolean} [options.includeProcessVariables] Indication to include process variables in the result.
 * @param {string} [options.tenantId] Only return process instances with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return process instances with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns process instances without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {string} [options.sort] Enum: id, processDefinitionId, tenantId, processDefinitionKey. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfProcessInstanceResponse>} Indicates request was successful and the process-instances are returned
 */
export function listProcessInstances(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      processDefinitionKey: options.processDefinitionKey,
      processDefinitionId: options.processDefinitionId,
      businessKey: options.businessKey,
      involvedUser: options.involvedUser,
      suspended: options.suspended,
      superProcessInstanceId: options.superProcessInstanceId,
      subProcessInstanceId: options.subProcessInstanceId,
      excludeSubprocesses: options.excludeSubprocesses,
      includeProcessVariables: options.includeProcessVariables,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      sort: options.sort
    }
  }
  return gateway.request(listProcessInstancesOperation, parameters)
}

/**
 * Note that also a *transientVariables* property is accepted as part of this json, that follows the same structure as the *variables* property.
 * 
 * Only one of *processDefinitionId*, *processDefinitionKey* or *message* can be used in the request body. 
 * 
 * Parameters *businessKey*, *variables* and *tenantId* are optional.
 * 
 *  If tenantId is omitted, the default tenant will be used. More information about the variable format can be found in the REST variables section.
 * 
 *  Note that the variable-scope that is supplied is ignored, process-variables are always local.
 * 
 * @param {module:types.ProcessInstanceCreateRequest} request request
 * @return {Promise<module:types.ProcessInstanceResponse>} OK
 */
export function createProcessInstanceUsingPOST(request) {
  const parameters = {
    body: {
      request
    }
  }
  return gateway.request(createProcessInstanceUsingPOSTOperation, parameters)
}

/**
 * Get a process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.ProcessInstanceResponse>} Indicates the process instance was found and returned.
 */
export function getProcessInstance(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(getProcessInstanceOperation, parameters)
}

/**
 * Activate or suspend a process instance
 * 
 * @param {module:types.ProcessInstanceActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.ProcessInstanceResponse>} Indicates the process instance was found and action was executed.
 */
export function performProcessInstanceActionUsingPUT(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(performProcessInstanceActionUsingPUTOperation, parameters)
}

/**
 * Delete a process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.deleteReason] deleteReason
 * @return {Promise<object>} OK
 */
export function deleteProcessInstance(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    query: {
      deleteReason: options.deleteReason
    }
  }
  return gateway.request(deleteProcessInstanceOperation, parameters)
}

/**
 * Change the state a process instance
 * 
 * @param {module:types.ExecutionChangeActivityStateRequest} activityStateRequest activityStateRequest
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<object>} Indicates the process instance was found and change state activity was executed.
 */
export function changeActivityStateUsingPOST_1(activityStateRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      activityStateRequest
    }
  }
  return gateway.request(changeActivityStateUsingPOST_1Operation, parameters)
}

/**
 * Get diagram for a process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<string>} Indicates the process instance was found and the diagram was returned.
 */
export function getProcessInstanceDiagramUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(getProcessInstanceDiagramUsingGETOperation, parameters)
}

const queryProcessInstancesUsingPOSTOperation = {
  path: '/query/process-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

const listProcessInstancesOperation = {
  path: '/runtime/process-instances',
  method: 'get'
}

const createProcessInstanceUsingPOSTOperation = {
  path: '/runtime/process-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

const getProcessInstanceOperation = {
  path: '/runtime/process-instances/{processInstanceId}',
  method: 'get'
}

const performProcessInstanceActionUsingPUTOperation = {
  path: '/runtime/process-instances/{processInstanceId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteProcessInstanceOperation = {
  path: '/runtime/process-instances/{processInstanceId}',
  method: 'delete'
}

const changeActivityStateUsingPOST_1Operation = {
  path: '/runtime/process-instances/{processInstanceId}/change-state',
  contentTypes: ['application/json'],
  method: 'post'
}

const getProcessInstanceDiagramUsingGETOperation = {
  path: '/runtime/process-instances/{processInstanceId}/diagram',
  method: 'get'
}

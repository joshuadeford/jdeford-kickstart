/** @module Deployment */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List Deployments
 * 
 * @param {object} options Optional options
 * @param {string} [options.name] Only return deployments with the given name.
 * @param {string} [options.nameLike] Only return deployments with a name like the given name.
 * @param {string} [options.category] Only return deployments with the given category.
 * @param {string} [options.categoryNotEquals] Only return deployments which don’t have the given category.
 * @param {string} [options.tenantId] Only return deployments with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return deployments with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns deployments without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {string} [options.sort] Enum: id, name, deployTime, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfDeploymentResponse>} Indicates the request was successful.
 */
export function listDeployments(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      name: options.name,
      nameLike: options.nameLike,
      category: options.category,
      categoryNotEquals: options.categoryNotEquals,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      sort: options.sort
    }
  }
  return gateway.request(listDeploymentsOperation, parameters)
}

/**
 * The request body should contain data of type multipart/form-data. There should be exactly one file in the request, any additional files will be ignored. The deployment name is the name of the file-field passed in. If multiple resources need to be deployed in a single deployment, compress the resources in a zip and make sure the file-name ends with .bar or .zip.
 * 
 * An additional parameter (form-field) can be passed in the request body with name tenantId. The value of this field will be used as the id of the tenant this deployment is done in.
 * 
 * @param {ref} file 
 * @param {object} options Optional options
 * @param {string} [options.deploymentKey] deploymentKey
 * @param {string} [options.deploymentName] deploymentName
 * @param {string} [options.tenantId] tenantId
 * @return {Promise<module:types.DeploymentResponse>} OK
 */
export function uploadDeploymentUsingPOST(file, options) {
  if (!options) options = {}
  const parameters = {
    query: {
      deploymentKey: options.deploymentKey,
      deploymentName: options.deploymentName,
      tenantId: options.tenantId
    },
    formData: {
      file
    }
  }
  return gateway.request(uploadDeploymentUsingPOSTOperation, parameters)
}

/**
 * Get a deployment
 * 
 * @param {object} options Optional options
 * @param {string} [options.deploymentId] The id of the deployment to get.
 * @return {Promise<module:types.DeploymentResponse>} Indicates the deployment was found and returned.
 */
export function getDeploymentUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      deploymentId: options.deploymentId
    }
  }
  return gateway.request(getDeploymentUsingGETOperation, parameters)
}

/**
 * Delete a deployment
 * 
 * @param {object} options Optional options
 * @param {string} [options.deploymentId] deploymentId
 * @param {boolean} [options.cascade] cascade
 * @return {Promise<object>} OK
 */
export function deleteDeploymentUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      deploymentId: options.deploymentId
    },
    query: {
      cascade: options.cascade
    }
  }
  return gateway.request(deleteDeploymentUsingDELETEOperation, parameters)
}

/**
 * The response body will contain the binary resource-content for the requested resource. The response content-type will be the same as the type returned in the resources mimeType property. Also, a content-disposition header is set, allowing browsers to download the file instead of displaying it.
 * 
 * @param {object} options Optional options
 * @param {string} [options.deploymentId] deploymentId
 * @param {string} [options.resourceName] The name of the resource to get. Make sure you URL-encode the resourceName in case it contains forward slashes. Eg: use diagrams%2Fmy-process.bpmn20.xml instead of diagrams/my-process.bpmn20.xml.
 * @return {Promise<string>} Indicates both deployment and resource have been found and the resource data has been returned.
 */
export function getDeploymentResourceData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      deploymentId: options.deploymentId,
      resourceName: options.resourceName
    }
  }
  return gateway.request(getDeploymentResourceDataOperation, parameters)
}

/**
 * The dataUrl property in the resulting JSON for a single resource contains the actual URL to use for retrieving the binary resource.
 * 
 * @param {object} options Optional options
 * @param {string} [options.deploymentId] deploymentId
 * @return {Promise<module:types.DeploymentResourceResponse[]>} Indicates the deployment was found and the resource list has been returned.
 */
export function listDeploymentResources(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      deploymentId: options.deploymentId
    }
  }
  return gateway.request(listDeploymentResourcesOperation, parameters)
}

/**
 * Replace ** by ResourceId
 * 
 * @param {object} options Optional options
 * @param {string} [options.deploymentId] deploymentId
 * @return {Promise<module:types.DeploymentResourceResponse>} Indicates both deployment and resource have been found and the resource has been returned.
 */
export function getDeploymentResourceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      deploymentId: options.deploymentId
    }
  }
  return gateway.request(getDeploymentResourceUsingGETOperation, parameters)
}

const listDeploymentsOperation = {
  path: '/repository/deployments',
  method: 'get'
}

const uploadDeploymentUsingPOSTOperation = {
  path: '/repository/deployments',
  method: 'post'
}

const getDeploymentUsingGETOperation = {
  path: '/repository/deployments/{deploymentId}',
  method: 'get'
}

const deleteDeploymentUsingDELETEOperation = {
  path: '/repository/deployments/{deploymentId}',
  method: 'delete'
}

const getDeploymentResourceDataOperation = {
  path: '/repository/deployments/{deploymentId}/resourcedata/{resourceName}',
  method: 'get'
}

const listDeploymentResourcesOperation = {
  path: '/repository/deployments/{deploymentId}/resources',
  method: 'get'
}

const getDeploymentResourceUsingGETOperation = {
  path: '/repository/deployments/{deploymentId}/resources/**',
  method: 'get'
}

/** @module Groups */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List groups
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return group with the given id
 * @param {string} [options.name] Only return groups with the given name
 * @param {string} [options.type] Only return groups with the given type
 * @param {string} [options.nameLike] Only return groups with a name like the given value. Use % as wildcard-character.
 * @param {string} [options.member] Only return groups which have a member with the given username.
 * @param {string} [options.potentialStarter] Only return groups which members are potential starters for a process-definition with the given id.
 * @param {string} [options.sort] Enum: id, name, type. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfGroupResponse>} Indicates the requested groups were returned.
 */
export function listGroups(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      name: options.name,
      type: options.type,
      nameLike: options.nameLike,
      member: options.member,
      potentialStarter: options.potentialStarter,
      sort: options.sort
    }
  }
  return gateway.request(listGroupsOperation, parameters)
}

/**
 * Create a group
 * 
 * @param {module:types.GroupRequest} groupRequest groupRequest
 * @return {Promise<module:types.GroupResponse>} OK
 */
export function createGroupUsingPOST(groupRequest) {
  const parameters = {
    body: {
      groupRequest
    }
  }
  return gateway.request(createGroupUsingPOSTOperation, parameters)
}

/**
 * Get a single group
 * 
 * @param {object} options Optional options
 * @param {string} [options.groupId] groupId
 * @return {Promise<module:types.GroupResponse>} Indicates the group exists and is returned.
 */
export function getGroupUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      groupId: options.groupId
    }
  }
  return gateway.request(getGroupUsingGETOperation, parameters)
}

/**
 * All request values are optional. For example, you can only include the name attribute in the request body JSON-object, only updating the name of the group, leaving all other fields unaffected. When an attribute is explicitly included and is set to null, the group-value will be updated to null.
 * 
 * @param {module:types.GroupRequest} groupRequest groupRequest
 * @param {object} options Optional options
 * @param {string} [options.groupId] groupId
 * @return {Promise<module:types.GroupResponse>} Indicates the group was updated.
 */
export function updateGroupUsingPUT(groupRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      groupId: options.groupId
    },
    body: {
      groupRequest
    }
  }
  return gateway.request(updateGroupUsingPUTOperation, parameters)
}

/**
 * Delete a group
 * 
 * @param {object} options Optional options
 * @param {string} [options.groupId] groupId
 * @return {Promise<object>} OK
 */
export function deleteGroupUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      groupId: options.groupId
    }
  }
  return gateway.request(deleteGroupUsingDELETEOperation, parameters)
}

/**
 * Add a member to a group
 * 
 * @param {module:types.MembershipRequest} memberShip memberShip
 * @param {object} options Optional options
 * @param {string} [options.groupId] groupId
 * @return {Promise<module:types.MembershipResponse>} OK
 */
export function createMembershipUsingPOST(memberShip, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      groupId: options.groupId
    },
    body: {
      memberShip
    }
  }
  return gateway.request(createMembershipUsingPOSTOperation, parameters)
}

/**
 * Delete a member from a group
 * 
 * @param {object} options Optional options
 * @param {string} [options.groupId] groupId
 * @param {string} [options.userId] userId
 * @return {Promise<object>} OK
 */
export function deleteMembershipUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      groupId: options.groupId,
      userId: options.userId
    }
  }
  return gateway.request(deleteMembershipUsingDELETEOperation, parameters)
}

const listGroupsOperation = {
  path: '/identity/groups',
  method: 'get'
}

const createGroupUsingPOSTOperation = {
  path: '/identity/groups',
  contentTypes: ['application/json'],
  method: 'post'
}

const getGroupUsingGETOperation = {
  path: '/identity/groups/{groupId}',
  method: 'get'
}

const updateGroupUsingPUTOperation = {
  path: '/identity/groups/{groupId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteGroupUsingDELETEOperation = {
  path: '/identity/groups/{groupId}',
  method: 'delete'
}

const createMembershipUsingPOSTOperation = {
  path: '/identity/groups/{groupId}/members',
  contentTypes: ['application/json'],
  method: 'post'
}

const deleteMembershipUsingDELETEOperation = {
  path: '/identity/groups/{groupId}/members/{userId}',
  method: 'delete'
}

/** @module ProcessInstanceVariables */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * In case the variable is a binary variable or serializable, the valueUrl points to an URL to fetch the raw value. If it’s a plain variable, the value is present in the response. Note that only local scoped variables are returned, as there is no global scope for process-instance variables.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.scope] scope
 * @return {Promise<module:types.RestVariable[]>} Indicates the process instance was found and variables are returned.
 */
export function listProcessInstanceVariables(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(listProcessInstanceVariablesOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable or an array of RestVariable) or by passing a multipart/form-data Object.
 * Nonexistent variables are created on the process-instance and existing ones are overridden without any error.
 * Any number of variables can be passed into the request body array.
 * Note that scope is ignored, only local variables can be set in a process instance.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.body] Create a variable on a process instance
 * @param {ref} [options.file] 
 * @param {string} [options.name] 
 * @param {string} [options.type] 
 * @return {Promise<object>} OK
 */
export function createProcessInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      body: options.body
    },
    formData: {
      file: options.file,
      name: options.name,
      type: options.type
    }
  }
  return gateway.request(createProcessInstanceVariableOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable or an array of RestVariable) or by passing a multipart/form-data Object.
 * Nonexistent variables are created on the process-instance and existing ones are overridden without any error.
 * Any number of variables can be passed into the request body array.
 * Note that scope is ignored, only local variables can be set in a process instance.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.body] Create a variable on a process instance
 * @param {ref} [options.file] 
 * @param {string} [options.name] 
 * @param {string} [options.type] 
 * @return {Promise<object>} OK
 */
export function createOrUpdateProcessVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      body: options.body
    },
    formData: {
      file: options.file,
      name: options.name,
      type: options.type
    }
  }
  return gateway.request(createOrUpdateProcessVariableOperation, parameters)
}

/**
 * Delete all variables
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<object>} OK
 */
export function deleteLocalProcessVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(deleteLocalProcessVariableOperation, parameters)
}

/**
 * Get a variable for a process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<module:types.RestVariable>} Indicates both the process instance and variable were found and variable is returned.
 */
export function getProcessInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getProcessInstanceVariableOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable) or by passing a multipart/form-data Object.
 * Nonexistent variables are created on the process-instance and existing ones are overridden without any error.
 * Note that scope is ignored, only local variables can be set in a process instance.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.body] Create a variable on a process instance
 * @param {ref} [options.file] 
 * @param {string} [options.name] 
 * @param {string} [options.type] 
 * @return {Promise<module:types.RestVariable>} OK
 */
export function updateProcessInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      variableName: options.variableName
    },
    body: {
      body: options.body
    },
    formData: {
      file: options.file,
      name: options.name,
      type: options.type
    }
  }
  return gateway.request(updateProcessInstanceVariableOperation, parameters)
}

/**
 * Delete a variable
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<object>} OK
 */
export function deleteProcessInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(deleteProcessInstanceVariableOperation, parameters)
}

/**
 * Get the binary data for a variable
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] scope
 * @return {Promise<string>} Indicates the process instance was found and the requested variables are returned.
 */
export function getProcessInstanceVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getProcessInstanceVariableDataOperation, parameters)
}

const listProcessInstanceVariablesOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables',
  method: 'get'
}

const createProcessInstanceVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'post'
}

const createOrUpdateProcessVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'put'
}

const deleteLocalProcessVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables',
  method: 'delete'
}

const getProcessInstanceVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables/{variableName}',
  method: 'get'
}

const updateProcessInstanceVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables/{variableName}',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'put'
}

const deleteProcessInstanceVariableOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables/{variableName}',
  method: 'delete'
}

const getProcessInstanceVariableDataOperation = {
  path: '/runtime/process-instances/{processInstanceId}/variables/{variableName}/data',
  method: 'get'
}

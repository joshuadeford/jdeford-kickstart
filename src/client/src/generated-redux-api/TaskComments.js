/** @module TaskComments */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List comments on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.CommentResponse[]>} Indicates the task was found and the comments are returned.
 */
export function listTaskComments(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listTaskCommentsOperation, parameters)
}

/**
 * Create a new comment on a task
 * 
 * @param {module:types.CommentRequest} comment comment
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.CommentResponse>} OK
 */
export function createTaskComments(comment, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      comment
    }
  }
  return gateway.request(createTaskCommentsOperation, parameters)
}

/**
 * Get a comment on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.commentId] commentId
 * @return {Promise<module:types.CommentResponse>} Indicates the task and comment were found and the comment is returned.
 */
export function getTaskComment(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      commentId: options.commentId
    }
  }
  return gateway.request(getTaskCommentOperation, parameters)
}

/**
 * Delete a comment on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.commentId] commentId
 * @return {Promise<object>} OK
 */
export function deleteTaskComment(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      commentId: options.commentId
    }
  }
  return gateway.request(deleteTaskCommentOperation, parameters)
}

const listTaskCommentsOperation = {
  path: '/runtime/tasks/{taskId}/comments',
  method: 'get'
}

const createTaskCommentsOperation = {
  path: '/runtime/tasks/{taskId}/comments',
  contentTypes: ['application/json'],
  method: 'post'
}

const getTaskCommentOperation = {
  path: '/runtime/tasks/{taskId}/comments/{commentId}',
  method: 'get'
}

const deleteTaskCommentOperation = {
  path: '/runtime/tasks/{taskId}/comments/{commentId}',
  method: 'delete'
}

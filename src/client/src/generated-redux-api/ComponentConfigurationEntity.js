/** @module ComponentConfigurationEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllComponentConfiguration
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfComponentConfiguration>} OK
 */
export function findAllComponentConfigurationUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllComponentConfigurationUsingGETOperation, parameters)
}

/**
 * saveComponentConfiguration
 * 
 * @param {module:types.ComponentConfiguration} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function saveComponentConfigurationUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveComponentConfigurationUsingPOSTOperation, parameters)
}

/**
 * findByVersionComponentConfiguration
 * 
 * @param {object} options Optional options
 * @param {number} [options.param0] param0
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function findByVersionComponentConfigurationUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      param0: options.param0
    }
  }
  return gateway.request(findByVersionComponentConfigurationUsingGETOperation, parameters)
}

/**
 * findOneComponentConfiguration
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function findOneComponentConfigurationUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneComponentConfigurationUsingGETOperation, parameters)
}

/**
 * saveComponentConfiguration
 * 
 * @param {string} id id
 * @param {module:types.ComponentConfiguration} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function saveComponentConfigurationUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveComponentConfigurationUsingPUTOperation, parameters)
}

/**
 * deleteComponentConfiguration
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteComponentConfigurationUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteComponentConfigurationUsingDELETEOperation, parameters)
}

/**
 * saveComponentConfiguration
 * 
 * @param {string} id id
 * @param {module:types.ComponentConfiguration} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function saveComponentConfigurationUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveComponentConfigurationUsingPATCHOperation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourcesOfComponentProperty>} OK
 */
export function componentConfigurationPropertiesUsingGET_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingGET_1Operation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfComponentProperty>} OK
 */
export function componentConfigurationPropertiesUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingPOSTOperation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfComponentProperty>} OK
 */
export function componentConfigurationPropertiesUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingPUTOperation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function componentConfigurationPropertiesUsingDELETE_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingDELETE_1Operation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfComponentProperty>} OK
 */
export function componentConfigurationPropertiesUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingPATCHOperation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @param {string} componentpropertyId componentpropertyId
 * @return {Promise<module:types.ResourceOfComponentProperty>} OK
 */
export function componentConfigurationPropertiesUsingGET(id, componentpropertyId) {
  const parameters = {
    path: {
      id,
      componentpropertyId
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingGETOperation, parameters)
}

/**
 * componentConfigurationProperties
 * 
 * @param {string} id id
 * @param {string} componentpropertyId componentpropertyId
 * @return {Promise<object>} OK
 */
export function componentConfigurationPropertiesUsingDELETE(id, componentpropertyId) {
  const parameters = {
    path: {
      id,
      componentpropertyId
    }
  }
  return gateway.request(componentConfigurationPropertiesUsingDELETEOperation, parameters)
}

const findAllComponentConfigurationUsingGETOperation = {
  path: '/api/componentConfigurations',
  method: 'get'
}

const saveComponentConfigurationUsingPOSTOperation = {
  path: '/api/componentConfigurations',
  contentTypes: ['application/json'],
  method: 'post'
}

const findByVersionComponentConfigurationUsingGETOperation = {
  path: '/api/componentConfigurations/search/findByVersion',
  method: 'get'
}

const findOneComponentConfigurationUsingGETOperation = {
  path: '/api/componentConfigurations/{id}',
  method: 'get'
}

const saveComponentConfigurationUsingPUTOperation = {
  path: '/api/componentConfigurations/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteComponentConfigurationUsingDELETEOperation = {
  path: '/api/componentConfigurations/{id}',
  method: 'delete'
}

const saveComponentConfigurationUsingPATCHOperation = {
  path: '/api/componentConfigurations/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const componentConfigurationPropertiesUsingGET_1Operation = {
  path: '/api/componentConfigurations/{id}/properties',
  method: 'get'
}

const componentConfigurationPropertiesUsingPOSTOperation = {
  path: '/api/componentConfigurations/{id}/properties',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const componentConfigurationPropertiesUsingPUTOperation = {
  path: '/api/componentConfigurations/{id}/properties',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const componentConfigurationPropertiesUsingDELETE_1Operation = {
  path: '/api/componentConfigurations/{id}/properties',
  method: 'delete'
}

const componentConfigurationPropertiesUsingPATCHOperation = {
  path: '/api/componentConfigurations/{id}/properties',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

const componentConfigurationPropertiesUsingGETOperation = {
  path: '/api/componentConfigurations/{id}/properties/{componentpropertyId}',
  method: 'get'
}

const componentConfigurationPropertiesUsingDELETEOperation = {
  path: '/api/componentConfigurations/{id}/properties/{componentpropertyId}',
  method: 'delete'
}

/** @module Eventsubscriptions */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List of event subscriptions
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return event subscriptions with the given id
 * @param {string} [options.eventType] Only return event subscriptions with the given event type
 * @param {string} [options.eventName] Only return event subscriptions with the given event name
 * @param {string} [options.activityId] Only return event subscriptions with the given activity id
 * @param {string} [options.executionId] Only return event subscriptions with the given execution id
 * @param {string} [options.processInstanceId] Only return event subscriptions part of a process with the given id
 * @param {string} [options.processDefinitionId] Only return event subscriptions with the given process definition id
 * @param {string} [options.createdBefore] Only return event subscriptions which are created before the given date.
 * @param {string} [options.createdAfter] Only return event subscriptions which are created after the given date.
 * @param {string} [options.tenantId] Only return event subscriptions with the given tenant id.
 * @param {string} [options.sort] Enum: id, created, executionId, processInstanceId, processDefinitionId, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfEventSubscriptionResponse>} Indicates the requested event subscriptions were returned.
 */
export function listEventSubscriptions(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      eventType: options.eventType,
      eventName: options.eventName,
      activityId: options.activityId,
      executionId: options.executionId,
      processInstanceId: options.processInstanceId,
      processDefinitionId: options.processDefinitionId,
      createdBefore: options.createdBefore,
      createdAfter: options.createdAfter,
      tenantId: options.tenantId,
      sort: options.sort
    }
  }
  return gateway.request(listEventSubscriptionsOperation, parameters)
}

/**
 * Get a single event subscription
 * 
 * @param {object} options Optional options
 * @param {string} [options.eventSubscriptionId] eventSubscriptionId
 * @return {Promise<module:types.EventSubscriptionResponse>} Indicates the event subscription exists and is returned.
 */
export function getEventSubscriptionUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      eventSubscriptionId: options.eventSubscriptionId
    }
  }
  return gateway.request(getEventSubscriptionUsingGETOperation, parameters)
}

const listEventSubscriptionsOperation = {
  path: '/runtime/event-subscriptions',
  method: 'get'
}

const getEventSubscriptionUsingGETOperation = {
  path: '/runtime/event-subscriptions/{eventSubscriptionId}',
  method: 'get'
}

These files are all generated from a swagger endpoint.  If your endpoint is http://localhost:8181/v2/api-docs
run these commands from the root directory
 * `openapi -s http://localhost:8181/v2/api-docs -l js -o src/generated-redux-api --redux`
 * `openapi -s http://localhost:8181/v2/api-docs -l ts -o generated-redux-api-ts --redux`
 * `mv generated-redux-api-ts/types.ts src/generated-redux-api`
 
The first command generates all the javascript redux actions and action creators for all the api endpoint on your server.
The second one does the same thing but for TypeScript.  
However, the openapi code generator generates broken TypesScript so only the type definitions can be used.
For that reason, the types.ts file is copied into the generated code directory and the rest is deleted.

There are a couple of other errors you'll see after generating the code.  None of that stuff
is being used so it can safely be deleted from the offending files.


You'll also need to change the base url for the api calls to use your dev-server's url. 
/** @module ProcessInstanceIdentityLinks */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * Note that the groupId in Response Body will always be null, as it’s only possible to involve users with a process-instance.
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.RestIdentityLink[]>} Indicates the process instance was found and links are returned.
 */
export function listProcessInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    }
  }
  return gateway.request(listProcessInstanceIdentityLinksOperation, parameters)
}

/**
 * Note that the groupId in Response Body will always be null, as it’s only possible to involve users with a process-instance.
 * 
 * @param {module:types.RestIdentityLink} identityLink identityLink
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @return {Promise<module:types.RestIdentityLink>} OK
 */
export function createProcessInstanceIdentityLinks(identityLink, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId
    },
    body: {
      identityLink
    }
  }
  return gateway.request(createProcessInstanceIdentityLinksOperation, parameters)
}

/**
 * Get a specific involved people from process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.identityId] identityId
 * @param {string} [options.type] type
 * @return {Promise<module:types.RestIdentityLink>} Indicates the process instance was found and the specified link is retrieved.
 */
export function getProcessInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      identityId: options.identityId,
      type: options.type
    }
  }
  return gateway.request(getProcessInstanceIdentityLinksOperation, parameters)
}

/**
 * Remove an involved user to from process instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.processInstanceId] processInstanceId
 * @param {string} [options.identityId] identityId
 * @param {string} [options.type] type
 * @return {Promise<object>} OK
 */
export function deleteProcessInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processInstanceId: options.processInstanceId,
      identityId: options.identityId,
      type: options.type
    }
  }
  return gateway.request(deleteProcessInstanceIdentityLinksOperation, parameters)
}

const listProcessInstanceIdentityLinksOperation = {
  path: '/runtime/process-instances/{processInstanceId}/identitylinks',
  method: 'get'
}

const createProcessInstanceIdentityLinksOperation = {
  path: '/runtime/process-instances/{processInstanceId}/identitylinks',
  contentTypes: ['application/json'],
  method: 'post'
}

const getProcessInstanceIdentityLinksOperation = {
  path: '/runtime/process-instances/{processInstanceId}/identitylinks/users/{identityId}/{type}',
  method: 'get'
}

const deleteProcessInstanceIdentityLinksOperation = {
  path: '/runtime/process-instances/{processInstanceId}/identitylinks/users/{identityId}/{type}',
  method: 'delete'
}

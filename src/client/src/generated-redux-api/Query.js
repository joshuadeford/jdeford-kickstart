/** @module Query */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of tasks (except for candidateGroupIn which is only available in this POST task query REST service), but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long. On top of that, the query allows for filtering based on task and process variables. The taskVariables and processInstanceVariables are both JSON-arrays containing objects with the format as described here.
 * 
 * @param {module:types.TaskQueryRequest} request request
 * @return {Promise<module:types.DataResponseOfTaskResponse>} Indicates request was successful and the tasks are returned.
 */
export function queryTasks(request) {
  const parameters = {
    body: {
      request
    }
  }
  return gateway.request(queryTasksOperation, parameters)
}

const queryTasksOperation = {
  path: '/query/tasks',
  contentTypes: ['application/json'],
  method: 'post'
}

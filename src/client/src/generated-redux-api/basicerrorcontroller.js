/** @module basicerrorcontroller */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * errorHtml
 */
export function errorHtmlUsingGET() {
  return gateway.request(errorHtmlUsingGETOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingHEAD() {
  return gateway.request(errorHtmlUsingHEADOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingPOST() {
  return gateway.request(errorHtmlUsingPOSTOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingPUT() {
  return gateway.request(errorHtmlUsingPUTOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingDELETE() {
  return gateway.request(errorHtmlUsingDELETEOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingOPTIONS() {
  return gateway.request(errorHtmlUsingOPTIONSOperation)
}

/**
 * errorHtml
 */
export function errorHtmlUsingPATCH() {
  return gateway.request(errorHtmlUsingPATCHOperation)
}

const errorHtmlUsingGETOperation = {
  path: '/error',
  method: 'get'
}

const errorHtmlUsingHEADOperation = {
  path: '/error',
  method: 'head'
}

const errorHtmlUsingPOSTOperation = {
  path: '/error',
  method: 'post'
}

const errorHtmlUsingPUTOperation = {
  path: '/error',
  method: 'put'
}

const errorHtmlUsingDELETEOperation = {
  path: '/error',
  method: 'delete'
}

const errorHtmlUsingOPTIONSOperation = {
  path: '/error',
  method: 'options'
}

const errorHtmlUsingPATCHOperation = {
  path: '/error',
  method: 'patch'
}

/** @module RoleEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllRole
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfRole>} OK
 */
export function findAllRoleUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllRoleUsingGETOperation, parameters)
}

/**
 * saveRole
 * 
 * @param {module:types.Role} body body
 * @return {Promise<module:types.ResourceOfRole>} OK
 */
export function saveRoleUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveRoleUsingPOSTOperation, parameters)
}

/**
 * findOneRole
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfRole>} OK
 */
export function findOneRoleUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneRoleUsingGETOperation, parameters)
}

/**
 * saveRole
 * 
 * @param {string} id id
 * @param {module:types.Role} body body
 * @return {Promise<module:types.ResourceOfRole>} OK
 */
export function saveRoleUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveRoleUsingPUTOperation, parameters)
}

/**
 * deleteRole
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteRoleUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteRoleUsingDELETEOperation, parameters)
}

/**
 * saveRole
 * 
 * @param {string} id id
 * @param {module:types.Role} body body
 * @return {Promise<module:types.ResourceOfRole>} OK
 */
export function saveRoleUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveRoleUsingPATCHOperation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourcesOfUser>} OK
 */
export function roleUsersUsingGET_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(roleUsersUsingGET_1Operation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfUser>} OK
 */
export function roleUsersUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(roleUsersUsingPOSTOperation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfUser>} OK
 */
export function roleUsersUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(roleUsersUsingPUTOperation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function roleUsersUsingDELETE_1(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(roleUsersUsingDELETE_1Operation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @param {string[]} body body
 * @return {Promise<module:types.ResourcesOfUser>} OK
 */
export function roleUsersUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(roleUsersUsingPATCHOperation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @param {string} userId userId
 * @return {Promise<module:types.ResourceOfUser>} OK
 */
export function roleUsersUsingGET(id, userId) {
  const parameters = {
    path: {
      id,
      userId
    }
  }
  return gateway.request(roleUsersUsingGETOperation, parameters)
}

/**
 * roleUsers
 * 
 * @param {string} id id
 * @param {string} userId userId
 * @return {Promise<object>} OK
 */
export function roleUsersUsingDELETE(id, userId) {
  const parameters = {
    path: {
      id,
      userId
    }
  }
  return gateway.request(roleUsersUsingDELETEOperation, parameters)
}

const findAllRoleUsingGETOperation = {
  path: '/api/roles',
  method: 'get'
}

const saveRoleUsingPOSTOperation = {
  path: '/api/roles',
  contentTypes: ['application/json'],
  method: 'post'
}

const findOneRoleUsingGETOperation = {
  path: '/api/roles/{id}',
  method: 'get'
}

const saveRoleUsingPUTOperation = {
  path: '/api/roles/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteRoleUsingDELETEOperation = {
  path: '/api/roles/{id}',
  method: 'delete'
}

const saveRoleUsingPATCHOperation = {
  path: '/api/roles/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const roleUsersUsingGET_1Operation = {
  path: '/api/roles/{id}/users',
  method: 'get'
}

const roleUsersUsingPOSTOperation = {
  path: '/api/roles/{id}/users',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const roleUsersUsingPUTOperation = {
  path: '/api/roles/{id}/users',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const roleUsersUsingDELETE_1Operation = {
  path: '/api/roles/{id}/users',
  method: 'delete'
}

const roleUsersUsingPATCHOperation = {
  path: '/api/roles/{id}/users',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

const roleUsersUsingGETOperation = {
  path: '/api/roles/{id}/users/{userId}',
  method: 'get'
}

const roleUsersUsingDELETEOperation = {
  path: '/api/roles/{id}/users/{userId}',
  method: 'delete'
}

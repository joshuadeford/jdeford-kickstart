/** @module action/ModelVariantEntity */
// Auto-generated, edits will be overwritten
import * as ModelVariantEntity from '../ModelVariantEntity'

export const FIND_ALL_MODEL_VARIANT_USING_GET_START = 's/ModelVariantEntity/FIND_ALL_MODEL_VARIANT_USING_GET_START'
export const FIND_ALL_MODEL_VARIANT_USING_GET = 's/ModelVariantEntity/FIND_ALL_MODEL_VARIANT_USING_GET'


export function findAllModelVariantUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_MODEL_VARIANT_USING_GET_START, meta: { info } })
    return ModelVariantEntity.findAllModelVariantUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_MODEL_VARIANT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_MODEL_VARIANT_USING_POST_START = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_POST_START'
export const SAVE_MODEL_VARIANT_USING_POST = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_POST'


export function saveModelVariantUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_MODEL_VARIANT_USING_POST_START, meta: { info } })
    return ModelVariantEntity.saveModelVariantUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_MODEL_VARIANT_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET_START = 's/ModelVariantEntity/FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET_START'
export const FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET = 's/ModelVariantEntity/FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET'


export function findByBrandAndModelLikeModelVariantUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET_START, meta: { info } })
    return ModelVariantEntity.findByBrandAndModelLikeModelVariantUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_BRAND_AND_MODEL_LIKE_MODEL_VARIANT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET_START = 's/ModelVariantEntity/FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET_START'
export const FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET = 's/ModelVariantEntity/FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET'


export function findByModelLikeModelVariantUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET_START, meta: { info } })
    return ModelVariantEntity.findByModelLikeModelVariantUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_MODEL_LIKE_MODEL_VARIANT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_MODEL_VARIANT_USING_GET_START = 's/ModelVariantEntity/FIND_ONE_MODEL_VARIANT_USING_GET_START'
export const FIND_ONE_MODEL_VARIANT_USING_GET = 's/ModelVariantEntity/FIND_ONE_MODEL_VARIANT_USING_GET'


export function findOneModelVariantUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_MODEL_VARIANT_USING_GET_START, meta: { info } })
    return ModelVariantEntity.findOneModelVariantUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_MODEL_VARIANT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_MODEL_VARIANT_USING_PUT_START = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_PUT_START'
export const SAVE_MODEL_VARIANT_USING_PUT = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_PUT'


export function saveModelVariantUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_MODEL_VARIANT_USING_PUT_START, meta: { info } })
    return ModelVariantEntity.saveModelVariantUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_MODEL_VARIANT_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_MODEL_VARIANT_USING_DELETE_START = 's/ModelVariantEntity/DELETE_MODEL_VARIANT_USING_DELETE_START'
export const DELETE_MODEL_VARIANT_USING_DELETE = 's/ModelVariantEntity/DELETE_MODEL_VARIANT_USING_DELETE'


export function deleteModelVariantUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_MODEL_VARIANT_USING_DELETE_START, meta: { info } })
    return ModelVariantEntity.deleteModelVariantUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_MODEL_VARIANT_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_MODEL_VARIANT_USING_PATCH_START = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_PATCH_START'
export const SAVE_MODEL_VARIANT_USING_PATCH = 's/ModelVariantEntity/SAVE_MODEL_VARIANT_USING_PATCH'


export function saveModelVariantUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_MODEL_VARIANT_USING_PATCH_START, meta: { info } })
    return ModelVariantEntity.saveModelVariantUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_MODEL_VARIANT_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

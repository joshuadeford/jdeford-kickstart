/** @module action/Forms */
// Auto-generated, edits will be overwritten
import * as Forms from '../Forms'

export const GET_FORM_DATA_USING_GET_START = 's/Forms/GET_FORM_DATA_USING_GET_START'
export const GET_FORM_DATA_USING_GET = 's/Forms/GET_FORM_DATA_USING_GET'


export function getFormDataUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_FORM_DATA_USING_GET_START, meta: { info } })
    return Forms.getFormDataUsingGET(options)
      .then(response => dispatch({
        type: GET_FORM_DATA_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SUBMIT_FORM_USING_POST_START = 's/Forms/SUBMIT_FORM_USING_POST_START'
export const SUBMIT_FORM_USING_POST = 's/Forms/SUBMIT_FORM_USING_POST'


export function submitFormUsingPOST(submitRequest, info) {
  return dispatch => {
    dispatch({ type: SUBMIT_FORM_USING_POST_START, meta: { info } })
    return Forms.submitFormUsingPOST(submitRequest)
      .then(response => dispatch({
        type: SUBMIT_FORM_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

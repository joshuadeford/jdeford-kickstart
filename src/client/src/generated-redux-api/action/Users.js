/** @module action/Users */
// Auto-generated, edits will be overwritten
import * as Users from '../Users'

export const LIST_USERS_START = 's/Users/LIST_USERS_START'
export const LIST_USERS = 's/Users/LIST_USERS'


export function listUsers(options, info) {
  return dispatch => {
    dispatch({ type: LIST_USERS_START, meta: { info } })
    return Users.listUsers(options)
      .then(response => dispatch({
        type: LIST_USERS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_USER_USING_POST_START = 's/Users/CREATE_USER_USING_POST_START'
export const CREATE_USER_USING_POST = 's/Users/CREATE_USER_USING_POST'


export function createUserUsingPOST(userRequest, info) {
  return dispatch => {
    dispatch({ type: CREATE_USER_USING_POST_START, meta: { info } })
    return Users.createUserUsingPOST(userRequest)
      .then(response => dispatch({
        type: CREATE_USER_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_USER_USING_GET_START = 's/Users/GET_USER_USING_GET_START'
export const GET_USER_USING_GET = 's/Users/GET_USER_USING_GET'


export function getUserUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_USER_USING_GET_START, meta: { info } })
    return Users.getUserUsingGET(options)
      .then(response => dispatch({
        type: GET_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_USER_USING_PUT_START = 's/Users/UPDATE_USER_USING_PUT_START'
export const UPDATE_USER_USING_PUT = 's/Users/UPDATE_USER_USING_PUT'


export function updateUserUsingPUT(userRequest, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_USER_USING_PUT_START, meta: { info } })
    return Users.updateUserUsingPUT(userRequest, options)
      .then(response => dispatch({
        type: UPDATE_USER_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_USER_USING_DELETE_1_START = 's/Users/DELETE_USER_USING_DELETE_1_START'
export const DELETE_USER_USING_DELETE_1 = 's/Users/DELETE_USER_USING_DELETE_1'


export function deleteUserUsingDELETE_1(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_USER_USING_DELETE_1_START, meta: { info } })
    return Users.deleteUserUsingDELETE_1(options)
      .then(response => dispatch({
        type: DELETE_USER_USING_DELETE_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_USER_INFO_START = 's/Users/LIST_USER_INFO_START'
export const LIST_USER_INFO = 's/Users/LIST_USER_INFO'


export function listUserInfo(options, info) {
  return dispatch => {
    dispatch({ type: LIST_USER_INFO_START, meta: { info } })
    return Users.listUserInfo(options)
      .then(response => dispatch({
        type: LIST_USER_INFO,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_USER_INFO_START = 's/Users/CREATE_USER_INFO_START'
export const CREATE_USER_INFO = 's/Users/CREATE_USER_INFO'


export function createUserInfo(userRequest, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_USER_INFO_START, meta: { info } })
    return Users.createUserInfo(userRequest, options)
      .then(response => dispatch({
        type: CREATE_USER_INFO,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_USER_INFO_USING_GET_START = 's/Users/GET_USER_INFO_USING_GET_START'
export const GET_USER_INFO_USING_GET = 's/Users/GET_USER_INFO_USING_GET'


export function getUserInfoUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_USER_INFO_USING_GET_START, meta: { info } })
    return Users.getUserInfoUsingGET(options)
      .then(response => dispatch({
        type: GET_USER_INFO_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_USER_INFO_START = 's/Users/UPDATE_USER_INFO_START'
export const UPDATE_USER_INFO = 's/Users/UPDATE_USER_INFO'


export function updateUserInfo(userRequest, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_USER_INFO_START, meta: { info } })
    return Users.updateUserInfo(userRequest, options)
      .then(response => dispatch({
        type: UPDATE_USER_INFO,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_USER_INFO_USING_DELETE_START = 's/Users/DELETE_USER_INFO_USING_DELETE_START'
export const DELETE_USER_INFO_USING_DELETE = 's/Users/DELETE_USER_INFO_USING_DELETE'


export function deleteUserInfoUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_USER_INFO_USING_DELETE_START, meta: { info } })
    return Users.deleteUserInfoUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_USER_INFO_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_USER_PICTURE_USING_GET_START = 's/Users/GET_USER_PICTURE_USING_GET_START'
export const GET_USER_PICTURE_USING_GET = 's/Users/GET_USER_PICTURE_USING_GET'


export function getUserPictureUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_USER_PICTURE_USING_GET_START, meta: { info } })
    return Users.getUserPictureUsingGET(options)
      .then(response => dispatch({
        type: GET_USER_PICTURE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_USER_PICTURE_USING_PUT_START = 's/Users/UPDATE_USER_PICTURE_USING_PUT_START'
export const UPDATE_USER_PICTURE_USING_PUT = 's/Users/UPDATE_USER_PICTURE_USING_PUT'


export function updateUserPictureUsingPUT(file, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_USER_PICTURE_USING_PUT_START, meta: { info } })
    return Users.updateUserPictureUsingPUT(file, options)
      .then(response => dispatch({
        type: UPDATE_USER_PICTURE_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/Databasetables */
// Auto-generated, edits will be overwritten
import * as Databasetables from '../Databasetables'

export const LIST_TABLES_START = 's/Databasetables/LIST_TABLES_START'
export const LIST_TABLES = 's/Databasetables/LIST_TABLES'


export function listTables(info) {
  return dispatch => {
    dispatch({ type: LIST_TABLES_START, meta: { info } })
    return Databasetables.listTables()
      .then(response => dispatch({
        type: LIST_TABLES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TABLE_USING_GET_START = 's/Databasetables/GET_TABLE_USING_GET_START'
export const GET_TABLE_USING_GET = 's/Databasetables/GET_TABLE_USING_GET'


export function getTableUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TABLE_USING_GET_START, meta: { info } })
    return Databasetables.getTableUsingGET(options)
      .then(response => dispatch({
        type: GET_TABLE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TABLE_META_DATA_USING_GET_START = 's/Databasetables/GET_TABLE_META_DATA_USING_GET_START'
export const GET_TABLE_META_DATA_USING_GET = 's/Databasetables/GET_TABLE_META_DATA_USING_GET'


export function getTableMetaDataUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TABLE_META_DATA_USING_GET_START, meta: { info } })
    return Databasetables.getTableMetaDataUsingGET(options)
      .then(response => dispatch({
        type: GET_TABLE_META_DATA_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TABLE_DATA_USING_GET_START = 's/Databasetables/GET_TABLE_DATA_USING_GET_START'
export const GET_TABLE_DATA_USING_GET = 's/Databasetables/GET_TABLE_DATA_USING_GET'


export function getTableDataUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TABLE_DATA_USING_GET_START, meta: { info } })
    return Databasetables.getTableDataUsingGET(options)
      .then(response => dispatch({
        type: GET_TABLE_DATA_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

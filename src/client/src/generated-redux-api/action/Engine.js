/** @module action/Engine */
// Auto-generated, edits will be overwritten
import * as Engine from '../Engine'

export const GET_ENGINE_INFO_USING_GET_START = 's/Engine/GET_ENGINE_INFO_USING_GET_START'
export const GET_ENGINE_INFO_USING_GET = 's/Engine/GET_ENGINE_INFO_USING_GET'


export function getEngineInfoUsingGET(info) {
  return dispatch => {
    dispatch({ type: GET_ENGINE_INFO_USING_GET_START, meta: { info } })
    return Engine.getEngineInfoUsingGET()
      .then(response => dispatch({
        type: GET_ENGINE_INFO_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROPERTIES_USING_GET_START = 's/Engine/GET_PROPERTIES_USING_GET_START'
export const GET_PROPERTIES_USING_GET = 's/Engine/GET_PROPERTIES_USING_GET'


export function getPropertiesUsingGET(info) {
  return dispatch => {
    dispatch({ type: GET_PROPERTIES_USING_GET_START, meta: { info } })
    return Engine.getPropertiesUsingGET()
      .then(response => dispatch({
        type: GET_PROPERTIES_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

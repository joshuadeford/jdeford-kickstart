/** @module action/Deployment */
// Auto-generated, edits will be overwritten
import * as Deployment from '../Deployment'

export const LIST_DEPLOYMENTS_START = 's/Deployment/LIST_DEPLOYMENTS_START'
export const LIST_DEPLOYMENTS = 's/Deployment/LIST_DEPLOYMENTS'


export function listDeployments(options, info) {
  return dispatch => {
    dispatch({ type: LIST_DEPLOYMENTS_START, meta: { info } })
    return Deployment.listDeployments(options)
      .then(response => dispatch({
        type: LIST_DEPLOYMENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPLOAD_DEPLOYMENT_USING_POST_START = 's/Deployment/UPLOAD_DEPLOYMENT_USING_POST_START'
export const UPLOAD_DEPLOYMENT_USING_POST = 's/Deployment/UPLOAD_DEPLOYMENT_USING_POST'


export function uploadDeploymentUsingPOST(file, options, info) {
  return dispatch => {
    dispatch({ type: UPLOAD_DEPLOYMENT_USING_POST_START, meta: { info } })
    return Deployment.uploadDeploymentUsingPOST(file, options)
      .then(response => dispatch({
        type: UPLOAD_DEPLOYMENT_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_DEPLOYMENT_USING_GET_START = 's/Deployment/GET_DEPLOYMENT_USING_GET_START'
export const GET_DEPLOYMENT_USING_GET = 's/Deployment/GET_DEPLOYMENT_USING_GET'


export function getDeploymentUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_DEPLOYMENT_USING_GET_START, meta: { info } })
    return Deployment.getDeploymentUsingGET(options)
      .then(response => dispatch({
        type: GET_DEPLOYMENT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_DEPLOYMENT_USING_DELETE_START = 's/Deployment/DELETE_DEPLOYMENT_USING_DELETE_START'
export const DELETE_DEPLOYMENT_USING_DELETE = 's/Deployment/DELETE_DEPLOYMENT_USING_DELETE'


export function deleteDeploymentUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_DEPLOYMENT_USING_DELETE_START, meta: { info } })
    return Deployment.deleteDeploymentUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_DEPLOYMENT_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_DEPLOYMENT_RESOURCE_DATA_START = 's/Deployment/GET_DEPLOYMENT_RESOURCE_DATA_START'
export const GET_DEPLOYMENT_RESOURCE_DATA = 's/Deployment/GET_DEPLOYMENT_RESOURCE_DATA'


export function getDeploymentResourceData(options, info) {
  return dispatch => {
    dispatch({ type: GET_DEPLOYMENT_RESOURCE_DATA_START, meta: { info } })
    return Deployment.getDeploymentResourceData(options)
      .then(response => dispatch({
        type: GET_DEPLOYMENT_RESOURCE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_DEPLOYMENT_RESOURCES_START = 's/Deployment/LIST_DEPLOYMENT_RESOURCES_START'
export const LIST_DEPLOYMENT_RESOURCES = 's/Deployment/LIST_DEPLOYMENT_RESOURCES'


export function listDeploymentResources(options, info) {
  return dispatch => {
    dispatch({ type: LIST_DEPLOYMENT_RESOURCES_START, meta: { info } })
    return Deployment.listDeploymentResources(options)
      .then(response => dispatch({
        type: LIST_DEPLOYMENT_RESOURCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_DEPLOYMENT_RESOURCE_USING_GET_START = 's/Deployment/GET_DEPLOYMENT_RESOURCE_USING_GET_START'
export const GET_DEPLOYMENT_RESOURCE_USING_GET = 's/Deployment/GET_DEPLOYMENT_RESOURCE_USING_GET'


export function getDeploymentResourceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_DEPLOYMENT_RESOURCE_USING_GET_START, meta: { info } })
    return Deployment.getDeploymentResourceUsingGET(options)
      .then(response => dispatch({
        type: GET_DEPLOYMENT_RESOURCE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

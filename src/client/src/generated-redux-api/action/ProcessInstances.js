/** @module action/ProcessInstances */
// Auto-generated, edits will be overwritten
import * as ProcessInstances from '../ProcessInstances'

export const QUERY_PROCESS_INSTANCES_USING_POST_START = 's/ProcessInstances/QUERY_PROCESS_INSTANCES_USING_POST_START'
export const QUERY_PROCESS_INSTANCES_USING_POST = 's/ProcessInstances/QUERY_PROCESS_INSTANCES_USING_POST'


export function queryProcessInstancesUsingPOST(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_PROCESS_INSTANCES_USING_POST_START, meta: { info } })
    return ProcessInstances.queryProcessInstancesUsingPOST(queryRequest)
      .then(response => dispatch({
        type: QUERY_PROCESS_INSTANCES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_PROCESS_INSTANCES_START = 's/ProcessInstances/LIST_PROCESS_INSTANCES_START'
export const LIST_PROCESS_INSTANCES = 's/ProcessInstances/LIST_PROCESS_INSTANCES'


export function listProcessInstances(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_INSTANCES_START, meta: { info } })
    return ProcessInstances.listProcessInstances(options)
      .then(response => dispatch({
        type: LIST_PROCESS_INSTANCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_PROCESS_INSTANCE_USING_POST_START = 's/ProcessInstances/CREATE_PROCESS_INSTANCE_USING_POST_START'
export const CREATE_PROCESS_INSTANCE_USING_POST = 's/ProcessInstances/CREATE_PROCESS_INSTANCE_USING_POST'


export function createProcessInstanceUsingPOST(request, info) {
  return dispatch => {
    dispatch({ type: CREATE_PROCESS_INSTANCE_USING_POST_START, meta: { info } })
    return ProcessInstances.createProcessInstanceUsingPOST(request)
      .then(response => dispatch({
        type: CREATE_PROCESS_INSTANCE_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_INSTANCE_START = 's/ProcessInstances/GET_PROCESS_INSTANCE_START'
export const GET_PROCESS_INSTANCE = 's/ProcessInstances/GET_PROCESS_INSTANCE'


export function getProcessInstance(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_INSTANCE_START, meta: { info } })
    return ProcessInstances.getProcessInstance(options)
      .then(response => dispatch({
        type: GET_PROCESS_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT_START = 's/ProcessInstances/PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT_START'
export const PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT = 's/ProcessInstances/PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT'


export function performProcessInstanceActionUsingPUT(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT_START, meta: { info } })
    return ProcessInstances.performProcessInstanceActionUsingPUT(actionRequest, options)
      .then(response => dispatch({
        type: PERFORM_PROCESS_INSTANCE_ACTION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_PROCESS_INSTANCE_START = 's/ProcessInstances/DELETE_PROCESS_INSTANCE_START'
export const DELETE_PROCESS_INSTANCE = 's/ProcessInstances/DELETE_PROCESS_INSTANCE'


export function deleteProcessInstance(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_PROCESS_INSTANCE_START, meta: { info } })
    return ProcessInstances.deleteProcessInstance(options)
      .then(response => dispatch({
        type: DELETE_PROCESS_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CHANGE_ACTIVITY_STATE_USING_POST_1_START = 's/ProcessInstances/CHANGE_ACTIVITY_STATE_USING_POST_1_START'
export const CHANGE_ACTIVITY_STATE_USING_POST_1 = 's/ProcessInstances/CHANGE_ACTIVITY_STATE_USING_POST_1'


export function changeActivityStateUsingPOST_1(activityStateRequest, options, info) {
  return dispatch => {
    dispatch({ type: CHANGE_ACTIVITY_STATE_USING_POST_1_START, meta: { info } })
    return ProcessInstances.changeActivityStateUsingPOST_1(activityStateRequest, options)
      .then(response => dispatch({
        type: CHANGE_ACTIVITY_STATE_USING_POST_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_INSTANCE_DIAGRAM_USING_GET_START = 's/ProcessInstances/GET_PROCESS_INSTANCE_DIAGRAM_USING_GET_START'
export const GET_PROCESS_INSTANCE_DIAGRAM_USING_GET = 's/ProcessInstances/GET_PROCESS_INSTANCE_DIAGRAM_USING_GET'


export function getProcessInstanceDiagramUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_INSTANCE_DIAGRAM_USING_GET_START, meta: { info } })
    return ProcessInstances.getProcessInstanceDiagramUsingGET(options)
      .then(response => dispatch({
        type: GET_PROCESS_INSTANCE_DIAGRAM_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

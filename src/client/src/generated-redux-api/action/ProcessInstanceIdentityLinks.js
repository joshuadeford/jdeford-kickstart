/** @module action/ProcessInstanceIdentityLinks */
// Auto-generated, edits will be overwritten
import * as ProcessInstanceIdentityLinks from '../ProcessInstanceIdentityLinks'

export const LIST_PROCESS_INSTANCE_IDENTITY_LINKS_START = 's/ProcessInstanceIdentityLinks/LIST_PROCESS_INSTANCE_IDENTITY_LINKS_START'
export const LIST_PROCESS_INSTANCE_IDENTITY_LINKS = 's/ProcessInstanceIdentityLinks/LIST_PROCESS_INSTANCE_IDENTITY_LINKS'


export function listProcessInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return ProcessInstanceIdentityLinks.listProcessInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: LIST_PROCESS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_PROCESS_INSTANCE_IDENTITY_LINKS_START = 's/ProcessInstanceIdentityLinks/CREATE_PROCESS_INSTANCE_IDENTITY_LINKS_START'
export const CREATE_PROCESS_INSTANCE_IDENTITY_LINKS = 's/ProcessInstanceIdentityLinks/CREATE_PROCESS_INSTANCE_IDENTITY_LINKS'


export function createProcessInstanceIdentityLinks(identityLink, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_PROCESS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return ProcessInstanceIdentityLinks.createProcessInstanceIdentityLinks(identityLink, options)
      .then(response => dispatch({
        type: CREATE_PROCESS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_INSTANCE_IDENTITY_LINKS_START = 's/ProcessInstanceIdentityLinks/GET_PROCESS_INSTANCE_IDENTITY_LINKS_START'
export const GET_PROCESS_INSTANCE_IDENTITY_LINKS = 's/ProcessInstanceIdentityLinks/GET_PROCESS_INSTANCE_IDENTITY_LINKS'


export function getProcessInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return ProcessInstanceIdentityLinks.getProcessInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: GET_PROCESS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_PROCESS_INSTANCE_IDENTITY_LINKS_START = 's/ProcessInstanceIdentityLinks/DELETE_PROCESS_INSTANCE_IDENTITY_LINKS_START'
export const DELETE_PROCESS_INSTANCE_IDENTITY_LINKS = 's/ProcessInstanceIdentityLinks/DELETE_PROCESS_INSTANCE_IDENTITY_LINKS'


export function deleteProcessInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_PROCESS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return ProcessInstanceIdentityLinks.deleteProcessInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: DELETE_PROCESS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

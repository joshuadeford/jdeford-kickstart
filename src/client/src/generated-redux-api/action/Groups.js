/** @module action/Groups */
// Auto-generated, edits will be overwritten
import * as Groups from '../Groups'

export const LIST_GROUPS_START = 's/Groups/LIST_GROUPS_START'
export const LIST_GROUPS = 's/Groups/LIST_GROUPS'


export function listGroups(options, info) {
  return dispatch => {
    dispatch({ type: LIST_GROUPS_START, meta: { info } })
    return Groups.listGroups(options)
      .then(response => dispatch({
        type: LIST_GROUPS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_GROUP_USING_POST_START = 's/Groups/CREATE_GROUP_USING_POST_START'
export const CREATE_GROUP_USING_POST = 's/Groups/CREATE_GROUP_USING_POST'


export function createGroupUsingPOST(groupRequest, info) {
  return dispatch => {
    dispatch({ type: CREATE_GROUP_USING_POST_START, meta: { info } })
    return Groups.createGroupUsingPOST(groupRequest)
      .then(response => dispatch({
        type: CREATE_GROUP_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_GROUP_USING_GET_START = 's/Groups/GET_GROUP_USING_GET_START'
export const GET_GROUP_USING_GET = 's/Groups/GET_GROUP_USING_GET'


export function getGroupUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_GROUP_USING_GET_START, meta: { info } })
    return Groups.getGroupUsingGET(options)
      .then(response => dispatch({
        type: GET_GROUP_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_GROUP_USING_PUT_START = 's/Groups/UPDATE_GROUP_USING_PUT_START'
export const UPDATE_GROUP_USING_PUT = 's/Groups/UPDATE_GROUP_USING_PUT'


export function updateGroupUsingPUT(groupRequest, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_GROUP_USING_PUT_START, meta: { info } })
    return Groups.updateGroupUsingPUT(groupRequest, options)
      .then(response => dispatch({
        type: UPDATE_GROUP_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_GROUP_USING_DELETE_START = 's/Groups/DELETE_GROUP_USING_DELETE_START'
export const DELETE_GROUP_USING_DELETE = 's/Groups/DELETE_GROUP_USING_DELETE'


export function deleteGroupUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_GROUP_USING_DELETE_START, meta: { info } })
    return Groups.deleteGroupUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_GROUP_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_MEMBERSHIP_USING_POST_START = 's/Groups/CREATE_MEMBERSHIP_USING_POST_START'
export const CREATE_MEMBERSHIP_USING_POST = 's/Groups/CREATE_MEMBERSHIP_USING_POST'


export function createMembershipUsingPOST(memberShip, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_MEMBERSHIP_USING_POST_START, meta: { info } })
    return Groups.createMembershipUsingPOST(memberShip, options)
      .then(response => dispatch({
        type: CREATE_MEMBERSHIP_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_MEMBERSHIP_USING_DELETE_START = 's/Groups/DELETE_MEMBERSHIP_USING_DELETE_START'
export const DELETE_MEMBERSHIP_USING_DELETE = 's/Groups/DELETE_MEMBERSHIP_USING_DELETE'


export function deleteMembershipUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_MEMBERSHIP_USING_DELETE_START, meta: { info } })
    return Groups.deleteMembershipUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_MEMBERSHIP_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

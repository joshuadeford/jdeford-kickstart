/** @module action/TaskAttachments */
// Auto-generated, edits will be overwritten
import * as TaskAttachments from '../TaskAttachments'

export const LIST_TASK_ATTACHMENTS_START = 's/TaskAttachments/LIST_TASK_ATTACHMENTS_START'
export const LIST_TASK_ATTACHMENTS = 's/TaskAttachments/LIST_TASK_ATTACHMENTS'


export function listTaskAttachments(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASK_ATTACHMENTS_START, meta: { info } })
    return TaskAttachments.listTaskAttachments(options)
      .then(response => dispatch({
        type: LIST_TASK_ATTACHMENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_ATTACHMENT_USING_POST_START = 's/TaskAttachments/CREATE_ATTACHMENT_USING_POST_START'
export const CREATE_ATTACHMENT_USING_POST = 's/TaskAttachments/CREATE_ATTACHMENT_USING_POST'


export function createAttachmentUsingPOST(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_ATTACHMENT_USING_POST_START, meta: { info } })
    return TaskAttachments.createAttachmentUsingPOST(options)
      .then(response => dispatch({
        type: CREATE_ATTACHMENT_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_ATTACHMENT_USING_GET_START = 's/TaskAttachments/GET_ATTACHMENT_USING_GET_START'
export const GET_ATTACHMENT_USING_GET = 's/TaskAttachments/GET_ATTACHMENT_USING_GET'


export function getAttachmentUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_ATTACHMENT_USING_GET_START, meta: { info } })
    return TaskAttachments.getAttachmentUsingGET(options)
      .then(response => dispatch({
        type: GET_ATTACHMENT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_ATTACHMENT_USING_DELETE_START = 's/TaskAttachments/DELETE_ATTACHMENT_USING_DELETE_START'
export const DELETE_ATTACHMENT_USING_DELETE = 's/TaskAttachments/DELETE_ATTACHMENT_USING_DELETE'


export function deleteAttachmentUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_ATTACHMENT_USING_DELETE_START, meta: { info } })
    return TaskAttachments.deleteAttachmentUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_ATTACHMENT_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_ATTACHMENT_CONTENT_USING_GET_START = 's/TaskAttachments/GET_ATTACHMENT_CONTENT_USING_GET_START'
export const GET_ATTACHMENT_CONTENT_USING_GET = 's/TaskAttachments/GET_ATTACHMENT_CONTENT_USING_GET'


export function getAttachmentContentUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_ATTACHMENT_CONTENT_USING_GET_START, meta: { info } })
    return TaskAttachments.getAttachmentContentUsingGET(options)
      .then(response => dispatch({
        type: GET_ATTACHMENT_CONTENT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/History */
// Auto-generated, edits will be overwritten
import * as History from '../History'

export const LIST_HISTORIC_ACTIVITY_INSTANCES_START = 's/History/LIST_HISTORIC_ACTIVITY_INSTANCES_START'
export const LIST_HISTORIC_ACTIVITY_INSTANCES = 's/History/LIST_HISTORIC_ACTIVITY_INSTANCES'


export function listHistoricActivityInstances(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_ACTIVITY_INSTANCES_START, meta: { info } })
    return History.listHistoricActivityInstances(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_ACTIVITY_INSTANCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_HISTORIC_DETAILS_START = 's/History/LIST_HISTORIC_DETAILS_START'
export const LIST_HISTORIC_DETAILS = 's/History/LIST_HISTORIC_DETAILS'


export function listHistoricDetails(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_DETAILS_START, meta: { info } })
    return History.listHistoricDetails(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_DETAILS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_HISTORIC_DETAIL_VARIABLE_DATA_START = 's/History/GET_HISTORIC_DETAIL_VARIABLE_DATA_START'
export const GET_HISTORIC_DETAIL_VARIABLE_DATA = 's/History/GET_HISTORIC_DETAIL_VARIABLE_DATA'


export function getHistoricDetailVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_HISTORIC_DETAIL_VARIABLE_DATA_START, meta: { info } })
    return History.getHistoricDetailVariableData(options)
      .then(response => dispatch({
        type: GET_HISTORIC_DETAIL_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA_START = 's/History/GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA_START'
export const GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA = 's/History/GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA'


export function getHistoricTaskInstanceVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA_START, meta: { info } })
    return History.getHistoricTaskInstanceVariableData(options)
      .then(response => dispatch({
        type: GET_HISTORIC_TASK_INSTANCE_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_HISTORIC_VARIABLE_INSTANCES_START = 's/History/LIST_HISTORIC_VARIABLE_INSTANCES_START'
export const LIST_HISTORIC_VARIABLE_INSTANCES = 's/History/LIST_HISTORIC_VARIABLE_INSTANCES'


export function listHistoricVariableInstances(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_VARIABLE_INSTANCES_START, meta: { info } })
    return History.listHistoricVariableInstances(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_VARIABLE_INSTANCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_HISTORIC_INSTANCE_VARIABLE_DATA_START = 's/History/GET_HISTORIC_INSTANCE_VARIABLE_DATA_START'
export const GET_HISTORIC_INSTANCE_VARIABLE_DATA = 's/History/GET_HISTORIC_INSTANCE_VARIABLE_DATA'


export function getHistoricInstanceVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_HISTORIC_INSTANCE_VARIABLE_DATA_START, meta: { info } })
    return History.getHistoricInstanceVariableData(options)
      .then(response => dispatch({
        type: GET_HISTORIC_INSTANCE_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const QUERY_ACTIVITY_INSTANCES_USING_POST_START = 's/History/QUERY_ACTIVITY_INSTANCES_USING_POST_START'
export const QUERY_ACTIVITY_INSTANCES_USING_POST = 's/History/QUERY_ACTIVITY_INSTANCES_USING_POST'


export function queryActivityInstancesUsingPOST(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_ACTIVITY_INSTANCES_USING_POST_START, meta: { info } })
    return History.queryActivityInstancesUsingPOST(queryRequest)
      .then(response => dispatch({
        type: QUERY_ACTIVITY_INSTANCES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const QUERY_HISTORIC_DETAIL_USING_POST_START = 's/History/QUERY_HISTORIC_DETAIL_USING_POST_START'
export const QUERY_HISTORIC_DETAIL_USING_POST = 's/History/QUERY_HISTORIC_DETAIL_USING_POST'


export function queryHistoricDetailUsingPOST(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_HISTORIC_DETAIL_USING_POST_START, meta: { info } })
    return History.queryHistoricDetailUsingPOST(queryRequest)
      .then(response => dispatch({
        type: QUERY_HISTORIC_DETAIL_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const QUERY_VARIABLE_INSTANCES_USING_POST_START = 's/History/QUERY_VARIABLE_INSTANCES_USING_POST_START'
export const QUERY_VARIABLE_INSTANCES_USING_POST = 's/History/QUERY_VARIABLE_INSTANCES_USING_POST'


export function queryVariableInstancesUsingPOST(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_VARIABLE_INSTANCES_USING_POST_START, meta: { info } })
    return History.queryVariableInstancesUsingPOST(queryRequest)
      .then(response => dispatch({
        type: QUERY_VARIABLE_INSTANCES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

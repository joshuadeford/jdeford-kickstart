/** @module action/TaskVariables */
// Auto-generated, edits will be overwritten
import * as TaskVariables from '../TaskVariables'

export const LIST_TASK_VARIABLES_START = 's/TaskVariables/LIST_TASK_VARIABLES_START'
export const LIST_TASK_VARIABLES = 's/TaskVariables/LIST_TASK_VARIABLES'


export function listTaskVariables(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASK_VARIABLES_START, meta: { info } })
    return TaskVariables.listTaskVariables(options)
      .then(response => dispatch({
        type: LIST_TASK_VARIABLES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_TASK_VARIABLE_USING_POST_START = 's/TaskVariables/CREATE_TASK_VARIABLE_USING_POST_START'
export const CREATE_TASK_VARIABLE_USING_POST = 's/TaskVariables/CREATE_TASK_VARIABLE_USING_POST'


export function createTaskVariableUsingPOST(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_TASK_VARIABLE_USING_POST_START, meta: { info } })
    return TaskVariables.createTaskVariableUsingPOST(options)
      .then(response => dispatch({
        type: CREATE_TASK_VARIABLE_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_INSTANCE_VARIABLE_START = 's/TaskVariables/GET_TASK_INSTANCE_VARIABLE_START'
export const GET_TASK_INSTANCE_VARIABLE = 's/TaskVariables/GET_TASK_INSTANCE_VARIABLE'


export function getTaskInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_INSTANCE_VARIABLE_START, meta: { info } })
    return TaskVariables.getTaskInstanceVariable(options)
      .then(response => dispatch({
        type: GET_TASK_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_TASK_INSTANCE_VARIABLE_START = 's/TaskVariables/UPDATE_TASK_INSTANCE_VARIABLE_START'
export const UPDATE_TASK_INSTANCE_VARIABLE = 's/TaskVariables/UPDATE_TASK_INSTANCE_VARIABLE'


export function updateTaskInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_TASK_INSTANCE_VARIABLE_START, meta: { info } })
    return TaskVariables.updateTaskInstanceVariable(options)
      .then(response => dispatch({
        type: UPDATE_TASK_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TASK_INSTANCE_VARIABLE_START = 's/TaskVariables/DELETE_TASK_INSTANCE_VARIABLE_START'
export const DELETE_TASK_INSTANCE_VARIABLE = 's/TaskVariables/DELETE_TASK_INSTANCE_VARIABLE'


export function deleteTaskInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TASK_INSTANCE_VARIABLE_START, meta: { info } })
    return TaskVariables.deleteTaskInstanceVariable(options)
      .then(response => dispatch({
        type: DELETE_TASK_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_VARIABLE_DATA_START = 's/TaskVariables/GET_TASK_VARIABLE_DATA_START'
export const GET_TASK_VARIABLE_DATA = 's/TaskVariables/GET_TASK_VARIABLE_DATA'


export function getTaskVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_VARIABLE_DATA_START, meta: { info } })
    return TaskVariables.getTaskVariableData(options)
      .then(response => dispatch({
        type: GET_TASK_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/ProcessInstanceVariables */
// Auto-generated, edits will be overwritten
import * as ProcessInstanceVariables from '../ProcessInstanceVariables'

export const LIST_PROCESS_INSTANCE_VARIABLES_START = 's/ProcessInstanceVariables/LIST_PROCESS_INSTANCE_VARIABLES_START'
export const LIST_PROCESS_INSTANCE_VARIABLES = 's/ProcessInstanceVariables/LIST_PROCESS_INSTANCE_VARIABLES'


export function listProcessInstanceVariables(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_INSTANCE_VARIABLES_START, meta: { info } })
    return ProcessInstanceVariables.listProcessInstanceVariables(options)
      .then(response => dispatch({
        type: LIST_PROCESS_INSTANCE_VARIABLES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_PROCESS_INSTANCE_VARIABLE_START = 's/ProcessInstanceVariables/CREATE_PROCESS_INSTANCE_VARIABLE_START'
export const CREATE_PROCESS_INSTANCE_VARIABLE = 's/ProcessInstanceVariables/CREATE_PROCESS_INSTANCE_VARIABLE'


export function createProcessInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_PROCESS_INSTANCE_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.createProcessInstanceVariable(options)
      .then(response => dispatch({
        type: CREATE_PROCESS_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_OR_UPDATE_PROCESS_VARIABLE_START = 's/ProcessInstanceVariables/CREATE_OR_UPDATE_PROCESS_VARIABLE_START'
export const CREATE_OR_UPDATE_PROCESS_VARIABLE = 's/ProcessInstanceVariables/CREATE_OR_UPDATE_PROCESS_VARIABLE'


export function createOrUpdateProcessVariable(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_OR_UPDATE_PROCESS_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.createOrUpdateProcessVariable(options)
      .then(response => dispatch({
        type: CREATE_OR_UPDATE_PROCESS_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_LOCAL_PROCESS_VARIABLE_START = 's/ProcessInstanceVariables/DELETE_LOCAL_PROCESS_VARIABLE_START'
export const DELETE_LOCAL_PROCESS_VARIABLE = 's/ProcessInstanceVariables/DELETE_LOCAL_PROCESS_VARIABLE'


export function deleteLocalProcessVariable(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_LOCAL_PROCESS_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.deleteLocalProcessVariable(options)
      .then(response => dispatch({
        type: DELETE_LOCAL_PROCESS_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_INSTANCE_VARIABLE_START = 's/ProcessInstanceVariables/GET_PROCESS_INSTANCE_VARIABLE_START'
export const GET_PROCESS_INSTANCE_VARIABLE = 's/ProcessInstanceVariables/GET_PROCESS_INSTANCE_VARIABLE'


export function getProcessInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_INSTANCE_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.getProcessInstanceVariable(options)
      .then(response => dispatch({
        type: GET_PROCESS_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_PROCESS_INSTANCE_VARIABLE_START = 's/ProcessInstanceVariables/UPDATE_PROCESS_INSTANCE_VARIABLE_START'
export const UPDATE_PROCESS_INSTANCE_VARIABLE = 's/ProcessInstanceVariables/UPDATE_PROCESS_INSTANCE_VARIABLE'


export function updateProcessInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_PROCESS_INSTANCE_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.updateProcessInstanceVariable(options)
      .then(response => dispatch({
        type: UPDATE_PROCESS_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_PROCESS_INSTANCE_VARIABLE_START = 's/ProcessInstanceVariables/DELETE_PROCESS_INSTANCE_VARIABLE_START'
export const DELETE_PROCESS_INSTANCE_VARIABLE = 's/ProcessInstanceVariables/DELETE_PROCESS_INSTANCE_VARIABLE'


export function deleteProcessInstanceVariable(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_PROCESS_INSTANCE_VARIABLE_START, meta: { info } })
    return ProcessInstanceVariables.deleteProcessInstanceVariable(options)
      .then(response => dispatch({
        type: DELETE_PROCESS_INSTANCE_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_INSTANCE_VARIABLE_DATA_START = 's/ProcessInstanceVariables/GET_PROCESS_INSTANCE_VARIABLE_DATA_START'
export const GET_PROCESS_INSTANCE_VARIABLE_DATA = 's/ProcessInstanceVariables/GET_PROCESS_INSTANCE_VARIABLE_DATA'


export function getProcessInstanceVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_INSTANCE_VARIABLE_DATA_START, meta: { info } })
    return ProcessInstanceVariables.getProcessInstanceVariableData(options)
      .then(response => dispatch({
        type: GET_PROCESS_INSTANCE_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

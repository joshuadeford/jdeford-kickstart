/** @module action/Models */
// Auto-generated, edits will be overwritten
import * as Models from '../Models'

export const LIST_MODELS_START = 's/Models/LIST_MODELS_START'
export const LIST_MODELS = 's/Models/LIST_MODELS'


export function listModels(options, info) {
  return dispatch => {
    dispatch({ type: LIST_MODELS_START, meta: { info } })
    return Models.listModels(options)
      .then(response => dispatch({
        type: LIST_MODELS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_MODEL_USING_POST_START = 's/Models/CREATE_MODEL_USING_POST_START'
export const CREATE_MODEL_USING_POST = 's/Models/CREATE_MODEL_USING_POST'


export function createModelUsingPOST(modelRequest, info) {
  return dispatch => {
    dispatch({ type: CREATE_MODEL_USING_POST_START, meta: { info } })
    return Models.createModelUsingPOST(modelRequest)
      .then(response => dispatch({
        type: CREATE_MODEL_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_MODEL_USING_GET_START = 's/Models/GET_MODEL_USING_GET_START'
export const GET_MODEL_USING_GET = 's/Models/GET_MODEL_USING_GET'


export function getModelUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_MODEL_USING_GET_START, meta: { info } })
    return Models.getModelUsingGET(options)
      .then(response => dispatch({
        type: GET_MODEL_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_MODEL_USING_PUT_START = 's/Models/UPDATE_MODEL_USING_PUT_START'
export const UPDATE_MODEL_USING_PUT = 's/Models/UPDATE_MODEL_USING_PUT'


export function updateModelUsingPUT(modelRequest, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_MODEL_USING_PUT_START, meta: { info } })
    return Models.updateModelUsingPUT(modelRequest, options)
      .then(response => dispatch({
        type: UPDATE_MODEL_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_MODEL_USING_DELETE_START = 's/Models/DELETE_MODEL_USING_DELETE_START'
export const DELETE_MODEL_USING_DELETE = 's/Models/DELETE_MODEL_USING_DELETE'


export function deleteModelUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_MODEL_USING_DELETE_START, meta: { info } })
    return Models.deleteModelUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_MODEL_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_MODEL_BYTES_USING_GET_START = 's/Models/GET_MODEL_BYTES_USING_GET_START'
export const GET_MODEL_BYTES_USING_GET = 's/Models/GET_MODEL_BYTES_USING_GET'


export function getModelBytesUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_MODEL_BYTES_USING_GET_START, meta: { info } })
    return Models.getModelBytesUsingGET(options)
      .then(response => dispatch({
        type: GET_MODEL_BYTES_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SET_MODEL_SOURCE_USING_PUT_START = 's/Models/SET_MODEL_SOURCE_USING_PUT_START'
export const SET_MODEL_SOURCE_USING_PUT = 's/Models/SET_MODEL_SOURCE_USING_PUT'


export function setModelSourceUsingPUT(file, options, info) {
  return dispatch => {
    dispatch({ type: SET_MODEL_SOURCE_USING_PUT_START, meta: { info } })
    return Models.setModelSourceUsingPUT(file, options)
      .then(response => dispatch({
        type: SET_MODEL_SOURCE_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EXTRA_EDITOR_SOURCE_START = 's/Models/GET_EXTRA_EDITOR_SOURCE_START'
export const GET_EXTRA_EDITOR_SOURCE = 's/Models/GET_EXTRA_EDITOR_SOURCE'


export function getExtraEditorSource(options, info) {
  return dispatch => {
    dispatch({ type: GET_EXTRA_EDITOR_SOURCE_START, meta: { info } })
    return Models.getExtraEditorSource(options)
      .then(response => dispatch({
        type: GET_EXTRA_EDITOR_SOURCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SET_EXTRA_EDITOR_SOURCE_START = 's/Models/SET_EXTRA_EDITOR_SOURCE_START'
export const SET_EXTRA_EDITOR_SOURCE = 's/Models/SET_EXTRA_EDITOR_SOURCE'


export function setExtraEditorSource(file, options, info) {
  return dispatch => {
    dispatch({ type: SET_EXTRA_EDITOR_SOURCE_START, meta: { info } })
    return Models.setExtraEditorSource(file, options)
      .then(response => dispatch({
        type: SET_EXTRA_EDITOR_SOURCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

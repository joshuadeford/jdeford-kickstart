/** @module action/UserEntity */
// Auto-generated, edits will be overwritten
import * as UserEntity from '../UserEntity'

export const FIND_ALL_USER_USING_GET_START = 's/UserEntity/FIND_ALL_USER_USING_GET_START'
export const FIND_ALL_USER_USING_GET = 's/UserEntity/FIND_ALL_USER_USING_GET'


export function findAllUserUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_USER_USING_GET_START, meta: { info } })
    return UserEntity.findAllUserUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_USER_USING_POST_START = 's/UserEntity/SAVE_USER_USING_POST_START'
export const SAVE_USER_USING_POST = 's/UserEntity/SAVE_USER_USING_POST'


export function saveUserUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_USER_USING_POST_START, meta: { info } })
    return UserEntity.saveUserUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_USER_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_EMAIL_USER_USING_GET_START = 's/UserEntity/FIND_BY_EMAIL_USER_USING_GET_START'
export const FIND_BY_EMAIL_USER_USING_GET = 's/UserEntity/FIND_BY_EMAIL_USER_USING_GET'


export function findByEmailUserUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_EMAIL_USER_USING_GET_START, meta: { info } })
    return UserEntity.findByEmailUserUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_EMAIL_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_ROLES_USER_USING_GET_START = 's/UserEntity/FIND_BY_ROLES_USER_USING_GET_START'
export const FIND_BY_ROLES_USER_USING_GET = 's/UserEntity/FIND_BY_ROLES_USER_USING_GET'


export function findByRolesUserUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_ROLES_USER_USING_GET_START, meta: { info } })
    return UserEntity.findByRolesUserUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_ROLES_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_USER_NAME_USER_USING_GET_START = 's/UserEntity/FIND_BY_USER_NAME_USER_USING_GET_START'
export const FIND_BY_USER_NAME_USER_USING_GET = 's/UserEntity/FIND_BY_USER_NAME_USER_USING_GET'


export function findByUserNameUserUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_USER_NAME_USER_USING_GET_START, meta: { info } })
    return UserEntity.findByUserNameUserUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_USER_NAME_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_USER_USING_GET_START = 's/UserEntity/FIND_ONE_USER_USING_GET_START'
export const FIND_ONE_USER_USING_GET = 's/UserEntity/FIND_ONE_USER_USING_GET'


export function findOneUserUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_USER_USING_GET_START, meta: { info } })
    return UserEntity.findOneUserUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_USER_USING_PUT_START = 's/UserEntity/SAVE_USER_USING_PUT_START'
export const SAVE_USER_USING_PUT = 's/UserEntity/SAVE_USER_USING_PUT'


export function saveUserUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_USER_USING_PUT_START, meta: { info } })
    return UserEntity.saveUserUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_USER_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_USER_USING_DELETE_START = 's/UserEntity/DELETE_USER_USING_DELETE_START'
export const DELETE_USER_USING_DELETE = 's/UserEntity/DELETE_USER_USING_DELETE'


export function deleteUserUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_USER_USING_DELETE_START, meta: { info } })
    return UserEntity.deleteUserUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_USER_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_USER_USING_PATCH_START = 's/UserEntity/SAVE_USER_USING_PATCH_START'
export const SAVE_USER_USING_PATCH = 's/UserEntity/SAVE_USER_USING_PATCH'


export function saveUserUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_USER_USING_PATCH_START, meta: { info } })
    return UserEntity.saveUserUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_USER_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_GET_1_START = 's/UserEntity/USER_DEVICES_USING_GET_1_START'
export const USER_DEVICES_USING_GET_1 = 's/UserEntity/USER_DEVICES_USING_GET_1'


export function userDevicesUsingGET_1(id, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_GET_1_START, meta: { info } })
    return UserEntity.userDevicesUsingGET_1(id)
      .then(response => dispatch({
        type: USER_DEVICES_USING_GET_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_POST_START = 's/UserEntity/USER_DEVICES_USING_POST_START'
export const USER_DEVICES_USING_POST = 's/UserEntity/USER_DEVICES_USING_POST'


export function userDevicesUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_POST_START, meta: { info } })
    return UserEntity.userDevicesUsingPOST(id, body)
      .then(response => dispatch({
        type: USER_DEVICES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_PUT_START = 's/UserEntity/USER_DEVICES_USING_PUT_START'
export const USER_DEVICES_USING_PUT = 's/UserEntity/USER_DEVICES_USING_PUT'


export function userDevicesUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_PUT_START, meta: { info } })
    return UserEntity.userDevicesUsingPUT(id, body)
      .then(response => dispatch({
        type: USER_DEVICES_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_DELETE_1_START = 's/UserEntity/USER_DEVICES_USING_DELETE_1_START'
export const USER_DEVICES_USING_DELETE_1 = 's/UserEntity/USER_DEVICES_USING_DELETE_1'


export function userDevicesUsingDELETE_1(id, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_DELETE_1_START, meta: { info } })
    return UserEntity.userDevicesUsingDELETE_1(id)
      .then(response => dispatch({
        type: USER_DEVICES_USING_DELETE_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_PATCH_START = 's/UserEntity/USER_DEVICES_USING_PATCH_START'
export const USER_DEVICES_USING_PATCH = 's/UserEntity/USER_DEVICES_USING_PATCH'


export function userDevicesUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_PATCH_START, meta: { info } })
    return UserEntity.userDevicesUsingPATCH(id, body)
      .then(response => dispatch({
        type: USER_DEVICES_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_GET_START = 's/UserEntity/USER_DEVICES_USING_GET_START'
export const USER_DEVICES_USING_GET = 's/UserEntity/USER_DEVICES_USING_GET'


export function userDevicesUsingGET(id, deviceId, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_GET_START, meta: { info } })
    return UserEntity.userDevicesUsingGET(id, deviceId)
      .then(response => dispatch({
        type: USER_DEVICES_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_DEVICES_USING_DELETE_START = 's/UserEntity/USER_DEVICES_USING_DELETE_START'
export const USER_DEVICES_USING_DELETE = 's/UserEntity/USER_DEVICES_USING_DELETE'


export function userDevicesUsingDELETE(id, deviceId, info) {
  return dispatch => {
    dispatch({ type: USER_DEVICES_USING_DELETE_START, meta: { info } })
    return UserEntity.userDevicesUsingDELETE(id, deviceId)
      .then(response => dispatch({
        type: USER_DEVICES_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_GET_1_START = 's/UserEntity/USER_ROLES_USING_GET_1_START'
export const USER_ROLES_USING_GET_1 = 's/UserEntity/USER_ROLES_USING_GET_1'


export function userRolesUsingGET_1(id, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_GET_1_START, meta: { info } })
    return UserEntity.userRolesUsingGET_1(id)
      .then(response => dispatch({
        type: USER_ROLES_USING_GET_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_POST_START = 's/UserEntity/USER_ROLES_USING_POST_START'
export const USER_ROLES_USING_POST = 's/UserEntity/USER_ROLES_USING_POST'


export function userRolesUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_POST_START, meta: { info } })
    return UserEntity.userRolesUsingPOST(id, body)
      .then(response => dispatch({
        type: USER_ROLES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_PUT_START = 's/UserEntity/USER_ROLES_USING_PUT_START'
export const USER_ROLES_USING_PUT = 's/UserEntity/USER_ROLES_USING_PUT'


export function userRolesUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_PUT_START, meta: { info } })
    return UserEntity.userRolesUsingPUT(id, body)
      .then(response => dispatch({
        type: USER_ROLES_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_DELETE_1_START = 's/UserEntity/USER_ROLES_USING_DELETE_1_START'
export const USER_ROLES_USING_DELETE_1 = 's/UserEntity/USER_ROLES_USING_DELETE_1'


export function userRolesUsingDELETE_1(id, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_DELETE_1_START, meta: { info } })
    return UserEntity.userRolesUsingDELETE_1(id)
      .then(response => dispatch({
        type: USER_ROLES_USING_DELETE_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_PATCH_START = 's/UserEntity/USER_ROLES_USING_PATCH_START'
export const USER_ROLES_USING_PATCH = 's/UserEntity/USER_ROLES_USING_PATCH'


export function userRolesUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_PATCH_START, meta: { info } })
    return UserEntity.userRolesUsingPATCH(id, body)
      .then(response => dispatch({
        type: USER_ROLES_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_GET_START = 's/UserEntity/USER_ROLES_USING_GET_START'
export const USER_ROLES_USING_GET = 's/UserEntity/USER_ROLES_USING_GET'


export function userRolesUsingGET(id, roleId, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_GET_START, meta: { info } })
    return UserEntity.userRolesUsingGET(id, roleId)
      .then(response => dispatch({
        type: USER_ROLES_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const USER_ROLES_USING_DELETE_START = 's/UserEntity/USER_ROLES_USING_DELETE_START'
export const USER_ROLES_USING_DELETE = 's/UserEntity/USER_ROLES_USING_DELETE'


export function userRolesUsingDELETE(id, roleId, info) {
  return dispatch => {
    dispatch({ type: USER_ROLES_USING_DELETE_START, meta: { info } })
    return UserEntity.userRolesUsingDELETE(id, roleId)
      .then(response => dispatch({
        type: USER_ROLES_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/AssetEntity */
// Auto-generated, edits will be overwritten
import * as AssetEntity from '../AssetEntity'

export const FIND_ALL_ASSET_USING_GET_START = 's/AssetEntity/FIND_ALL_ASSET_USING_GET_START'
export const FIND_ALL_ASSET_USING_GET = 's/AssetEntity/FIND_ALL_ASSET_USING_GET'


export function findAllAssetUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_ASSET_USING_GET_START, meta: { info } })
    return AssetEntity.findAllAssetUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_ASSET_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_USING_POST_START = 's/AssetEntity/SAVE_ASSET_USING_POST_START'
export const SAVE_ASSET_USING_POST = 's/AssetEntity/SAVE_ASSET_USING_POST'


export function saveAssetUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_USING_POST_START, meta: { info } })
    return AssetEntity.saveAssetUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_ASSET_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_MEDIA_ID_ASSET_USING_GET_START = 's/AssetEntity/FIND_BY_MEDIA_ID_ASSET_USING_GET_START'
export const FIND_BY_MEDIA_ID_ASSET_USING_GET = 's/AssetEntity/FIND_BY_MEDIA_ID_ASSET_USING_GET'


export function findByMediaIdAssetUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_MEDIA_ID_ASSET_USING_GET_START, meta: { info } })
    return AssetEntity.findByMediaIdAssetUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_MEDIA_ID_ASSET_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_ASSET_USING_GET_START = 's/AssetEntity/FIND_ONE_ASSET_USING_GET_START'
export const FIND_ONE_ASSET_USING_GET = 's/AssetEntity/FIND_ONE_ASSET_USING_GET'


export function findOneAssetUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_ASSET_USING_GET_START, meta: { info } })
    return AssetEntity.findOneAssetUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_ASSET_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_USING_PUT_START = 's/AssetEntity/SAVE_ASSET_USING_PUT_START'
export const SAVE_ASSET_USING_PUT = 's/AssetEntity/SAVE_ASSET_USING_PUT'


export function saveAssetUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_USING_PUT_START, meta: { info } })
    return AssetEntity.saveAssetUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_ASSET_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_ASSET_USING_DELETE_START = 's/AssetEntity/DELETE_ASSET_USING_DELETE_START'
export const DELETE_ASSET_USING_DELETE = 's/AssetEntity/DELETE_ASSET_USING_DELETE'


export function deleteAssetUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_ASSET_USING_DELETE_START, meta: { info } })
    return AssetEntity.deleteAssetUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_ASSET_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_USING_PATCH_START = 's/AssetEntity/SAVE_ASSET_USING_PATCH_START'
export const SAVE_ASSET_USING_PATCH = 's/AssetEntity/SAVE_ASSET_USING_PATCH'


export function saveAssetUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_USING_PATCH_START, meta: { info } })
    return AssetEntity.saveAssetUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_ASSET_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_OWNER_USING_GET_START = 's/AssetEntity/ASSET_OWNER_USING_GET_START'
export const ASSET_OWNER_USING_GET = 's/AssetEntity/ASSET_OWNER_USING_GET'


export function assetOwnerUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_OWNER_USING_GET_START, meta: { info } })
    return AssetEntity.assetOwnerUsingGET(id)
      .then(response => dispatch({
        type: ASSET_OWNER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_OWNER_USING_POST_START = 's/AssetEntity/ASSET_OWNER_USING_POST_START'
export const ASSET_OWNER_USING_POST = 's/AssetEntity/ASSET_OWNER_USING_POST'


export function assetOwnerUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_OWNER_USING_POST_START, meta: { info } })
    return AssetEntity.assetOwnerUsingPOST(id, body)
      .then(response => dispatch({
        type: ASSET_OWNER_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_OWNER_USING_PUT_START = 's/AssetEntity/ASSET_OWNER_USING_PUT_START'
export const ASSET_OWNER_USING_PUT = 's/AssetEntity/ASSET_OWNER_USING_PUT'


export function assetOwnerUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_OWNER_USING_PUT_START, meta: { info } })
    return AssetEntity.assetOwnerUsingPUT(id, body)
      .then(response => dispatch({
        type: ASSET_OWNER_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_OWNER_USING_DELETE_START = 's/AssetEntity/ASSET_OWNER_USING_DELETE_START'
export const ASSET_OWNER_USING_DELETE = 's/AssetEntity/ASSET_OWNER_USING_DELETE'


export function assetOwnerUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_OWNER_USING_DELETE_START, meta: { info } })
    return AssetEntity.assetOwnerUsingDELETE(id)
      .then(response => dispatch({
        type: ASSET_OWNER_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_OWNER_USING_PATCH_START = 's/AssetEntity/ASSET_OWNER_USING_PATCH_START'
export const ASSET_OWNER_USING_PATCH = 's/AssetEntity/ASSET_OWNER_USING_PATCH'


export function assetOwnerUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_OWNER_USING_PATCH_START, meta: { info } })
    return AssetEntity.assetOwnerUsingPATCH(id, body)
      .then(response => dispatch({
        type: ASSET_OWNER_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

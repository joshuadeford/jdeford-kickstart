/** @module action/HistoryTask */
// Auto-generated, edits will be overwritten
import * as HistoryTask from '../HistoryTask'

export const LIST_HISTORIC_TASK_INSTANCES_START = 's/HistoryTask/LIST_HISTORIC_TASK_INSTANCES_START'
export const LIST_HISTORIC_TASK_INSTANCES = 's/HistoryTask/LIST_HISTORIC_TASK_INSTANCES'


export function listHistoricTaskInstances(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_TASK_INSTANCES_START, meta: { info } })
    return HistoryTask.listHistoricTaskInstances(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_TASK_INSTANCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_INSTANCE_USING_GET_START = 's/HistoryTask/GET_TASK_INSTANCE_USING_GET_START'
export const GET_TASK_INSTANCE_USING_GET = 's/HistoryTask/GET_TASK_INSTANCE_USING_GET'


export function getTaskInstanceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_INSTANCE_USING_GET_START, meta: { info } })
    return HistoryTask.getTaskInstanceUsingGET(options)
      .then(response => dispatch({
        type: GET_TASK_INSTANCE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TASK_INSTANCE_USING_DELETE_START = 's/HistoryTask/DELETE_TASK_INSTANCE_USING_DELETE_START'
export const DELETE_TASK_INSTANCE_USING_DELETE = 's/HistoryTask/DELETE_TASK_INSTANCE_USING_DELETE'


export function deleteTaskInstanceUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TASK_INSTANCE_USING_DELETE_START, meta: { info } })
    return HistoryTask.deleteTaskInstanceUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_TASK_INSTANCE_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS_START = 's/HistoryTask/LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS_START'
export const LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS = 's/HistoryTask/LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS'


export function listHistoricTaskInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return HistoryTask.listHistoricTaskInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_TASK_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const QUERY_HISTORIC_TASK_INSTANCE_START = 's/HistoryTask/QUERY_HISTORIC_TASK_INSTANCE_START'
export const QUERY_HISTORIC_TASK_INSTANCE = 's/HistoryTask/QUERY_HISTORIC_TASK_INSTANCE'


export function queryHistoricTaskInstance(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_HISTORIC_TASK_INSTANCE_START, meta: { info } })
    return HistoryTask.queryHistoricTaskInstance(queryRequest)
      .then(response => dispatch({
        type: QUERY_HISTORIC_TASK_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

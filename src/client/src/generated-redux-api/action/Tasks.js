/** @module action/Tasks */
// Auto-generated, edits will be overwritten
import * as Tasks from '../Tasks'

export const LIST_TASKS_START = 's/Tasks/LIST_TASKS_START'
export const LIST_TASKS = 's/Tasks/LIST_TASKS'


export function listTasks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASKS_START, meta: { info } })
    return Tasks.listTasks(options)
      .then(response => dispatch({
        type: LIST_TASKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_TASK_USING_POST_START = 's/Tasks/CREATE_TASK_USING_POST_START'
export const CREATE_TASK_USING_POST = 's/Tasks/CREATE_TASK_USING_POST'


export function createTaskUsingPOST(taskRequest, info) {
  return dispatch => {
    dispatch({ type: CREATE_TASK_USING_POST_START, meta: { info } })
    return Tasks.createTaskUsingPOST(taskRequest)
      .then(response => dispatch({
        type: CREATE_TASK_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_USING_GET_START = 's/Tasks/GET_TASK_USING_GET_START'
export const GET_TASK_USING_GET = 's/Tasks/GET_TASK_USING_GET'


export function getTaskUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_USING_GET_START, meta: { info } })
    return Tasks.getTaskUsingGET(options)
      .then(response => dispatch({
        type: GET_TASK_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_TASK_ACTION_USING_POST_START = 's/Tasks/EXECUTE_TASK_ACTION_USING_POST_START'
export const EXECUTE_TASK_ACTION_USING_POST = 's/Tasks/EXECUTE_TASK_ACTION_USING_POST'


export function executeTaskActionUsingPOST(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_TASK_ACTION_USING_POST_START, meta: { info } })
    return Tasks.executeTaskActionUsingPOST(actionRequest, options)
      .then(response => dispatch({
        type: EXECUTE_TASK_ACTION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_TASK_USING_PUT_START = 's/Tasks/UPDATE_TASK_USING_PUT_START'
export const UPDATE_TASK_USING_PUT = 's/Tasks/UPDATE_TASK_USING_PUT'


export function updateTaskUsingPUT(taskRequest, options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_TASK_USING_PUT_START, meta: { info } })
    return Tasks.updateTaskUsingPUT(taskRequest, options)
      .then(response => dispatch({
        type: UPDATE_TASK_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TASK_USING_DELETE_START = 's/Tasks/DELETE_TASK_USING_DELETE_START'
export const DELETE_TASK_USING_DELETE = 's/Tasks/DELETE_TASK_USING_DELETE'


export function deleteTaskUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TASK_USING_DELETE_START, meta: { info } })
    return Tasks.deleteTaskUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_TASK_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_TASK_EVENTS_START = 's/Tasks/LIST_TASK_EVENTS_START'
export const LIST_TASK_EVENTS = 's/Tasks/LIST_TASK_EVENTS'


export function listTaskEvents(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASK_EVENTS_START, meta: { info } })
    return Tasks.listTaskEvents(options)
      .then(response => dispatch({
        type: LIST_TASK_EVENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EVENT_USING_GET_START = 's/Tasks/GET_EVENT_USING_GET_START'
export const GET_EVENT_USING_GET = 's/Tasks/GET_EVENT_USING_GET'


export function getEventUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_EVENT_USING_GET_START, meta: { info } })
    return Tasks.getEventUsingGET(options)
      .then(response => dispatch({
        type: GET_EVENT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_EVENT_USING_DELETE_START = 's/Tasks/DELETE_EVENT_USING_DELETE_START'
export const DELETE_EVENT_USING_DELETE = 's/Tasks/DELETE_EVENT_USING_DELETE'


export function deleteEventUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_EVENT_USING_DELETE_START, meta: { info } })
    return Tasks.deleteEventUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_EVENT_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_TASK_SUBTASKS_START = 's/Tasks/LIST_TASK_SUBTASKS_START'
export const LIST_TASK_SUBTASKS = 's/Tasks/LIST_TASK_SUBTASKS'


export function listTaskSubtasks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASK_SUBTASKS_START, meta: { info } })
    return Tasks.listTaskSubtasks(options)
      .then(response => dispatch({
        type: LIST_TASK_SUBTASKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE_START = 's/Tasks/DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE_START'
export const DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE = 's/Tasks/DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE'


export function deleteAllLocalTaskVariablesUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE_START, meta: { info } })
    return Tasks.deleteAllLocalTaskVariablesUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_ALL_LOCAL_TASK_VARIABLES_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

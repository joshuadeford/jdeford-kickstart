/** @module action/ComponentPropertyEntity */
// Auto-generated, edits will be overwritten
import * as ComponentPropertyEntity from '../ComponentPropertyEntity'

export const FIND_ALL_COMPONENT_PROPERTY_USING_GET_START = 's/ComponentPropertyEntity/FIND_ALL_COMPONENT_PROPERTY_USING_GET_START'
export const FIND_ALL_COMPONENT_PROPERTY_USING_GET = 's/ComponentPropertyEntity/FIND_ALL_COMPONENT_PROPERTY_USING_GET'


export function findAllComponentPropertyUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_COMPONENT_PROPERTY_USING_GET_START, meta: { info } })
    return ComponentPropertyEntity.findAllComponentPropertyUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_COMPONENT_PROPERTY_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_PROPERTY_USING_POST_START = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_POST_START'
export const SAVE_COMPONENT_PROPERTY_USING_POST = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_POST'


export function saveComponentPropertyUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_PROPERTY_USING_POST_START, meta: { info } })
    return ComponentPropertyEntity.saveComponentPropertyUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_PROPERTY_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_COMPONENT_PROPERTY_USING_GET_START = 's/ComponentPropertyEntity/FIND_ONE_COMPONENT_PROPERTY_USING_GET_START'
export const FIND_ONE_COMPONENT_PROPERTY_USING_GET = 's/ComponentPropertyEntity/FIND_ONE_COMPONENT_PROPERTY_USING_GET'


export function findOneComponentPropertyUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_COMPONENT_PROPERTY_USING_GET_START, meta: { info } })
    return ComponentPropertyEntity.findOneComponentPropertyUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_COMPONENT_PROPERTY_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_PROPERTY_USING_PUT_START = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_PUT_START'
export const SAVE_COMPONENT_PROPERTY_USING_PUT = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_PUT'


export function saveComponentPropertyUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_PROPERTY_USING_PUT_START, meta: { info } })
    return ComponentPropertyEntity.saveComponentPropertyUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_PROPERTY_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_COMPONENT_PROPERTY_USING_DELETE_START = 's/ComponentPropertyEntity/DELETE_COMPONENT_PROPERTY_USING_DELETE_START'
export const DELETE_COMPONENT_PROPERTY_USING_DELETE = 's/ComponentPropertyEntity/DELETE_COMPONENT_PROPERTY_USING_DELETE'


export function deleteComponentPropertyUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_COMPONENT_PROPERTY_USING_DELETE_START, meta: { info } })
    return ComponentPropertyEntity.deleteComponentPropertyUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_COMPONENT_PROPERTY_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_PROPERTY_USING_PATCH_START = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_PATCH_START'
export const SAVE_COMPONENT_PROPERTY_USING_PATCH = 's/ComponentPropertyEntity/SAVE_COMPONENT_PROPERTY_USING_PATCH'


export function saveComponentPropertyUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_PROPERTY_USING_PATCH_START, meta: { info } })
    return ComponentPropertyEntity.saveComponentPropertyUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_PROPERTY_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_PROPERTY_CONFIGURATION_USING_GET_START = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_GET_START'
export const COMPONENT_PROPERTY_CONFIGURATION_USING_GET = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_GET'


export function componentPropertyConfigurationUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_PROPERTY_CONFIGURATION_USING_GET_START, meta: { info } })
    return ComponentPropertyEntity.componentPropertyConfigurationUsingGET(id)
      .then(response => dispatch({
        type: COMPONENT_PROPERTY_CONFIGURATION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_PROPERTY_CONFIGURATION_USING_POST_START = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_POST_START'
export const COMPONENT_PROPERTY_CONFIGURATION_USING_POST = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_POST'


export function componentPropertyConfigurationUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_PROPERTY_CONFIGURATION_USING_POST_START, meta: { info } })
    return ComponentPropertyEntity.componentPropertyConfigurationUsingPOST(id, body)
      .then(response => dispatch({
        type: COMPONENT_PROPERTY_CONFIGURATION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_PROPERTY_CONFIGURATION_USING_PUT_START = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_PUT_START'
export const COMPONENT_PROPERTY_CONFIGURATION_USING_PUT = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_PUT'


export function componentPropertyConfigurationUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_PROPERTY_CONFIGURATION_USING_PUT_START, meta: { info } })
    return ComponentPropertyEntity.componentPropertyConfigurationUsingPUT(id, body)
      .then(response => dispatch({
        type: COMPONENT_PROPERTY_CONFIGURATION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE_START = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE_START'
export const COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE'


export function componentPropertyConfigurationUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE_START, meta: { info } })
    return ComponentPropertyEntity.componentPropertyConfigurationUsingDELETE(id)
      .then(response => dispatch({
        type: COMPONENT_PROPERTY_CONFIGURATION_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH_START = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH_START'
export const COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH = 's/ComponentPropertyEntity/COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH'


export function componentPropertyConfigurationUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH_START, meta: { info } })
    return ComponentPropertyEntity.componentPropertyConfigurationUsingPATCH(id, body)
      .then(response => dispatch({
        type: COMPONENT_PROPERTY_CONFIGURATION_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/Executions */
// Auto-generated, edits will be overwritten
import * as Executions from '../Executions'

export const QUERY_EXECUTIONS_START = 's/Executions/QUERY_EXECUTIONS_START'
export const QUERY_EXECUTIONS = 's/Executions/QUERY_EXECUTIONS'


export function queryExecutions(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_EXECUTIONS_START, meta: { info } })
    return Executions.queryExecutions(queryRequest)
      .then(response => dispatch({
        type: QUERY_EXECUTIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_EXECUTIONS_START = 's/Executions/LIST_EXECUTIONS_START'
export const LIST_EXECUTIONS = 's/Executions/LIST_EXECUTIONS'


export function listExecutions(options, info) {
  return dispatch => {
    dispatch({ type: LIST_EXECUTIONS_START, meta: { info } })
    return Executions.listExecutions(options)
      .then(response => dispatch({
        type: LIST_EXECUTIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_EXECUTION_ACTION_USING_PUT_START = 's/Executions/EXECUTE_EXECUTION_ACTION_USING_PUT_START'
export const EXECUTE_EXECUTION_ACTION_USING_PUT = 's/Executions/EXECUTE_EXECUTION_ACTION_USING_PUT'


export function executeExecutionActionUsingPUT(actionRequest, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_EXECUTION_ACTION_USING_PUT_START, meta: { info } })
    return Executions.executeExecutionActionUsingPUT(actionRequest)
      .then(response => dispatch({
        type: EXECUTE_EXECUTION_ACTION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EXECUTION_USING_GET_START = 's/Executions/GET_EXECUTION_USING_GET_START'
export const GET_EXECUTION_USING_GET = 's/Executions/GET_EXECUTION_USING_GET'


export function getExecutionUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_EXECUTION_USING_GET_START, meta: { info } })
    return Executions.getExecutionUsingGET(options)
      .then(response => dispatch({
        type: GET_EXECUTION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const PERFORM_EXECUTION_ACTION_USING_PUT_START = 's/Executions/PERFORM_EXECUTION_ACTION_USING_PUT_START'
export const PERFORM_EXECUTION_ACTION_USING_PUT = 's/Executions/PERFORM_EXECUTION_ACTION_USING_PUT'


export function performExecutionActionUsingPUT(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: PERFORM_EXECUTION_ACTION_USING_PUT_START, meta: { info } })
    return Executions.performExecutionActionUsingPUT(actionRequest, options)
      .then(response => dispatch({
        type: PERFORM_EXECUTION_ACTION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_EXECUTION_ACTIVE_ACTIVITIES_START = 's/Executions/LIST_EXECUTION_ACTIVE_ACTIVITIES_START'
export const LIST_EXECUTION_ACTIVE_ACTIVITIES = 's/Executions/LIST_EXECUTION_ACTIVE_ACTIVITIES'


export function listExecutionActiveActivities(options, info) {
  return dispatch => {
    dispatch({ type: LIST_EXECUTION_ACTIVE_ACTIVITIES_START, meta: { info } })
    return Executions.listExecutionActiveActivities(options)
      .then(response => dispatch({
        type: LIST_EXECUTION_ACTIVE_ACTIVITIES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CHANGE_ACTIVITY_STATE_USING_POST_START = 's/Executions/CHANGE_ACTIVITY_STATE_USING_POST_START'
export const CHANGE_ACTIVITY_STATE_USING_POST = 's/Executions/CHANGE_ACTIVITY_STATE_USING_POST'


export function changeActivityStateUsingPOST(activityStateRequest, options, info) {
  return dispatch => {
    dispatch({ type: CHANGE_ACTIVITY_STATE_USING_POST_START, meta: { info } })
    return Executions.changeActivityStateUsingPOST(activityStateRequest, options)
      .then(response => dispatch({
        type: CHANGE_ACTIVITY_STATE_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_EXECUTION_VARIABLES_START = 's/Executions/LIST_EXECUTION_VARIABLES_START'
export const LIST_EXECUTION_VARIABLES = 's/Executions/LIST_EXECUTION_VARIABLES'


export function listExecutionVariables(options, info) {
  return dispatch => {
    dispatch({ type: LIST_EXECUTION_VARIABLES_START, meta: { info } })
    return Executions.listExecutionVariables(options)
      .then(response => dispatch({
        type: LIST_EXECUTION_VARIABLES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_EXECUTION_VARIABLE_START = 's/Executions/CREATE_EXECUTION_VARIABLE_START'
export const CREATE_EXECUTION_VARIABLE = 's/Executions/CREATE_EXECUTION_VARIABLE'


export function createExecutionVariable(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_EXECUTION_VARIABLE_START, meta: { info } })
    return Executions.createExecutionVariable(options)
      .then(response => dispatch({
        type: CREATE_EXECUTION_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_OR_UPDATE_EXECUTION_VARIABLE_START = 's/Executions/CREATE_OR_UPDATE_EXECUTION_VARIABLE_START'
export const CREATE_OR_UPDATE_EXECUTION_VARIABLE = 's/Executions/CREATE_OR_UPDATE_EXECUTION_VARIABLE'


export function createOrUpdateExecutionVariable(options, info) {
  return dispatch => {
    dispatch({ type: CREATE_OR_UPDATE_EXECUTION_VARIABLE_START, meta: { info } })
    return Executions.createOrUpdateExecutionVariable(options)
      .then(response => dispatch({
        type: CREATE_OR_UPDATE_EXECUTION_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_LOCAL_VARIABLES_USING_DELETE_START = 's/Executions/DELETE_LOCAL_VARIABLES_USING_DELETE_START'
export const DELETE_LOCAL_VARIABLES_USING_DELETE = 's/Executions/DELETE_LOCAL_VARIABLES_USING_DELETE'


export function deleteLocalVariablesUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_LOCAL_VARIABLES_USING_DELETE_START, meta: { info } })
    return Executions.deleteLocalVariablesUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_LOCAL_VARIABLES_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EXECUTION_VARIABLE_START = 's/Executions/GET_EXECUTION_VARIABLE_START'
export const GET_EXECUTION_VARIABLE = 's/Executions/GET_EXECUTION_VARIABLE'


export function getExecutionVariable(options, info) {
  return dispatch => {
    dispatch({ type: GET_EXECUTION_VARIABLE_START, meta: { info } })
    return Executions.getExecutionVariable(options)
      .then(response => dispatch({
        type: GET_EXECUTION_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const UPDATE_EXECUTION_VARIABLE_START = 's/Executions/UPDATE_EXECUTION_VARIABLE_START'
export const UPDATE_EXECUTION_VARIABLE = 's/Executions/UPDATE_EXECUTION_VARIABLE'


export function updateExecutionVariable(options, info) {
  return dispatch => {
    dispatch({ type: UPDATE_EXECUTION_VARIABLE_START, meta: { info } })
    return Executions.updateExecutionVariable(options)
      .then(response => dispatch({
        type: UPDATE_EXECUTION_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETED_EXECUTION_VARIABLE_START = 's/Executions/DELETED_EXECUTION_VARIABLE_START'
export const DELETED_EXECUTION_VARIABLE = 's/Executions/DELETED_EXECUTION_VARIABLE'


export function deletedExecutionVariable(options, info) {
  return dispatch => {
    dispatch({ type: DELETED_EXECUTION_VARIABLE_START, meta: { info } })
    return Executions.deletedExecutionVariable(options)
      .then(response => dispatch({
        type: DELETED_EXECUTION_VARIABLE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EXECUTION_VARIABLE_DATA_START = 's/Executions/GET_EXECUTION_VARIABLE_DATA_START'
export const GET_EXECUTION_VARIABLE_DATA = 's/Executions/GET_EXECUTION_VARIABLE_DATA'


export function getExecutionVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_EXECUTION_VARIABLE_DATA_START, meta: { info } })
    return Executions.getExecutionVariableData(options)
      .then(response => dispatch({
        type: GET_EXECUTION_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

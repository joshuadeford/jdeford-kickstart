/** @module action/HistoryProcess */
// Auto-generated, edits will be overwritten
import * as HistoryProcess from '../HistoryProcess'

export const LIST_HISTORIC_PROCESS_INSTANCES_START = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCES_START'
export const LIST_HISTORIC_PROCESS_INSTANCES = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCES'


export function listHistoricProcessInstances(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_PROCESS_INSTANCES_START, meta: { info } })
    return HistoryProcess.listHistoricProcessInstances(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_PROCESS_INSTANCES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_HISTORIC_PROCESS_INSTANCE_START = 's/HistoryProcess/GET_HISTORIC_PROCESS_INSTANCE_START'
export const GET_HISTORIC_PROCESS_INSTANCE = 's/HistoryProcess/GET_HISTORIC_PROCESS_INSTANCE'


export function getHistoricProcessInstance(options, info) {
  return dispatch => {
    dispatch({ type: GET_HISTORIC_PROCESS_INSTANCE_START, meta: { info } })
    return HistoryProcess.getHistoricProcessInstance(options)
      .then(response => dispatch({
        type: GET_HISTORIC_PROCESS_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_HISTORIC_PROCESS_INSTANCE_START = 's/HistoryProcess/DELETE_HISTORIC_PROCESS_INSTANCE_START'
export const DELETE_HISTORIC_PROCESS_INSTANCE = 's/HistoryProcess/DELETE_HISTORIC_PROCESS_INSTANCE'


export function deleteHistoricProcessInstance(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_HISTORIC_PROCESS_INSTANCE_START, meta: { info } })
    return HistoryProcess.deleteHistoricProcessInstance(options)
      .then(response => dispatch({
        type: DELETE_HISTORIC_PROCESS_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS_START = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS_START'
export const LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS'


export function listHistoricProcessInstanceComments(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS_START, meta: { info } })
    return HistoryProcess.listHistoricProcessInstanceComments(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_PROCESS_INSTANCE_COMMENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_COMMENT_USING_POST_START = 's/HistoryProcess/CREATE_COMMENT_USING_POST_START'
export const CREATE_COMMENT_USING_POST = 's/HistoryProcess/CREATE_COMMENT_USING_POST'


export function createCommentUsingPOST(comment, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_COMMENT_USING_POST_START, meta: { info } })
    return HistoryProcess.createCommentUsingPOST(comment, options)
      .then(response => dispatch({
        type: CREATE_COMMENT_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_COMMENT_USING_GET_START = 's/HistoryProcess/GET_COMMENT_USING_GET_START'
export const GET_COMMENT_USING_GET = 's/HistoryProcess/GET_COMMENT_USING_GET'


export function getCommentUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_COMMENT_USING_GET_START, meta: { info } })
    return HistoryProcess.getCommentUsingGET(options)
      .then(response => dispatch({
        type: GET_COMMENT_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_COMMENT_USING_DELETE_START = 's/HistoryProcess/DELETE_COMMENT_USING_DELETE_START'
export const DELETE_COMMENT_USING_DELETE = 's/HistoryProcess/DELETE_COMMENT_USING_DELETE'


export function deleteCommentUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_COMMENT_USING_DELETE_START, meta: { info } })
    return HistoryProcess.deleteCommentUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_COMMENT_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS_START = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS_START'
export const LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS = 's/HistoryProcess/LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS'


export function listHistoricProcessInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return HistoryProcess.listHistoricProcessInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: LIST_HISTORIC_PROCESS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA_START = 's/HistoryProcess/GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA_START'
export const GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA = 's/HistoryProcess/GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA'


export function getHistoricProcessInstanceVariableData(options, info) {
  return dispatch => {
    dispatch({ type: GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA_START, meta: { info } })
    return HistoryProcess.getHistoricProcessInstanceVariableData(options)
      .then(response => dispatch({
        type: GET_HISTORIC_PROCESS_INSTANCE_VARIABLE_DATA,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const QUERY_HISTORIC_PROCESS_INSTANCE_START = 's/HistoryProcess/QUERY_HISTORIC_PROCESS_INSTANCE_START'
export const QUERY_HISTORIC_PROCESS_INSTANCE = 's/HistoryProcess/QUERY_HISTORIC_PROCESS_INSTANCE'


export function queryHistoricProcessInstance(queryRequest, info) {
  return dispatch => {
    dispatch({ type: QUERY_HISTORIC_PROCESS_INSTANCE_START, meta: { info } })
    return HistoryProcess.queryHistoricProcessInstance(queryRequest)
      .then(response => dispatch({
        type: QUERY_HISTORIC_PROCESS_INSTANCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

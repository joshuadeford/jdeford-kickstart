/** @module action/ComponentConfigurationEntity */
// Auto-generated, edits will be overwritten
import * as ComponentConfigurationEntity from '../ComponentConfigurationEntity'

export const FIND_ALL_COMPONENT_CONFIGURATION_USING_GET_START = 's/ComponentConfigurationEntity/FIND_ALL_COMPONENT_CONFIGURATION_USING_GET_START'
export const FIND_ALL_COMPONENT_CONFIGURATION_USING_GET = 's/ComponentConfigurationEntity/FIND_ALL_COMPONENT_CONFIGURATION_USING_GET'


export function findAllComponentConfigurationUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_COMPONENT_CONFIGURATION_USING_GET_START, meta: { info } })
    return ComponentConfigurationEntity.findAllComponentConfigurationUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_COMPONENT_CONFIGURATION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_CONFIGURATION_USING_POST_START = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_POST_START'
export const SAVE_COMPONENT_CONFIGURATION_USING_POST = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_POST'


export function saveComponentConfigurationUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_CONFIGURATION_USING_POST_START, meta: { info } })
    return ComponentConfigurationEntity.saveComponentConfigurationUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_CONFIGURATION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET_START = 's/ComponentConfigurationEntity/FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET_START'
export const FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET = 's/ComponentConfigurationEntity/FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET'


export function findByVersionComponentConfigurationUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET_START, meta: { info } })
    return ComponentConfigurationEntity.findByVersionComponentConfigurationUsingGET(options)
      .then(response => dispatch({
        type: FIND_BY_VERSION_COMPONENT_CONFIGURATION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_COMPONENT_CONFIGURATION_USING_GET_START = 's/ComponentConfigurationEntity/FIND_ONE_COMPONENT_CONFIGURATION_USING_GET_START'
export const FIND_ONE_COMPONENT_CONFIGURATION_USING_GET = 's/ComponentConfigurationEntity/FIND_ONE_COMPONENT_CONFIGURATION_USING_GET'


export function findOneComponentConfigurationUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_COMPONENT_CONFIGURATION_USING_GET_START, meta: { info } })
    return ComponentConfigurationEntity.findOneComponentConfigurationUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_COMPONENT_CONFIGURATION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_CONFIGURATION_USING_PUT_START = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_PUT_START'
export const SAVE_COMPONENT_CONFIGURATION_USING_PUT = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_PUT'


export function saveComponentConfigurationUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_CONFIGURATION_USING_PUT_START, meta: { info } })
    return ComponentConfigurationEntity.saveComponentConfigurationUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_CONFIGURATION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_COMPONENT_CONFIGURATION_USING_DELETE_START = 's/ComponentConfigurationEntity/DELETE_COMPONENT_CONFIGURATION_USING_DELETE_START'
export const DELETE_COMPONENT_CONFIGURATION_USING_DELETE = 's/ComponentConfigurationEntity/DELETE_COMPONENT_CONFIGURATION_USING_DELETE'


export function deleteComponentConfigurationUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_COMPONENT_CONFIGURATION_USING_DELETE_START, meta: { info } })
    return ComponentConfigurationEntity.deleteComponentConfigurationUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_COMPONENT_CONFIGURATION_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_COMPONENT_CONFIGURATION_USING_PATCH_START = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_PATCH_START'
export const SAVE_COMPONENT_CONFIGURATION_USING_PATCH = 's/ComponentConfigurationEntity/SAVE_COMPONENT_CONFIGURATION_USING_PATCH'


export function saveComponentConfigurationUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_COMPONENT_CONFIGURATION_USING_PATCH_START, meta: { info } })
    return ComponentConfigurationEntity.saveComponentConfigurationUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_COMPONENT_CONFIGURATION_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1 = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1'


export function componentConfigurationPropertiesUsingGET_1(id, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingGET_1(id)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_POST_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_POST_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_POST = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_POST'


export function componentConfigurationPropertiesUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_POST_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingPOST(id, body)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT'


export function componentConfigurationPropertiesUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingPUT(id, body)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1 = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1'


export function componentConfigurationPropertiesUsingDELETE_1(id, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingDELETE_1(id)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH'


export function componentConfigurationPropertiesUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingPATCH(id, body)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_GET = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_GET'


export function componentConfigurationPropertiesUsingGET(id, componentpropertyId, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_GET_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingGET(id, componentpropertyId)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_START = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_START'
export const COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE = 's/ComponentConfigurationEntity/COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE'


export function componentConfigurationPropertiesUsingDELETE(id, componentpropertyId, info) {
  return dispatch => {
    dispatch({ type: COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE_START, meta: { info } })
    return ComponentConfigurationEntity.componentConfigurationPropertiesUsingDELETE(id, componentpropertyId)
      .then(response => dispatch({
        type: COMPONENT_CONFIGURATION_PROPERTIES_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

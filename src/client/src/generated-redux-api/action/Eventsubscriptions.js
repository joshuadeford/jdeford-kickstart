/** @module action/Eventsubscriptions */
// Auto-generated, edits will be overwritten
import * as Eventsubscriptions from '../Eventsubscriptions'

export const LIST_EVENT_SUBSCRIPTIONS_START = 's/Eventsubscriptions/LIST_EVENT_SUBSCRIPTIONS_START'
export const LIST_EVENT_SUBSCRIPTIONS = 's/Eventsubscriptions/LIST_EVENT_SUBSCRIPTIONS'


export function listEventSubscriptions(options, info) {
  return dispatch => {
    dispatch({ type: LIST_EVENT_SUBSCRIPTIONS_START, meta: { info } })
    return Eventsubscriptions.listEventSubscriptions(options)
      .then(response => dispatch({
        type: LIST_EVENT_SUBSCRIPTIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_EVENT_SUBSCRIPTION_USING_GET_START = 's/Eventsubscriptions/GET_EVENT_SUBSCRIPTION_USING_GET_START'
export const GET_EVENT_SUBSCRIPTION_USING_GET = 's/Eventsubscriptions/GET_EVENT_SUBSCRIPTION_USING_GET'


export function getEventSubscriptionUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_EVENT_SUBSCRIPTION_USING_GET_START, meta: { info } })
    return Eventsubscriptions.getEventSubscriptionUsingGET(options)
      .then(response => dispatch({
        type: GET_EVENT_SUBSCRIPTION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

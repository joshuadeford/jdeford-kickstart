/** @module action/RoleEntity */
// Auto-generated, edits will be overwritten
import * as RoleEntity from '../RoleEntity'

export const FIND_ALL_ROLE_USING_GET_START = 's/RoleEntity/FIND_ALL_ROLE_USING_GET_START'
export const FIND_ALL_ROLE_USING_GET = 's/RoleEntity/FIND_ALL_ROLE_USING_GET'


export function findAllRoleUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_ROLE_USING_GET_START, meta: { info } })
    return RoleEntity.findAllRoleUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_ROLE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ROLE_USING_POST_START = 's/RoleEntity/SAVE_ROLE_USING_POST_START'
export const SAVE_ROLE_USING_POST = 's/RoleEntity/SAVE_ROLE_USING_POST'


export function saveRoleUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ROLE_USING_POST_START, meta: { info } })
    return RoleEntity.saveRoleUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_ROLE_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_ROLE_USING_GET_START = 's/RoleEntity/FIND_ONE_ROLE_USING_GET_START'
export const FIND_ONE_ROLE_USING_GET = 's/RoleEntity/FIND_ONE_ROLE_USING_GET'


export function findOneRoleUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_ROLE_USING_GET_START, meta: { info } })
    return RoleEntity.findOneRoleUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_ROLE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ROLE_USING_PUT_START = 's/RoleEntity/SAVE_ROLE_USING_PUT_START'
export const SAVE_ROLE_USING_PUT = 's/RoleEntity/SAVE_ROLE_USING_PUT'


export function saveRoleUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ROLE_USING_PUT_START, meta: { info } })
    return RoleEntity.saveRoleUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_ROLE_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_ROLE_USING_DELETE_START = 's/RoleEntity/DELETE_ROLE_USING_DELETE_START'
export const DELETE_ROLE_USING_DELETE = 's/RoleEntity/DELETE_ROLE_USING_DELETE'


export function deleteRoleUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_ROLE_USING_DELETE_START, meta: { info } })
    return RoleEntity.deleteRoleUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_ROLE_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ROLE_USING_PATCH_START = 's/RoleEntity/SAVE_ROLE_USING_PATCH_START'
export const SAVE_ROLE_USING_PATCH = 's/RoleEntity/SAVE_ROLE_USING_PATCH'


export function saveRoleUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ROLE_USING_PATCH_START, meta: { info } })
    return RoleEntity.saveRoleUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_ROLE_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_GET_1_START = 's/RoleEntity/ROLE_USERS_USING_GET_1_START'
export const ROLE_USERS_USING_GET_1 = 's/RoleEntity/ROLE_USERS_USING_GET_1'


export function roleUsersUsingGET_1(id, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_GET_1_START, meta: { info } })
    return RoleEntity.roleUsersUsingGET_1(id)
      .then(response => dispatch({
        type: ROLE_USERS_USING_GET_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_POST_START = 's/RoleEntity/ROLE_USERS_USING_POST_START'
export const ROLE_USERS_USING_POST = 's/RoleEntity/ROLE_USERS_USING_POST'


export function roleUsersUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_POST_START, meta: { info } })
    return RoleEntity.roleUsersUsingPOST(id, body)
      .then(response => dispatch({
        type: ROLE_USERS_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_PUT_START = 's/RoleEntity/ROLE_USERS_USING_PUT_START'
export const ROLE_USERS_USING_PUT = 's/RoleEntity/ROLE_USERS_USING_PUT'


export function roleUsersUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_PUT_START, meta: { info } })
    return RoleEntity.roleUsersUsingPUT(id, body)
      .then(response => dispatch({
        type: ROLE_USERS_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_DELETE_1_START = 's/RoleEntity/ROLE_USERS_USING_DELETE_1_START'
export const ROLE_USERS_USING_DELETE_1 = 's/RoleEntity/ROLE_USERS_USING_DELETE_1'


export function roleUsersUsingDELETE_1(id, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_DELETE_1_START, meta: { info } })
    return RoleEntity.roleUsersUsingDELETE_1(id)
      .then(response => dispatch({
        type: ROLE_USERS_USING_DELETE_1,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_PATCH_START = 's/RoleEntity/ROLE_USERS_USING_PATCH_START'
export const ROLE_USERS_USING_PATCH = 's/RoleEntity/ROLE_USERS_USING_PATCH'


export function roleUsersUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_PATCH_START, meta: { info } })
    return RoleEntity.roleUsersUsingPATCH(id, body)
      .then(response => dispatch({
        type: ROLE_USERS_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_GET_START = 's/RoleEntity/ROLE_USERS_USING_GET_START'
export const ROLE_USERS_USING_GET = 's/RoleEntity/ROLE_USERS_USING_GET'


export function roleUsersUsingGET(id, userId, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_GET_START, meta: { info } })
    return RoleEntity.roleUsersUsingGET(id, userId)
      .then(response => dispatch({
        type: ROLE_USERS_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ROLE_USERS_USING_DELETE_START = 's/RoleEntity/ROLE_USERS_USING_DELETE_START'
export const ROLE_USERS_USING_DELETE = 's/RoleEntity/ROLE_USERS_USING_DELETE'


export function roleUsersUsingDELETE(id, userId, info) {
  return dispatch => {
    dispatch({ type: ROLE_USERS_USING_DELETE_START, meta: { info } })
    return RoleEntity.roleUsersUsingDELETE(id, userId)
      .then(response => dispatch({
        type: ROLE_USERS_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

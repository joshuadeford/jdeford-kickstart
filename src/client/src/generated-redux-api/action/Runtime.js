/** @module action/Runtime */
// Auto-generated, edits will be overwritten
import * as Runtime from '../Runtime'

export const SIGNAL_EVENT_RECEIVED_USING_POST_START = 's/Runtime/SIGNAL_EVENT_RECEIVED_USING_POST_START'
export const SIGNAL_EVENT_RECEIVED_USING_POST = 's/Runtime/SIGNAL_EVENT_RECEIVED_USING_POST'


export function signalEventReceivedUsingPOST(signalRequest, info) {
  return dispatch => {
    dispatch({ type: SIGNAL_EVENT_RECEIVED_USING_POST_START, meta: { info } })
    return Runtime.signalEventReceivedUsingPOST(signalRequest)
      .then(response => dispatch({
        type: SIGNAL_EVENT_RECEIVED_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

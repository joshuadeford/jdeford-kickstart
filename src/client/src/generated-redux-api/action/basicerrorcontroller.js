/** @module action/basicerrorcontroller */
// Auto-generated, edits will be overwritten
import * as basicerrorcontroller from '../basicerrorcontroller'

export const ERROR_HTML_USING_GET_START = 's/basicerrorcontroller/ERROR_HTML_USING_GET_START'
export const ERROR_HTML_USING_GET = 's/basicerrorcontroller/ERROR_HTML_USING_GET'


export function errorHtmlUsingGET(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_GET_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingGET()
      .then(response => dispatch({
        type: ERROR_HTML_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_HEAD_START = 's/basicerrorcontroller/ERROR_HTML_USING_HEAD_START'
export const ERROR_HTML_USING_HEAD = 's/basicerrorcontroller/ERROR_HTML_USING_HEAD'


export function errorHtmlUsingHEAD(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_HEAD_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingHEAD()
      .then(response => dispatch({
        type: ERROR_HTML_USING_HEAD,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_POST_START = 's/basicerrorcontroller/ERROR_HTML_USING_POST_START'
export const ERROR_HTML_USING_POST = 's/basicerrorcontroller/ERROR_HTML_USING_POST'


export function errorHtmlUsingPOST(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_POST_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingPOST()
      .then(response => dispatch({
        type: ERROR_HTML_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_PUT_START = 's/basicerrorcontroller/ERROR_HTML_USING_PUT_START'
export const ERROR_HTML_USING_PUT = 's/basicerrorcontroller/ERROR_HTML_USING_PUT'


export function errorHtmlUsingPUT(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_PUT_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingPUT()
      .then(response => dispatch({
        type: ERROR_HTML_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_DELETE_START = 's/basicerrorcontroller/ERROR_HTML_USING_DELETE_START'
export const ERROR_HTML_USING_DELETE = 's/basicerrorcontroller/ERROR_HTML_USING_DELETE'


export function errorHtmlUsingDELETE(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_DELETE_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingDELETE()
      .then(response => dispatch({
        type: ERROR_HTML_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_OPTIONS_START = 's/basicerrorcontroller/ERROR_HTML_USING_OPTIONS_START'
export const ERROR_HTML_USING_OPTIONS = 's/basicerrorcontroller/ERROR_HTML_USING_OPTIONS'


export function errorHtmlUsingOPTIONS(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_OPTIONS_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingOPTIONS()
      .then(response => dispatch({
        type: ERROR_HTML_USING_OPTIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ERROR_HTML_USING_PATCH_START = 's/basicerrorcontroller/ERROR_HTML_USING_PATCH_START'
export const ERROR_HTML_USING_PATCH = 's/basicerrorcontroller/ERROR_HTML_USING_PATCH'


export function errorHtmlUsingPATCH(info) {
  return dispatch => {
    dispatch({ type: ERROR_HTML_USING_PATCH_START, meta: { info } })
    return basicerrorcontroller.errorHtmlUsingPATCH()
      .then(response => dispatch({
        type: ERROR_HTML_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/Jobs */
// Auto-generated, edits will be overwritten
import * as Jobs from '../Jobs'

export const LIST_DEAD_LETTER_JOBS_START = 's/Jobs/LIST_DEAD_LETTER_JOBS_START'
export const LIST_DEAD_LETTER_JOBS = 's/Jobs/LIST_DEAD_LETTER_JOBS'


export function listDeadLetterJobs(options, info) {
  return dispatch => {
    dispatch({ type: LIST_DEAD_LETTER_JOBS_START, meta: { info } })
    return Jobs.listDeadLetterJobs(options)
      .then(response => dispatch({
        type: LIST_DEAD_LETTER_JOBS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_DEADLETTER_JOB_USING_GET_START = 's/Jobs/GET_DEADLETTER_JOB_USING_GET_START'
export const GET_DEADLETTER_JOB_USING_GET = 's/Jobs/GET_DEADLETTER_JOB_USING_GET'


export function getDeadletterJobUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_DEADLETTER_JOB_USING_GET_START, meta: { info } })
    return Jobs.getDeadletterJobUsingGET(options)
      .then(response => dispatch({
        type: GET_DEADLETTER_JOB_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST_START = 's/Jobs/EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST_START'
export const EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST = 's/Jobs/EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST'


export function executeDeadLetterJobActionUsingPOST(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST_START, meta: { info } })
    return Jobs.executeDeadLetterJobActionUsingPOST(actionRequest, options)
      .then(response => dispatch({
        type: EXECUTE_DEAD_LETTER_JOB_ACTION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_DEAD_LETTER_JOB_USING_DELETE_START = 's/Jobs/DELETE_DEAD_LETTER_JOB_USING_DELETE_START'
export const DELETE_DEAD_LETTER_JOB_USING_DELETE = 's/Jobs/DELETE_DEAD_LETTER_JOB_USING_DELETE'


export function deleteDeadLetterJobUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_DEAD_LETTER_JOB_USING_DELETE_START, meta: { info } })
    return Jobs.deleteDeadLetterJobUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_DEAD_LETTER_JOB_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET_START = 's/Jobs/GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET_START'
export const GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET = 's/Jobs/GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET'


export function getDeadLetterJobStacktraceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET_START, meta: { info } })
    return Jobs.getDeadLetterJobStacktraceUsingGET(options)
      .then(response => dispatch({
        type: GET_DEAD_LETTER_JOB_STACKTRACE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_JOBS_START = 's/Jobs/LIST_JOBS_START'
export const LIST_JOBS = 's/Jobs/LIST_JOBS'


export function listJobs(options, info) {
  return dispatch => {
    dispatch({ type: LIST_JOBS_START, meta: { info } })
    return Jobs.listJobs(options)
      .then(response => dispatch({
        type: LIST_JOBS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_JOB_USING_GET_START = 's/Jobs/GET_JOB_USING_GET_START'
export const GET_JOB_USING_GET = 's/Jobs/GET_JOB_USING_GET'


export function getJobUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_JOB_USING_GET_START, meta: { info } })
    return Jobs.getJobUsingGET(options)
      .then(response => dispatch({
        type: GET_JOB_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_JOB_ACTION_USING_POST_START = 's/Jobs/EXECUTE_JOB_ACTION_USING_POST_START'
export const EXECUTE_JOB_ACTION_USING_POST = 's/Jobs/EXECUTE_JOB_ACTION_USING_POST'


export function executeJobActionUsingPOST(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_JOB_ACTION_USING_POST_START, meta: { info } })
    return Jobs.executeJobActionUsingPOST(actionRequest, options)
      .then(response => dispatch({
        type: EXECUTE_JOB_ACTION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_JOB_USING_DELETE_START = 's/Jobs/DELETE_JOB_USING_DELETE_START'
export const DELETE_JOB_USING_DELETE = 's/Jobs/DELETE_JOB_USING_DELETE'


export function deleteJobUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_JOB_USING_DELETE_START, meta: { info } })
    return Jobs.deleteJobUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_JOB_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_JOB_STACKTRACE_USING_GET_START = 's/Jobs/GET_JOB_STACKTRACE_USING_GET_START'
export const GET_JOB_STACKTRACE_USING_GET = 's/Jobs/GET_JOB_STACKTRACE_USING_GET'


export function getJobStacktraceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_JOB_STACKTRACE_USING_GET_START, meta: { info } })
    return Jobs.getJobStacktraceUsingGET(options)
      .then(response => dispatch({
        type: GET_JOB_STACKTRACE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_SUSPENDED_JOBS_START = 's/Jobs/LIST_SUSPENDED_JOBS_START'
export const LIST_SUSPENDED_JOBS = 's/Jobs/LIST_SUSPENDED_JOBS'


export function listSuspendedJobs(options, info) {
  return dispatch => {
    dispatch({ type: LIST_SUSPENDED_JOBS_START, meta: { info } })
    return Jobs.listSuspendedJobs(options)
      .then(response => dispatch({
        type: LIST_SUSPENDED_JOBS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_SUSPENDED_JOB_USING_GET_START = 's/Jobs/GET_SUSPENDED_JOB_USING_GET_START'
export const GET_SUSPENDED_JOB_USING_GET = 's/Jobs/GET_SUSPENDED_JOB_USING_GET'


export function getSuspendedJobUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_SUSPENDED_JOB_USING_GET_START, meta: { info } })
    return Jobs.getSuspendedJobUsingGET(options)
      .then(response => dispatch({
        type: GET_SUSPENDED_JOB_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_SUSPENDE_JOB_USING_DELETE_START = 's/Jobs/DELETE_SUSPENDE_JOB_USING_DELETE_START'
export const DELETE_SUSPENDE_JOB_USING_DELETE = 's/Jobs/DELETE_SUSPENDE_JOB_USING_DELETE'


export function deleteSuspendeJobUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_SUSPENDE_JOB_USING_DELETE_START, meta: { info } })
    return Jobs.deleteSuspendeJobUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_SUSPENDE_JOB_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_SUSPENDED_JOB_STACKTRACE_USING_GET_START = 's/Jobs/GET_SUSPENDED_JOB_STACKTRACE_USING_GET_START'
export const GET_SUSPENDED_JOB_STACKTRACE_USING_GET = 's/Jobs/GET_SUSPENDED_JOB_STACKTRACE_USING_GET'


export function getSuspendedJobStacktraceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_SUSPENDED_JOB_STACKTRACE_USING_GET_START, meta: { info } })
    return Jobs.getSuspendedJobStacktraceUsingGET(options)
      .then(response => dispatch({
        type: GET_SUSPENDED_JOB_STACKTRACE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_TIMER_JOBS_START = 's/Jobs/LIST_TIMER_JOBS_START'
export const LIST_TIMER_JOBS = 's/Jobs/LIST_TIMER_JOBS'


export function listTimerJobs(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TIMER_JOBS_START, meta: { info } })
    return Jobs.listTimerJobs(options)
      .then(response => dispatch({
        type: LIST_TIMER_JOBS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TIMER_JOB_USING_GET_START = 's/Jobs/GET_TIMER_JOB_USING_GET_START'
export const GET_TIMER_JOB_USING_GET = 's/Jobs/GET_TIMER_JOB_USING_GET'


export function getTimerJobUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TIMER_JOB_USING_GET_START, meta: { info } })
    return Jobs.getTimerJobUsingGET(options)
      .then(response => dispatch({
        type: GET_TIMER_JOB_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_TIMER_JOB_ACTION_USING_POST_START = 's/Jobs/EXECUTE_TIMER_JOB_ACTION_USING_POST_START'
export const EXECUTE_TIMER_JOB_ACTION_USING_POST = 's/Jobs/EXECUTE_TIMER_JOB_ACTION_USING_POST'


export function executeTimerJobActionUsingPOST(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_TIMER_JOB_ACTION_USING_POST_START, meta: { info } })
    return Jobs.executeTimerJobActionUsingPOST(actionRequest, options)
      .then(response => dispatch({
        type: EXECUTE_TIMER_JOB_ACTION_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TIMER_JOB_USING_DELETE_START = 's/Jobs/DELETE_TIMER_JOB_USING_DELETE_START'
export const DELETE_TIMER_JOB_USING_DELETE = 's/Jobs/DELETE_TIMER_JOB_USING_DELETE'


export function deleteTimerJobUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TIMER_JOB_USING_DELETE_START, meta: { info } })
    return Jobs.deleteTimerJobUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_TIMER_JOB_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TIMER_JOB_STACKTRACE_USING_GET_START = 's/Jobs/GET_TIMER_JOB_STACKTRACE_USING_GET_START'
export const GET_TIMER_JOB_STACKTRACE_USING_GET = 's/Jobs/GET_TIMER_JOB_STACKTRACE_USING_GET'


export function getTimerJobStacktraceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_TIMER_JOB_STACKTRACE_USING_GET_START, meta: { info } })
    return Jobs.getTimerJobStacktraceUsingGET(options)
      .then(response => dispatch({
        type: GET_TIMER_JOB_STACKTRACE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

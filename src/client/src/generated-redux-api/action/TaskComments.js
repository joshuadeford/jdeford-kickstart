/** @module action/TaskComments */
// Auto-generated, edits will be overwritten
import * as TaskComments from '../TaskComments'

export const LIST_TASK_COMMENTS_START = 's/TaskComments/LIST_TASK_COMMENTS_START'
export const LIST_TASK_COMMENTS = 's/TaskComments/LIST_TASK_COMMENTS'


export function listTaskComments(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASK_COMMENTS_START, meta: { info } })
    return TaskComments.listTaskComments(options)
      .then(response => dispatch({
        type: LIST_TASK_COMMENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_TASK_COMMENTS_START = 's/TaskComments/CREATE_TASK_COMMENTS_START'
export const CREATE_TASK_COMMENTS = 's/TaskComments/CREATE_TASK_COMMENTS'


export function createTaskComments(comment, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_TASK_COMMENTS_START, meta: { info } })
    return TaskComments.createTaskComments(comment, options)
      .then(response => dispatch({
        type: CREATE_TASK_COMMENTS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_COMMENT_START = 's/TaskComments/GET_TASK_COMMENT_START'
export const GET_TASK_COMMENT = 's/TaskComments/GET_TASK_COMMENT'


export function getTaskComment(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_COMMENT_START, meta: { info } })
    return TaskComments.getTaskComment(options)
      .then(response => dispatch({
        type: GET_TASK_COMMENT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TASK_COMMENT_START = 's/TaskComments/DELETE_TASK_COMMENT_START'
export const DELETE_TASK_COMMENT = 's/TaskComments/DELETE_TASK_COMMENT'


export function deleteTaskComment(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TASK_COMMENT_START, meta: { info } })
    return TaskComments.deleteTaskComment(options)
      .then(response => dispatch({
        type: DELETE_TASK_COMMENT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/profilecontroller */
// Auto-generated, edits will be overwritten
import * as profilecontroller from '../profilecontroller'

export const LIST_ALL_FORMS_OF_METADATA_USING_GET_START = 's/profilecontroller/LIST_ALL_FORMS_OF_METADATA_USING_GET_START'
export const LIST_ALL_FORMS_OF_METADATA_USING_GET = 's/profilecontroller/LIST_ALL_FORMS_OF_METADATA_USING_GET'


export function listAllFormsOfMetadataUsingGET(info) {
  return dispatch => {
    dispatch({ type: LIST_ALL_FORMS_OF_METADATA_USING_GET_START, meta: { info } })
    return profilecontroller.listAllFormsOfMetadataUsingGET()
      .then(response => dispatch({
        type: LIST_ALL_FORMS_OF_METADATA_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const PROFILE_OPTIONS_USING_OPTIONS_START = 's/profilecontroller/PROFILE_OPTIONS_USING_OPTIONS_START'
export const PROFILE_OPTIONS_USING_OPTIONS = 's/profilecontroller/PROFILE_OPTIONS_USING_OPTIONS'


export function profileOptionsUsingOPTIONS(info) {
  return dispatch => {
    dispatch({ type: PROFILE_OPTIONS_USING_OPTIONS_START, meta: { info } })
    return profilecontroller.profileOptionsUsingOPTIONS()
      .then(response => dispatch({
        type: PROFILE_OPTIONS_USING_OPTIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

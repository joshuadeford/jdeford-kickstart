/** @module action/DeviceEntity */
// Auto-generated, edits will be overwritten
import * as DeviceEntity from '../DeviceEntity'

export const FIND_ALL_DEVICE_USING_GET_START = 's/DeviceEntity/FIND_ALL_DEVICE_USING_GET_START'
export const FIND_ALL_DEVICE_USING_GET = 's/DeviceEntity/FIND_ALL_DEVICE_USING_GET'


export function findAllDeviceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_DEVICE_USING_GET_START, meta: { info } })
    return DeviceEntity.findAllDeviceUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_DEVICE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_DEVICE_USING_POST_START = 's/DeviceEntity/SAVE_DEVICE_USING_POST_START'
export const SAVE_DEVICE_USING_POST = 's/DeviceEntity/SAVE_DEVICE_USING_POST'


export function saveDeviceUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_DEVICE_USING_POST_START, meta: { info } })
    return DeviceEntity.saveDeviceUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_DEVICE_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_DEVICE_USING_GET_START = 's/DeviceEntity/FIND_ONE_DEVICE_USING_GET_START'
export const FIND_ONE_DEVICE_USING_GET = 's/DeviceEntity/FIND_ONE_DEVICE_USING_GET'


export function findOneDeviceUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_DEVICE_USING_GET_START, meta: { info } })
    return DeviceEntity.findOneDeviceUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_DEVICE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_DEVICE_USING_PUT_START = 's/DeviceEntity/SAVE_DEVICE_USING_PUT_START'
export const SAVE_DEVICE_USING_PUT = 's/DeviceEntity/SAVE_DEVICE_USING_PUT'


export function saveDeviceUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_DEVICE_USING_PUT_START, meta: { info } })
    return DeviceEntity.saveDeviceUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_DEVICE_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_DEVICE_USING_DELETE_START = 's/DeviceEntity/DELETE_DEVICE_USING_DELETE_START'
export const DELETE_DEVICE_USING_DELETE = 's/DeviceEntity/DELETE_DEVICE_USING_DELETE'


export function deleteDeviceUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_DEVICE_USING_DELETE_START, meta: { info } })
    return DeviceEntity.deleteDeviceUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_DEVICE_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_DEVICE_USING_PATCH_START = 's/DeviceEntity/SAVE_DEVICE_USING_PATCH_START'
export const SAVE_DEVICE_USING_PATCH = 's/DeviceEntity/SAVE_DEVICE_USING_PATCH'


export function saveDeviceUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_DEVICE_USING_PATCH_START, meta: { info } })
    return DeviceEntity.saveDeviceUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_DEVICE_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DEVICE_USER_USING_GET_START = 's/DeviceEntity/DEVICE_USER_USING_GET_START'
export const DEVICE_USER_USING_GET = 's/DeviceEntity/DEVICE_USER_USING_GET'


export function deviceUserUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: DEVICE_USER_USING_GET_START, meta: { info } })
    return DeviceEntity.deviceUserUsingGET(id)
      .then(response => dispatch({
        type: DEVICE_USER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DEVICE_USER_USING_POST_START = 's/DeviceEntity/DEVICE_USER_USING_POST_START'
export const DEVICE_USER_USING_POST = 's/DeviceEntity/DEVICE_USER_USING_POST'


export function deviceUserUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: DEVICE_USER_USING_POST_START, meta: { info } })
    return DeviceEntity.deviceUserUsingPOST(id, body)
      .then(response => dispatch({
        type: DEVICE_USER_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DEVICE_USER_USING_PUT_START = 's/DeviceEntity/DEVICE_USER_USING_PUT_START'
export const DEVICE_USER_USING_PUT = 's/DeviceEntity/DEVICE_USER_USING_PUT'


export function deviceUserUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: DEVICE_USER_USING_PUT_START, meta: { info } })
    return DeviceEntity.deviceUserUsingPUT(id, body)
      .then(response => dispatch({
        type: DEVICE_USER_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DEVICE_USER_USING_DELETE_START = 's/DeviceEntity/DEVICE_USER_USING_DELETE_START'
export const DEVICE_USER_USING_DELETE = 's/DeviceEntity/DEVICE_USER_USING_DELETE'


export function deviceUserUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DEVICE_USER_USING_DELETE_START, meta: { info } })
    return DeviceEntity.deviceUserUsingDELETE(id)
      .then(response => dispatch({
        type: DEVICE_USER_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DEVICE_USER_USING_PATCH_START = 's/DeviceEntity/DEVICE_USER_USING_PATCH_START'
export const DEVICE_USER_USING_PATCH = 's/DeviceEntity/DEVICE_USER_USING_PATCH'


export function deviceUserUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: DEVICE_USER_USING_PATCH_START, meta: { info } })
    return DeviceEntity.deviceUserUsingPATCH(id, body)
      .then(response => dispatch({
        type: DEVICE_USER_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

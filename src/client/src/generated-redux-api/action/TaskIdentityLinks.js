/** @module action/TaskIdentityLinks */
// Auto-generated, edits will be overwritten
import * as TaskIdentityLinks from '../TaskIdentityLinks'

export const LIST_TASKS_INSTANCE_IDENTITY_LINKS_START = 's/TaskIdentityLinks/LIST_TASKS_INSTANCE_IDENTITY_LINKS_START'
export const LIST_TASKS_INSTANCE_IDENTITY_LINKS = 's/TaskIdentityLinks/LIST_TASKS_INSTANCE_IDENTITY_LINKS'


export function listTasksInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_TASKS_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return TaskIdentityLinks.listTasksInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: LIST_TASKS_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_TASK_INSTANCE_IDENTITY_LINKS_START = 's/TaskIdentityLinks/CREATE_TASK_INSTANCE_IDENTITY_LINKS_START'
export const CREATE_TASK_INSTANCE_IDENTITY_LINKS = 's/TaskIdentityLinks/CREATE_TASK_INSTANCE_IDENTITY_LINKS'


export function createTaskInstanceIdentityLinks(identityLink, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_TASK_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return TaskIdentityLinks.createTaskInstanceIdentityLinks(identityLink, options)
      .then(response => dispatch({
        type: CREATE_TASK_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_IDENTITY_LINKS_FOR_FAMILY_START = 's/TaskIdentityLinks/LIST_IDENTITY_LINKS_FOR_FAMILY_START'
export const LIST_IDENTITY_LINKS_FOR_FAMILY = 's/TaskIdentityLinks/LIST_IDENTITY_LINKS_FOR_FAMILY'


export function listIdentityLinksForFamily(options, info) {
  return dispatch => {
    dispatch({ type: LIST_IDENTITY_LINKS_FOR_FAMILY_START, meta: { info } })
    return TaskIdentityLinks.listIdentityLinksForFamily(options)
      .then(response => dispatch({
        type: LIST_IDENTITY_LINKS_FOR_FAMILY,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_TASK_INSTANCE_IDENTITY_LINKS_START = 's/TaskIdentityLinks/GET_TASK_INSTANCE_IDENTITY_LINKS_START'
export const GET_TASK_INSTANCE_IDENTITY_LINKS = 's/TaskIdentityLinks/GET_TASK_INSTANCE_IDENTITY_LINKS'


export function getTaskInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: GET_TASK_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return TaskIdentityLinks.getTaskInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: GET_TASK_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_TASK_INSTANCE_IDENTITY_LINKS_START = 's/TaskIdentityLinks/DELETE_TASK_INSTANCE_IDENTITY_LINKS_START'
export const DELETE_TASK_INSTANCE_IDENTITY_LINKS = 's/TaskIdentityLinks/DELETE_TASK_INSTANCE_IDENTITY_LINKS'


export function deleteTaskInstanceIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_TASK_INSTANCE_IDENTITY_LINKS_START, meta: { info } })
    return TaskIdentityLinks.deleteTaskInstanceIdentityLinks(options)
      .then(response => dispatch({
        type: DELETE_TASK_INSTANCE_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

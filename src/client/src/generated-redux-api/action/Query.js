/** @module action/Query */
// Auto-generated, edits will be overwritten
import * as Query from '../Query'

export const QUERY_TASKS_START = 's/Query/QUERY_TASKS_START'
export const QUERY_TASKS = 's/Query/QUERY_TASKS'


export function queryTasks(request, info) {
  return dispatch => {
    dispatch({ type: QUERY_TASKS_START, meta: { info } })
    return Query.queryTasks(request)
      .then(response => dispatch({
        type: QUERY_TASKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/AssetOrderEntity */
// Auto-generated, edits will be overwritten
import * as AssetOrderEntity from '../AssetOrderEntity'

export const FIND_ALL_ASSET_ORDER_USING_GET_START = 's/AssetOrderEntity/FIND_ALL_ASSET_ORDER_USING_GET_START'
export const FIND_ALL_ASSET_ORDER_USING_GET = 's/AssetOrderEntity/FIND_ALL_ASSET_ORDER_USING_GET'


export function findAllAssetOrderUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: FIND_ALL_ASSET_ORDER_USING_GET_START, meta: { info } })
    return AssetOrderEntity.findAllAssetOrderUsingGET(options)
      .then(response => dispatch({
        type: FIND_ALL_ASSET_ORDER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_ORDER_USING_POST_START = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_POST_START'
export const SAVE_ASSET_ORDER_USING_POST = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_POST'


export function saveAssetOrderUsingPOST(body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_ORDER_USING_POST_START, meta: { info } })
    return AssetOrderEntity.saveAssetOrderUsingPOST(body)
      .then(response => dispatch({
        type: SAVE_ASSET_ORDER_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const FIND_ONE_ASSET_ORDER_USING_GET_START = 's/AssetOrderEntity/FIND_ONE_ASSET_ORDER_USING_GET_START'
export const FIND_ONE_ASSET_ORDER_USING_GET = 's/AssetOrderEntity/FIND_ONE_ASSET_ORDER_USING_GET'


export function findOneAssetOrderUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: FIND_ONE_ASSET_ORDER_USING_GET_START, meta: { info } })
    return AssetOrderEntity.findOneAssetOrderUsingGET(id)
      .then(response => dispatch({
        type: FIND_ONE_ASSET_ORDER_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_ORDER_USING_PUT_START = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_PUT_START'
export const SAVE_ASSET_ORDER_USING_PUT = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_PUT'


export function saveAssetOrderUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_ORDER_USING_PUT_START, meta: { info } })
    return AssetOrderEntity.saveAssetOrderUsingPUT(id, body)
      .then(response => dispatch({
        type: SAVE_ASSET_ORDER_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_ASSET_ORDER_USING_DELETE_START = 's/AssetOrderEntity/DELETE_ASSET_ORDER_USING_DELETE_START'
export const DELETE_ASSET_ORDER_USING_DELETE = 's/AssetOrderEntity/DELETE_ASSET_ORDER_USING_DELETE'


export function deleteAssetOrderUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: DELETE_ASSET_ORDER_USING_DELETE_START, meta: { info } })
    return AssetOrderEntity.deleteAssetOrderUsingDELETE(id)
      .then(response => dispatch({
        type: DELETE_ASSET_ORDER_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const SAVE_ASSET_ORDER_USING_PATCH_START = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_PATCH_START'
export const SAVE_ASSET_ORDER_USING_PATCH = 's/AssetOrderEntity/SAVE_ASSET_ORDER_USING_PATCH'


export function saveAssetOrderUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: SAVE_ASSET_ORDER_USING_PATCH_START, meta: { info } })
    return AssetOrderEntity.saveAssetOrderUsingPATCH(id, body)
      .then(response => dispatch({
        type: SAVE_ASSET_ORDER_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_ASSET_USING_GET_START = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_GET_START'
export const ASSET_ORDER_ASSET_USING_GET = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_GET'


export function assetOrderAssetUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_ASSET_USING_GET_START, meta: { info } })
    return AssetOrderEntity.assetOrderAssetUsingGET(id)
      .then(response => dispatch({
        type: ASSET_ORDER_ASSET_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_ASSET_USING_POST_START = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_POST_START'
export const ASSET_ORDER_ASSET_USING_POST = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_POST'


export function assetOrderAssetUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_ASSET_USING_POST_START, meta: { info } })
    return AssetOrderEntity.assetOrderAssetUsingPOST(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_ASSET_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_ASSET_USING_PUT_START = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_PUT_START'
export const ASSET_ORDER_ASSET_USING_PUT = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_PUT'


export function assetOrderAssetUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_ASSET_USING_PUT_START, meta: { info } })
    return AssetOrderEntity.assetOrderAssetUsingPUT(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_ASSET_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_ASSET_USING_DELETE_START = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_DELETE_START'
export const ASSET_ORDER_ASSET_USING_DELETE = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_DELETE'


export function assetOrderAssetUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_ASSET_USING_DELETE_START, meta: { info } })
    return AssetOrderEntity.assetOrderAssetUsingDELETE(id)
      .then(response => dispatch({
        type: ASSET_ORDER_ASSET_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_ASSET_USING_PATCH_START = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_PATCH_START'
export const ASSET_ORDER_ASSET_USING_PATCH = 's/AssetOrderEntity/ASSET_ORDER_ASSET_USING_PATCH'


export function assetOrderAssetUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_ASSET_USING_PATCH_START, meta: { info } })
    return AssetOrderEntity.assetOrderAssetUsingPATCH(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_ASSET_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_CREATOR_USING_GET_START = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_GET_START'
export const ASSET_ORDER_CREATOR_USING_GET = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_GET'


export function assetOrderCreatorUsingGET(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_CREATOR_USING_GET_START, meta: { info } })
    return AssetOrderEntity.assetOrderCreatorUsingGET(id)
      .then(response => dispatch({
        type: ASSET_ORDER_CREATOR_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_CREATOR_USING_POST_START = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_POST_START'
export const ASSET_ORDER_CREATOR_USING_POST = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_POST'


export function assetOrderCreatorUsingPOST(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_CREATOR_USING_POST_START, meta: { info } })
    return AssetOrderEntity.assetOrderCreatorUsingPOST(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_CREATOR_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_CREATOR_USING_PUT_START = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_PUT_START'
export const ASSET_ORDER_CREATOR_USING_PUT = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_PUT'


export function assetOrderCreatorUsingPUT(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_CREATOR_USING_PUT_START, meta: { info } })
    return AssetOrderEntity.assetOrderCreatorUsingPUT(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_CREATOR_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_CREATOR_USING_DELETE_START = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_DELETE_START'
export const ASSET_ORDER_CREATOR_USING_DELETE = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_DELETE'


export function assetOrderCreatorUsingDELETE(id, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_CREATOR_USING_DELETE_START, meta: { info } })
    return AssetOrderEntity.assetOrderCreatorUsingDELETE(id)
      .then(response => dispatch({
        type: ASSET_ORDER_CREATOR_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const ASSET_ORDER_CREATOR_USING_PATCH_START = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_PATCH_START'
export const ASSET_ORDER_CREATOR_USING_PATCH = 's/AssetOrderEntity/ASSET_ORDER_CREATOR_USING_PATCH'


export function assetOrderCreatorUsingPATCH(id, body, info) {
  return dispatch => {
    dispatch({ type: ASSET_ORDER_CREATOR_USING_PATCH_START, meta: { info } })
    return AssetOrderEntity.assetOrderCreatorUsingPATCH(id, body)
      .then(response => dispatch({
        type: ASSET_ORDER_CREATOR_USING_PATCH,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module action/ProcessDefinitions */
// Auto-generated, edits will be overwritten
import * as ProcessDefinitions from '../ProcessDefinitions'

export const LIST_PROCESS_DEFINITIONS_START = 's/ProcessDefinitions/LIST_PROCESS_DEFINITIONS_START'
export const LIST_PROCESS_DEFINITIONS = 's/ProcessDefinitions/LIST_PROCESS_DEFINITIONS'


export function listProcessDefinitions(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_DEFINITIONS_START, meta: { info } })
    return ProcessDefinitions.listProcessDefinitions(options)
      .then(response => dispatch({
        type: LIST_PROCESS_DEFINITIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_DEFINITION_USING_GET_START = 's/ProcessDefinitions/GET_PROCESS_DEFINITION_USING_GET_START'
export const GET_PROCESS_DEFINITION_USING_GET = 's/ProcessDefinitions/GET_PROCESS_DEFINITION_USING_GET'


export function getProcessDefinitionUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_DEFINITION_USING_GET_START, meta: { info } })
    return ProcessDefinitions.getProcessDefinitionUsingGET(options)
      .then(response => dispatch({
        type: GET_PROCESS_DEFINITION_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT_START = 's/ProcessDefinitions/EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT_START'
export const EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT = 's/ProcessDefinitions/EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT'


export function executeProcessDefinitionActionUsingPUT(actionRequest, options, info) {
  return dispatch => {
    dispatch({ type: EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT_START, meta: { info } })
    return ProcessDefinitions.executeProcessDefinitionActionUsingPUT(actionRequest, options)
      .then(response => dispatch({
        type: EXECUTE_PROCESS_DEFINITION_ACTION_USING_PUT,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_PROCESS_DEFINITION_DECISION_TABLES_START = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_DECISION_TABLES_START'
export const LIST_PROCESS_DEFINITION_DECISION_TABLES = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_DECISION_TABLES'


export function listProcessDefinitionDecisionTables(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_DEFINITION_DECISION_TABLES_START, meta: { info } })
    return ProcessDefinitions.listProcessDefinitionDecisionTables(options)
      .then(response => dispatch({
        type: LIST_PROCESS_DEFINITION_DECISION_TABLES,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_PROCESS_DEFINITION_FORM_DEFINITIONS_START = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_FORM_DEFINITIONS_START'
export const LIST_PROCESS_DEFINITION_FORM_DEFINITIONS = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_FORM_DEFINITIONS'


export function listProcessDefinitionFormDefinitions(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_DEFINITION_FORM_DEFINITIONS_START, meta: { info } })
    return ProcessDefinitions.listProcessDefinitionFormDefinitions(options)
      .then(response => dispatch({
        type: LIST_PROCESS_DEFINITION_FORM_DEFINITIONS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const LIST_PROCESS_DEFINITION_IDENTITY_LINKS_START = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_IDENTITY_LINKS_START'
export const LIST_PROCESS_DEFINITION_IDENTITY_LINKS = 's/ProcessDefinitions/LIST_PROCESS_DEFINITION_IDENTITY_LINKS'


export function listProcessDefinitionIdentityLinks(options, info) {
  return dispatch => {
    dispatch({ type: LIST_PROCESS_DEFINITION_IDENTITY_LINKS_START, meta: { info } })
    return ProcessDefinitions.listProcessDefinitionIdentityLinks(options)
      .then(response => dispatch({
        type: LIST_PROCESS_DEFINITION_IDENTITY_LINKS,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const CREATE_IDENTITY_LINK_USING_POST_START = 's/ProcessDefinitions/CREATE_IDENTITY_LINK_USING_POST_START'
export const CREATE_IDENTITY_LINK_USING_POST = 's/ProcessDefinitions/CREATE_IDENTITY_LINK_USING_POST'


export function createIdentityLinkUsingPOST(identityLink, options, info) {
  return dispatch => {
    dispatch({ type: CREATE_IDENTITY_LINK_USING_POST_START, meta: { info } })
    return ProcessDefinitions.createIdentityLinkUsingPOST(identityLink, options)
      .then(response => dispatch({
        type: CREATE_IDENTITY_LINK_USING_POST,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_IDENTITY_LINK_USING_GET_START = 's/ProcessDefinitions/GET_IDENTITY_LINK_USING_GET_START'
export const GET_IDENTITY_LINK_USING_GET = 's/ProcessDefinitions/GET_IDENTITY_LINK_USING_GET'


export function getIdentityLinkUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_IDENTITY_LINK_USING_GET_START, meta: { info } })
    return ProcessDefinitions.getIdentityLinkUsingGET(options)
      .then(response => dispatch({
        type: GET_IDENTITY_LINK_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const DELETE_IDENTITY_LINK_USING_DELETE_START = 's/ProcessDefinitions/DELETE_IDENTITY_LINK_USING_DELETE_START'
export const DELETE_IDENTITY_LINK_USING_DELETE = 's/ProcessDefinitions/DELETE_IDENTITY_LINK_USING_DELETE'


export function deleteIdentityLinkUsingDELETE(options, info) {
  return dispatch => {
    dispatch({ type: DELETE_IDENTITY_LINK_USING_DELETE_START, meta: { info } })
    return ProcessDefinitions.deleteIdentityLinkUsingDELETE(options)
      .then(response => dispatch({
        type: DELETE_IDENTITY_LINK_USING_DELETE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_MODEL_RESOURCE_USING_GET_START = 's/ProcessDefinitions/GET_MODEL_RESOURCE_USING_GET_START'
export const GET_MODEL_RESOURCE_USING_GET = 's/ProcessDefinitions/GET_MODEL_RESOURCE_USING_GET'


export function getModelResourceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_MODEL_RESOURCE_USING_GET_START, meta: { info } })
    return ProcessDefinitions.getModelResourceUsingGET(options)
      .then(response => dispatch({
        type: GET_MODEL_RESOURCE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_BPMN_MODEL_RESOURCE_START = 's/ProcessDefinitions/GET_BPMN_MODEL_RESOURCE_START'
export const GET_BPMN_MODEL_RESOURCE = 's/ProcessDefinitions/GET_BPMN_MODEL_RESOURCE'


export function getBpmnModelResource(options, info) {
  return dispatch => {
    dispatch({ type: GET_BPMN_MODEL_RESOURCE_START, meta: { info } })
    return ProcessDefinitions.getBpmnModelResource(options)
      .then(response => dispatch({
        type: GET_BPMN_MODEL_RESOURCE,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

export const GET_PROCESS_DEFINITION_RESOURCE_USING_GET_START = 's/ProcessDefinitions/GET_PROCESS_DEFINITION_RESOURCE_USING_GET_START'
export const GET_PROCESS_DEFINITION_RESOURCE_USING_GET = 's/ProcessDefinitions/GET_PROCESS_DEFINITION_RESOURCE_USING_GET'


export function getProcessDefinitionResourceUsingGET(options, info) {
  return dispatch => {
    dispatch({ type: GET_PROCESS_DEFINITION_RESOURCE_USING_GET_START, meta: { info } })
    return ProcessDefinitions.getProcessDefinitionResourceUsingGET(options)
      .then(response => dispatch({
        type: GET_PROCESS_DEFINITION_RESOURCE_USING_GET,
        payload: response.data,
        error: response.error,
        meta: {
          res: response.raw,
          info
        }
      }))
  }
}

/** @module HistoryTask */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List historic task instances
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] An id of the historic task instance.
 * @param {string} [options.processInstanceId] The process instance id of the historic task instance.
 * @param {string} [options.processDefinitionKey] The process definition key of the historic task instance.
 * @param {string} [options.processDefinitionKeyLike] The process definition key of the historic task instance, which matches the given value.
 * @param {string} [options.processDefinitionId] The process definition id of the historic task instance.
 * @param {string} [options.processDefinitionName] The process definition name of the historic task instance.
 * @param {string} [options.processDefinitionNameLike] The process definition name of the historic task instance, which matches the given value.
 * @param {string} [options.processBusinessKey] The process instance business key of the historic task instance.
 * @param {string} [options.processBusinessKeyLike] The process instance business key of the historic task instance that matches the given value.
 * @param {string} [options.executionId] The execution id of the historic task instance.
 * @param {string} [options.taskDefinitionKey] The task definition key for tasks part of a process
 * @param {string} [options.taskName] The task name of the historic task instance.
 * @param {string} [options.taskNameLike] The task name with like operator for the historic task instance.
 * @param {string} [options.taskDescription] The task description of the historic task instance.
 * @param {string} [options.taskDescriptionLike] The task description with like operator for the historic task instance.
 * @param {string} [options.taskCategory] Select tasks with the given category. Note that this is the task category, not the category of the process definition (namespace within the BPMN Xml).
 * @param {string} [options.taskDeleteReason] The task delete reason of the historic task instance.
 * @param {string} [options.taskDeleteReasonLike] The task delete reason with like operator for the historic task instance.
 * @param {string} [options.taskAssignee] The assignee of the historic task instance.
 * @param {string} [options.taskAssigneeLike] The assignee with like operator for the historic task instance.
 * @param {string} [options.taskOwner] The owner of the historic task instance.
 * @param {string} [options.taskOwnerLike] The owner with like operator for the historic task instance.
 * @param {string} [options.taskInvolvedUser] An involved user of the historic task instance
 * @param {string} [options.taskPriority] The priority of the historic task instance.
 * @param {boolean} [options.finished] Indication if the historic task instance is finished.
 * @param {boolean} [options.processFinished] Indication if the process instance of the historic task instance is finished.
 * @param {string} [options.parentTaskId] An optional parent task id of the historic task instance.
 * @param {string} [options.dueDate] Return only historic task instances that have a due date equal this date.
 * @param {string} [options.dueDateAfter] Return only historic task instances that have a due date after this date.
 * @param {string} [options.dueDateBefore] Return only historic task instances that have a due date before this date.
 * @param {boolean} [options.withoutDueDate] Return only historic task instances that have no due-date. When false is provided as value, this parameter is ignored.
 * @param {string} [options.taskCompletedOn] Return only historic task instances that have been completed on this date.
 * @param {string} [options.taskCompletedAfter] Return only historic task instances that have been completed after this date.
 * @param {string} [options.taskCompletedBefore] Return only historic task instances that have been completed before this date.
 * @param {string} [options.taskCreatedOn] Return only historic task instances that were created on this date.
 * @param {string} [options.taskCreatedBefore] Return only historic task instances that were created before this date.
 * @param {string} [options.taskCreatedAfter] Return only historic task instances that were created after this date.
 * @param {boolean} [options.includeTaskLocalVariables] An indication if the historic task instance local variables should be returned as well.
 * @param {boolean} [options.includeProcessVariables] An indication if the historic task instance global variables should be returned as well.
 * @param {string} [options.tenantId] Only return historic task instances with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return historic task instances with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns historic task instances without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @return {Promise<module:types.DataResponseOfHistoricTaskInstanceResponse>} Indicates that historic task instances could be queried.
 */
export function listHistoricTaskInstances(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      taskId: options.taskId,
      processInstanceId: options.processInstanceId,
      processDefinitionKey: options.processDefinitionKey,
      processDefinitionKeyLike: options.processDefinitionKeyLike,
      processDefinitionId: options.processDefinitionId,
      processDefinitionName: options.processDefinitionName,
      processDefinitionNameLike: options.processDefinitionNameLike,
      processBusinessKey: options.processBusinessKey,
      processBusinessKeyLike: options.processBusinessKeyLike,
      executionId: options.executionId,
      taskDefinitionKey: options.taskDefinitionKey,
      taskName: options.taskName,
      taskNameLike: options.taskNameLike,
      taskDescription: options.taskDescription,
      taskDescriptionLike: options.taskDescriptionLike,
      taskCategory: options.taskCategory,
      taskDeleteReason: options.taskDeleteReason,
      taskDeleteReasonLike: options.taskDeleteReasonLike,
      taskAssignee: options.taskAssignee,
      taskAssigneeLike: options.taskAssigneeLike,
      taskOwner: options.taskOwner,
      taskOwnerLike: options.taskOwnerLike,
      taskInvolvedUser: options.taskInvolvedUser,
      taskPriority: options.taskPriority,
      finished: options.finished,
      processFinished: options.processFinished,
      parentTaskId: options.parentTaskId,
      dueDate: options.dueDate,
      dueDateAfter: options.dueDateAfter,
      dueDateBefore: options.dueDateBefore,
      withoutDueDate: options.withoutDueDate,
      taskCompletedOn: options.taskCompletedOn,
      taskCompletedAfter: options.taskCompletedAfter,
      taskCompletedBefore: options.taskCompletedBefore,
      taskCreatedOn: options.taskCreatedOn,
      taskCreatedBefore: options.taskCreatedBefore,
      taskCreatedAfter: options.taskCreatedAfter,
      includeTaskLocalVariables: options.includeTaskLocalVariables,
      includeProcessVariables: options.includeProcessVariables,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId
    }
  }
  return gateway.request(listHistoricTaskInstancesOperation, parameters)
}

/**
 * Get a single historic task instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.HistoricTaskInstanceResponse>} Indicates that the historic task instances could be found.
 */
export function getTaskInstanceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(getTaskInstanceUsingGETOperation, parameters)
}

/**
 * Delete a historic task instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<object>} OK
 */
export function deleteTaskInstanceUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(deleteTaskInstanceUsingDELETEOperation, parameters)
}

/**
 * List identity links of a historic task instance
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.HistoricIdentityLinkResponse[]>} Indicates request was successful and the identity links are returned
 */
export function listHistoricTaskInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listHistoricTaskInstanceIdentityLinksOperation, parameters)
}

/**
 * All supported JSON parameter fields allowed are exactly the same as the parameters found for getting a collection of historic task instances, but passed in as JSON-body arguments rather than URL-parameters to allow for more advanced querying and preventing errors with request-uri’s that are too long. On top of that, the query allows for filtering based on process variables. The taskVariables and processVariables properties are JSON-arrays containing objects with the format as described here.
 * 
 * @param {module:types.HistoricTaskInstanceQueryRequest} queryRequest queryRequest
 * @return {Promise<module:types.DataResponseOfHistoricTaskInstanceResponse>} Indicates request was successful and the tasks are returned
 */
export function queryHistoricTaskInstance(queryRequest) {
  const parameters = {
    body: {
      queryRequest
    }
  }
  return gateway.request(queryHistoricTaskInstanceOperation, parameters)
}

const listHistoricTaskInstancesOperation = {
  path: '/history/historic-task-instances',
  method: 'get'
}

const getTaskInstanceUsingGETOperation = {
  path: '/history/historic-task-instances/{taskId}',
  method: 'get'
}

const deleteTaskInstanceUsingDELETEOperation = {
  path: '/history/historic-task-instances/{taskId}',
  method: 'delete'
}

const listHistoricTaskInstanceIdentityLinksOperation = {
  path: '/history/historic-task-instances/{taskId}/identitylinks',
  method: 'get'
}

const queryHistoricTaskInstanceOperation = {
  path: '/query/historic-task-instances',
  contentTypes: ['application/json'],
  method: 'post'
}

/** @module types */
// Auto-generated, edits will be overwritten

export interface Artifact {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Asset {
  assetDescription?: string;
  assetId?: string;
  assetModel?: string;
  assetModelYears?: string;
  assetName?: string;
  assetType?: string;
  availabilityStartDate?: Date;
  copyrightArea?: string;
  copyrightOwner?: string;
  copyrightUsageMedia?: string;
  description?: string;
  displayId?: string;
  expiryDate?: string;
  fileUrl?: string;
  id?: string;
  mediaId?: string;
  owner?: User;
  practicableCount?: string;
  variant?: string;
}

export interface AssetOrder {
  approved?: boolean;
  asset?: Asset;
  creator?: User;
  id?: string;
  status?: string;
}

export interface AttachmentResponse {
  contentUrl?: string;
  description?: string;
  /**
   * contentUrl:In case the attachment is a link to an external resource, the externalUrl contains the URL to the external content. If the attachment content is present in the Flowable engine, the contentUrl will contain an URL where the binary content can be streamed from.
   */
  externalUrl?: string;
  id?: string;
  name?: string;
  processInstanceUrl?: string;
  taskUrl?: string;
  time?: Date;
  /**
   * Can be any arbitrary value. When a valid formatted media-type (e.g. application/xml, text/plain) is included, the binary content HTTP response content-type will be set the the given value.
   */
  type?: string;
  url?: string;
  userId?: string;
}

export interface BaseElement {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface BpmnModel {
  dataStores?: {[key: string]: DataStore};
  definitionsAttributes?: {[key: string]: ExtensionAttribute[]};
  errors?: {[key: string]: string};
  flowLocationMap?: {[key: string]: GraphicInfo[]};
  globalArtifacts?: Artifact[];
  imports?: Import[];
  interfaces?: Interface[];
  itemDefinitions?: {[key: string]: ItemDefinition};
  labelLocationMap?: {[key: string]: GraphicInfo};
  locationMap?: {[key: string]: GraphicInfo};
  mainProcess?: Process;
  messageFlows?: {[key: string]: MessageFlow};
  messages?: Message[];
  namespaces?: {[key: string]: string};
  pools?: Pool[];
  processes?: Process[];
  resources?: Resource[];
  signals?: Signal[];
  sourceSystemId?: string;
  startEventFormTypes?: string[];
  targetNamespace?: string;
  userTaskFormTypes?: string[];
}

export interface CommentRequest {
  author?: string;
  id?: string;
  message?: string;
  saveProcessInstanceId?: boolean;
  type?: string;
  url?: string;
}

export interface CommentResponse {
  author?: string;
  id?: string;
  message?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  taskId?: string;
  taskUrl?: string;
  time?: Date;
}

export interface ComponentConfiguration {
  componentName?: string;
  id?: string;
  properties?: ComponentProperty[];
  version?: number;
}

export interface ComponentProperty {
  configuration?: ComponentConfiguration;
  id?: string;
  name?: string;
  value?: string;
}

export interface DataResponseOfDeploymentResponse {
  data?: DeploymentResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfEventSubscriptionResponse {
  data?: EventSubscriptionResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfExecutionResponse {
  data?: ExecutionResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfGroupResponse {
  data?: GroupResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfHistoricActivityInstanceResponse {
  data?: HistoricActivityInstanceResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfHistoricDetailResponse {
  data?: HistoricDetailResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfHistoricProcessInstanceResponse {
  data?: HistoricProcessInstanceResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfHistoricTaskInstanceResponse {
  data?: HistoricTaskInstanceResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfHistoricVariableInstanceResponse {
  data?: HistoricVariableInstanceResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfJobResponse {
  data?: JobResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfListOfMapOfstringAndobject {
  // data?: MapOfstringAndobject[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfModelResponse {
  data?: ModelResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfProcessDefinitionResponse {
  data?: ProcessDefinitionResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfProcessInstanceResponse {
  data?: ProcessInstanceResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfTaskResponse {
  data?: TaskResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataResponseOfUserResponse {
  data?: UserResponse[];
  order?: string;
  size?: number;
  sort?: string;
  start?: number;
  total?: number;
}

export interface DataSpec {
  attributes?: {[key: string]: ExtensionAttribute[]};
  collection?: boolean;
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  itemSubjectRef?: string;
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface DataStore {
  attributes?: {[key: string]: ExtensionAttribute[]};
  dataState?: string;
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  itemSubjectRef?: string;
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface DecisionTableResponse {
  category?: string;
  deploymentId?: string;
  description?: string;
  id?: string;
  key?: string;
  name?: string;
  parentDeploymentId?: string;
  resourceName?: string;
  tenantId?: string;
  url?: string;
  version?: number;
}

export interface DeploymentResourceResponse {
  contentUrl?: string;
  id?: string;
  /**
   * Contains the media-type the resource has. This is resolved using a (pluggable) MediaTypeResolver and contains, by default, a limited number of mime-type mappings.
   */
  mediaType?: string;
  /**
   * Type of resource
   */
  type?: "resource"|"processDefinition"|"processImage";
  /**
   * For a single resource contains the actual URL to use for retrieving the binary resource
   */
  url?: string;
}

export interface DeploymentResponse {
  category?: string;
  deploymentTime?: Date;
  id?: string;
  name?: string;
  tenantId?: string;
  url?: string;
}

export interface Device {
  id?: string;
  modelName?: string;
  name?: string;
  user?: User;
}

export interface EventListener {
  attributes?: {[key: string]: ExtensionAttribute[]};
  entityType?: string;
  events?: string;
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  implementation?: string;
  implementationType?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface EventResponse {
  action?: string;
  id?: string;
  message?: string[];
  processInstanceUrl?: string;
  taskUrl?: string;
  time?: Date;
  url?: string;
  userId?: string;
}

export interface EventSubscriptionResponse {
  activityId?: string;
  configuration?: string;
  created?: Date;
  eventName?: string;
  eventType?: string;
  executionId?: string;
  executionUrl?: string;
  id?: string;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  tenantId?: string;
  url?: string;
}

export interface ExecutionActionRequest {
  /**
   * Action to perform: Either signal, trigger, signalEventReceived or messageEventReceived
   */
  action: string;
  /**
   * Message of the signal
   */
  messageName?: string;
  /**
   * Name of the signal
   */
  signalName?: string;
  transientVariables?: RestVariable[];
  variables?: RestVariable[];
}

export interface ExecutionChangeActivityStateRequest {
  /**
   * activityId to be canceled
   */
  cancelActivityId?: string;
  /**
   * activityId to be started
   */
  startActivityId?: string;
}

export interface ExecutionQueryRequest {
  activityId?: string;
  id?: string;
  messageEventSubscriptionName?: string;
  order?: string;
  parentId?: string;
  processBusinessKey?: string;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  processInstanceId?: string;
  processInstanceVariables?: QueryVariable[];
  signalEventSubscriptionName?: string;
  size?: number;
  sort?: string;
  start?: number;
  tenantId?: string;
  tenantIdLike?: string;
  variables?: QueryVariable[];
  withoutTenantId?: boolean;
}

export interface ExecutionResponse {
  activityId?: string;
  id?: string;
  parentId?: string;
  parentUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  superExecutionId?: string;
  superExecutionUrl?: string;
  suspended?: boolean;
  tenantId?: string;
  url?: string;
}

export interface ExtensionAttribute {
  name?: string;
  namespace?: string;
  namespacePrefix?: string;
  value?: string;
}

export interface ExtensionElement {
  attributes?: {[key: string]: ExtensionAttribute[]};
  childElements?: {[key: string]: ExtensionElement[]};
  elementText?: string;
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  name?: string;
  namespace?: string;
  namespacePrefix?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface FieldExtension {
  attributes?: {[key: string]: ExtensionAttribute[]};
  expression?: string;
  extensionElements?: {[key: string]: ExtensionElement[]};
  fieldName?: string;
  id?: string;
  stringValue?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface FlowElement {
  attributes?: {[key: string]: ExtensionAttribute[]};
  documentation?: string;
  executionListeners?: FlowableListener[];
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface FlowableListener {
  attributes?: {[key: string]: ExtensionAttribute[]};
  customPropertiesResolverImplementation?: string;
  customPropertiesResolverImplementationType?: string;
  event?: string;
  extensionElements?: {[key: string]: ExtensionElement[]};
  fieldExtensions?: FieldExtension[];
  id?: string;
  implementation?: string;
  implementationType?: string;
  onTransaction?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface FormDataResponse {
  deploymentId?: string;
  formKey?: string;
  formProperties?: RestFormProperty[];
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  taskId?: string;
  taskUrl?: string;
}

export interface FormDefinitionResponse {
  category?: string;
  deploymentId?: string;
  description?: string;
  id?: string;
  key?: string;
  name?: string;
  resourceName?: string;
  tenantId?: string;
  url?: string;
  version?: number;
}

export interface GraphicInfo {
  element?: BaseElement;
  expanded?: boolean;
  height?: number;
  width?: number;
  x?: number;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
  y?: number;
}

export interface GroupRequest {
  id?: string;
  name?: string;
  type?: string;
  url?: string;
}

export interface GroupResponse {
  id?: string;
  name?: string;
  type?: string;
  url?: string;
}

export interface HistoricActivityInstanceQueryRequest {
  activityId?: string;
  activityInstanceId?: string;
  activityName?: string;
  activityType?: string;
  executionId?: string;
  finished?: boolean;
  order?: string;
  processDefinitionId?: string;
  processInstanceId?: string;
  size?: number;
  sort?: string;
  start?: number;
  taskAssignee?: string;
  tenantId?: string;
  tenantIdLike?: string;
  withoutTenantId?: boolean;
}

export interface HistoricActivityInstanceResponse {
  activityId?: string;
  activityName?: string;
  activityType?: string;
  assignee?: string;
  calledProcessInstanceId?: string;
  durationInMillis?: number;
  endTime?: Date;
  executionId?: string;
  id?: string;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  startTime?: Date;
  taskId?: string;
  tenantId?: string;
}

export interface HistoricDetailQueryRequest {
  activityInstanceId?: string;
  executionId?: string;
  id?: string;
  order?: string;
  processInstanceId?: string;
  selectOnlyFormProperties?: boolean;
  selectOnlyVariableUpdates?: boolean;
  size?: number;
  sort?: string;
  start?: number;
  taskId?: string;
}

export interface HistoricDetailResponse {
  activityInstanceId?: string;
  detailType?: string;
  executionId?: string;
  id?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  propertyId?: string;
  propertyValue?: string;
  revision?: number;
  taskId?: string;
  taskUrl?: string;
  time?: Date;
  variable?: RestVariable;
}

export interface HistoricIdentityLinkResponse {
  groupId?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  taskId?: string;
  taskUrl?: string;
  type?: string;
  userId?: string;
}

export interface HistoricProcessInstanceQueryRequest {
  excludeSubprocesses?: boolean;
  finished?: boolean;
  finishedAfter?: Date;
  finishedBefore?: Date;
  includeProcessVariables?: boolean;
  involvedUser?: string;
  order?: string;
  processBusinessKey?: string;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  processInstanceId?: string;
  processInstanceIds?: string[];
  size?: number;
  sort?: string;
  start?: number;
  startedAfter?: Date;
  startedBefore?: Date;
  startedBy?: string;
  superProcessInstanceId?: string;
  tenantId?: string;
  tenantIdLike?: string;
  variables?: QueryVariable[];
  withoutTenantId?: boolean;
}

export interface HistoricProcessInstanceResponse {
  businessKey?: string;
  deleteReason?: string;
  durationInMillis?: number;
  endActivityId?: string;
  endTime?: Date;
  id?: string;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  startActivityId?: string;
  startTime?: Date;
  startUserId?: string;
  superProcessInstanceId?: string;
  tenantId?: string;
  url?: string;
  variables?: RestVariable[];
}

export interface HistoricTaskInstanceQueryRequest {
  dueDate?: Date;
  dueDateAfter?: Date;
  dueDateBefore?: Date;
  executionId?: string;
  finished?: boolean;
  includeProcessVariables?: boolean;
  includeTaskLocalVariables?: boolean;
  order?: string;
  parentTaskId?: string;
  processBusinessKey?: string;
  processBusinessKeyLike?: string;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  processDefinitionKeyLike?: string;
  processDefinitionName?: string;
  processDefinitionNameLike?: string;
  processFinished?: boolean;
  processInstanceId?: string;
  processVariables?: QueryVariable[];
  size?: number;
  sort?: string;
  start?: number;
  taskAssignee?: string;
  taskAssigneeLike?: string;
  taskCandidateGroup?: string;
  taskCategory?: string;
  taskCompletedAfter?: Date;
  taskCompletedBefore?: Date;
  taskCompletedOn?: Date;
  taskCreatedAfter?: Date;
  taskCreatedBefore?: Date;
  taskCreatedOn?: Date;
  taskDefinitionKey?: string;
  taskDefinitionKeyLike?: string;
  taskDeleteReason?: string;
  taskDeleteReasonLike?: string;
  taskDescription?: string;
  taskDescriptionLike?: string;
  taskId?: string;
  taskInvolvedUser?: string;
  taskMaxPriority?: number;
  taskMinPriority?: number;
  taskName?: string;
  taskNameLike?: string;
  taskOwner?: string;
  taskOwnerLike?: string;
  taskPriority?: number;
  taskVariables?: QueryVariable[];
  tenantId?: string;
  tenantIdLike?: string;
  withoutDueDate?: boolean;
  withoutTenantId?: boolean;
}

export interface HistoricTaskInstanceResponse {
  assignee?: string;
  category?: string;
  claimTime?: Date;
  deleteReason?: string;
  description?: string;
  dueDate?: Date;
  durationInMillis?: number;
  endTime?: Date;
  executionId?: string;
  formKey?: string;
  id?: string;
  name?: string;
  owner?: string;
  parentTaskId?: string;
  priority?: number;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  startTime?: Date;
  taskDefinitionKey?: string;
  tenantId?: string;
  url?: string;
  variables?: RestVariable[];
  workTimeInMillis?: number;
}

export interface HistoricVariableInstanceQueryRequest {
  excludeTaskVariables?: boolean;
  executionId?: string;
  processInstanceId?: string;
  taskId?: string;
  variableName?: string;
  variableNameLike?: string;
  variables?: QueryVariable[];
}

export interface HistoricVariableInstanceResponse {
  id?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  taskId?: string;
  variable?: RestVariable;
}

export interface IOSpecification {
  attributes?: {[key: string]: ExtensionAttribute[]};
  dataInputRefs?: string[];
  dataInputs?: DataSpec[];
  dataOutputRefs?: string[];
  dataOutputs?: DataSpec[];
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Import {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  importType?: string;
  location?: string;
  namespace?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Interface {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  implementationRef?: string;
  name?: string;
  operations?: Operation[];
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface ItemDefinition {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  itemKind?: string;
  structureRef?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface JobResponse {
  createTime?: Date;
  dueDate?: Date;
  exceptionMessage?: string;
  executionId?: string;
  executionUrl?: string;
  id?: string;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  retries?: number;
  tenantId?: string;
  url?: string;
}

export interface Lane {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  flowReferences?: string[];
  id?: string;
  name?: string;
  parentProcess?: Process;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Link {
  href?: string;
  rel?: string;
  templated?: boolean;
}

export interface MembershipRequest {
  userId?: string;
}

export interface MembershipResponse {
  groupId?: string;
  url?: string;
  userId?: string;
}

export interface Message {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  itemRef?: string;
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface MessageFlow {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  messageRef?: string;
  name?: string;
  sourceRef?: string;
  targetRef?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface ModelAndView {
  empty?: boolean;
  model?: any;
  modelMap?: {[key: string]: any};
  reference?: boolean;
  status?: "100"|"101"|"102"|"103"|"200"|"201"|"202"|"203"|"204"|"205"|"206"|"207"|"208"|"226"|"300"|"301"|"302"|"303"|"304"|"305"|"307"|"308"|"400"|"401"|"402"|"403"|"404"|"405"|"406"|"407"|"408"|"409"|"410"|"411"|"412"|"413"|"414"|"415"|"416"|"417"|"418"|"419"|"420"|"421"|"422"|"423"|"424"|"426"|"428"|"429"|"431"|"451"|"500"|"501"|"502"|"503"|"504"|"505"|"506"|"507"|"508"|"509"|"510"|"511";
  view?: View;
  viewName?: string;
}

export interface ModelRequest {
  category?: string;
  deploymentId?: string;
  key?: string;
  metaInfo?: string;
  name?: string;
  tenantId?: string;
  version?: number;
}

export interface ModelResponse {
  category?: string;
  createTime?: Date;
  deploymentId?: string;
  deploymentUrl?: string;
  id?: string;
  key?: string;
  lastUpdateTime?: Date;
  metaInfo?: string;
  name?: string;
  sourceExtraUrl?: string;
  sourceUrl?: string;
  tenantId?: string;
  url?: string;
  version?: number;
}

export interface ModelVariant {
  brand?: string;
  id?: string;
  model?: string;
  variant?: string;
}

export interface Operation {
  attributes?: {[key: string]: ExtensionAttribute[]};
  errorMessageRef?: string[];
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  implementationRef?: string;
  inMessageRef?: string;
  name?: string;
  outMessageRef?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Pool {
  attributes?: {[key: string]: ExtensionAttribute[]};
  executable?: boolean;
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  name?: string;
  processRef?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface Process {
  artifacts?: Artifact[];
  attributes?: {[key: string]: ExtensionAttribute[]};
  candidateStarterGroups?: string[];
  candidateStarterUsers?: string[];
  dataObjects?: ValuedDataObject[];
  documentation?: string;
  eventListeners?: EventListener[];
  executable?: boolean;
  executionListeners?: FlowableListener[];
  extensionElements?: {[key: string]: ExtensionElement[]};
  flowElementMap?: {[key: string]: FlowElement};
  flowElements?: FlowElement[];
  id?: string;
  initialFlowElement?: FlowElement;
  ioSpecification?: IOSpecification;
  lanes?: Lane[];
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface ProcessDefinitionActionRequest {
  /**
   * Action to perform: Either activate or suspend
   */
  action: string;
  category?: string;
  /**
   * Date (ISO-8601) when the suspension/activation should be executed. If omitted, the suspend/activation is effective immediately.
   */
  date?: Date;
  /**
   * Whether or not to suspend/activate running process-instances for this process-definition. If omitted, the process-instances are left in the state they are
   */
  includeProcessInstances?: boolean;
}

export interface ProcessDefinitionResponse {
  category?: string;
  deploymentId?: string;
  deploymentUrl?: string;
  description?: string;
  /**
   * Contains a graphical representation of the process, null when no diagram is available.
   */
  diagramResource?: string;
  /**
   * Indicates the process definition contains graphical information (BPMN DI).
   */
  graphicalNotationDefined?: boolean;
  id?: string;
  key?: string;
  name?: string;
  /**
   * Contains the actual deployed BPMN 2.0 xml.
   */
  resource?: string;
  startFormDefined?: boolean;
  suspended?: boolean;
  tenantId?: string;
  url?: string;
  version?: number;
}

export interface ProcessEngineInfoResponse {
  exception?: string;
  name?: string;
  resourceUrl?: string;
  version?: string;
}

export interface ProcessInstanceActionRequest {
  /**
   * Action to perform: Either activate or suspend
   */
  action: string;
}

/**
 * Only one of processDefinitionId, processDefinitionKey or message can be used in the request body
 */
export interface ProcessInstanceCreateRequest {
  businessKey?: string;
  message?: string;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  returnVariables?: boolean;
  tenantId?: string;
  transientVariables?: RestVariable[];
  variables?: RestVariable[];
}

export interface ProcessInstanceQueryRequest {
  excludeSubprocesses?: boolean;
  includeProcessVariables?: boolean;
  involvedUser?: string;
  order?: string;
  processBusinessKey?: string;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  processInstanceId?: string;
  size?: number;
  sort?: string;
  start?: number;
  subProcessInstanceId?: string;
  superProcessInstanceId?: string;
  suspended?: boolean;
  tenantId?: string;
  tenantIdLike?: string;
  variables?: QueryVariable[];
  withoutTenantId?: boolean;
}

export interface ProcessInstanceResponse {
  activityId?: string;
  businessKey?: string;
  completed?: boolean;
  ended?: boolean;
  id?: string;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  suspended?: boolean;
  tenantId?: string;
  url?: string;
  variables?: RestVariable[];
}

export interface QueryVariable {
  name?: string;
  operation?: string;
  type?: string;
  value?: any;
}

export interface Resource {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  name?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface ResourceOfAsset {
  assetDescription?: string;
  assetId?: string;
  assetModel?: string;
  assetModelYears?: string;
  assetName?: string;
  assetType?: string;
  availabilityStartDate?: Date;
  copyrightArea?: string;
  copyrightOwner?: string;
  copyrightUsageMedia?: string;
  description?: string;
  displayId?: string;
  expiryDate?: string;
  fileUrl?: string;
  id?: string;
  links?: Link[];
  mediaId?: string;
  owner?: User;
  practicableCount?: string;
  variant?: string;
}

export interface ResourceOfAssetOrder {
  approved?: boolean;
  asset?: Asset;
  creator?: User;
  id?: string;
  links?: Link[];
  status?: string;
}

export interface ResourceOfComponentConfiguration {
  componentName?: string;
  id?: string;
  links?: Link[];
  properties?: ComponentProperty[];
  version?: number;
}

export interface ResourceOfComponentProperty {
  configuration?: ComponentConfiguration;
  id?: string;
  links?: Link[];
  name?: string;
  value?: string;
}

export interface ResourceOfDevice {
  id?: string;
  links?: Link[];
  modelName?: string;
  name?: string;
  user?: User;
}

export interface ResourceOfModelVariant {
  brand?: string;
  id?: string;
  links?: Link[];
  model?: string;
  variant?: string;
}

export interface ResourceOfRole {
  id?: string;
  links?: Link[];
  name?: string;
  users?: User[];
}

export interface ResourceOfUser {
  devices?: Device[];
  dob?: string;
  email?: string;
  id?: string;
  links?: Link[];
  password?: string;
  role?: string;
  roles?: Role[];
  salary?: number;
  userName?: string;
}

export interface ResourceSupport {
  links?: Link[];
}

export interface ResourcesOfAsset {
  content?: Asset[];
  links?: Link[];
}

export interface ResourcesOfAssetOrder {
  content?: AssetOrder[];
  links?: Link[];
}

export interface ResourcesOfComponentConfiguration {
  content?: ComponentConfiguration[];
  links?: Link[];
}

export interface ResourcesOfComponentProperty {
  content?: ComponentProperty[];
  links?: Link[];
}

export interface ResourcesOfDevice {
  content?: Device[];
  links?: Link[];
}

export interface ResourcesOfListOfAsset {
  content?: Asset[][];
  links?: Link[];
}

export interface ResourcesOfListOfModelVariant {
  content?: ModelVariant[][];
  links?: Link[];
}

export interface ResourcesOfListOfUser {
  content?: User[][];
  links?: Link[];
}

export interface ResourcesOfModelVariant {
  content?: ModelVariant[];
  links?: Link[];
}

export interface ResourcesOfRole {
  content?: Role[];
  links?: Link[];
}

export interface ResourcesOfUser {
  content?: User[];
  links?: Link[];
}

export interface RestActionRequest {
  action?: string;
}

export interface RestEnumFormProperty {
  id?: string;
  name?: string;
}

export interface RestFormProperty {
  datePattern?: string;
  enumValues?: RestEnumFormProperty[];
  id?: string;
  name?: string;
  readable?: boolean;
  required?: boolean;
  type?: string;
  value?: string;
  writable?: boolean;
}

export interface RestIdentityLink {
  group?: string;
  type?: string;
  url?: string;
  user?: string;
}

export interface RestVariable {
  /**
   * Name of the variable
   */
  name?: string;
  scope?: string;
  /**
   * Type of the variable.
   */
  type?: string;
  /**
   * Value of the variable.
   */
  value?: any;
  /**
   * When reading a variable of type binary or serializable, this attribute will point to the URL where the raw binary data can be fetched from.
   */
  valueUrl?: string;
}

export interface Role {
  id?: string;
  name?: string;
  users?: User[];
}

export interface Signal {
  attributes?: {[key: string]: ExtensionAttribute[]};
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  name?: string;
  scope?: string;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface SignalEventReceivedRequest {
  /**
   * If true, handling of the signal will happen asynchronously. Return code will be 202 - Accepted to indicate the request is accepted but not yet executed. If false,
   *                       handling the signal will be done immediately and result (200 - OK) will only return after this completed successfully. Defaults to false if omitted.
   */
  async?: boolean;
  /**
   * Name of the signal
   */
  signalName?: string;
  /**
   * ID of the tenant that the signal event should be processed in
   */
  tenantId?: string;
  /**
   * Array of variables (in the general variables format) to use as payload to pass along with the signal. Cannot be used in case async is set to true, this will result in an error.
   */
  variables?: RestVariable[];
}

export interface SubmitFormRequest {
  action?: string;
  businessKey?: string;
  processDefinitionId?: string;
  properties?: RestFormProperty[];
  taskId?: string;
}

export interface TableMetaData {
  columnNames?: string[];
  columnTypes?: string[];
  tableName?: string;
}

export interface TableResponse {
  count?: number;
  name?: string;
  url?: string;
}

export interface TaskActionRequest {
  /**
   * Action to perform: Either complete, claim, delegate or resolve
   */
  action: string;
  /**
   * If action is claim or delegate, you can use this parameter to set the assignee associated
   */
  assignee?: string;
  /**
   * If action is complete, you can use this parameter to set transient variables
   */
  transientVariables?: RestVariable[];
  /**
   * If action is complete, you can use this parameter to set variables
   */
  variables?: RestVariable[];
}

export interface TaskQueryRequest {
  active?: boolean;
  assignee?: string;
  assigneeLike?: string;
  candidateGroup?: string;
  candidateGroupIn?: string[];
  candidateOrAssigned?: string;
  candidateUser?: string;
  category?: string;
  createdAfter?: Date;
  createdBefore?: Date;
  createdOn?: Date;
  delegationState?: string;
  description?: string;
  descriptionLike?: string;
  dueAfter?: Date;
  dueBefore?: Date;
  dueDate?: Date;
  excludeSubTasks?: boolean;
  executionId?: string;
  includeProcessVariables?: boolean;
  includeTaskLocalVariables?: boolean;
  involvedUser?: string;
  maximumPriority?: number;
  minimumPriority?: number;
  name?: string;
  nameLike?: string;
  order?: string;
  owner?: string;
  ownerLike?: string;
  priority?: number;
  processDefinitionId?: string;
  processDefinitionKey?: string;
  processDefinitionKeyLike?: string;
  processDefinitionName?: string;
  processDefinitionNameLike?: string;
  processInstanceBusinessKey?: string;
  processInstanceBusinessKeyLike?: string;
  processInstanceId?: string;
  processInstanceVariables?: QueryVariable[];
  size?: number;
  sort?: string;
  start?: number;
  taskDefinitionKey?: string;
  taskDefinitionKeyLike?: string;
  taskVariables?: QueryVariable[];
  tenantId?: string;
  tenantIdLike?: string;
  unassigned?: boolean;
  withoutDueDate?: boolean;
  withoutTenantId?: boolean;
}

export interface TaskRequest {
  assignee?: string;
  assigneeSet?: boolean;
  category?: string;
  categorySet?: boolean;
  delegationState?: string;
  delegationStateSet?: boolean;
  description?: string;
  descriptionSet?: boolean;
  dueDate?: Date;
  duedateSet?: boolean;
  formKey?: string;
  formKeySet?: boolean;
  name?: string;
  nameSet?: boolean;
  owner?: string;
  ownerSet?: boolean;
  parentTaskId?: string;
  parentTaskIdSet?: boolean;
  priority?: number;
  prioritySet?: boolean;
  tenantId?: string;
  tenantIdSet?: boolean;
}

export interface TaskResponse {
  assignee?: string;
  category?: string;
  createTime?: Date;
  /**
   * Delegation-state of the task, can be null, "pending" or "resolved".
   */
  delegationState?: string;
  description?: string;
  dueDate?: Date;
  executionId?: string;
  executionUrl?: string;
  formKey?: string;
  id?: string;
  name?: string;
  owner?: string;
  parentTaskId?: string;
  parentTaskUrl?: string;
  priority?: number;
  processDefinitionId?: string;
  processDefinitionUrl?: string;
  processInstanceId?: string;
  processInstanceUrl?: string;
  suspended?: boolean;
  taskDefinitionKey?: string;
  tenantId?: string;
  url?: string;
  variables?: RestVariable[];
}

export interface User {
  devices?: Device[];
  dob?: string;
  email?: string;
  id?: string;
  password?: string;
  role?: string;
  roles?: Role[];
  salary?: number;
  userName?: string;
}

export interface UserInfoRequest {
  key?: string;
  value?: string;
}

export interface UserInfoResponse {
  key?: string;
  url?: string;
  value?: string;
}

export interface UserRequest {
  email?: string;
  firstName?: string;
  id?: string;
  lastName?: string;
  password?: string;
  pictureUrl?: string;
  url?: string;
}

export interface UserResponse {
  email?: string;
  firstName?: string;
  id?: string;
  lastName?: string;
  password?: string;
  pictureUrl?: string;
  url?: string;
}

export interface ValuedDataObject {
  attributes?: {[key: string]: ExtensionAttribute[]};
  documentation?: string;
  executionListeners?: FlowableListener[];
  extensionElements?: {[key: string]: ExtensionElement[]};
  id?: string;
  itemSubjectRef?: ItemDefinition;
  name?: string;
  type?: string;
  value?: any;
  xmlColumnNumber?: number;
  xmlRowNumber?: number;
}

export interface View {
  contentType?: string;
}

export interface OpenApiSpec {
  host: string;
  basePath: string;
  schemes: string[];
  contentTypes: string[];
  accepts: string[];
  securityDefinitions?: {[key: string]: SecurityDefinition};
}

export interface SecurityDefinition {
  type: "basic"|"apiKey"|"oauth2";
  description?: string;
  name?: string;
  in?: "query"|"header";
  flow?: "implicit"|"password"|"application"|"accessCode";
  authorizationUrl?: string;
  tokenUrl?: string;
  scopes?: {[key: string]: string};
}

export type CollectionFormat = "csv"|"ssv"|"tsv"|"pipes"|"multi";
export type HttpMethod = "get"|"put"|"post"|"delete"|"options"|"head"|"patch";

export interface OperationInfo {
  path: string;
  method: HttpMethod;
  security?: OperationSecurity[];
  contentTypes?: string[];
  accepts?: string[];
}

export interface OperationSecurity {
  id: string;
  scopes?: string[];
}

export interface OperationParamGroups {
  header?: {[key: string]: string};
  path?: {[key: string]: string|number|boolean};
  query?: {[key: string]: string|string[]|number|boolean};
  formData?: {[key: string]: string|number|boolean};
  body?: any;
}

export interface ServiceRequest {
  method: HttpMethod;
  url: string;
  headers: { [index: string]: string };
  body: any;
}

export interface RequestInfo {
  baseUrl: string;
  parameters: OperationParamGroups;
}

export interface ResponseOutcome {
  retry?: boolean;
  res: Response<any>;
}

export interface ServiceOptions {
  /**
   * The service url.
   *
   * If not specified then defaults to the one defined in the Open API
   * spec used to generate the service api.
   */
  url?: string;
  fetchOptions?: any;
  authorizationHeader?: string;
  getAuthorization?(security: OperationSecurity, securityDefinitions: any, op: OperationInfo): Promise<OperationRights>;
  formatServiceError?(response: FetchResponse, data: any): ServiceError;
  processRequest?(op: OperationInfo, reqInfo: RequestInfo): RequestInfo;
  processResponse?(req: ServiceRequest, res: Response<any>, attempt: number): Promise<ResponseOutcome>;
  processError?(req: ServiceRequest, res: ResponseOutcome): Promise<ResponseOutcome>;
}

export interface OperationRights {[key: string]: OperationRightsInfo;}

export interface OperationRightsInfo {
  username?: string;
  password?: string;
  token?: string;
  apiKey?: string;
}

export interface Response<T> {
  raw: FetchResponse;
  /**
   * If 'error' is true then data will be of type ServiceError
   */
  data?: T;
  /**
   * True if there was a service error, false if not
   */
  error?: boolean;
}

export interface FetchResponse extends FetchBody {
  url: string;
  status: number;
  statusText: string;
  ok: boolean;
  headers: Headers;
  type: string | FetchResponseType;
  size: number;
  timeout: number;
  redirect(url: string, status: number): FetchResponse;
  error(): FetchResponse;
  clone(): FetchResponse;
}

export interface FetchBody {
  bodyUsed: boolean;
  arrayBuffer(): Promise<ArrayBuffer>;
  blob(): Promise<Blob>;
  formData(): Promise<FormData>;
  json(): Promise<any>;
  json<T>(): Promise<T>;
  text(): Promise<string>;
}

export interface FetchHeaders {
  get(name: string): string;
  getAll(name: string): string[];
  has(name: string): boolean;
}

export declare enum FetchResponseType { "basic", "cors", "default", "error", "opaque" }

export class ServiceError extends Error {
  public status: number;
}

/**
 * Flux standard action meta for service action
 */
export interface ServiceMeta {
  res: FetchResponse;
  info: any;
}


export interface ListTasksOptions {
  /**
   * Only return models with the given version.
   */
  name?: string;
  /**
   * Only return tasks with a name like the given name.
   */
  nameLike?: string;
  /**
   * Only return tasks with the given description.
   */
  description?: string;
  /**
   * Only return tasks with the given priority.
   */
  priority?: string;
  /**
   * Only return tasks with a priority greater than the given value.
   */
  minimumPriority?: string;
  /**
   * Only return tasks with a priority lower than the given value.
   */
  maximumPriority?: string;
  /**
   * Only return tasks assigned to the given user.
   */
  assignee?: string;
  /**
   * Only return tasks assigned with an assignee like the given value.
   */
  assigneeLike?: string;
  /**
   * Only return tasks owned by the given user.
   */
  owner?: string;
  /**
   * Only return tasks assigned with an owner like the given value.
   */
  ownerLike?: string;
  /**
   * Only return tasks that are not assigned to anyone. If false is passed, the value is ignored.
   */
  unassigned?: string;
  /**
   * Only return tasks that have the given delegation state. Possible values are pending and resolved.
   */
  delegationState?: string;
  /**
   * Only return tasks that can be claimed by the given user. This includes both tasks where the user is an explicit candidate for and task that are claimable by a group that the user is a member of.
   */
  candidateUser?: string;
  /**
   * Only return tasks that can be claimed by a user in the given group.
   */
  candidateGroup?: string;
  /**
   * Only return tasks that can be claimed by a user in the given groups. Values split by comma.
   */
  candidateGroups?: string;
  /**
   * Only return tasks in which the given user is involved.
   */
  involvedUser?: string;
  /**
   * Only return tasks with the given task definition id.
   */
  taskDefinitionKey?: string;
  /**
   * Only return tasks with a given task definition id like the given value.
   */
  taskDefinitionKeyLike?: string;
  /**
   * Only return tasks which are part of the process instance with the given id.
   */
  processInstanceId?: string;
  /**
   * Only return tasks which are part of the process instance with the given business key.
   */
  processInstanceBusinessKey?: string;
  /**
   * Only return tasks which are part of the process instance which has a business key like the given value.
   */
  processInstanceBusinessKeyLike?: string;
  /**
   * Only return tasks which are part of a process instance which has a process definition with the given id.
   */
  processDefinitionId?: string;
  /**
   * Only return tasks which are part of a process instance which has a process definition with the given key.
   */
  processDefinitionKey?: string;
  /**
   * Only return tasks which are part of a process instance which has a process definition with a key like the given value.
   */
  processDefinitionKeyLike?: string;
  /**
   * Only return tasks which are part of a process instance which has a process definition with the given name.
   */
  processDefinitionName?: string;
  /**
   * Only return tasks which are part of a process instance which has a process definition with a name like the given value.
   */
  processDefinitionNameLike?: string;
  /**
   * Only return tasks which are part of the execution with the given id.
   */
  executionId?: string;
  /**
   * Only return tasks which are created on the given date.
   */
  createdOn?: string;
  /**
   * Only return tasks which are created before the given date.
   */
  createdBefore?: string;
  /**
   * Only return tasks which are created after the given date.
   */
  createdAfter?: string;
  /**
   * Only return tasks which are due on the given date.
   */
  dueOn?: string;
  /**
   * Only return tasks which are due before the given date.
   */
  dueBefore?: string;
  /**
   * Only return tasks which are due after the given date.
   */
  dueAfter?: string;
  /**
   * Only return tasks which don’t have a due date. The property is ignored if the value is false.
   */
  withoutDueDate?: boolean;
  /**
   * Only return tasks that are not a subtask of another task.
   */
  excludeSubTasks?: boolean;
  /**
   * If true, only return tasks that are not suspended (either part of a process that is not suspended or not part of a process at all). If false, only tasks that are part of suspended process instances are returned.
   */
  active?: boolean;
  /**
   * Indication to include task local variables in the result.
   */
  includeTaskLocalVariables?: boolean;
  /**
   * Indication to include process variables in the result.
   */
  includeProcessVariables?: boolean;
  /**
   * Only return tasks with the given tenantId.
   */
  tenantId?: string;
  /**
   * Only return tasks with a tenantId like the given value.
   */
  tenantIdLike?: string;
  /**
   * If true, only returns tasks without a tenantId set. If false, the withoutTenantId parameter is ignored.
   */
  withoutTenantId?: boolean;
  /**
   * Select tasks that has been claimed or assigned to user or waiting to claim by user (candidate user or groups).
   */
  candidateOrAssigned?: string;
  /**
   * Select tasks with the given category. Note that this is the task category, not the category of the process definition (namespace within the BPMN Xml).
   */
  category?: string;
}

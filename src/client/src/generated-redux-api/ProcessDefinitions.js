/** @module ProcessDefinitions */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List of process definitions
 * 
 * @param {object} options Optional options
 * @param {ref} [options.version] Only return process definitions with the given version.
 * @param {string} [options.name] Only return process definitions with the given name.
 * @param {string} [options.nameLike] Only return process definitions with a name like the given name.
 * @param {string} [options.key] Only return process definitions with the given key.
 * @param {string} [options.keyLike] Only return process definitions with a name like the given key.
 * @param {string} [options.resourceName] Only return process definitions with the given resource name.
 * @param {string} [options.resourceNameLike] Only return process definitions with a name like the given resource name.
 * @param {string} [options.category] Only return process definitions with the given category.
 * @param {string} [options.categoryLike] Only return process definitions with a category like the given name.
 * @param {string} [options.categoryNotEquals] Only return process definitions which don’t have the given category.
 * @param {string} [options.deploymentId] Only return process definitions with the given category.
 * @param {string} [options.startableByUser] Only return process definitions which are part of a deployment with the given id.
 * @param {boolean} [options.latest] Only return the latest process definition versions. Can only be used together with key and keyLike parameters, using any other parameter will result in a 400-response.
 * @param {boolean} [options.suspended] If true, only returns process definitions which are suspended. If false, only active process definitions (which are not suspended) are returned.
 * @param {string} [options.sort] Enum: name, id, key, category, deploymentId, version. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfProcessDefinitionResponse>} Indicates request was successful and the process-definitions are returned
 */
export function listProcessDefinitions(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      version: options.version,
      name: options.name,
      nameLike: options.nameLike,
      key: options.key,
      keyLike: options.keyLike,
      resourceName: options.resourceName,
      resourceNameLike: options.resourceNameLike,
      category: options.category,
      categoryLike: options.categoryLike,
      categoryNotEquals: options.categoryNotEquals,
      deploymentId: options.deploymentId,
      startableByUser: options.startableByUser,
      latest: options.latest,
      suspended: options.suspended,
      sort: options.sort
    }
  }
  return gateway.request(listProcessDefinitionsOperation, parameters)
}

/**
 * Get a process definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.ProcessDefinitionResponse>} Indicates request was successful and the process-definitions are returned
 */
export function getProcessDefinitionUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(getProcessDefinitionUsingGETOperation, parameters)
}

/**
 * Execute actions for a process definition (Update category, Suspend or Activate)
 * 
 * @param {module:types.ProcessDefinitionActionRequest} actionRequest actionRequest
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.ProcessDefinitionResponse>} Indicates action has been executed for the specified process. (category altered, activate or suspend)
 */
export function executeProcessDefinitionActionUsingPUT(actionRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    },
    body: {
      actionRequest
    }
  }
  return gateway.request(executeProcessDefinitionActionUsingPUTOperation, parameters)
}

/**
 * List decision tables for a process-definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.DecisionTableResponse[]>} Indicates the process definition was found and the decision tables are returned.
 */
export function listProcessDefinitionDecisionTables(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(listProcessDefinitionDecisionTablesOperation, parameters)
}

/**
 * List form definitions for a process-definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.FormDefinitionResponse[]>} Indicates the process definition was found and the form definitions are returned.
 */
export function listProcessDefinitionFormDefinitions(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(listProcessDefinitionFormDefinitionsOperation, parameters)
}

/**
 * List candidate starters for a process-definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.RestIdentityLink[]>} Indicates the process definition was found and the requested identity links are returned.
 */
export function listProcessDefinitionIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(listProcessDefinitionIdentityLinksOperation, parameters)
}

/**
 * It's possible to add either a user or a group.
 * 
 * @param {module:types.RestIdentityLink} identityLink identityLink
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.RestIdentityLink>} OK
 */
export function createIdentityLinkUsingPOST(identityLink, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    },
    body: {
      identityLink
    }
  }
  return gateway.request(createIdentityLinkUsingPOSTOperation, parameters)
}

/**
 * Get a candidate starter from a process definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @param {string} [options.family] family
 * @param {string} [options.identityId] identityId
 * @return {Promise<module:types.RestIdentityLink>} Indicates the process definition was found and the identity link was returned.
 */
export function getIdentityLinkUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId,
      family: options.family,
      identityId: options.identityId
    }
  }
  return gateway.request(getIdentityLinkUsingGETOperation, parameters)
}

/**
 * Delete a candidate starter from a process definition
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @param {string} [options.family] family
 * @param {string} [options.identityId] identityId
 * @return {Promise<object>} OK
 */
export function deleteIdentityLinkUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId,
      family: options.family,
      identityId: options.identityId
    }
  }
  return gateway.request(deleteIdentityLinkUsingDELETEOperation, parameters)
}

/**
 * Get a process definition image
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<string>} Indicates request was successful and the process-definitions are returned
 */
export function getModelResourceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(getModelResourceUsingGETOperation, parameters)
}

/**
 * Get a process definition BPMN model
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.BpmnModel>} Indicates the process definition was found and the model is returned. The response contains the full process definition model.
 */
export function getBpmnModelResource(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(getBpmnModelResourceOperation, parameters)
}

/**
 * Get a process definition resource content
 * 
 * @param {object} options Optional options
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<string>} Indicates both process definition and resource have been found and the resource data has been returned.
 */
export function getProcessDefinitionResourceUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(getProcessDefinitionResourceUsingGETOperation, parameters)
}

const listProcessDefinitionsOperation = {
  path: '/repository/process-definitions',
  method: 'get'
}

const getProcessDefinitionUsingGETOperation = {
  path: '/repository/process-definitions/{processDefinitionId}',
  method: 'get'
}

const executeProcessDefinitionActionUsingPUTOperation = {
  path: '/repository/process-definitions/{processDefinitionId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const listProcessDefinitionDecisionTablesOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/decision-tables',
  method: 'get'
}

const listProcessDefinitionFormDefinitionsOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/form-definitions',
  method: 'get'
}

const listProcessDefinitionIdentityLinksOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/identitylinks',
  method: 'get'
}

const createIdentityLinkUsingPOSTOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/identitylinks',
  contentTypes: ['application/json'],
  method: 'post'
}

const getIdentityLinkUsingGETOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/identitylinks/{family}/{identityId}',
  method: 'get'
}

const deleteIdentityLinkUsingDELETEOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/identitylinks/{family}/{identityId}',
  method: 'delete'
}

const getModelResourceUsingGETOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/image',
  method: 'get'
}

const getBpmnModelResourceOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/model',
  method: 'get'
}

const getProcessDefinitionResourceUsingGETOperation = {
  path: '/repository/process-definitions/{processDefinitionId}/resourcedata',
  method: 'get'
}

/** @module TaskIdentityLinks */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List identity links for a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.RestIdentityLink[]>} Indicates the task was found and the requested identity links are returned.
 */
export function listTasksInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listTasksInstanceIdentityLinksOperation, parameters)
}

/**
 * It's possible to add either a user or a group.
 * 
 * @param {module:types.RestIdentityLink} identityLink identityLink
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.RestIdentityLink>} OK
 */
export function createTaskInstanceIdentityLinks(identityLink, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      identityLink
    }
  }
  return gateway.request(createTaskInstanceIdentityLinksOperation, parameters)
}

/**
 * Returns only identity links targetting either users or groups. Response body and status-codes are exactly the same as when getting the full list of identity links for a task.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.family] family
 * @return {Promise<module:types.RestIdentityLink[]>} Indicates the task was found and the requested identity links are returned.
 */
export function listIdentityLinksForFamily(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      family: options.family
    }
  }
  return gateway.request(listIdentityLinksForFamilyOperation, parameters)
}

/**
 * Get a single identity link on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.family] family
 * @param {string} [options.identityId] identityId
 * @param {string} [options.type] type
 * @return {Promise<module:types.RestIdentityLink>} Indicates the task and identity link was found and returned.
 */
export function getTaskInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      family: options.family,
      identityId: options.identityId,
      type: options.type
    }
  }
  return gateway.request(getTaskInstanceIdentityLinksOperation, parameters)
}

/**
 * Delete an identity link on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.family] family
 * @param {string} [options.identityId] identityId
 * @param {string} [options.type] type
 * @return {Promise<object>} OK
 */
export function deleteTaskInstanceIdentityLinks(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      family: options.family,
      identityId: options.identityId,
      type: options.type
    }
  }
  return gateway.request(deleteTaskInstanceIdentityLinksOperation, parameters)
}

const listTasksInstanceIdentityLinksOperation = {
  path: '/runtime/tasks/{taskId}/identitylinks',
  method: 'get'
}

const createTaskInstanceIdentityLinksOperation = {
  path: '/runtime/tasks/{taskId}/identitylinks',
  contentTypes: ['application/json'],
  method: 'post'
}

const listIdentityLinksForFamilyOperation = {
  path: '/runtime/tasks/{taskId}/identitylinks/{family}',
  method: 'get'
}

const getTaskInstanceIdentityLinksOperation = {
  path: '/runtime/tasks/{taskId}/identitylinks/{family}/{identityId}/{type}',
  method: 'get'
}

const deleteTaskInstanceIdentityLinksOperation = {
  path: '/runtime/tasks/{taskId}/identitylinks/{family}/{identityId}/{type}',
  method: 'delete'
}

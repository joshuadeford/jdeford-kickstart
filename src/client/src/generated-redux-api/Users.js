/** @module Users */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List users
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return group with the given id
 * @param {string} [options.firstName] Only return users with the given firstname
 * @param {string} [options.lastName] Only return users with the given lastname
 * @param {string} [options.email] Only return users with the given email
 * @param {string} [options.firstNameLike] Only return userswith a firstname like the given value. Use % as wildcard-character.
 * @param {string} [options.lastNameLike] Only return users with a lastname like the given value. Use % as wildcard-character.
 * @param {string} [options.emailLike] Only return users with an email like the given value. Use % as wildcard-character.
 * @param {string} [options.memberOfGroup] Only return users which are a member of the given group.
 * @param {string} [options.potentialStarter] Only return users  which members are potential starters for a process-definition with the given id.
 * @param {string} [options.sort] Enum: id, firstName, lastname, email. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfUserResponse>} Indicates the group exists and is returned.
 */
export function listUsers(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      firstName: options.firstName,
      lastName: options.lastName,
      email: options.email,
      firstNameLike: options.firstNameLike,
      lastNameLike: options.lastNameLike,
      emailLike: options.emailLike,
      memberOfGroup: options.memberOfGroup,
      potentialStarter: options.potentialStarter,
      sort: options.sort
    }
  }
  return gateway.request(listUsersOperation, parameters)
}

/**
 * Create a user
 * 
 * @param {module:types.UserRequest} userRequest userRequest
 * @return {Promise<module:types.UserResponse>} OK
 */
export function createUserUsingPOST(userRequest) {
  const parameters = {
    body: {
      userRequest
    }
  }
  return gateway.request(createUserUsingPOSTOperation, parameters)
}

/**
 * Get a single user
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<module:types.UserResponse>} Indicates the user exists and is returned.
 */
export function getUserUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    }
  }
  return gateway.request(getUserUsingGETOperation, parameters)
}

/**
 * All request values are optional. For example, you can only include the firstName attribute in the request body JSON-object, only updating the firstName of the user, leaving all other fields unaffected. When an attribute is explicitly included and is set to null, the user-value will be updated to null. Example: {"firstName" : null} will clear the firstName of the user).
 * 
 * @param {module:types.UserRequest} userRequest userRequest
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<module:types.UserResponse>} Indicates the user was updated.
 */
export function updateUserUsingPUT(userRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    },
    body: {
      userRequest
    }
  }
  return gateway.request(updateUserUsingPUTOperation, parameters)
}

/**
 * Delete a user
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<object>} OK
 */
export function deleteUserUsingDELETE_1(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    }
  }
  return gateway.request(deleteUserUsingDELETE_1Operation, parameters)
}

/**
 * List user’s info
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<module:types.UserInfoResponse[]>} Indicates the user was found and list of info (key and url) is returned.
 */
export function listUserInfo(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    }
  }
  return gateway.request(listUserInfoOperation, parameters)
}

/**
 * Create a new user’s info entry
 * 
 * @param {module:types.UserInfoRequest} userRequest userRequest
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<module:types.UserInfoResponse>} OK
 */
export function createUserInfo(userRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    },
    body: {
      userRequest
    }
  }
  return gateway.request(createUserInfoOperation, parameters)
}

/**
 * Get a user’s info
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @param {string} [options.key] key
 * @return {Promise<module:types.UserInfoResponse>} Indicates the user was found and the user has info for the given key.
 */
export function getUserInfoUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId,
      key: options.key
    }
  }
  return gateway.request(getUserInfoUsingGETOperation, parameters)
}

/**
 * Update a user’s info
 * 
 * @param {module:types.UserInfoRequest} userRequest userRequest
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @param {string} [options.key] key
 * @return {Promise<module:types.UserInfoResponse>} Indicates the user was found and the info has been updated.
 */
export function updateUserInfo(userRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId,
      key: options.key
    },
    body: {
      userRequest
    }
  }
  return gateway.request(updateUserInfoOperation, parameters)
}

/**
 * Delete a user’s info
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @param {string} [options.key] key
 * @return {Promise<object>} OK
 */
export function deleteUserInfoUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId,
      key: options.key
    }
  }
  return gateway.request(deleteUserInfoUsingDELETEOperation, parameters)
}

/**
 * The response body contains the raw picture data, representing the user’s picture. The Content-type of the response corresponds to the mimeType that was set when creating the picture.
 * 
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<string>} Indicates the user was found and has a picture, which is returned in the body.
 */
export function getUserPictureUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    }
  }
  return gateway.request(getUserPictureUsingGETOperation, parameters)
}

/**
 * The request should be of type multipart/form-data. There should be a single file-part included with the binary value of the picture. On top of that, the following additional form-fields can be present:
 * 
 * mimeType: Optional mime-type for the uploaded picture. If omitted, the default of image/jpeg is used as a mime-type for the picture.
 * 
 * @param {ref} file Picture to update
 * @param {object} options Optional options
 * @param {string} [options.userId] userId
 * @return {Promise<object>} OK
 */
export function updateUserPictureUsingPUT(file, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      userId: options.userId
    },
    formData: {
      file
    }
  }
  return gateway.request(updateUserPictureUsingPUTOperation, parameters)
}

const listUsersOperation = {
  path: '/identity/users',
  method: 'get'
}

const createUserUsingPOSTOperation = {
  path: '/identity/users',
  contentTypes: ['application/json'],
  method: 'post'
}

const getUserUsingGETOperation = {
  path: '/identity/users/{userId}',
  method: 'get'
}

const updateUserUsingPUTOperation = {
  path: '/identity/users/{userId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteUserUsingDELETE_1Operation = {
  path: '/identity/users/{userId}',
  method: 'delete'
}

const listUserInfoOperation = {
  path: '/identity/users/{userId}/info',
  method: 'get'
}

const createUserInfoOperation = {
  path: '/identity/users/{userId}/info',
  contentTypes: ['application/json'],
  method: 'post'
}

const getUserInfoUsingGETOperation = {
  path: '/identity/users/{userId}/info/{key}',
  method: 'get'
}

const updateUserInfoOperation = {
  path: '/identity/users/{userId}/info/{key}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteUserInfoUsingDELETEOperation = {
  path: '/identity/users/{userId}/info/{key}',
  method: 'delete'
}

const getUserPictureUsingGETOperation = {
  path: '/identity/users/{userId}/picture',
  method: 'get'
}

const updateUserPictureUsingPUTOperation = {
  path: '/identity/users/{userId}/picture',
  method: 'put'
}

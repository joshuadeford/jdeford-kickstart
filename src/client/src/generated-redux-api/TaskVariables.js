/** @module TaskVariables */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List variables for a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable.
 * @return {Promise<module:types.RestVariable[]>} Indicates the task was found and the requested variables are returned
 */
export function listTaskVariables(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(listTaskVariablesOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable or an Array of RestVariable) or by passing a multipart/form-data Object.
 * It's possible to create simple (non-binary) variable or list of variables or new binary variable 
 * Any number of variables can be passed into the request body array.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.body] Create a variable on a task
 * @param {string} [options.name] Required name of the variable
 * @param {string} [options.type] Type of variable that is created. If omitted, reverts to raw JSON-value type (string, boolean, integer or double)
 * @param {string} [options.scope] Scope of variable that is created. If omitted, local is assumed.
 * @return {Promise<object>} OK
 */
export function createTaskVariableUsingPOST(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      body: options.body
    },
    formData: {
      name: options.name,
      type: options.type,
      scope: options.scope
    }
  }
  return gateway.request(createTaskVariableUsingPOSTOperation, parameters)
}

/**
 * Get a variable from a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable.
 * @return {Promise<module:types.RestVariable>} Indicates the task was found and the requested variables are returned.
 */
export function getTaskInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getTaskInstanceVariableOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (RestVariable) or by passing a multipart/form-data Object.
 * It's possible to update simple (non-binary) variable or  binary variable 
 * Any number of variables can be passed into the request body array.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.body] Update a task variable
 * @param {string} [options.name] Required name of the variable
 * @param {string} [options.type] Type of variable that is updated. If omitted, reverts to raw JSON-value type (string, boolean, integer or double)
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable..
 * @return {Promise<module:types.RestVariable>} Indicates the variables was updated and the result is returned.
 */
export function updateTaskInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      variableName: options.variableName
    },
    body: {
      body: options.body
    },
    formData: {
      name: options.name,
      type: options.type,
      scope: options.scope
    }
  }
  return gateway.request(updateTaskInstanceVariableOperation, parameters)
}

/**
 * Delete a variable on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable.
 * @return {Promise<object>} OK
 */
export function deleteTaskInstanceVariable(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(deleteTaskInstanceVariableOperation, parameters)
}

/**
 * The response body contains the binary value of the variable. When the variable is of type binary, the content-type of the response is set to application/octet-stream, regardless of the content of the variable or the request accept-type header. In case of serializable, application/x-java-serialized-object is used as content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.variableName] variableName
 * @param {string} [options.scope] Scope of variable to be returned. When local, only task-local variable value is returned. When global, only variable value from the task’s parent execution-hierarchy are returned. When the parameter is omitted, a local variable will be returned if it exists, otherwise a global variable.
 * @return {Promise<string>} Indicates the task was found and the requested variables are returned.
 */
export function getTaskVariableData(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      variableName: options.variableName
    },
    query: {
      scope: options.scope
    }
  }
  return gateway.request(getTaskVariableDataOperation, parameters)
}

const listTaskVariablesOperation = {
  path: '/runtime/tasks/{taskId}/variables',
  method: 'get'
}

const createTaskVariableUsingPOSTOperation = {
  path: '/runtime/tasks/{taskId}/variables',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'post'
}

const getTaskInstanceVariableOperation = {
  path: '/runtime/tasks/{taskId}/variables/{variableName}',
  method: 'get'
}

const updateTaskInstanceVariableOperation = {
  path: '/runtime/tasks/{taskId}/variables/{variableName}',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'put'
}

const deleteTaskInstanceVariableOperation = {
  path: '/runtime/tasks/{taskId}/variables/{variableName}',
  method: 'delete'
}

const getTaskVariableDataOperation = {
  path: '/runtime/tasks/{taskId}/variables/{variableName}/data',
  method: 'get'
}

/** @module TaskAttachments */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List attachments on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @return {Promise<module:types.AttachmentResponse[]>} Indicates the task was found and the attachments are returned.
 */
export function listTaskAttachments(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    }
  }
  return gateway.request(listTaskAttachmentsOperation, parameters)
}

/**
 * This endpoint can be used in 2 ways: By passing a JSON Body (AttachmentRequest) to link an external resource or by passing a multipart/form-data Object to attach a file.
 * NB: Swagger V2 specification doesn't support this use case that's why this endpoint might be buggy/incomplete if used with other tools.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.body] create an attachment containing a link to an external resource
 * @param {ref} [options.file] Attachment file
 * @param {string} [options.name] Required name of the variable
 * @param {string} [options.description] Description of the attachment, optional
 * @param {string} [options.type] Type of attachment, optional. Supports any arbitrary string or a valid HTTP content-type.
 * @return {Promise<module:types.AttachmentResponse>} OK
 */
export function createAttachmentUsingPOST(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId
    },
    body: {
      body: options.body
    },
    formData: {
      file: options.file,
      name: options.name,
      description: options.description,
      type: options.type
    }
  }
  return gateway.request(createAttachmentUsingPOSTOperation, parameters)
}

/**
 * Get an attachment on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.attachmentId] attachmentId
 * @return {Promise<module:types.AttachmentResponse>} Indicates the task and attachment were found and the attachment is returned.
 */
export function getAttachmentUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      attachmentId: options.attachmentId
    }
  }
  return gateway.request(getAttachmentUsingGETOperation, parameters)
}

/**
 * Delete an attachment on a task
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.attachmentId] attachmentId
 * @return {Promise<object>} OK
 */
export function deleteAttachmentUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      attachmentId: options.attachmentId
    }
  }
  return gateway.request(deleteAttachmentUsingDELETEOperation, parameters)
}

/**
 * The response body contains the binary content. By default, the content-type of the response is set to application/octet-stream unless the attachment type contains a valid Content-type.
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.attachmentId] attachmentId
 * @return {Promise<string>} Indicates the task and attachment was found and the requested content is returned.
 */
export function getAttachmentContentUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      taskId: options.taskId,
      attachmentId: options.attachmentId
    }
  }
  return gateway.request(getAttachmentContentUsingGETOperation, parameters)
}

const listTaskAttachmentsOperation = {
  path: '/runtime/tasks/{taskId}/attachments',
  method: 'get'
}

const createAttachmentUsingPOSTOperation = {
  path: '/runtime/tasks/{taskId}/attachments',
  contentTypes: ['application/json','multipart/form-data'],
  method: 'post'
}

const getAttachmentUsingGETOperation = {
  path: '/runtime/tasks/{taskId}/attachments/{attachmentId}',
  method: 'get'
}

const deleteAttachmentUsingDELETEOperation = {
  path: '/runtime/tasks/{taskId}/attachments/{attachmentId}',
  method: 'delete'
}

const getAttachmentContentUsingGETOperation = {
  path: '/runtime/tasks/{taskId}/attachments/{attachmentId}/content',
  method: 'get'
}

/** @module Databasetables */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List tables
 */
export function listTables() {
  return gateway.request(listTablesOperation)
}

/**
 * Get a single table
 * 
 * @param {object} options Optional options
 * @param {string} [options.tableName] tableName
 * @return {Promise<module:types.TableResponse>} Indicates the table exists and the table count is returned.
 */
export function getTableUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      tableName: options.tableName
    }
  }
  return gateway.request(getTableUsingGETOperation, parameters)
}

/**
 * Get column info for a single table
 * 
 * @param {object} options Optional options
 * @param {string} [options.tableName] tableName
 * @return {Promise<module:types.TableMetaData>} Indicates the table exists and the table column info is returned.
 */
export function getTableMetaDataUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      tableName: options.tableName
    }
  }
  return gateway.request(getTableMetaDataUsingGETOperation, parameters)
}

/**
 * Get row data for a single table
 * 
 * @param {object} options Optional options
 * @param {string} [options.tableName] tableName
 * @param {ref} [options.start] Index of the first row to fetch. Defaults to 0.
 * @param {ref} [options.size] Number of rows to fetch, starting from start. Defaults to 10.
 * @param {string} [options.orderAscendingColumn] Name of the column to sort the resulting rows on, ascending.
 * @param {string} [options.orderDescendingColumn] Name of the column to sort the resulting rows on, descending.
 * @return {Promise<module:types.DataResponseOfListOfMapOfstringAndobject>} Indicates the table exists and the table row data is returned
 */
export function getTableDataUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      tableName: options.tableName
    },
    query: {
      start: options.start,
      size: options.size,
      orderAscendingColumn: options.orderAscendingColumn,
      orderDescendingColumn: options.orderDescendingColumn
    }
  }
  return gateway.request(getTableDataUsingGETOperation, parameters)
}

const listTablesOperation = {
  path: '/management/tables',
  method: 'get'
}

const getTableUsingGETOperation = {
  path: '/management/tables/{tableName}',
  method: 'get'
}

const getTableMetaDataUsingGETOperation = {
  path: '/management/tables/{tableName}/columns',
  method: 'get'
}

const getTableDataUsingGETOperation = {
  path: '/management/tables/{tableName}/data',
  method: 'get'
}

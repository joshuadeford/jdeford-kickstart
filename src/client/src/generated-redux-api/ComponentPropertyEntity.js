/** @module ComponentPropertyEntity */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * findAllComponentProperty
 * 
 * @param {object} options Optional options
 * @param {string} [options.page] page
 * @param {string} [options.size] size
 * @param {string} [options.sort] sort
 * @return {Promise<module:types.ResourcesOfComponentProperty>} OK
 */
export function findAllComponentPropertyUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      page: options.page,
      size: options.size,
      sort: options.sort
    }
  }
  return gateway.request(findAllComponentPropertyUsingGETOperation, parameters)
}

/**
 * saveComponentProperty
 * 
 * @param {module:types.ComponentProperty} body body
 * @return {Promise<module:types.ResourceOfComponentProperty>} OK
 */
export function saveComponentPropertyUsingPOST(body) {
  const parameters = {
    body: {
      body
    }
  }
  return gateway.request(saveComponentPropertyUsingPOSTOperation, parameters)
}

/**
 * findOneComponentProperty
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfComponentProperty>} OK
 */
export function findOneComponentPropertyUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(findOneComponentPropertyUsingGETOperation, parameters)
}

/**
 * saveComponentProperty
 * 
 * @param {string} id id
 * @param {module:types.ComponentProperty} body body
 * @return {Promise<module:types.ResourceOfComponentProperty>} OK
 */
export function saveComponentPropertyUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveComponentPropertyUsingPUTOperation, parameters)
}

/**
 * deleteComponentProperty
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function deleteComponentPropertyUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(deleteComponentPropertyUsingDELETEOperation, parameters)
}

/**
 * saveComponentProperty
 * 
 * @param {string} id id
 * @param {module:types.ComponentProperty} body body
 * @return {Promise<module:types.ResourceOfComponentProperty>} OK
 */
export function saveComponentPropertyUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(saveComponentPropertyUsingPATCHOperation, parameters)
}

/**
 * componentPropertyConfiguration
 * 
 * @param {string} id id
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function componentPropertyConfigurationUsingGET(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(componentPropertyConfigurationUsingGETOperation, parameters)
}

/**
 * componentPropertyConfiguration
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function componentPropertyConfigurationUsingPOST(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentPropertyConfigurationUsingPOSTOperation, parameters)
}

/**
 * componentPropertyConfiguration
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function componentPropertyConfigurationUsingPUT(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentPropertyConfigurationUsingPUTOperation, parameters)
}

/**
 * componentPropertyConfiguration
 * 
 * @param {string} id id
 * @return {Promise<object>} OK
 */
export function componentPropertyConfigurationUsingDELETE(id) {
  const parameters = {
    path: {
      id
    }
  }
  return gateway.request(componentPropertyConfigurationUsingDELETEOperation, parameters)
}

/**
 * componentPropertyConfiguration
 * 
 * @param {string} id id
 * @param {string} body body
 * @return {Promise<module:types.ResourceOfComponentConfiguration>} OK
 */
export function componentPropertyConfigurationUsingPATCH(id, body) {
  const parameters = {
    path: {
      id
    },
    body: {
      body
    }
  }
  return gateway.request(componentPropertyConfigurationUsingPATCHOperation, parameters)
}

const findAllComponentPropertyUsingGETOperation = {
  path: '/api/componentProperties',
  method: 'get'
}

const saveComponentPropertyUsingPOSTOperation = {
  path: '/api/componentProperties',
  contentTypes: ['application/json'],
  method: 'post'
}

const findOneComponentPropertyUsingGETOperation = {
  path: '/api/componentProperties/{id}',
  method: 'get'
}

const saveComponentPropertyUsingPUTOperation = {
  path: '/api/componentProperties/{id}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteComponentPropertyUsingDELETEOperation = {
  path: '/api/componentProperties/{id}',
  method: 'delete'
}

const saveComponentPropertyUsingPATCHOperation = {
  path: '/api/componentProperties/{id}',
  contentTypes: ['application/json'],
  method: 'patch'
}

const componentPropertyConfigurationUsingGETOperation = {
  path: '/api/componentProperties/{id}/configuration',
  method: 'get'
}

const componentPropertyConfigurationUsingPOSTOperation = {
  path: '/api/componentProperties/{id}/configuration',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'post'
}

const componentPropertyConfigurationUsingPUTOperation = {
  path: '/api/componentProperties/{id}/configuration',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'put'
}

const componentPropertyConfigurationUsingDELETEOperation = {
  path: '/api/componentProperties/{id}/configuration',
  method: 'delete'
}

const componentPropertyConfigurationUsingPATCHOperation = {
  path: '/api/componentProperties/{id}/configuration',
  contentTypes: ['text/uri-list','application/x-spring-data-compact+json'],
  method: 'patch'
}

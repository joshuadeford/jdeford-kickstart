/** @module Models */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * List models
 * 
 * @param {object} options Optional options
 * @param {string} [options.id] Only return models with the given version.
 * @param {string} [options.category] Only return models with the given category.
 * @param {string} [options.categoryLike] Only return models with a category like the given name.
 * @param {string} [options.categoryNotEquals] Only return models which don’t have the given category.
 * @param {string} [options.name] Only return models with the given name.
 * @param {string} [options.nameLike] Only return models with a name like the given name.
 * @param {string} [options.key] Only return models with the given key.
 * @param {string} [options.deploymentId] Only return models with the given category.
 * @param {ref} [options.version] Only return models with the given version.
 * @param {boolean} [options.latestVersion] If true, only return models which are the latest version. Best used in combination with key. If false is passed in as value, this is ignored and all versions are returned.
 * @param {boolean} [options.deployed] If true, only deployed models are returned. If false, only undeployed models are returned (deploymentId is null).
 * @param {string} [options.tenantId] Only return models with the given tenantId.
 * @param {string} [options.tenantIdLike] Only return models with a tenantId like the given value.
 * @param {boolean} [options.withoutTenantId] If true, only returns models without a tenantId set. If false, the withoutTenantId parameter is ignored.
 * @param {string} [options.sort] Enum: id, category, createTime, key, lastUpdateTime, name, version, tenantId. Property to sort on, to be used together with the order.
 * @return {Promise<module:types.DataResponseOfModelResponse>} Indicates request was successful and the models are returned
 */
export function listModels(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      id: options.id,
      category: options.category,
      categoryLike: options.categoryLike,
      categoryNotEquals: options.categoryNotEquals,
      name: options.name,
      nameLike: options.nameLike,
      key: options.key,
      deploymentId: options.deploymentId,
      version: options.version,
      latestVersion: options.latestVersion,
      deployed: options.deployed,
      tenantId: options.tenantId,
      tenantIdLike: options.tenantIdLike,
      withoutTenantId: options.withoutTenantId,
      sort: options.sort
    }
  }
  return gateway.request(listModelsOperation, parameters)
}

/**
 * All request values are optional. For example, you can only include the name attribute in the request body JSON-object, only setting the name of the model, leaving all other fields null.
 * 
 * @param {module:types.ModelRequest} modelRequest modelRequest
 * @return {Promise<module:types.ModelResponse>} OK
 */
export function createModelUsingPOST(modelRequest) {
  const parameters = {
    body: {
      modelRequest
    }
  }
  return gateway.request(createModelUsingPOSTOperation, parameters)
}

/**
 * Get a model
 * 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<module:types.ModelResponse>} Indicates the model was found and returned.
 */
export function getModelUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    }
  }
  return gateway.request(getModelUsingGETOperation, parameters)
}

/**
 * All request values are optional. For example, you can only include the name attribute in the request body JSON-object, only updating the name of the model, leaving all other fields unaffected. When an attribute is explicitly included and is set to null, the model-value will be updated to null.
 * 
 * @param {module:types.ModelRequest} modelRequest modelRequest
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<module:types.ModelResponse>} Indicates the model was found and updated.
 */
export function updateModelUsingPUT(modelRequest, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    },
    body: {
      modelRequest
    }
  }
  return gateway.request(updateModelUsingPUTOperation, parameters)
}

/**
 * Delete a model
 * 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<object>} OK
 */
export function deleteModelUsingDELETE(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    }
  }
  return gateway.request(deleteModelUsingDELETEOperation, parameters)
}

/**
 * Response body contains the model’s raw editor source. The response’s content-type is set to application/octet-stream, regardless of the content of the source.
 * 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<string>} Indicates the model was found and source is returned.
 */
export function getModelBytesUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    }
  }
  return gateway.request(getModelBytesUsingGETOperation, parameters)
}

/**
 * Response body contains the model’s raw editor source. The response’s content-type is set to application/octet-stream, regardless of the content of the source.
 * 
 * @param {ref} file 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<object>} OK
 */
export function setModelSourceUsingPUT(file, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    },
    formData: {
      file
    }
  }
  return gateway.request(setModelSourceUsingPUTOperation, parameters)
}

/**
 * Response body contains the model’s raw editor source. The response’s content-type is set to application/octet-stream, regardless of the content of the source.
 * 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<string>} Indicates the model was found and source is returned.
 */
export function getExtraEditorSource(options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    }
  }
  return gateway.request(getExtraEditorSourceOperation, parameters)
}

/**
 * Response body contains the model’s raw editor source. The response’s content-type is set to application/octet-stream, regardless of the content of the source.
 * 
 * @param {ref} file 
 * @param {object} options Optional options
 * @param {string} [options.modelId] modelId
 * @return {Promise<object>} OK
 */
export function setExtraEditorSource(file, options) {
  if (!options) options = {}
  const parameters = {
    path: {
      modelId: options.modelId
    },
    formData: {
      file
    }
  }
  return gateway.request(setExtraEditorSourceOperation, parameters)
}

const listModelsOperation = {
  path: '/repository/models',
  method: 'get'
}

const createModelUsingPOSTOperation = {
  path: '/repository/models',
  contentTypes: ['application/json'],
  method: 'post'
}

const getModelUsingGETOperation = {
  path: '/repository/models/{modelId}',
  method: 'get'
}

const updateModelUsingPUTOperation = {
  path: '/repository/models/{modelId}',
  contentTypes: ['application/json'],
  method: 'put'
}

const deleteModelUsingDELETEOperation = {
  path: '/repository/models/{modelId}',
  method: 'delete'
}

const getModelBytesUsingGETOperation = {
  path: '/repository/models/{modelId}/source',
  method: 'get'
}

const setModelSourceUsingPUTOperation = {
  path: '/repository/models/{modelId}/source',
  method: 'put'
}

const getExtraEditorSourceOperation = {
  path: '/repository/models/{modelId}/source-extra',
  method: 'get'
}

const setExtraEditorSourceOperation = {
  path: '/repository/models/{modelId}/source-extra',
  method: 'put'
}

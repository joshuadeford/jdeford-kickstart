/** @module Forms */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * Get form data
 * 
 * @param {object} options Optional options
 * @param {string} [options.taskId] taskId
 * @param {string} [options.processDefinitionId] processDefinitionId
 * @return {Promise<module:types.FormDataResponse>} Indicates that form data could be queried.
 */
export function getFormDataUsingGET(options) {
  if (!options) options = {}
  const parameters = {
    query: {
      taskId: options.taskId,
      processDefinitionId: options.processDefinitionId
    }
  }
  return gateway.request(getFormDataUsingGETOperation, parameters)
}

/**
 * Submit task form data
 * 
 * @param {module:types.SubmitFormRequest} submitRequest submitRequest
 * @return {Promise<module:types.ProcessInstanceResponse>} Indicates request was successful and the form data was submitted
 */
export function submitFormUsingPOST(submitRequest) {
  const parameters = {
    body: {
      submitRequest
    }
  }
  return gateway.request(submitFormUsingPOSTOperation, parameters)
}

const getFormDataUsingGETOperation = {
  path: '/form/form-data',
  method: 'get'
}

const submitFormUsingPOSTOperation = {
  path: '/form/form-data',
  contentTypes: ['application/json'],
  method: 'post'
}

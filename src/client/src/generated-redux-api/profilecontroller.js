/** @module profilecontroller */
// Auto-generated, edits will be overwritten
import * as gateway from './gateway'

/**
 * listAllFormsOfMetadata
 */
export function listAllFormsOfMetadataUsingGET() {
  return gateway.request(listAllFormsOfMetadataUsingGETOperation)
}

/**
 * profileOptions
 */
export function profileOptionsUsingOPTIONS() {
  return gateway.request(profileOptionsUsingOPTIONSOperation)
}

const listAllFormsOfMetadataUsingGETOperation = {
  path: '/api/profile',
  method: 'get'
}

const profileOptionsUsingOPTIONSOperation = {
  path: '/api/profile',
  method: 'options'
}

import * as React from "react";
import { Route, Switch } from "react-router";
import Layout from "../containers/Layout";
import NoMatch from "../components/NoMatch";
import EntityTable from "../containers/EntityTable";
import TaskList from "../containers/TaskList";

const routes = (
  <div>
    <Layout />
    <Switch>
      <Route path="/tasks" component={TaskList} />
      <Route path="/entities/:entityRel" component={EntityTable} />
      <Route component={NoMatch} />
    </Switch>
  </div>
);

export default routes;

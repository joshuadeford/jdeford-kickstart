import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import * as React from "react";
import InjectedIntl = ReactIntl.InjectedIntl;

interface DeleteDialogProps {
  isOpen: boolean;
  intl: InjectedIntl;
  nameForDeletion: string;
  handleDelete(): void;
  handleClose(): void;
}

/**
 * A modal Dialog used to prompt the user to confirm deletion.
 * It provides a confirmation button and a cancel button.
 */
export class DeleteDialog extends React.Component<DeleteDialogProps> {
  public render () {
    const {
      isOpen,
      handleDelete,
      handleClose,
      nameForDeletion,
      intl
    } = this.props;

    return (
    <div>
      <Dialog open={isOpen} >
        <DialogTitle id="delete-dialog-title">{intl.formatMessage({ "id": "delete_title" }, { "name": nameForDeletion })}</DialogTitle>
        <DialogContent>
          <DialogContentText id="delete-dialog-description">
            {intl.formatMessage({ "id": "delete_message" }, { "name": nameForDeletion })}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {intl.formatMessage({ "id": "cancel_label" })}
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus={true}>
            {intl.formatMessage({ "id": "delete_label" })}
          </Button>
        </DialogActions>
      </Dialog>
    </div>

    );
  }

}




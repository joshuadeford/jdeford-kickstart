import Button from "@material-ui/core/Button";
import * as React from "react";

import { faTrashAlt, faUnlink, faLink } from "@fortawesome/fontawesome-free-solid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Entity, EntitySchema, SchemaProperty } from "../store/entities/types";
import * as _ from "lodash";
import {
  Divider,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary, Grid, Switch,
  TextField,
  Typography, withStyles, WithStyles
} from "@material-ui/core";
import ExpandMoreIcon from "../../node_modules/@material-ui/icons/ExpandMore";
import { styles } from "../common/theme";

interface EntityRowProps extends WithStyles<typeof styles> {
  entity: Entity;
  itemMapsByRel: {[rel: string]: {[uri: string]: Entity}};
  entitySchema: EntitySchema;
  id?: string;
  startOpen?: boolean;
  handleShowUnrelated(rel: string): void;
  handleDeleteEntity(entity: Entity): void;
  handleRelateEntity(entity: Entity, property: SchemaProperty): void;
  handleUnrelateEntity(entity: Entity, property: SchemaProperty): void;
  handleUpdateEntity(entity: Entity): void;
}

interface EntityRowState {
  showUnrelatedMap: { [rel: string]: boolean };
  fieldValues: Entity;
}

/**
 * Represents an entity as a row.
 * It expects props containing entity bag of data and functions to switch
 * the entity into edit mode or to delete the entity.
 */
class EntityRow extends React.Component<EntityRowProps, EntityRowState> {

  private saveEntity = _.debounce(() => {
    const { handleUpdateEntity } = this.props;
    handleUpdateEntity(this.state.fieldValues);
  }, 2000);

  constructor(props: EntityRowProps) {
    super(props);
    const { entity } = props;
    this.state = {
      "showUnrelatedMap": { /* */ },
      "fieldValues": entity
    };
  }

  public render () {
    const { entity, entitySchema, classes, itemMapsByRel, startOpen } = this.props;
    const propertyCells = [];
    const relationshipCells = [];


    for (const propertyKey in entitySchema.properties) {
      if (entitySchema.properties.hasOwnProperty(propertyKey)) {
        const property = entitySchema.properties[propertyKey];
        if (!property.format || property.format !== "uri") {
          // This should be something you want to do in a real system.
          // Use the paging mechanism instead.  This is just going to spew out ALL
          // components that COULD be releated to via this property
          propertyCells.push(<Grid item={true} xs={12} md={6} xl={4}>
            <div className={ classes.gridTextFieldWrapper }>
            <TextField
              key={`${entity.uri}_${propertyKey}`}
              label={property.name}
              className={classes.textField}
              value={this.state.fieldValues[propertyKey] ? this.state.fieldValues[propertyKey] : ""}
              onChange={this.updateFieldValue(property)}
              margin="normal"
          />
            </div>
          </Grid>);
        } else {
          const relatedEntityUri = entity[propertyKey];
          let relatedEntityUris: string[];
          if (!relatedEntityUri) {
            relatedEntityUris = [];
          } else if (relatedEntityUri instanceof Array) {
            relatedEntityUris = relatedEntityUri;
          } else {
            relatedEntityUris = [relatedEntityUri];
          }
          let unrelatedUrisToShow: string[] = [];
          if(this.state.showUnrelatedMap[property.returnTypeRel]) {
            const allUris = Object.keys(itemMapsByRel[property.returnTypeRel]);
            unrelatedUrisToShow = allUris.filter((uri) => relatedEntityUris.indexOf(uri) < 0);
          }

          relationshipCells.push(<ExpansionPanel
              key={`${entitySchema.rel}_${propertyKey}`}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
              <Typography className={classes.heading}>{property.title}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                Show Unrelated
                <Switch
                    checked={this.state.showUnrelatedMap[property.returnTypeRel]}
                    onChange={this.toggleUnrelated(property.returnTypeRel)}
                    value="checkedB"
                    color="primary"
                />
                <Grid container={true}>
                  { this.getRelatedItems(relatedEntityUris, property, faLink, "outlined", this.unrelateEntity) }
                  { this.getRelatedItems(unrelatedUrisToShow, property, faUnlink, "raised", this.relateEntity) }
                </Grid>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>);
        }
      }
    }
    return <ExpansionPanel
        defaultExpanded={startOpen}
        key={`${entity.uri}`}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div>
        <Typography paragraph={true} className={classes.heading}>{entitySchema.title}: {entity.entityName}</Typography>
        <br/>
        <Button variant="outlined" onClick={this.deleteEntity}>
          <FontAwesomeIcon icon={faTrashAlt} />
        </Button>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.innerPanel}>
        <div>
          <Grid container={true} spacing={16}>
          { propertyCells }
          </Grid>
        <Divider/>
          <div className={classes.relationshipWrapper}>
          { relationshipCells }
          </div>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>;
  }


  private getRelatedItems = (uris: string[],
                          property: SchemaProperty,
                          icon: any,
                          variant: "raised" | "outlined",
                          action: (entity: Entity, property: SchemaProperty) => () => void) => {
    const { itemMapsByRel, classes } = this.props;
    const rel = property.returnTypeRel;
    return uris.map((uri: string) => {
      const entity = itemMapsByRel[rel][uri];
      if (entity) {
        return <Grid item={true} key={uri} xs={12} lg={2}>
          <Button variant={variant} onClick={action(entity, property)}>
            <FontAwesomeIcon icon={icon}/>
          </Button>
          <span className={classes.linkLabel}>{`${entity.entityName}`}</span>
        </Grid>;
      } else {
        return null;
      }
    });
  }

  private updateFieldValue = (property: SchemaProperty) => (changeEvent: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      "fieldValues": {
        ...this.state.fieldValues,
        [property.name]: changeEvent.target.value
      }
    });
    this.saveEntity();
  }


  private unrelateEntity = (entity: Entity, property: SchemaProperty) => () => {
    const { handleUnrelateEntity } = this.props;
    handleUnrelateEntity(entity, property);
  }
  private relateEntity = (entity: Entity, property: SchemaProperty) => () => {
    const { handleRelateEntity } = this.props;
    handleRelateEntity(entity, property);
  }

  private toggleUnrelated = (rel: string) => (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    const { handleShowUnrelated } = this.props;
    this.setState({ "showUnrelatedMap": {
      ...this.state.showUnrelatedMap,
      [rel]: checked }
    });
    handleShowUnrelated(rel);
  }

  private deleteEntity = () => {
    const { handleDeleteEntity, entity } = this.props;
    handleDeleteEntity(entity);
  }


}

export default (withStyles(styles)<EntityRowProps>(EntityRow));

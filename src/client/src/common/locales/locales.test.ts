import { getLocaleMessages } from "./locales";

it("returns known languages", () => {
  const enMessages = getLocaleMessages("en");
  expect(enMessages.users_title).toBe("Users");

  const esMessages = getLocaleMessages("es");
  expect(esMessages.users_title).toBe("Empleados");
});

it("returns English for unknown languages", () => {
  const enMessages = getLocaleMessages("fr");
  expect(enMessages.users_title).toBe("Users");
});

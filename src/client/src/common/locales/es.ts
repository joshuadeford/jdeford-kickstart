export const messages=
    {
      "counted": "Mostrador",
      "hello": "Hola",
      "users_title": "Usuario",
      "DOB_title": "FDN",
      "userName_title": "Nombre Completo",
      "role_title": "Papel",
      "delete_label": "Borrar",
      "cancel_label": "Cancelar",
      "delete_message": "¿Seguro que quieres eliminar {name}?",
      "delete_title": "Confirma la eliminación de {name}",
      "ok_label": "De acuerdo",
      "error_message": "Ha habido un error",
      "error_title": "Error",
      "nomessage": "bueno",
      "error_creating_user": "Hubo un error al crear el usuario",
      "error_retrieving_users": "Hubo un error al recuperar los usuarios",
      "error_updating_user": "Hubo un error al recuperar los usuario",
      "error_deleting_user": "There was an error deleting the user",
      "create_user_success": "El usuario fue creado con éxito",
      "update_user_success": "El usuario fue actualizado con éxito",
      "delete_user_success": "El usuario fue eliminado con éxito"
    };


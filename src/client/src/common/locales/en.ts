export const messages=
{
  "counted": "Counted",
  "complete_task": "Complete",
  "entity_type": "Entity Type:",
  "error_creating_entity": "Error creating entity.  Make sure all required fields are set.",
  "hello": "Hello",
  "tasks": "Tasks",
  "users_title": "Users",
  "DOB_title": "DOB",
  "userName_title": "User Name",
  "role_title": "Role",
  "delete_label": "Delete",
  "cancel_label": "Cancel",
  "delete_message": "Are you sure you want to delete {name}?",
  "delete_title": "Confirm deletion of {name}",
  "ok_label": "OK",
  "error_message": "There's been an error",
  "error_title": "Error",
  "nomessage": "Everything is fine",
  "error_creating_user": "There was an error creating the user",
  "error_retrieving_users": "There was an error retrieving the users",
  "error_updating_user": "There was an error updating the user",
  "error_deleting_user": "There was an error deleting the user",
  "create_user_success": "The user was successfully created",
  "update_user_success": "The user was successfully updated",
  "delete_user_success": "The user was successfully deleted"
};


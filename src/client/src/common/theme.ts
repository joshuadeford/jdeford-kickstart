import { Theme } from "@material-ui/core";
import { indigo, pink, red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";
import createStyles from "@material-ui/core/styles/createStyles";


export const styles = (muiTheme: Theme) => createStyles({
  "button": { /* ... */ },
  "paper": {
    "margin": 10
  },
  "textField": {
    "marginLeft": theme.spacing.unit,
    "marginRight": theme.spacing.unit,
    "width": 200,
  },
  "pageTitle": {
    "margin": 9,
    "fontSize": 24
  },
  "newEntityButton": {
    "margin": 9
  },
  "root": { /* ... */ },
  "innerPanel": {
    "display": "block",
  },
  "relationshipWrapper": {
    "paddingTop": 2,
    "width": "100%"
  },
  "linkLabel": {
    "padding": 4
  },
 "gridTextFieldWrapper": {
    "margin": 3
  },
  "menuButton": {
    "marginLeft": -12,
    "marginRight": 20
  },
  "entityList": {
    "width": 250,
  },
  "heading": {
    "fontSize": theme.typography.pxToRem(15),
    "fontWeight": theme.typography.fontWeightRegular
  },
  "formControl": {
    "margin": theme.spacing.unit,
    "margin-left": 24,
    "minWidth": 120
  },
  "selectEmpty": {
    "marginTop": theme.spacing.unit * 2,
  },
});

export const theme = createMuiTheme({
  "palette": {
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    "contrastThreshold": 3,
    "error": red,
    "primary": indigo,
    "secondary": pink,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    "tonalOffset": 0.2,
  },
  "overrides": {
    "MuiCard": {
      "root": {
        "margin": 6
        }
    }
  }
});

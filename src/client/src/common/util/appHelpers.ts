import { AppStore } from "../../store/app/reducers";
import { EntityStore } from "../../store/entities/reducers";
import { Entity } from "../../store/entities/types";

/**
 * Given a page collection name and a rel, creates a map of entities, by uri, by getting the
 * named collection page from the app store and the corresponding entities from the entity store
 */
export const extractPageEntities = (app: AppStore,
                                    entities: EntityStore,
                                    pageCollectionName: string, rel: string): { [uri: string]: Entity} => {
  const entityPage = app.pages[pageCollectionName];
  if (entityPage) {

    const entityPageItems = entityPage.reduce((map: {[uri: string]: Entity}, entityUri: string) => {
      if (entities.itemMapsByRel[rel][entityUri]) {
        map[entityUri] = entities.itemMapsByRel[rel][entityUri];
      }
      return map;
    }, {});
    return entityPageItems;
  } else {
    return {};
  }
  // (elementUri: string) => entities[elementUri]),
};

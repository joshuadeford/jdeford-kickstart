
import * as rest from "rest";
import * as defaultRequest from "rest/interceptor/defaultRequest";
import * as mime from "rest/interceptor/mime";
import * as registry from "rest/mime/registry";
import * as hal from "rest/mime/type/application/hal";
import uriTemplateInterceptor from "./uriTemplateInterceptor";
import { uriListConverter } from "./urlListConverter";

registry.register("text/uri-list", uriListConverter);
registry.register("application/hal+json", hal);

export const wrappedRestClient = rest.wrap(mime, { "registry": registry })
                              .wrap(uriTemplateInterceptor)
                              .wrap(defaultRequest, { "headers": { "Accept": "application/hal+json" } });

export interface HATEOSResource {
  _links: {
    self: {
      href: string
    }
  };
}



/*
 'use strict';

var rest = require('rest');
var defaultRequest = require('rest/interceptor/defaultRequest');
var mime = require('rest/interceptor/mime');
var uriTemplateInterceptor = require('./api/uriTemplateInterceptor');
var errorCode = require('rest/interceptor/errorCode');
var baseRegistry = require('rest/mime/registry');

var registry = baseRegistry.child();

registry.register('text/uri-list', require('./api/uriListConverter'));
registry.register('application/hal+json', require('rest/mime/type/application/hal'));

module.exports = rest
.wrap(mime, { registry: registry })
.wrap(uriTemplateInterceptor)
.wrap(errorCode)
.wrap(defaultRequest, { headers: { 'Accept': 'application/hal+json' }});

 */

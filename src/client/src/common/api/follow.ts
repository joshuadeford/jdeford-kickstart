import { Client, Request, Response, ResponsePromise } from "rest";

export interface RelationOptions {
  rel: string;
  params: {};
}

const follow = (restClient: Client, rootPath: string, relArray: [string | RelationOptions]): ResponsePromise => {
  const rootRequest: Request = {
    "method": "GET",
    "path": rootPath
  };

  const rootResponse: ResponsePromise = restClient(rootRequest);

  const followed: ResponsePromise = relArray.reduce((response: ResponsePromise, arrayItem: string | RelationOptions, currentIndex: number) => {
    const relationItem: RelationOptions = arrayItem instanceof String ? { "rel": arrayItem.toString(), "params": {} } : arrayItem as RelationOptions;
    const next: ResponsePromise = traverseNext(restClient, response, relationItem);
    return next;
  }, rootResponse);

  return followed;
};


const traverseNext = (restClient: Client, next: ResponsePromise, arrayItem: RelationOptions): ResponsePromise => {
  return next.then((response: Response) => {
    if (hasEmbeddedRel(response.entity, arrayItem.rel)) {
      return response.entity._embedded[arrayItem.rel];
    } else if(!response.entity._links) {
      return [];
    } else {
      return restClient({
        "method": "GET",
        "path": response.entity._links[arrayItem.rel].href,
        "params": arrayItem.params
      });
    }
  }) as ResponsePromise;
};

const hasEmbeddedRel = (entity: any, rel: string) => {
  return entity._embedded && entity._embedded.hasOwnProperty(rel);
};

export default follow;



/*
module.exports = function follow(restClient, rootPath, relArray) {
  var root = restClient({
    method: 'GET',
    path: rootPath
  });

  return relArray.reduce(function(root, arrayItem) {
    var rel = typeof arrayItem === 'string' ? arrayItem : arrayItem.rel;
    return traverseNext(root, rel, arrayItem);
  }, root);

  function traverseNext (root, rel, arrayItem) {
    return root.then(function (response) {
      if (hasEmbeddedRel(response.entity, rel)) {
        return response.entity._embedded[rel];
      }

      if(!response.entity._links) {
        return [];
      }

      if (typeof arrayItem === 'string') {
        return restClient({
          method: 'GET',
          path: response.entity._links[rel].href
        });
      } else {
        return restClient({
          method: 'GET',
          path: response.entity._links[rel].href,
          params: arrayItem.params
        });
      }
    });
  }

  function hasEmbeddedRel (entity, rel) {
    return entity._embedded && entity._embedded.hasOwnProperty(rel);
  }
};
*/

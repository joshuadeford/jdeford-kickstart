import { Request } from "rest";
import { Entity, EntitySchema, SchemaProperty } from "./types";
import { PageRequest } from "../app/reducers";

export const root = "/api";

export const entitySchemaRequest = (path: string): Request => {
  return {
    "method": "GET",
    "path": path,
    "headers": { "Accept": "application/schema+json" }
  };
};

export const jsonSchemaRequest = (): Request => {
  // Fetch the list of all the entity types and their corresponding links to their type
  // definition.
  return {
    "method": "GET",
    // This is the only url you will find.  HAL/ALPS are formats that describe the REST api this stuff is consuming
    "path": `${root}/profile`,
    "headers": { "Accept": "application/schema+json" }
  };
};

export const halProfileRequest = (path: string): Request => {
  return {
    "method": "GET",
    "path": path,
    "headers": { "Accept": "application/hal+json" }
  };
};

export const entityCreateRequest = (nascentEntity: Entity): Request => {
  return {
    "path": nascentEntity.schema.endpoint,
    "entity": nascentEntity,
    "method": "POST",
    "headers": { "Content-Type": "application/json" }
  };
};

export const entityRelateRequest = (owningEntity: Entity, property: SchemaProperty, relatedEntity: Entity): Request => {
  // TODO these should navigate the HAL api
  return {
    "method": "PUT",
    "path": `${owningEntity.uri}/${property.name}`,
    "entity": relatedEntity,
    "headers": {
      "Accept": "*/*",
      "Content-Type": "text/uri-list"
    }
    // "body": relatedEntity.uri// If more than one uri is going to be provided they are '/n' delimited
  };
};

export const entityFetchRequest = (entitySchema: EntitySchema, pageRequest: PageRequest): Request => {
  return {
    "method": "GET",
    "path": `${entitySchema.endpoint}?page=${pageRequest.page}&size=${pageRequest.size}&sort=${pageRequest.sort}`
  };
};

export const relatedEntitiesRequest = (relationshipUri: string): Request => {
  const getRelatedEntityRequest = {
    "method": "GET",
    "path": relationshipUri
  };
  return getRelatedEntityRequest;
};

export const entityUpdateRequest = (entity: Entity): Request => {
  return {
    "entity": entity,
    "path": entity.uri,
    "method": "PATCH",
    "headers": { "Content-Type": "application/json" }
  };
};

export const entityDeleteRequest = (entity: Entity): Request => {
  return {
    "path": entity.uri,
    "method": "DELETE"
  };
};

export const entityUnrelateRequest = (owningEntity: Entity, property: SchemaProperty, noLongerRelated: Entity): Request => {
// TODO these should navigate the HAL api
  return {
    "method": "DELETE",
    "path": `${owningEntity.uri}/${property.name}/${noLongerRelated.id}`
  };
};

import { combineReducers } from "redux";
import { appStore } from "./app/reducers";
import { entityStore } from "./entities/reducers";
import { flowableStore } from "./flowable/reducers";

const rootReducer = combineReducers({
  "entityStore": entityStore,
  "appStore": appStore,
  "flowableStore": flowableStore
});

export default rootReducer;

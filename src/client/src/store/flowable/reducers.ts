import * as tasks from "../../generated-redux-api/action/Tasks";
import * as formData from "../../generated-redux-api/action/Forms";
import {
  FormDataResponse, ProcessDefinitionResponse,
  ProcessInstanceResponse,
  Role,
  TaskResponse
} from "../../generated-redux-api/types";
import { SET_ACTIVE_ROLES } from "./types";
import { FlowableAction, GetFormDataAction, ListTasksAction } from "./actions";

export interface FlowableStore {
  isFetching: boolean;
  tasks: {
    [taskUri: string]: TaskResponse
  };
  forms: {
    [taskUri: string]: FormDataResponse
  };
  processInstances: {
    [processInstanceUri: string]: ProcessInstanceResponse
  };
  processDefinitions: {
    [processDefinitionUri: string]: ProcessDefinitionResponse
  };
  processInstanceDiagramUrls: {
    [processInstanceUri: string]: string
  };
  processDefinitionDiagramUrls: {
    [processDefinitionUri: string]: string
  };
  formsByTaskUri: {
    [taskUri: string]: string[]
  };
  tasksByProcessInstanceUri: {
    [processInstanceUri: string]: string[]
  };
  processInstancesByProcessDefinitionUri: {
    [processDefinitionUri: string]: string[]
  };
  activeRole: Role;// The role for which tasks will be retrieved
}

const initialState = {
  "isFetching": false,
  "tasks": { },
  "forms": { },
  "processInstances": { },
  "processDefinitions": { },
  "processInstanceDiagramUrls": { },
  "formsByTaskUri": { },
  "tasksByProcessInstanceUri": { },
  "processInstancesByProcessDefinitionUri": { },
};
export const flowableStore = (state = initialState, action: FlowableAction & ListTasksAction & GetFormDataAction) => {
  switch (action.type) {
    case SET_ACTIVE_ROLES:
      return Object.freeze({
        ...state,
        "activeRole": action.role
      });
    case formData.GET_FORM_DATA_USING_GET_START:
    case tasks.LIST_TASKS_START:
      return Object.freeze({
        ...state,
        "isFetching": true
      });
    case formData.SUBMIT_FORM_USING_POST:
      return Object.freeze({
        ...state,
        "isFetching": false
      });
    case formData.GET_FORM_DATA_USING_GET:
      return Object.freeze({
        ...state,
        "isFetching": false,
        "forms": { ...state.forms, [action.payload.taskUrl as string]: action.payload }
      });
    case tasks.LIST_TASKS:
      const tasksByUri = action.payload.data.reduce((map: FlowableStore["tasks"], task: TaskResponse) => {
        map[task.url || "there was no url!!"] = task;
        return map;
      }, {});
      return Object.freeze({
        ...state,
        "isFetching": false,
        "tasks": tasksByUri
      });
    default:
      return state;
  }
};


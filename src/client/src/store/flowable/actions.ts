// A lot of the actions you may be looking for are in the generated-redux-api/action directory

import {
  Role,
  TaskResponse,
  FormDataResponse
} from "../../generated-redux-api/types";
import { Action, Dispatch } from "redux";
import { SET_ACTIVE_ROLES } from "./types";

export interface FlowableAction extends Action {
  role: Role;
  payload?: {
    data: TaskResponse[]
  };
}

export interface GetFormDataAction extends Action {
  payload: FormDataResponse;
}

export interface ListTasksAction extends Action {
  payload: {
    data: TaskResponse[]
  };
}

export const setActiveRole = (role: Role) => (dispatch: Dispatch) => {
  dispatch({
    "type": SET_ACTIVE_ROLES,
    "role": role
  });
};


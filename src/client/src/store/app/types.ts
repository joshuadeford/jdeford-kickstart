const namespace = "ONPREM_OKS_APP";

export const OPEN_NAV_DRAWER = `${namespace}@OPEN_NAV_DRAWER`;
export const CLOSE_NAV_DRAWER = `${namespace}@CLOSE_NAV_DRAWER`;
export const PAGE_LOADED = `${namespace}@PAGE_LOADED`;
export const ADD_TO_PAGE = `${namespace}@ADD_TO_PAGE`;
export const ERROR = `${namespace}@ERROR`;
export const ERROR_ACKNOWLEDGED = `${namespace}@ERROR_ACKNOWLEDGED`;
export const SHOW_TOAST = `${namespace}@SHOW_TOAST`;

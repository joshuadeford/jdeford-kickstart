import {
  ADD_TO_PAGE,
  CLOSE_NAV_DRAWER,
  ERROR,
  ERROR_ACKNOWLEDGED,
  OPEN_NAV_DRAWER,
  PAGE_LOADED
} from "./types";
import { Entity } from "../entities/types";
import { AnyAction, Dispatch } from "redux";

export const errorAcknowledged = () => (dispatch: Dispatch) => {
  dispatch({
    "type": ERROR_ACKNOWLEDGED
  });
};

export const errorAction = (messageKey: string, details: string) => (dispatch: Dispatch) => {
  dispatch({
    "type": ERROR,
    "messageKey": messageKey,
    "details": details
  });
};

/**
 * Action indicating the nav drawer should open
 */
export const openNavDrawer = () => ({
  "type": OPEN_NAV_DRAWER
});

/**
 * Action indicating the nav drawer should close
 */
export const closeNavDrawer = () => ({
  "type": CLOSE_NAV_DRAWER
});

export const addToEntityPage = (entity: Entity, pageCollectionName: string): AnyAction => {
  return {
    "type": ADD_TO_PAGE,
    "element": entity.uri,
    "collectionName": pageCollectionName
  };
};

export const entityPageLoaded = (entities: Entity[], pageCollectionName: string): AnyAction => {
  const uris = entities.map((entity) => entity.uri);
  return {
    "type": PAGE_LOADED,
    "elements": uris,
    "collectionName": pageCollectionName
  };
};

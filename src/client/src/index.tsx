import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./App";
import registerServiceWorker from "./common/util/registerServiceWorker";
import "./index.css";

import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import { AppContainer } from "react-hot-loader";
import { IntlProvider } from "react-intl";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore } from "redux";
import { createLogger } from "redux-logger";
import { getLocaleMessages, getSupportedLocale } from "./common/locales/locales";


import thunk from "redux-thunk";
import { theme } from "./common/theme";
import rootReducer from "./store/index";

const history = createBrowserHistory();
const middlewares = [routerMiddleware(history), thunk];

if (process.env.NODE_ENV === "development") {
  const logger = createLogger({
    "collapsed": true
  });

  middlewares.push(logger);
}


const composeEnhancer: typeof compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    connectRouter(history)(rootReducer),
    composeEnhancer(
        applyMiddleware(
            ...middlewares
        ),
    ),
);

const render = () => {
  ReactDOM.render(
      <React.Fragment>
        <CssBaseline/>

        <AppContainer>
          <Provider store={store}>
            <IntlProvider locale={getSupportedLocale()} key={getSupportedLocale()}
                          messages={getLocaleMessages(getSupportedLocale())}>
              <MuiThemeProvider theme={theme}>
                <App history={history}/>
              </MuiThemeProvider>
            </IntlProvider>
          </Provider>
        </AppContainer>
      </React.Fragment>,
      document.getElementById("root")
  );
};
render();

registerServiceWorker();

package com.onprem.oks.device;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 */
@Entity
public class Device extends BaseEntity {

  @ManyToOne
  User user;

  public String name;
  public String modelName;

  /**
   * Default constructor
   */
  public Device() {
  }
}

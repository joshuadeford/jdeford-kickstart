package com.onprem.oks.configmanagement;

import java.util.Date;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 */
@RepositoryRestResource
public interface ComponentConfigurationRepository extends CrudRepository<ComponentConfiguration, String> {

  ComponentConfiguration findByVersion(int i);

  ComponentConfiguration findByComponentName(String componentName);

  ComponentConfiguration findByTimestampGreaterThan(Date timestamp);
}

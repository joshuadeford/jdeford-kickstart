package com.onprem.oks.configmanagement;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

/**
 */
@Entity
@Data
public class ComponentConfiguration extends BaseEntity {


  private Date timestamp;

  @NotNull
  private Integer version;

  @NotNull
  private String componentName;

  @OneToMany(mappedBy = "configuration")
  private List<ComponentProperty> properties = new ArrayList();

  public ComponentConfiguration() {
  }

  public ComponentConfiguration(Integer version, String componentName) {
    this.version = version;
    this.componentName = componentName;
    this.setEntityName(componentName);
  }

  public String getComponentName() {
    return componentName;
  }

  public void setComponentName(String componentName) {
    this.componentName = componentName;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public List<ComponentProperty> getProperties() {
    return properties;
  }

  public void setProperties(List<ComponentProperty> properties) {
    this.properties = properties;
  }

  public ComponentProperty addProperty(ComponentProperty property) {
    this.properties.add(property);
    property.setConfiguration(this);
    return property;
  }
}

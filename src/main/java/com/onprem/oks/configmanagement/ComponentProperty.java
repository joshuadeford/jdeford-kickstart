package com.onprem.oks.configmanagement;

import com.onprem.oks.entity.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 * A simple name value pair
 **/
@Entity
@Data
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name", "configuration_id"}))
public class ComponentProperty extends BaseEntity {


  @ManyToOne
  private ComponentConfiguration configuration;

  private String name;
  private String value;

  public ComponentProperty() {

  }

  public ComponentProperty(String name, String value) {
    this.name = name;
    this.value = value;
    this.setEntityName(name);
  }
}

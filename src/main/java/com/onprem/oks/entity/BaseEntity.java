package com.onprem.oks.entity;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@EntityListeners(BaseEntityListener.class)
@MappedSuperclass
@Data
@Embeddable
public class BaseEntity implements Serializable {


  @Id
  @GeneratedValue(generator = "uuid", strategy = GenerationType.TABLE)
  @Column(columnDefinition = "char(36) not null", length = 100, nullable = false)
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  @NotNull
  private String entityName;
}

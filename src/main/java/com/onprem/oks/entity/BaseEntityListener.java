package com.onprem.oks.entity;

import com.onprem.oks.configuration.WebSocketConfiguration;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleAfterLinkDelete;
import org.springframework.data.rest.core.annotation.HandleAfterLinkSave;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkSave;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


/**
 * Responds to lifecycle events on entities manipulated through REST interfaces
 * This is not a general purpose JPA repository listener.  For that, use @PreUpdate etc..
 * or @EntityListener.  In those cases, you'll have to come up with your own solution around
 * dependencie injection as its not available in those cases.
 */
@Component
@RepositoryEventHandler
public class BaseEntityListener {

  public static final String UPDATED = "updated";
  public static final String LINKED = "linked";
  public static final String DELETED = "deleted";
  public static final String UNLINKED = "unlinked";
  @Autowired
  SimpMessagingTemplate websocket;

  @Autowired
  EntityLinks links;

  @HandleBeforeCreate
  public void entityPrePersist(BaseEntity entity) {
    System.out.println("Listening Entity Pre Create : " + entity.getId());
  }

  @HandleAfterCreate
  public void entityAfterCreate(BaseEntity entity) {
    LinkBuilder entityLink = links.linkForSingleResource(entity.getClass(), entity.getId());
    System.out.println("Listening Entity Post Create : " + entityLink.toString());
    publishSocketEvent("new", entity);
  }

  @HandleBeforeSave
  public void entityBeforeSave(BaseEntity entity) {
    System.out.println("Listening Entity Pre Save : " + entity.getId());
  }

  @HandleAfterSave
  public void entityAfterSave(BaseEntity entity) {
    System.out.println("Listening Entity After Save : " + entity.getId());
    publishSocketEvent(UPDATED, entity);
  }

  @HandleBeforeLinkSave
  public void entityBeforeLinkSave(BaseEntity entity, Object collection) {
    System.out.println("Listening Entity Pre Update : " + entity.getId());
  }

  @HandleAfterLinkSave
  public void entityAfterLinkSave(BaseEntity entity, Object collection) {
    System.out.println("Listening Entity Post Link Save : " + entity.getId());
    publishSocketEvent(LINKED, entity);
  }

  @HandleBeforeDelete
  public void entityBeforeDelete(BaseEntity entity) {
    System.out.println("Listening Entity Pre Remove : " + entity.getId());
  }

  @HandleAfterDelete
  public void entityAfterDelete(BaseEntity entity) {
    System.out.println("Listening Entity Post Remove : " + entity.getId());
    publishSocketEvent(DELETED, entity);
  }

  @HandleBeforeLinkDelete
  public void entityBeforeLinkDelete(BaseEntity entity, Object collection) {
    System.out.println("Listening Entity Post Remove Link: " + entity.getId());
  }

  @HandleAfterLinkDelete
  public void entityAfterLinkDelete(BaseEntity entity, Object collection) {
    System.out.println("Listening Entity Post Remove Link: " + entity.getId());
    publishSocketEvent(UNLINKED, entity);
  }

  private void publishSocketEvent(String eventName, BaseEntity entity) {
    LinkBuilder entityLink = links.linkForSingleResource(entity.getClass(), entity.getId());
    String rel = links.linkToCollectionResource(entity.getClass()).getRel();
    String destination = WebSocketConfiguration.MESSAGE_PREFIX + "/" + rel + "/" + eventName;
    System.out.println("Publishing message to " + destination + ": " + entityLink.toString());
    this.websocket.convertAndSend(destination, entityLink.toString());
  }

}

package com.onprem.oks.role;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import lombok.Data;

/**
 */
@Entity
@Data
public class WaterCooler extends BaseEntity {

    @ManyToMany
    List<User> users = new ArrayList();

    private String waterType;

    private int loudness;

    /**
     * Default constructor
     */
    public WaterCooler() {
    }
}

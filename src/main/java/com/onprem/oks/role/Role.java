package com.onprem.oks.role;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import lombok.Data;

/**
 */
@Entity
@Data
public class Role extends BaseEntity {

    @ManyToMany
    List<User> users = new ArrayList();

    public String name;

    /**
     * Default constructor
     */
    public Role() {
    }
}

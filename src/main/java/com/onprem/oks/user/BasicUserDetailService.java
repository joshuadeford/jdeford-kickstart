package com.onprem.oks.user;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class BasicUserDetailService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;


  @Transactional
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    try {
      User user = userRepository.findByUserName(username);

      UserBuilder builder = null;
      if (user != null) {
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        builder.roles(user.getRoles().toArray(new String[]{}));
      } else {
        throw new UsernameNotFoundException("User not found.");
      }

      return builder.build();
    } catch (Throwable e) {
      e.printStackTrace();
      throw e;
    }
  }

}

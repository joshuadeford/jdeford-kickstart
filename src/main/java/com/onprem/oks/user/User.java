package com.onprem.oks.user;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.device.Device;
import com.onprem.oks.role.Role;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 */
@Entity
@Data
public class User extends BaseEntity {


  @ManyToMany(mappedBy = "users")
  List<Role> roles = new ArrayList();

  @OneToMany(mappedBy = "user")
  List<Device> devices = new ArrayList();


  double salary;

  @NotNull
  @Column(unique = true)
  private String userName;

  private String DOB;
  private String role;

  private String password;

  private String email;

  /**
   * Default constructor
   */
  public User() {
  }

  /**
   * Convenience constructor for a User.
   */
  public User(String username, String password, String email) {
    this.userName = username;
    setEntityName(username);
    this.password = password;
    this.email = email;
  }
}

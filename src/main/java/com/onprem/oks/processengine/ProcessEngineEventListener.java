package com.onprem.oks.processengine;

import static java.lang.Thread.sleep;
import static org.flowable.common.engine.api.delegate.event.FlowableEngineEventType.ENTITY_INITIALIZED;
import static org.flowable.common.engine.api.delegate.event.FlowableEngineEventType.TASK_COMPLETED;

import com.onprem.oks.configuration.WebSocketConfiguration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.flowable.common.engine.api.delegate.event.FlowableEntityEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.event.FlowableActivityEvent;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskQuery;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;

public class ProcessEngineEventListener implements FlowableEventListener {

  final SimpMessagingTemplate websocket;

  final TaskService taskService;

  public ProcessEngineEventListener(SimpMessagingTemplate websocket, TaskService taskService) {
    this.websocket = websocket;
    this.taskService = taskService;
  }

  @Override
  public void onEvent(FlowableEvent event) {
    if (ENTITY_INITIALIZED.equals(event.getType())) {
      FlowableEntityEvent flowableEvent = (FlowableEntityEvent) event;
      if (flowableEvent.getEntity() instanceof TaskEntity) {
        final TaskEntity taskEntity = (TaskEntity) flowableEvent.getEntity();
        //Flowable will be queried in other controllers as a result of this
        //websocket event being fired but for some reason activiti fires the events before
        //other things that query the runtime service see the changes.. so this
        //kicks off a thread to wait for the change to show up before firing the event.
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
          List<Task> results = null;
          while (results == null || results.size() == 0) {
            TaskQuery query = taskService.createTaskQuery().taskId(taskEntity.getId());
            results = query.list();
          }
          try {
            this.websocket.convertAndSend(WebSocketConfiguration.MESSAGE_PREFIX + "/taskCreated", "");
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
      }
    }
    if (event.getType().equals(TASK_COMPLETED)) {
      FlowableEntityEvent flowableEvent = (FlowableEntityEvent) event;
        //Activiti will be queried in other controllers as a result of this
        //websocket event being fired but for some reason activiti fires the events before
        //other things that query the runtime service see the changes.. so this
        //kicks off a thread to wait for the change to show up before firing the event.
      if (flowableEvent.getEntity() instanceof TaskEntity) {
        final TaskEntity taskEntity = (TaskEntity) flowableEvent.getEntity();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
          Task result = taskService.createTaskQuery().taskId(taskEntity.getId()).singleResult();
          while (result != null) {
            try {
              TaskQuery query = taskService.createTaskQuery().taskId(taskEntity.getId());
              sleep(1000);//Give the rest of the system time to update before telling listeners its updated.
              result = taskService.createTaskQuery().taskId(taskEntity.getId()).singleResult();
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
          try {
            this.websocket.convertAndSend(WebSocketConfiguration.MESSAGE_PREFIX + "/taskCompleted", taskEntity.getId());
          } catch (MessagingException e) {
            e.printStackTrace();
          }
        });
      }
    }
  }

  @Override
  public boolean isFailOnException() {
    return false;
  }

  @Override
  public boolean isFireOnTransactionLifecycleEvent() {
    return false;
  }

  @Override
  public String getOnTransaction() {
    return null;
  }
}

package com.onprem.oks.asset;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 */
@RepositoryRestResource
public interface AssetRepository extends CrudRepository<Asset, String> {

  List<Asset> findByMediaId(String mediaId);
}

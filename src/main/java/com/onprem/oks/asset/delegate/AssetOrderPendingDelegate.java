package com.onprem.oks.asset.delegate;

import com.onprem.oks.asset.AssetOrder;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component("assetOrderPending")
public class AssetOrderPendingDelegate implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) {
      AssetOrder assetOrder = execution.getVariable(AssetOrderDelegate.ASSET_ORDER, AssetOrder.class);
      //Notice that there is no need to invoke repository methods. Neato!!
      assetOrder.setStatus("pending_review");
  }
}

package com.onprem.oks.asset.delegate;

import com.onprem.oks.asset.Asset;
import com.onprem.oks.asset.AssetOrder;
import com.onprem.oks.asset.AssetOrderRepository;
import com.onprem.oks.asset.AssetRepository;
import com.onprem.oks.user.User;
import com.onprem.oks.user.UserRepository;
import java.util.List;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("assetOrderDelegate")
public class AssetOrderDelegate implements JavaDelegate {

  @Autowired
  UserRepository userRepository;

  @Autowired
  AssetRepository assetRepository;

  @Autowired
  AssetOrderRepository assetOrderRepository;


  public static final String ASSET_ORDER = "assetOrder";

  @Override
  public void execute(DelegateExecution execution) {
    String creatorEmail = execution.getVariable("creatorEmail", String.class);
    String assetMediaId = execution.getVariable("assetMediaId", String.class);
    List<Asset> assets = assetRepository.findByMediaId(assetMediaId);
    if (assets.size() == 0) {
      throw new IllegalArgumentException("There is no asset with mediaId: " + assetMediaId);
    }

    List<User> creatorUsers = userRepository.findByEmail(creatorEmail);
    if (creatorUsers.size() == 0) {
      throw new IllegalArgumentException("There is no user with email: " + creatorEmail);
    }
    AssetOrder order = new AssetOrder();
    order.setStatus("new");
    order.setAsset(assets.get(0));
    order.setCreator(creatorUsers.get(0));
    order = assetOrderRepository.save(order);
    execution.setVariable(ASSET_ORDER, order);
  }
}

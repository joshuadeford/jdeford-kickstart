package com.onprem.oks.asset.delegate;

import com.onprem.oks.asset.AssetOrder;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component("reviewAssetOrder")
public class ReviewAssetOrderDelegate implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) {
      AssetOrder assetOrder = execution.getVariable(AssetOrderDelegate.ASSET_ORDER, AssetOrder.class);
      assetOrder.setStatus(execution.getVariable("isApproved", Boolean.class) ? "approved" : "rejected");
  }
}

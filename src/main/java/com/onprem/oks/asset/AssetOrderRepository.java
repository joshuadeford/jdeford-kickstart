package com.onprem.oks.asset;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 */
@RepositoryRestResource
public interface AssetOrderRepository extends CrudRepository<AssetOrder, String> {
}

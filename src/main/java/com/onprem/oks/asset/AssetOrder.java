package com.onprem.oks.asset;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
@Entity
@Data
public class AssetOrder extends BaseEntity implements Serializable {

  @ManyToOne
  private User creator;

  @ManyToOne
  private Asset asset;

  private String status;

  private boolean approved;
}

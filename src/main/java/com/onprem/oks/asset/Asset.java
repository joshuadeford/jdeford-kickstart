package com.onprem.oks.asset;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import jdk.Exported;
import lombok.Data;
import org.joda.time.DateTime;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
@Entity
@Data
public class Asset extends BaseEntity {

  @NotNull
  @ManyToOne
  private User owner;

  private String copyrightArea;
  private String copyrightOwner;
  private String copyrightUsageMedia;
  private DateTime availabilityStartDate;
  private String assetDescription;

  //The id of the asset in the DAM
  @NotNull
  private String assetId;

  @NotNull
  private String mediaId;
  private String assetName;
  private String description;
  private String expiryDate;
  private String fileUrl;
  private String variant;
  private String assetType;
  private String assetModel;
  private String assetModelYears;
  private String practicableCount;
  private String displayId;

  public Asset() {

  }
  public Asset(String assetName, User assetOwner, String mediaId, String fileUrl, String assetId) {
    this.assetName = assetName;
    this.setEntityName(assetName);
    this.owner = assetOwner;
    this.mediaId = mediaId;
    this.fileUrl = fileUrl;
    this.assetId = assetId;
  }
}

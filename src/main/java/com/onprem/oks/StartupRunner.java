package com.onprem.oks;


import com.onprem.oks.asset.Asset;
import com.onprem.oks.asset.AssetRepository;
import com.onprem.oks.configmanagement.ComponentConfiguration;
import com.onprem.oks.configmanagement.ComponentConfigurationRepository;
import com.onprem.oks.configmanagement.ComponentProperty;
import com.onprem.oks.configmanagement.ComponentPropertyRepository;
import com.onprem.oks.modelvariant.ModelVariantRepository;
import com.onprem.oks.processengine.ProcessEngineEventListener;
import com.onprem.oks.user.User;
import com.onprem.oks.user.UserRepository;
import java.util.Arrays;
import java.util.List;
import javax.transaction.Transactional;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {

  @Autowired
  SimpMessagingTemplate websocket;

  @Autowired
  TaskService taskService;

  @Autowired
  RuntimeService runtimeService;


  private static final Logger logger = LoggerFactory.getLogger(StartupRunner.class);

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Autowired
  AssetRepository assetRepository;

  @Autowired
  ComponentPropertyRepository componentPropertyRepository;

  @Autowired
  ComponentConfigurationRepository componentConfigurationRepository;

  @Autowired
  ModelVariantRepository modelVariantRepository;

  @Autowired
  UserRepository userRepository;

  @Override
  public void run(String... args) throws Exception {
    logger.info("Application started with command-line arguments: {} . \n To kill this application, press Ctrl + C.", Arrays.toString(args));
    runtimeService.addEventListener(new ProcessEngineEventListener(websocket, taskService));
    //loadInitialData();

/**
    modelVariantRepository.save(new ModelVariant("Toyota", "Tacoma", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "4Runner", "UMAL"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Tacoma", "lDLFK"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Tacoma", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Tacoma", "F"));
    modelVariantRepository.save(new ModelVariant("Toyota", "4Runner", "55FR"));
    modelVariantRepository.save(new ModelVariant("Toyota", "4Runner", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "4Runner", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Camry", "XZ"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Camry", "78"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Camry", "55FXLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Camry", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Camry", "XLE"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "XZ"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "SS"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "IT"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "JAVA"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "DR"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "KL"));
    modelVariantRepository.save(new ModelVariant("Toyota", "Corolla", "GTR"));
 **/
  }

  private void loadInitialData() {
    User admin = loadOrCreateUser("admin", "test", "oks@onprem.com");
    User jdeford = loadOrCreateUser("jdeford", "jdeford", "joshdeford@onprem.com");
    loadOrCreateAsset("1", jdeford, "https://fortunedotcom.files.wordpress.com/2015/09/backblaze-b2-06-datacenter-corner.jpg", "123");
    loadOrCreateComponentProperties();
  }

  @Transactional
  protected void loadOrCreateComponentProperties() {
    ComponentConfiguration config = componentConfigurationRepository.findByComponentName("DemoData");

    if (config == null) {
      config = componentConfigurationRepository.save( new ComponentConfiguration(1, "DemoData"));
      componentPropertyRepository.save(config.addProperty(new ComponentProperty("key1", "value1")));
      componentPropertyRepository.save(config.addProperty(new ComponentProperty("key2", "value2")));
      config = componentConfigurationRepository.save( config);
    }
  }

  private void loadOrCreateAsset(String mediaId, User assetOwner, String fileUrl, String assetId) {
     List<Asset> assets = assetRepository.findByMediaId(mediaId);
     if (assets.size() == 0) {
       assetRepository.save(new Asset("First Asset", assetOwner, mediaId, fileUrl, assetId));
     }
  }

  private User loadOrCreateUser(String username, String password, String email) {
    User user = userRepository.findByUserName(username);
    if (user == null) {
      user = userRepository.save(new User(username, passwordEncoder.encode(password), email));
    }

    return user;
  }
}

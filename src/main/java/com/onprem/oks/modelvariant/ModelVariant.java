package com.onprem.oks.modelvariant;

import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.user.User;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 */
@Entity
@Data
public class ModelVariant extends BaseEntity {

  String brand;
  String model;
  String variant;

  public ModelVariant() {

  }

  public ModelVariant(String brand, String model, String variant) {

    this.brand = brand;
    this.model = model;
    this.variant = variant;
  }
}

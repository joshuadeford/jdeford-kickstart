package com.onprem.oks.configuration;

import com.onprem.oks.configmanagement.ComponentConfiguration;
import com.onprem.oks.device.Device;
import com.onprem.oks.entity.BaseEntity;
import com.onprem.oks.modelvariant.ModelVariant;
import com.onprem.oks.role.Role;
import com.onprem.oks.user.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.hateoas.config.EnableEntityLinks;

/**
 * This configuration ensures that IDs for entities are marshalled to the front end.
 */
@Configuration
@EnableEntityLinks
public class JPAConfiguration extends RepositoryRestConfigurerAdapter {

    /**
     * Configure entities to expose ids to the front end
     * @param config the auto-injected repository configuration to add classes to for id exposure.
     */
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(BaseEntity.class);
    }

}

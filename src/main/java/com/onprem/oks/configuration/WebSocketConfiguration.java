package com.onprem.oks.configuration;

import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 *
 */
// tag::code[]
@Component
@EnableWebSocketMessageBroker
public class WebSocketConfiguration extends AbstractWebSocketMessageBrokerConfigurer {

  public static final String MESSAGE_PREFIX = "/topic";
  public static final String MESSAGE_ENDPOINT = "/entity";
  public static final String MESSAGE_DESTINATION = "/app";

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint(MESSAGE_ENDPOINT).setAllowedOrigins("*").withSockJS();
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
    registry.enableSimpleBroker(MESSAGE_PREFIX);
    registry.setApplicationDestinationPrefixes(MESSAGE_DESTINATION);
  }
}
// end::code[]

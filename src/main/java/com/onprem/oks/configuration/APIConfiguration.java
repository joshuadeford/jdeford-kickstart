package com.onprem.oks.configuration;

import java.util.ArrayList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * This is the configuration class for <a target="_blank" href="https://swagger.io/">Swagger</a> auto-generated
 * REST endpoint documentation.  The implementation is provided by
 * <a target="_blank" href="http://springfox.github.io/springfox/">SpringFox</a>
 */
@EnableSwagger2WebMvc
@Configuration
@Import({SpringDataRestConfiguration.class})
public class APIConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .forCodeGeneration(true)
                .select()
                //.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.flowable")))
                //.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .apis(RequestHandlerSelectors.any())
                //.paths(Predicates.not(PathSelectors.regex("/api/profile")))
                .build().apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Hello World and Fibonacci REST API",
                "Simple API demonstrating basic REST functionality and a REST based fibonacci generator.  " +
                        "The functionality below allows you try various entity endpoints with sample data.",
                "1",
                "https://raw.githubusercontent.com/JoshuaEDeFord/HelloFib/master/tos.html",
                new Contact("Joshua DeFord", "https://github.com/JoshuaEDeFord/HelloFib", "jdeford@gmail.com"),
                "MIT License",
                "view-source:https://raw.githubusercontent.com/JoshuaEDeFord/HelloFib/master/LICENSE",
                new ArrayList()
        );

        return apiInfo;
    }
}

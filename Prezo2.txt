Some ideas I have about another prezo

1) Add a brand, model and variant with corresponding repositories to demonstrate the quickness of entity design and REST endpoint generation as well as TS generation.
2) Demonstrate testing of Spring repositories
3) Demonstrate "Test First" development with "Self - Writing code" because the queries are just generated
4) Demonstrate security annotations
5) Demonstrate workflow testing.
6) Point everybody to a docker container that has all the things
7) Demonstrate unique keys with modelvariant and foreign key DDL generation


7) Part series - should have education links everywhere
 - A configuration service
 - A BPMN tool that works with roles
 - Websockets  - Push and Pull
 - Full CARRUDD almost for free
   - Create
   - Add New
   - Read
   - Relate
   - Update
   - Delete
   - Remove
 - Domain Modeling
 - Make a workflow out of the series that shows who's watched what and how many times.
 - Security
 - Server side performance including object graphs
 - Client side performance including n + 1 server queries
 - Overriding endpoints
 - Documenting and using swagger
 - Code generation
 - Testing workflows
 - Authoring workflow
 - All the things you never have to do
   - Action creators
   - CRUD endpoints
   - REST API design (What's the best way).
   - OAuth
 - Generic API consumption
 - Maybe all videos have a front-end half and a back-end half
 - Have a table of contents link to media segments
 - Where do type definitions live

 - The workflow should have three roles
   - ROLE_AFFILATE
   - ROLE_ASSET_OWNER
   - ROLE_AGENCY
   - ROLE_REVIEWER
 - TaskListeners
 - EntityListeners


7 branches and how to use OnPrem kickstart
1 branch with lots of examples (my current branch)

Maybe leave the generated code thing out.


Standards, Conventions and Policies
Where possible, use URIs.  That is the resource representation of all entities.  IDs are no good.
Show the addition of a generic role that then just shows up in the roles dropdown
Show the Material ui theme and explain it and show the code completion feature.
deconstruct props and state
types go where they make sense.
Actions are typed
Database design with code
